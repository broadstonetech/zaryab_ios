//
//  AppDelegate.swift
//  Parsl
//
//  Created by Billal on 19/02/2021.
//

import UIKit
import Stripe
import Firebase
import UserNotifications
import AppTrackingTransparency
import FirebaseMessaging
import IQKeyboardManagerSwift
import Alamofire

let deviceID = UIDevice.current.identifierForVendor!.uuidString
@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {

    let gcmMessageIDKey = "gcm.message_id"
    var window: UIWindow?

    
 
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        if #available(iOS 13.0, *) {
                  UIWindow.appearance().overrideUserInterfaceStyle = .light
              }
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(ChatViewController.self)
//        IQKeyboardManager.shared.keyboardDistanceFromTextField = 50
//        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
        //Stripe key
        STPAPIClient.shared().publishableKey = PaymentAPIKeys.STRIPE_PUBLISHABLE_KEY
        
        Stripe.setDefaultPublishableKey("pk_test_51IPtixKfB8onMcB4J2OMKggeNB4i5CQqh76tppB3bnanGnteXVE0MozloHVFgUoaxCDdFb74MVsu1GTIAqbczJ1X003NuT3YPJ")
        
        FirebaseApp.configure()

       let settings = FirestoreSettings()
         settings.isPersistenceEnabled = false
         // Enable offline data persistence
         let db = Firestore.firestore()
         db.settings = settings
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self
        
       // Messaging.messaging().shouldEstablishDirectChannel = true
        
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                //you got permission to track
                if status == ATTrackingManager.AuthorizationStatus.authorized {
                    print("got permissions for ios14+")
                } else {
                    print("permissions denied for ios14+")
                }

            })

        } else {
            //you got permission to track, iOS 14 is not yet installed
            print("got permission below ios14")

        }
        
        return true
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
 
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
          
        let dataDict:[String: String] = ["token": fcmToken!]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        ParslUtils.sharedInstance.DEVICE_TOKEN = fcmToken
        print("devicr token is \(fcmToken)")
         // TODO: If necessary send token to application server.
         // Note: This callback is fired at each app startup and whenever a new token is generated.
     }
    
    private func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("device token is",deviceToken)
      Messaging.messaging().apnsToken = deviceToken
        
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingDelegate) {

        print("Received data message 71: \(remoteMessage.description)")
    }
    

    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print("97")
        print(userInfo)

    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
       Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }
        print("116")

      // Print full message.
      print(userInfo)

      completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        print("82")
        // Do whatever you want when the user tapped on a notification
        // If you are waiting for a specific value from the notification
        // (e.g., associated with key "valueKey"),
        // you can capture it as follows then do the navigation:

        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        
        if userInfo["notification_type"] as! String == "chat_notification"{

        }

        let keyExists = userInfo["notification_type"] != nil
        print(userInfo["notification_type"] as! String)
        if keyExists {
            let id = UserDefaults.standard.value(forKey: "channelFirebaseID") as! String? ?? ""
            if id != "" {
                let channel = Channel(id:id)
                let vc = ChatViewController(channel: channel)

                let mvc = UINavigationController(rootViewController: vc)
                self.window?.rootViewController = mvc
                self.window?.makeKeyAndVisible()
            }

        }else{
            print("key doesnot exist in userinfo")
        }
//        notificationNav.goToNotificationScreen = true
//        guard let window = UIApplication.shared.keyWindow else { return }
//
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let yourVC = storyboard.instantiateViewController(identifier: "MainViewController")
//
//            let navController = UINavigationController(rootViewController: yourVC)
//            navController.modalPresentationStyle = .fullScreen
//
//            // you can assign your vc directly or push it in navigation stack as follows:
//            window.rootViewController = navController
//            window.makeKeyAndVisible()

    }
    
    // Receive displayed notifications for iOS 10 devices.
      func userNotificationCenter(_ center: UNUserNotificationCenter,
                                  willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo

        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)

        // ...

        // Print full message.
        print(userInfo)

        // Change this to your preferred presentation option
        completionHandler([[.alert, .sound]])
      }
    
    
//    func application(_ application: UIApplication,
//                     continue userActivity: NSUserActivity,
//                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//        // Get URL components from the incoming user activity.
//        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
//            let incomingURL = userActivity.webpageURL,
//            let components = NSURLComponents(url: incomingURL, resolvingAgainstBaseURL: true) else {
//            return false
//        }
//
//        print(incomingURL.absoluteString)
//        print(components)
//        // Check for specific URL components that you need.
//        guard let path = components.path,
//        let params = components.queryItems else {
//            return false
//        }
//        print("path = \(path)")
//
//        if let albumName = params.first(where: { $0.name == "albumname" } )?.value,
//            let photoIndex = params.first(where: { $0.name == "index" })?.value {
//
//            print("album = \(albumName)")
//            print("photoIndex = \(photoIndex)")
//            return true
//
//        } else {
//            print("Either album name or photo index missing")
//            return false
//        }
//    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        print("In URL opening delegate 1")
      // 1
      guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
        let url = userActivity.webpageURL,
        let components = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
          return false
      }
      
        print("Deeplink: \(url.absoluteString)")
        print("Deeplink Components: \(components)")
      
      return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {

        print("In URL opening delegate")
            let urlPath : String = url.path
            print("URL Path \(urlPath)")
            return UIApplication.shared.canOpenURL(url)
    }
    
    func testMethod () {
        print("test method called")
        let method = ImageUPloadMethods()
        method.uploadModelImages()
    }
}
