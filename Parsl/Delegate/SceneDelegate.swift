//
//  SceneDelegate.swift
//  Parsl
//
//  Created by Billal on 19/02/2021.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo
               session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
       
        // Get URL components from the incoming user activity.
        guard let userActivity = connectionOptions.userActivities.first,
            userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let incomingURL = userActivity.webpageURL,
            let components = NSURLComponents(url: incomingURL, resolvingAgainstBaseURL: true) else {
            return
        }
                
        // Check for specific URL components that you need.
        guard let path = components.path,
            let params = components.queryItems else {
            return
        }
        print("path = \(path)")
                
        if let albumName = params.first(where: { $0.name == "albumname" })?.value,
            let photoIndex = params.first(where: { $0.name == "index" })?.value {
                
            print("album = \(albumName)")
            print("photoIndex = \(photoIndex)")
                    
        } else {
            print("Either album name or photo index missing")
        }
    }


}

