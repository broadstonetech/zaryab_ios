//
//  Avatars.swift
//  Parsl
//
//  Created by M Zaryab on 28/12/2021.
//

import Foundation

struct Avatars{

    var data : [AvatarsData]!
    var message : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        data = [AvatarsData]()
        if let dataArray = dictionary["data"] as? [[String:Any]]{
            for dic in dataArray{
                let value = AvatarsData(fromDictionary: dic)
                data.append(value)
            }
        }
        message = dictionary["message"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if data != nil{
            var dictionaryElements = [[String:Any]]()
            for dataElement in data {
                dictionaryElements.append(dataElement.toDictionary())
            }
            dictionary["data"] = dictionaryElements
        }
        if message != nil{
            dictionary["message"] = message
        }
        return dictionary
    }

}

//
//    Data.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AvatarsData{

    var appId : String!
    var avatarId : String!
    var descriptionField : String!
    var namespace : String!
    var status : String!
    var timestamp : String!
    var title : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        appId = dictionary["app_id"] as? String
        avatarId = dictionary["avatar_id"] as? String
        descriptionField = dictionary["description"] as? String
        namespace = dictionary["namespace"] as? String
        status = dictionary["status"] as? String
        timestamp = dictionary["timestamp"] as? String
        title = dictionary["title"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if appId != nil{
            dictionary["app_id"] = appId
        }
        if avatarId != nil{
            dictionary["avatar_id"] = avatarId
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if namespace != nil{
            dictionary["namespace"] = namespace
        }
        if status != nil{
            dictionary["status"] = status
        }
        if timestamp != nil{
            dictionary["timestamp"] = timestamp
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }

}
