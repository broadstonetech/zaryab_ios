//
//  NetworkManager.swift
//  Parsl
//
//  Created by Billal on 25/02/2021.
//


import Foundation
import UIKit

class NetworkManager {
    
    static let BASE_URL = "https://api.mybeautyadvisor.com"
    static let namespaceTestUser = "rf0000"

    private static let timeOutIntervalForRequest = 160.0
    private static let timeOutIntervalForResource = 160.0
    
    fileprivate class func processResponse(data: Data?, response: URLResponse?, responseError: Error?, completion: @escaping ([String: Any]) -> ()) {
        
        guard responseError == nil else {
            print(responseError?.localizedDescription ?? "Something went wrong")
            return
        }
        
        guard let responseData = data else {
            print("No Data")
            return
        }
        
        print(String(decoding: responseData, as: UTF8.self))
        
        guard let json = (try? JSONSerialization.jsonObject(with: responseData, options: [])) as? [String: Any] else {
            print("Not containing JSON")
            return
        }
        
        completion(json)
    }
    
    fileprivate class func getSession() -> URLSession {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = timeOutIntervalForRequest
        sessionConfig.timeoutIntervalForResource = timeOutIntervalForResource
        let session = URLSession(configuration: sessionConfig)
        return session
    }
    
    fileprivate class func postGenericRequest(urlString: String, paramsBag: NSDictionary, completion: @escaping ([String: Any]) -> ()) {
        
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        request.allHTTPHeaderFields = headers
        
        let data = try! JSONSerialization.data(withJSONObject: paramsBag, options: JSONSerialization.WritingOptions.prettyPrinted)
        let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        request.httpBody = json!.data(using: String.Encoding.utf8.rawValue)
        
        let task = getSession().dataTask(with: request) { (data, response, responseError) in
            if let response = response as? HTTPURLResponse {
                print(response)
            }
            NetworkManager.processResponse(data: data, response: response, responseError: responseError, completion: completion)
        }
        task.resume()
    }
    
    fileprivate class func postDataWithImage(urlString: String, imageParamName: String, image: UIImage?, params: NSDictionary, completion: @escaping ([String: Any]?) -> ()) {
        guard let url = URL(string: urlString) else { return }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        
        let boundary = "Boundary-\(NSUUID().uuidString)"
        
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        let data = NetworkManager.createBodyWithParameters(parameters: params, filePathKey: imageParamName, image: image, boundary: boundary)
        
        // Send a POST request to the URL, with the data we created earlier
        
        getSession().uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, responseError in
            NetworkManager.processResponse(data: responseData, response: response, responseError: responseError, completion: completion)
        }).resume()
    }
    
    // Helper method for multipart for parameters
    private class func createBodyWithParameters(parameters: NSDictionary?, filePathKey: String?, image: UIImage?, boundary: String) -> Data {
        var body = Data()
        
        if let parameters = parameters {
            for (key, value) in parameters {
                
                body.append("--\(boundary)\r\n".data(using: .utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                body.append("\(value)\r\n".data(using: .utf8)!)
            }
        }
        
//        if let image = image, let imageData = image.jpeg(.lowest) {
//            let filename = "\(filePathKey ?? "filename").jpg"
//
//            let mimetype = "image/jpg"
//
//            body.append("--\(boundary)\r\n".data(using: .utf8)!)
//            body.append("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
//            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: .utf8)!)
//            body.append(imageData)
//            body.append("\r\n".data(using: .utf8)!)
//        }
        
        body.append("--\(boundary)--\r\n".data(using: .utf8)!)

        return body
    }
}


extension NetworkManager {
    
//    class func loginWithApple(email: String, username: String, loginType: String, token: String, name: String,  completion: @escaping (_ response: [String: Any]?) -> ()) {
//
//        let deviceId = UIDevice.current.identifierForVendor?.uuidString
//        let params = ["email":email ,"username": username,"password": "", "device_token": "token_1", "type": loginType, "token": token, "name": name, "device_token": MyUserDefaults.getFcmToken(), "device_id": deviceId]
//
//        let userParams = ["app_id":"jedi","data": params] as [String : Any]
//
//        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/login", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
//            completion(response)
//        }
//    }
    
//    class func loginUser(username: String, password: String, loginType: String,  completion: @escaping (_ response: [String: Any]?) -> ()) {
//
//        let sha1 = SHA1.hexString(from: password)
//
//        let deviceId = UIDevice.current.identifierForVendor?.uuidString
//
//        let params = ["email":"" ,"username": username,"password": sha1, "device_token": MyUserDefaults.getFcmToken(), "type": loginType, "device_id": deviceId]
//
//        let userParams = ["app_id":"jedi","data": params] as [String : Any]
//
//        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/login", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
//            completion(response)
//        }
//    }
    
//    class func signUpUser(firstName: String, lastName: String, username: String, email: String, password: String,  completion: @escaping (_ response: [String: Any]?) -> ()) {
//
//
//        let sha1 = SHA1.hexString(from: password) ?? ""
//        let params = ["first_name":firstName, "last_name": lastName, "username": username, "email": email, "password": sha1]
//
//        let userParams = ["data": params] as [String : Any]
//
//        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/user/signup", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
//            completion(response)
//        }
//    }
    
    class func getAllModels(completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["search_type":"all", "skin_tone": "a"]
        
        let userParams = ["app_id": "jedi", "namespace" : namespaceTestUser, "data": params] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/models", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func getAllModelsForAR(completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["search_type":"augmentedface"] //augmentedface augmented_face
        
        let userParams = ["app_id": "jedi","namespace" : namespaceTestUser, "data": params] as [String : Any]
        print("PARAMS == \(userParams) URL == /get/models")
        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/models", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func getAllModelsOnSkinTone(h: Double, s: Double, l: Double, dressColor: String? = nil, completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let lValue = Double(String(format:"%.2f", l)) ?? 0.0
        
        let hslParams = ["h": h, "s": s, "v": l, "l": lValue]
        var params = ["search_type":"hsvl_value", "hsvl_value": hslParams] as [String : Any]
        
        if let dressColor = dressColor {
            params["dress_color_code"] = dressColor
        }
        
        let userParams = ["app_id": "jedi","namespace" : namespaceTestUser, "data": params] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/models", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func getAllIdeas(completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["request_for":"recreate", "event_key": "a"]
        
        let userParams = ["app_id": "jedi","namespace" : namespaceTestUser, "data": params] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/ideas", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func getOccassionIdea(keyWord: String ,completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["request_for":"special_occasions", "event_key": keyWord]
        
        let userParams = ["app_id": "jedi","namespace" : namespaceTestUser, "data": params] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/ideas", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    
    class func getIdea(eyeShade: String, lipstick: String, base: String ,completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["eyeshade_colour_code": eyeShade, "lipstick_colour_code": lipstick, "base_colour_code": base, "idea_id": "", "request_for": "recreate"]
        
        let userParams = ["app_id": "ar_art","namespace" : namespaceTestUser, "data": params] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/idea/data", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func getIdeaForHalloween(ideaId: String ,completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["eyeshade_colour_code": "", "lipstick_colour_code": "", "base_colour_code": "", "idea_id": ideaId, "request_for": "special_occasions"]
        
        let userParams = ["app_id": "ar_art","namespace" : namespaceTestUser, "data": params] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/idea/data", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func getEvents(completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let userParams = ["app_id": "jedi"] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/events", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
//    class func saveIdea(idea:IdeaModel, ideaName: String, image: UIImage?, userNameSpace: String ,completion: @escaping (_ response: [String: Any]?) -> ()) {
//
//        let userParams = ["lipstick": idea.lipstick ?? "", "lipliner": idea.lipLiner ?? "", "blushon": idea.blushOn ?? "", "eyelashes": idea.eyeLashes ?? "", "eyeliner": idea.eyeLiner ?? "", "eyeshade": idea.eyeShade ?? "", "eyebrows": idea.eyeBrows ?? "", "namespace": userNameSpace, "idea_name": ideaName, "idea_of": "ml_screen", "foundation": "", "lipgloss": "", "mascara": "", "lens": ""] as [String : Any]
//
//        NetworkManager.postDataWithImage(urlString: NetworkManager.BASE_URL + "/save/idea", imageParamName: "image_file", image: image, params: userParams as NSDictionary, completion: completion)
//    }
    
    class func getUserIdeas(nameSpace: String, completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["idea_of": "ml_screen"]
        let userParams = ["namespace": nameSpace, "data": params] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/view/user/ideas", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func deleteUserIdea(nameSpace: String, ideaId: String, completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["idea_id": ideaId]
        let userParams = ["namespace": nameSpace, "data": params] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/delete/user/idea", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
//    class func logoutUser(nameSpace: String, completion: @escaping (_ response: [String: Any]?) -> ()) {
//
//        let deviceId = UIDevice.current.identifierForVendor?.uuidString
//        let params = ["device_token": MyUserDefaults.getFcmToken(), "device_id": deviceId]
//        let userParams = ["app_id":"jedi", "namespace": nameSpace, "data": params] as [String : Any]
//
//        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/signout", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
//            completion(response)
//        }
//    }
    
    class func getIdeaDetail(nameSpace: String, ideaId: String, completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["idea_id": ideaId]
        let userParams = ["namespace": nameSpace, "data": params] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/user/idea/data", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func getSurveys(nameSpace: String, completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let userParams = ["namespace": "ven9242"] as [String : Any]

        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/surveys", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func submitSurvey(nameSpace: String, questionAnswers: [String: String], completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["survey": questionAnswers, "survey_type": "app_survey"] as [String : Any]
        
        let userParams = ["namespace": "ven9242", "app_id": "jedi", "data": params] as [String : Any]
        
        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/submit/survey", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func get3dModels(completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["search_type": "3dmodels"] as [String : Any]
        
        let userParams = ["app_id": "jedi", "namespace" : namespaceTestUser, "data": params] as [String : Any]

        print("Params== \(userParams) == get/models")
        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/models", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func getTrendingModels(completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let params = ["search_type":"featured"] as [String : Any]
        
        let userParams = ["app_id":"ios","namespace" : namespaceTestUser, "data": params] as [String : Any]
        
        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/get/models", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
    class func saveProfile(nameSpace: String, skinTone: String, hairColor: String, completion: @escaping (_ response: [String: Any]?) -> ()) {
        
        let userParams = ["namespace": nameSpace, "skintone": skinTone, "haircolor": hairColor] as [String : Any]
        
        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/update/profile", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
            completion(response)
        }
    }
    
//    class func sendFCM(completion: @escaping (_ response: [String: Any]?) -> ()) {
//        
//        let params = ["device_token": MyUserDefaults.getFcmToken()]
//        let userParams = ["app_id": "ios", "data": params] as [String : Any]
//        
//        NetworkManager.postGenericRequest(urlString: NetworkManager.BASE_URL + "/send/push/notification", paramsBag: userParams as NSDictionary) { (response: [String: Any]) in
//            completion(response)
//        }
//    }
}
