

//
//  ImagesUploadMethod.swift
//  Parsl
//
//  Created by M Zaryab on 12/02/2022.
//

import Foundation
import NotificationCenter
import UserNotifications
import Alamofire
import SwiftUI

import UIKit

 class ImageUPloadMethods {

  func uploadModelImages() {
    ParslUtils.sharedInstance.saveIfImagesUploadingDone(true)
    let url = URL(string: "https://api.parsl.io/upload/user/model")!
    Alamofire.upload(multipartFormData: { multipart in
        multipart.append(NSURL(fileURLWithPath: ParslUtils.sharedInstance.getDirectoryPath()) as URL, withName: "zipfile")
        multipart.append("\(ParslUtils.sharedInstance.getSizeOfImages())".data(using: .utf8)!, withName :"size")
        multipart.append("pr6196".data(using: .utf8)!, withName :"namespace")
        multipart.append("parsl_ios".data(using: .utf8)!, withName :"app_id")

    }, to: url, method: .post, headers: nil) { encodingResult in
        switch encodingResult {
        
        case .success(let upload, _, _):
            upload.response { answer in
                print(answer.response!)
                print("statusCode: \(String(describing: answer.response?.statusCode))")
                if answer.response?.statusCode == 200 {
                    print("completed")
                    ParslUtils.sharedInstance.saveIfImagesUploadingDone(false)
                    self.notifyOnCompletion()
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UploadSuccess"), object: nil)
                }
                else {
                    ParslUtils.sharedInstance.saveIfImagesUploadingDone(false)
                    print("not completed")
                }
            }
            upload.uploadProgress { progress in
                //call progress callback here if you need it
                ParslUtils.sharedInstance.saveIfImagesUploadingDone(true)
                print(progress)
            }
        case .failure(let encodingError):
            ParslUtils.sharedInstance.saveIfImagesUploadingDone(false)
            print("multipart upload encodingError: \(encodingError)")
        }
    }
    
}

func notifyOnCompletion() {
    let center = UNUserNotificationCenter.current()
    let content = UNMutableNotificationContent()
    content.title = "PARSL"
    content.body = "images uploaded successfully"
    content.sound = .default
    
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    let request = UNNotificationRequest(identifier: "notify", content: content, trigger: trigger)
    center.add(request) { (error) in
        if error != nil {
            print("error")
        }
    }
}
// zip the images folder.
func deleteZipFile() {
    do {
         let fileManager = FileManager.default
        // Check if file exists
        if fileManager.fileExists(atPath: ParslUtils.sharedInstance.getDirectoryPath()) {
            // Delete file
            try fileManager.removeItem(atPath: ParslUtils.sharedInstance.getDirectoryPath())
        } else {
            print("File does not exist")
        }
     
    }
    catch let error as NSError {
        print("An error took place: \(error)")
    }
    }
 
}
