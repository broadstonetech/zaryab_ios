//
//  AppVersionCheck.swift
//  Parsl
//
//  Created by broadstone on 08/05/2021.
//

import Foundation

class AppVersionCheck{
    
    static let sharedInstance = AppVersionCheck()

    // MARK: Public
    
    func getAppEnviornment() -> String {
        if  isRunningInTestFlightEnvironment() {
            return "testflight"
        }else if isRunningInAppStoreEnvironment() {
            return "production"
        }else {
            return "simulator"
        }
    }
    
    func getAppId() -> String {
        return "parsl"
    }
    
    
    func getAppVersion() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String? ?? "x.x"
    }
    
    func getAppBuildNumber() -> String {
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String? ?? "x"
    }
    
    func getCheckVersionApiParams() -> [String: AnyObject] {
        let data = [
            "platform" : "ios",
            "app_id": getAppId(),
            "version_no": getAppVersion(),
            "environment": getAppEnviornment(),
            "build_no": getAppBuildNumber()]
        
        let params = ["data": data as AnyObject]
        
        return params
    }

    // MARK: Private
    
    private func isRunningInTestFlightEnvironment() -> Bool{
        if isSimulator() {
            return false
        } else {
            if isAppStoreReceiptSandbox() && !hasEmbeddedMobileProvision() {
                return true
            } else {
                return false
            }
        }
    }

    private func isRunningInAppStoreEnvironment() -> Bool {
        if isSimulator(){
            return false
        } else {
            if isAppStoreReceiptSandbox() || hasEmbeddedMobileProvision() {
                return false
            } else {
                return true
            }
        }
    }

    

    private func hasEmbeddedMobileProvision() -> Bool{
      if let _ = Bundle.main.path(forResource: "embedded", ofType: "mobileprovision") {
            return true
        }
        return false
    }

    private func isAppStoreReceiptSandbox() -> Bool {
        if isSimulator() {
            return false
        } else {
          if let appStoreReceiptURL = Bundle.main.appStoreReceiptURL,
            let appStoreReceiptLastComponent = appStoreReceiptURL.lastPathComponent as? String, appStoreReceiptLastComponent == "sandboxReceipt" {
                    return true
            }
            return false
        }
    }

    private func isSimulator() -> Bool {
        #if arch(i386) || arch(x86_64)
            return true
            #else
            return false
        #endif
    }
    
}
