//
//  UnityMehodsExtensions.swift
//  Parsl
//
//  Created by M Zaryab on 24/02/2022.
//

// This extensios has methods written at native side by are called by unity.
import Foundation

extension MainViewController {
    // Calls by unity when projection of a model completed
    func modelProjectionDone(_ message: String!){
        print("model projection done",message)
    }
    // Calls by unity to show a view present on native side
    func showContainerView(_ message: String!) {
        self.mainPresentedView.isHidden = true
        self.view.sendSubviewToBack(self.unityView!)
      
    }
    
    func avatarModule(_ message1: String!,forRedeem message2: String!){
        print(message1)
        print(message2)
        self.view.sendSubviewToBack(self.unityView!)

        
  
    }
    
    func sendMessage(onReload message: String!) {
        self.redeemAnimatingView.isHidden = true
        modelsInCart.removeAll()
        modelsInView.removeAll()
        self.mainPresentedView.isHidden = true
        cartCountLabel.text = String(modelsInCart.count)
        self.view.sendSubviewToBack(self.unityView!)
    }
    
    // model description will be shown.
    func sendMessage(toMobileApp message: String!) {
        //add to cart tapped
        if !isRedeem{
            self.view.sendSubviewToBack(self.unityView!)
            //populate main presented view with corresonding item
         //   print("recieved id is",message)
            let parsed = message.replacingOccurrences(of: "(Clone)", with: "") 
            self.mainPresentedView.isHidden = false
            tappedItem = modelsInView[parsed]!
            if tappedItem.defaultShadow == false {
                shadowBtn.isUserInteractionEnabled = true
            }
            else {
                shadowBtn.isUserInteractionEnabled = true
            }
            itemNameLabel.text = tappedItem.basicInfo.name
            let imagePath = tappedItem.modelFile.thumbnail
            let url = URL(string: imagePath!)

            presentedModelImage.sd_setImage(with: url)
            presentedTextView.text = tappedItem.description
        }
    }
    // close the unitview
    func closeUnityView(_ message: String!) {
        
        self.view.sendSubviewToBack(self.unityView!)
    }
    
    // load and initiliza when first video ends.
    func setUpUnityView() { 
        //load unity
        UnityEmbeddedSwift.showUnity()
        unityView = UnityEmbeddedSwift.getUnityView()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.view.insertSubview(self.unityView!, at: 0)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                if self.isfrom == "MyAvatars"{
                    
                }
                else {
                self.setUpFloatingActionbutton()
                self.getCategory()
                }
            })
        })
    }
    
    // projects the avatar
    func loadAvatar(){
        let avatarCharacter = parslRedeemModel.data.avatarCharacter
        print("avatar character is \(avatarCharacter)")
        let avatarString = avatarCharacter! + "," + parslRedeemModel.data.avatarAnimation
        print("avaatar string is ...\(avatarString)")
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "RedeemCharacter", message: avatarString)
    }
    // reload unity in native side
    func reloadUnity() {
        print("reloading ")
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ReLoadUnity", message: "")
    }
}
