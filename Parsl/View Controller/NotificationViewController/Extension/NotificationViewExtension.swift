//
//  Extension.swift
//  Parsl
//
//  Created by M Zaryab on 28/02/2022.
//

import Foundation
import UIKit


// MARK: - Extensions
extension NotificationsSummryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationSummaryTableViewCell", for: indexPath) as! NotificationSummaryTableViewCell
//        cell.mainTitle.text = "\(notificationData[indexPath.row].notificationRecieverName)"
        let date = NSDate(timeIntervalSince1970: notificationData[indexPath.row].sent_time)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = constantDateFormate.dateFormate
        let strDate = dateFormatter.string(from: date as Date)
        cell.timeLabel.text = strDate 
        cell.descriptionTitle.text = "\(notificationData[indexPath.row].not_title) \(notificationData[indexPath.row].notificationRecieverName)\n\(notificationData[indexPath.row].not_message)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

