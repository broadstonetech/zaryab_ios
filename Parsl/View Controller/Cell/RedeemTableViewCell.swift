//
//  RedeemTableViewCell.swift
//  Parsl
//
//  Created by Mufaza on 02/08/2021.
//

import UIKit

//protocol PreviewBtnDelegate {
//    func previewBtnPressed(itemIndex: Int)
//}

class RedeemTableViewCell: UITableViewCell {
    @IBOutlet weak var thumbnail:       UIImageView!
    @IBOutlet weak var title:           UILabel!
    @IBOutlet weak var downloadedImage:   UIButton!
    
    @IBOutlet weak var addToWalletBtn: UIButton!
    @IBOutlet weak var messageTextView:UITextView!
    var cellIndex: Int?
//    var delegate: PreviewBtnDelegate?
    var imagePath = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
//    @IBAction func previewBtnPressed(_ sender: UIButton) {
//        delegate?.previewBtnPressed(itemIndex: cellIndex!)
//    }
}



class RedeemTitleTableViewCell: UITableViewCell {

    @IBOutlet weak var title:           UILabel!

    var imagePath = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        //cellView.layer.cornerRadius = cellView.frame.height / 8
    }
    
//    @IBAction func previewBtnPressed(_ sender: UIButton) {
//        delegate?.previewBtnPressed(itemIndex: cellIndex!)
//    }
}


class RedeemVideoTableViewCell: UITableViewCell {
  
    @IBOutlet weak var videoButton:   UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        //cellView.layer.cornerRadius = cellView.frame.height / 8
    }
    
//    @IBAction func previewBtnPressed(_ sender: UIButton) {
//        delegate?.previewBtnPressed(itemIndex: cellIndex!)
//    }
}


