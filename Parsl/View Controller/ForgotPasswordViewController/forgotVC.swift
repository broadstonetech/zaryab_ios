//
//  forgotVC.swift
//  test
//
//  Created by Hussnain Ali on 31/12/2021.
// This class enable the user to recover the account if user has forgot the password.

import UIKit
import Toast_Swift

class forgotVC: BaseViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var bgView: UIView!
    
    // MARK: - Properties
    var style = ToastStyle()
    let skipButton = UIButton(type: .system)
    
    // MARK: - LifeCycles
    override func viewDidLoad() {
        super.viewDidLoad()
        self.designUI()
        self.setUpSkipbtn()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Methods
    func designUI()
        {
            self.bgView.layer.cornerRadius = 16
            self.bgView.layer.borderWidth = 2
            self.bgView.layer.borderColor = UIColor(red: 94, green: 183, blue: 201, alpha: 1).cgColor
            
            self.doneBtn.layer.cornerRadius = self.doneBtn.frame.height/2
            self.doneBtn.layer.shadowColor = UIColor.lightGray.cgColor
            self.doneBtn.layer.shadowRadius = 4
            self.doneBtn.layer.shadowOpacity = 2
            self.doneBtn.layer.shadowOffset = CGSize(width: 0, height: 0)
            
            self.emailView.layer.borderColor = UIColor(named: "toastBackColor")!.cgColor
            self.emailView.layer.borderWidth = 1
        }
    
    func setUpSkipbtn() {
    skipButton.frame = CGRect(x: view.frame.size.width/1.3, y: 20, width: 80, height: 25)
        skipButton.setTitle(constantsStrings.cancelLabel, for: .normal)
    skipButton.backgroundColor = .clear
    skipButton.layer.cornerRadius = 5
    skipButton.clipsToBounds = true
    skipButton.titleLabel!.font = UIFont.systemFont(ofSize: 18)
    skipButton.setTitleColor(UIColor.link, for: .normal)
    skipButton.addTarget(self, action: #selector(skipBtnActions(_:)), for: .touchUpInside)
    view.addSubview(skipButton)
    }
    
    @objc func skipBtnActions(_ sender:UIButton!) {
        dismiss(animated: true, completion: nil)
        
    }
    
    func resetForgotPassword() {
        
        ProgressHUD.show()
        let params = ["app_id": "parsl_ios", "user_email": emailTf.text!] as [String: AnyObject]
            print(params)
            
        let url = constantsUrl.forgotPasswordUrl
            
            postRequest(serviceName: url, sendData: params, success: { (response) in
                do {
                    let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                    let status = json["status"] as! Bool
                    let message = json["message"] as! String
                    if status {
                        DispatchQueue.main.async{
                        ProgressHUD.dismiss()
                            self.view.makeToast( message, duration: 1.0, position: .bottom, style: self.style)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    } else {
                        ProgressHUD.dismiss()
                        self.showAlert(withMessage: message)
                        
                    }
                    
                } catch {
                    ProgressHUD.dismiss()
                }
            }, failure: { (error) in
                print(error)
                ProgressHUD.dismiss()
            })
        
    }
    
    // valid email checking...
    
    
    // MARK: - IBActions
    @IBAction func doneBtnPressed(_ sender: Any) {
        if !isValidEmail(emailTf.text!) {
            self.view.makeToast(constantsStrings.enterValidEmail, duration: 1.0, position: .bottom, style: style)
            return
        }
        resetForgotPassword()
        
    }
    
}
