//
//  CaptureViewController.swift
//  Parsl
//
//  Created by M Zaryab on 10/02/2022.
//
// this class enable the images capture functionality
import UIKit
import SwiftUI

class CaptureViewController: UIViewController {

    // MARK: - Properties
    @StateObject var model = CameraViewModel()
    
    // MARK: - LifeCycles
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        goToCameraModule()
        
    }
    
    // MARK: - Methods
    
    func goToCameraModule (){
        let swiftUIView = ContentView(model: model)
            let hostingController = UIHostingController(rootView: swiftUIView)
//        navigationController?.pushViewController(hostingController, animated: false)
        hostingController.modalPresentationStyle = .fullScreen
        present(hostingController, animated: false)
    }
    
 
}
