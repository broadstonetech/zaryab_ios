//
//  LoginViewController.swift
//  Parsl
//
//  Created by M Zaryab on 22/12/2021.
// This class enables the user to singin/ signup at same time using email passwowrd or using sing in with apple (apple login)

import UIKit
import AuthenticationServices
import Toast_Swift

class LoginViewController: BaseViewController {

    // MARK: - Iboutlets
    
    @IBOutlet weak var ppView: UIView!
    @IBOutlet weak var ppCheckBtn: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var TCBtn: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var checkUncheckBtn: UIImageView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailview: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var passwordView: UIView!
    
    // MARK: - Properties
    let skipButton = UIButton(type: .system)
    var fromSendOrRedeem = ""
    var style = ToastStyle()
    var socialLoginPressed = false
    var isCheckBtnPressed = true
    var isPPCheckBtnPressed = true
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpSkipbtn()
        setUpSignInAppleButton()
        setUPUi()
        createTapAbleButton() 
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        generateNotifications()
        
    }
    
    // MARK: - Methods
    
    @objc func tapTCPressed(_ sender: UITapGestureRecognizer? = nil) {
        if let url = constantsUrl.termsConditionsUrl {
            UIApplication.shared.open(url)
        }
    }
    
    @objc func ppViewPressed(_ sender: UITapGestureRecognizer? = nil) {
        if let url = constantsUrl.pricacyPolicyUrl {
            UIApplication.shared.open(url)
        }
    }
    
    @objc func checkUncheckPressed(_ sender: UITapGestureRecognizer? = nil) {
        if isCheckBtnPressed == true {
            self.checkUncheckBtn.image = UIImage(named: "unCheckBox")
            isCheckBtnPressed = false
        }
        else {
            self.checkUncheckBtn.image = UIImage(named: "checkBox")
            isCheckBtnPressed = true
        }
    }
    
    @objc func ppCheckBtnPressed(_ sender: UITapGestureRecognizer? = nil) {
        if isPPCheckBtnPressed == true {
            self.ppCheckBtn.image = UIImage(named: "unCheckBox")
            isPPCheckBtnPressed = false
        }
        else {
            self.ppCheckBtn.image = UIImage(named: "checkBox")
            isPPCheckBtnPressed = true
        }
    }
    
    
    @objc func handleAppleIdRequest() {
        
        if isCheckBtnPressed == false {
            self.view.makeToast(constantsStrings.acceptParslTC, duration: 1.0, position: .bottom, style: style)
            return
        }
        if isPPCheckBtnPressed == false {
            self.view.makeToast(constantsStrings.acceptParslPP, duration: 1.0, position: .bottom, style: style)
            return
        }
        
    let appleIDProvider = ASAuthorizationAppleIDProvider()
    let request = appleIDProvider.createRequest()
    request.requestedScopes = [.fullName, .email]
    let authorizationController = ASAuthorizationController(authorizationRequests: [request])
    authorizationController.delegate = self
    authorizationController.performRequests()
    }
    
    func setUPUi() {
        emailField.clipsToBounds = true
        passwordField.clipsToBounds = true
        emailview.clipsToBounds = true
        passwordView.clipsToBounds = true
        mainStackView.layer.cornerRadius = 7
        mainStackView.clipsToBounds = true
//        withoutSignView.layer.cornerRadius = 10
//        withoutSignView.clipsToBounds = true
        loginBtn.layer.cornerRadius = loginBtn.frame.size.height / 2
        loginBtn.clipsToBounds = true
        emailField.placeholder = constantsStrings.emailPlaceHolder
        passwordField.placeholder = constantsStrings.passwordPlaceholder
        emailField.attributedPlaceholder = NSAttributedString(
            string: constantsStrings.emailPlaceHolder,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "toastBackColor")]
        )
        passwordField.attributedPlaceholder = NSAttributedString(
            string: constantsStrings.passwordPlaceholder,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "toastBackColor")]
        )
    }
    
    func createTapAbleButton () {
        let tapTC = UITapGestureRecognizer(target: self, action: #selector(self.tapTCPressed(_:)))
        TCBtn.addGestureRecognizer(tapTC)
        
        let ppTap = UITapGestureRecognizer(target: self, action: #selector(self.ppViewPressed(_:)))
        ppView.addGestureRecognizer(ppTap)
        
        let checkUncheck = UITapGestureRecognizer(target: self, action: #selector(self.checkUncheckPressed(_:)))
        checkUncheckBtn.addGestureRecognizer(checkUncheck)
        let ppChecktap = UITapGestureRecognizer(target: self, action: #selector(self.ppCheckBtnPressed(_:)))
        ppCheckBtn.addGestureRecognizer(ppChecktap)
    }
    
    func generateNotifications () {
        if fromSendOrRedeem == "send"{
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowPaymentMehods"), object: nil)
        }
        else if fromSendOrRedeem == "redeem" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowRedeemView"), object: nil)
        }
        else if  fromSendOrRedeem == "avatar" {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MyAvatarLoginCompleted"), object: nil)
            }
        }
        else if  fromSendOrRedeem == "joinNow" {
            if ParslUtils.sharedInstance.getIfuserLogin() == true {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "joinNowLoginCompleted"), object: nil)
            }
            }
        }
    }
    // Return Bool value if a password is valid or not
//    func isValidPassword(_ testStr:String) -> Bool {
//        let passwordRegEx = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"
//        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
//        
//        return passwordTest.evaluate(with: testStr)
//    }
    // Setup UI of apple sign in button
    func setUpSignInAppleButton () {
      let authorizationButton = ASAuthorizationAppleIDButton()
      authorizationButton.addTarget(self, action: #selector(handleAppleIdRequest), for: .touchUpInside)
        authorizationButton.layer.cornerRadius = 10
        authorizationButton.frame = CGRect(x: 0, y: 0, width: self.bottomView.frame.size.width - 10, height: self.bottomView.frame.size.height - 10)
        bottomView.addSubview(authorizationButton)
        
    }
    // setup UI of skip button
    func setUpSkipbtn() {
    skipButton.frame = CGRect(x: view.frame.size.width/1.3, y: 20, width: 80, height: 25)
        if self.fromSendOrRedeem == "avatar" {
            skipButton.isUserInteractionEnabled = false
            skipButton.setTitle("", for: .normal)
//            skipButton.backgroundColor = UIColor.white
        }
        else {
            skipButton.setTitle(constantsStrings.cancelLabel, for: .normal)
        }
    skipButton.backgroundColor = .clear
    skipButton.layer.cornerRadius = 5
    skipButton.clipsToBounds = true
    skipButton.titleLabel!.font = UIFont.systemFont(ofSize: 18)
    skipButton.setTitleColor(UIColor.link, for: .normal)
    skipButton.addTarget(self, action: #selector(skipBtnActions(_:)), for: .touchUpInside)
    view.addSubview(skipButton)
    }
    
    @objc func skipBtnActions(_ sender:UIButton!) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - IBActions
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "forgotVC") as! forgotVC
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func loginBtnPressend(_ sender: Any) {
        if emailField.text!.isEmpty || emailField.text == "" || emailField.text == " " {
            self.view.makeToast(constantsStrings.emptyUserName, duration: 1.0, position: .bottom, style: style)
            return
        }
        if passwordField.text!.isEmpty || passwordField.text == "" || passwordField.text == " " {
            self.view.makeToast(constantsStrings.emptyPassword, duration: 1.0, position: .bottom, style: style)
            return
        }
        if !isValidEmail(emailField.text!) {
            self.view.makeToast(constantsStrings.invalidUserEmail, duration: 1.0, position: .bottom, style: style)
            return
        }
        if !isPasswordEmail(passwordField.text!) {
            self.view.makeToast(constantsStrings.enterValidEmail, duration: 1.0, position: .bottom, style: style)
            return
        }
        if isCheckBtnPressed == false {
            self.view.makeToast(constantsStrings.acceptParslTC, duration: 1.0, position: .bottom, style: style)
            return
        }
        if isPPCheckBtnPressed == false {
            self.view.makeToast(constantsStrings.acceptParslPP, duration: 1.0, position: .bottom, style: style)
            return
        }
        // call method of login if above conditions match.
        loginUser(socialLogin: "",email: emailField.text!)
    }
    
    func loginUser(socialLogin: String, email: String) {
       
        ProgressHUD.show()
        var data = [String: Any]()
        if socialLogin != ""{
            // if user is login using apple login
            data = ["email": email, "password": "", "sociallogin": socialLogin, "old_mail": ParslUtils.sharedInstance.getuserOldEmail()]
        }
        else {
            // if user is login using email password fields
            var password = passwordField.text!
//            password = password.sha256()
            
            data = ["email": email ,"password": password, "sociallogin": "", "old_mail": ParslUtils.sharedInstance.getuserOldEmail()]
        }
        
        let params = ["app_id": "parsl_ios", "data": data] as [String: AnyObject]
        print(params)
        
        let url = constantsUrl.loginurl
        if Reachability.isConnectedToNetwork() {
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                print(json)
                let status = json["status"] as! Bool
                let message = json["message"] as! String
                
                if status == true{
                    DispatchQueue.main.async{
                    ProgressHUD.dismiss()
                        let data = json["data"]!
                        let userNameSpace = data["namespace"] as! String
                        self.view.makeToast(constantsStrings.loginSuccess)
                        ParslUtils.sharedInstance.saveIfUserLogin(true)
                        ParslUtils.sharedInstance.saveUserNameSpace(userNameSpace)
                        if self.socialLoginPressed == true {
                            let email: String!  = ParslUtils.sharedInstance.getuserSocialEmail()
                            ParslUtils.sharedInstance.saveUserEmail(email)
                            self.socialLoginPressed = false
                        }
                        else {
                            ParslUtils.sharedInstance.saveUserEmail(email)
                        }
                       
                        print("...\(ParslUtils.sharedInstance.getuserEmail())")
                        self.dismiss(animated: true, completion: nil)
                        if self.fromSendOrRedeem == "cart" {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "cartLoginSuccess"), object: nil)
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        ProgressHUD.dismiss()
                        self.view.makeToast(message)
                    }
                    
                }
                
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
            ProgressHUD.dismiss()
        })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
}


