//
//  MyAvatarsExtension.swift
//  Parsl
//
//  Created by M Zaryab on 01/03/2022.
//

import Foundation
import UIKit

//MARK: - Extensions
extension MyAvatarsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAvatarsTableViewCell", for: indexPath) as! MyAvatarsTableViewCell
        cell.avatarTitle.text = "\(dataArray[indexPath.row].title ?? "")\n\(dataArray[indexPath.row].descriptionField ?? "")"
        cell.avatarStatus.text = dataArray[indexPath.row].status
        let url = URL(string: "")
        cell.avatarThumbNail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        vc.isfrom = "MyAvatars"
        vc.forAvatar =  dataArray[indexPath.row].avatarId
        self.navigationController?.pushViewController(vc, animated: true)
    }
     
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView,
                       trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = UIContextualAction(style: .normal,
                                         title: "Edit") { [weak self] (action, view, completionHandler) in
                                            self?.editAvatar()
                                            completionHandler(true)
        }
        self.avatarindex = indexPath.row
        edit.backgroundColor = .systemGreen

        // Trash action
       
        let delete = UIContextualAction(style: .normal,
                                         title: "Delete") { [weak self] (action, view, completionHandler) in
                                            self?.deleteAvatar()
                                            completionHandler(true)
        }
        self.avatarindex = indexPath.row
        delete.backgroundColor = .red
        let configuration = UISwipeActionsConfiguration(actions: [delete, edit])

        return configuration
    }
    
    private func editAvatar() {
        print("Marked as edit")
        
        self.avatarId = dataArray[avatarindex].avatarId
        self.editUserAvatar()
    }
    
    private func deleteAvatar() {
        print("avatar deleted")
        self.avatarId = dataArray[avatarindex].avatarId
        self.deleteUserAvatar()
    }
}
