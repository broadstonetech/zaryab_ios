//
//  MyAvatarsViewController.swift
//  Parsl
//
//  Created by M Zaryab on 28/12/2021.
// This class shows the saved avatars along the status of the avatar. It helps the user to request, edit and delete the avatar.

import UIKit
import WebKit
import Toast_Swift

class MyAvatarsViewController: BaseViewController, WebViewDelegate{

    //MARK: - IBOutlets
  
    @IBOutlet weak var avatarsTableView: UITableView!
    var dataArray = [AvatarsData]()
    
    //MARK: - Properties
    var webView: WKWebView!
    let webViewControllerTag = 100
    let webViewIdentifier = "WebViewController"
    var webViewController = WebViewController()
    var pasteboardString: String? = UIPasteboard.general.string
    var isAddPressed = false
    var avatarId = ""
    var avatarindex = 0
    
    var style = ToastStyle()
    
    
    
    //MARK: - lifeCycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "toastBackColor")
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "toastBackColor")
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        getListofMyAvatar()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
 
        let add = UIBarButtonItem(title: "+", style: .plain, target: self, action: #selector(addTapped))
        navigationItem.rightBarButtonItems = [add]
        avatarsTableView.delegate = self
        avatarsTableView.dataSource = self
        avatarsTableView.rowHeight = UITableView.automaticDimension
        avatarsTableView.reloadData()
        title = constantsStrings.myAvatarScreenTitle
        
        NotificationCenter.default.addObserver(self, selector: #selector(avatarUpdated(_:)), name: Notification.Name(rawValue: "AvatarUpdated"), object: nil)
        
    }
    //MARK: - Methods

    @objc func avatarUpdated (_ notification: NSNotification){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.title = constantsStrings.myAvatarScreenTitle
            self.getListofMyAvatar()
        }
        
    }
    
    @objc func addTapped() {
        print("add tapped")
        
        if isAddPressed == false {
            title = constantsStrings.createAvatarScreenTitle
            destroyWebView()
            createWebView()
            webViewController.reloadPage(clearHistory: true)
            webViewController.view.isHidden = false
            isAddPressed = true
        }
        else {
            isAddPressed = false
            webViewController.view.isHidden = true
            title = constantsStrings.myAvatarScreenTitle
        }
    }
    
    func getListofMyAvatar() {
        ProgressHUD.show()
        let url = constantsUrl.getDetailsAvatarUrl
        let params = ["app_id": "parsl_ios", "namespace": ParslUtils.sharedInstance.getUserNameSpace()] as [String: AnyObject]
        if Reachability.isConnectedToNetwork() {
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                
                print("...count is \(json)")
                let dictonary = json as [String: Any]
                let obj = Avatars(fromDictionary: dictonary)
                self.dataArray = obj.data
                if obj.message == "Success" {
                    DispatchQueue.main.async{
                    self.avatarsTableView.reloadData()
                    ProgressHUD.dismiss()
                    }
                } else {
                    self.showAlert(withMessage: constantsStrings.avatarsNotavailable)
                    ProgressHUD.dismiss()
                }

            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
        })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    
    func createWebView(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: webViewIdentifier) as UIViewController

        guard let viewController = controller as? WebViewController else {
            return
        }
        webViewController = viewController
        webViewController.avatarUrlDelegate = self
        
        addChild(controller)

        self.view.addSubview(controller.view)
        controller.view.frame = view.safeAreaLayoutGuide.layoutFrame
        controller.view.tag = webViewControllerTag
        controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        controller.didMove(toParent: self)
    }
    
    func avatarUrlCallback(url: String){
        
        webViewController.view.isHidden = true
        let pasteboardString: String? = UIPasteboard.general.string
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SubmitAvatarViewController")as! SubmitAvatarViewController
        vc.avatarUrl = url
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showAlert(message: String){
        let alert = UIAlertController(title: constantsStrings.avatarUrlGenerated, message: message, preferredStyle: .alert)

        let okButton = UIAlertAction(title: constantsStrings.okLabel, style: .default, handler: { action in
             })
             alert.addAction(okButton)
             DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
        })
    }
    
    func destroyWebView(){
        if let viewWithTag = self.view.viewWithTag(webViewControllerTag) {
            
            webViewController.dismiss(animated: true, completion: nil)
            viewWithTag.removeFromSuperview()
        }else{
            print("No WebView to destroy!")
        }
    }
    
    func editUserAvatar () {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SubmitAvatarViewController") as! SubmitAvatarViewController
        vc.isfrom = "edit"
        vc.avatarId = avatarId
        self.present(vc, animated: true, completion: nil)
    }
    
    func deleteUserAvatar () {
        
        ProgressHUD.show()
        let url = constantsUrl.DeleteAvatarUrl
        
        let params = ["namespace": ParslUtils.sharedInstance.getUserNameSpace(), "avatar_id": self.avatarId] as [String: AnyObject]
        print(params)
        
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]

                print("...count is \(json)")
                let status = json["status"] as! Bool
                let message = json["message"] as! String
                if status == true {
                    DispatchQueue.main.async {
                        self.view.makeToast(constantsStrings.deleted, duration: 1.0, position: .bottom, style: self.self.style)
                        ProgressHUD.dismiss()
                        self.getListofMyAvatar()
                    }
                }
                else {
                    ProgressHUD.dismiss()
                    self.showAlert(withMessage: message)
                }
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
            ProgressHUD.dismiss()
        })
    }
    //MARK: - IBActions

}
