//
//  LoginScreenExtensions.swift
//  Parsl
//
//  Created by M Zaryab on 23/02/2022.
//

import Foundation
import UIKit
import AuthenticationServices


// MARK: - Extensions
// Related to the apple login.
extension LoginViewController: ASAuthorizationControllerDelegate {
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
    if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
    let userIdentifier = appleIDCredential.user
    let fullName = appleIDCredential.fullName
    let email = appleIDCredential.email
        print("email is \(String(describing: email))")
        print("user id is  \(userIdentifier) user full name is \(String(describing: fullName)) user email is \(String(describing: email))")
        self.socialLoginPressed = true
        if email == nil {
        self.loginUser(socialLogin: userIdentifier, email: String(describing: email))
        }
        else {
            ParslUtils.sharedInstance.saveUserSocialEmail(String(describing: email))
            self.loginUser(socialLogin: userIdentifier, email: String(describing: email))
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    // Handle error.
        print(error)
    }
  }
}
