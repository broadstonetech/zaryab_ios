//
//  CollectionViewExtension.swift
//  Parsl
//
//  Created by M Zaryab on 24/02/2022.
//

import Foundation
import UIKit


// MARK: Collection View
extension CartViewController:UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let returnValue = 0
        if collectionView == ExtrasCollectionView{
            return giftModels.data!.count
        }
        else if collectionView == avatarCollectionView{
            return giftModels.avatars!.count
        }
        else if collectionView == animationCollectionView{
            return giftModels.animations.count
        }
        else if collectionView == myLiveAvatarCollectionView{
            return giftModels.animations.count
        }
        return returnValue
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == ExtrasCollectionView {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExtrasCollectionViewCell", for: indexPath) as! ExtrasCollectionViewCell
        let item = giftModels.data![indexPath.row]
        cell.imagePath = item.modelFiles!.thumbnail!
        let url = URL(string: cell.imagePath)
        cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
        cell.title.text = item.basicInfo!.name!
        print("boxes are ",item.basicInfo?.name,item.productID)
            cell.mainView.layer.cornerRadius = 7
            cell.mainView.clipsToBounds = true
            cell.thumbnail.layer.cornerRadius = 7
            cell.thumbnail.clipsToBounds = true
        
        if item.isModelSelected == true{
            cell.SelectedImage.isHidden = false
        }
        else{
            cell.SelectedImage.isHidden = true
        }
        
        return cell
        }
        else if collectionView == avatarCollectionView {
            let avatarCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvatarCollectionViewCell", for: indexPath) as! AvatarCollectionViewCell
            let avatarItem = giftModels.avatars![indexPath.row]
            avatarCell.imagePath = avatarItem.chracter_thumbnail!
            let url = URL(string: avatarCell.imagePath)
            avatarCell.characterThumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            if avatarItem.isAvatarSelected == true{
                avatarCell.avatarselectedImg.isHidden = false
            }
            else{
                avatarCell.avatarselectedImg.isHidden = true
            }
            return avatarCell
        }
        else if collectionView == animationCollectionView {
            let animationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnimationCollectionViewCell", for: indexPath) as! AnimationCollectionViewCell
            let animationitem = giftModels.animations[indexPath.row]
            animationCell.imagePath = animationitem.animation_thumbnail!
            let url = URL(string: animationCell.imagePath)
            animationCell.animatiomThumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            if animationitem.isAnimationSelected == true{
                animationCell.animationSelectedImg.isHidden = false
            }
            else{
                animationCell.animationSelectedImg.isHidden = true
            }
            return animationCell
        }
        
        else if collectionView == myLiveAvatarCollectionView {
            let animationCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyLiveAvatarsCollectionViewCell", for: indexPath) as! MyLiveAvatarsCollectionViewCell
            let animationitem = giftModels.animations[indexPath.row]
            animationCell.imagePath = animationitem.animation_thumbnail!
            let url = URL(string: animationCell.imagePath)
            animationCell.animatiomThumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            if animationitem.isAnimationSelected == true{
                animationCell.animationSelectedImg.isHidden = false
            }
            else{
                animationCell.animationSelectedImg.isHidden = true
            }
            return animationCell
        }
        
       return UICollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == ExtrasCollectionView{
        var item = giftModels.data!
        for i in 0..<giftModels.data!.count{
            giftModels.data![i].isModelSelected = false
        }
        giftWrapSelectedIndex = indexPath.row
        giftModels.data![indexPath.row].isModelSelected = true
//        scrollToCell(index: indexPath.row)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 ) {
//            //self.ExtrasCollectionView.reloadData()
            self.ExtrasCollectionView.reloadSections(IndexSet(integer: 0))
//        }
        }
        else if collectionView == animationCollectionView{
            for j in 0..<giftModels.animations.count{
                giftModels.animations[j].isAnimationSelected = false
            }
            animationSelectedIndex = indexPath.row
            giftModels.animations[indexPath.row].isAnimationSelected = true
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 ) {
                self.animationCollectionView.reloadSections(IndexSet(integer: 0))
//            }
        }
        else if collectionView == avatarCollectionView{
            for k in 0..<giftModels.avatars!.count{
                giftModels.avatars![k].isAvatarSelected = false
            }
            avatarSelectedIndex = indexPath.row
            giftModels.avatars![indexPath.row].isAvatarSelected = true
//            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5 ) {
                self.avatarCollectionView.reloadSections(IndexSet(integer: 0))
//            }
        }
    }
    
    func previewBtnPressed(itemIndex: Int) {
        let fileName = giftModels.data![itemIndex].basicInfo!.name!
        let fileUrl = giftModels.data![itemIndex].modelFiles!.previewVideo ?? ""
        if fileUrl == "" {
            self.view.makeToast(constantsStrings.NoPreview)
            return
        }
        let index = isAnimationAlreadyDownloaded(fileName: fileName)
        if index == -1 {
            downloadGiftBoxPreviewVideo(animationFileUrl: fileUrl, animationFileName: fileName)
        }else {
            playGiftBoxVideo(filePath: downloadedAnimationsFilePaths[index])
        }
    }
    
    @objc private func giftBoxDoubleTapped() {
        let fileName = giftModels.data![giftWrapSelectedIndex].basicInfo!.name!
        let fileUrl = giftModels.data![giftWrapSelectedIndex].modelFiles!.previewVideo ?? ""
        if fileUrl == "" {
            self.view.makeToast(constantsStrings.NoPreview)
            return
        }
        let index = isAnimationAlreadyDownloaded(fileName: fileName)
        if index == -1 {
            downloadGiftBoxPreviewVideo(animationFileUrl: fileUrl, animationFileName: fileName)
        }else {
            playGiftBoxVideo(filePath: downloadedAnimationsFilePaths[index])
        }
    }
    
    private func isAnimationAlreadyDownloaded(fileName: String) -> Int {
        
        for i in 0..<downloadedAnimationsFilePaths.count {
            if downloadedAnimationsFilePaths[i].lowercased().contains(fileName.lowercased()) {
                return i
            }
        }
        
        return -1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = 0
//        let height = 0
        
         if collectionView == ExtrasCollectionView{
            let width = ExtrasCollectionView.frame.size.width/3
            let height = ExtrasCollectionView.frame.size.height/1.3
            return CGSize(width: width, height: height)
        }
        else if collectionView == avatarCollectionView{
            let width = animationCollectionView.frame.size.width/3
            let height = animationCollectionView.frame.size.height/1.5
            return CGSize(width: width, height: height)
        }
        
        else if collectionView == animationCollectionView{
            let width = animationCollectionView.frame.size.width/3
            let height = animationCollectionView.frame.size.height/1.5
            return CGSize(width: width, height: height)
        }
        
        
        return collectionView.frame.size
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
