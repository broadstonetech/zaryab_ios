//
//  Extensions.swift
//  Parsl
//
//  Created by M Zaryab on 24/02/2022.
//

import Foundation
import UIKit
import AVKit

extension CartViewController {
    
    func downloadGiftBoxPreviewVideo(animationFileUrl : String, animationFileName: String) {
        ProgressHUD.show(constantsStrings.downloadingLabel)
        let url = URL(string: animationFileUrl)
        let request = URLRequest(url: url!)
        let session = URLSession.shared
        
        let downloadTask = session.downloadTask(with: request,completionHandler: { (location:URL?, response:URLResponse?, error:Error?) -> Void in
                var statusCode : Int = 100
            if let httpResponse = response as? HTTPURLResponse {
                print("StatusCode== \(httpResponse.statusCode)")
                let downloadedFile = httpResponse.suggestedFilename ?? url!.lastPathComponent
                print("filename==\(downloadedFile)")
                statusCode = httpResponse.statusCode
                
                if response != nil  && statusCode == 200 {
                    print("Response:\(String(describing: response))")
                    
                    // Get the document directory url
                    if location?.path != nil {
                        let locationPath = location!.path
                        let documentsDestPath:String = NSHomeDirectory() + "/Documents/\(animationFileName).mp4"
                        let fileManager = FileManager.default
                        if (fileManager.fileExists(atPath: documentsDestPath)){
                            try! fileManager.removeItem(atPath: documentsDestPath)
                        }
                        try! fileManager.moveItem(atPath: locationPath, toPath: documentsDestPath)
                        //get stored file path and url
                        let fileName = "\(animationFileName).mp4"
                        let filePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(fileName)"
                        print("File path \(filePath)")
                        
                        
                        DispatchQueue.main.async { [self] in
                            ProgressHUD.dismiss()
                            self.downloadedAnimationsFilePaths.append(filePath)
                            self.playGiftBoxVideo(filePath: filePath)
                            //animationView!.play()
                        }
                    }
                    else {
                        // show network error message and pop vc
                        DispatchQueue.main.async {
                            ProgressHUD.dismiss()
                            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.unabletoDownloadGift)
                        }
                    }
                }else {
                    ProgressHUD.dismiss()
                    debugPrint("downloading failed")
                    
                }
            }
        })
        downloadTask.resume()
    }
    
     func playGiftBoxVideo(filePath: String) {
        let url = URL(fileURLWithPath: filePath)
        
        let player = AVPlayer(url: url)
        
        let vc = AVPlayerViewController()
        vc.player = player
        
        self.present(vc, animated: true) {
            vc.player?.play()
            
        }
    }
}
