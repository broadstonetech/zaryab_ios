//
//  TableViewExtension.swift
//  Parsl
//
//  Created by M Zaryab on 24/02/2022.
//

import Foundation
import UIKit
// MARK: - Extensions
extension CartViewController: UITableViewDelegate, UITableViewDataSource{
    //Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return selectedItemsDataList.count
        return 1 + modelsInCart.count + modelsPrices.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeadingLabelsCell")!
            return cell
        }
        
        if indexPath.row < 1 + modelsInCart.count {
            let item = modelsInCart[indexPath.row - 1]
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell") as? CartTableViewCell
            cell?.delegate = self
            cell?.plusButton.tag = indexPath.row - 1
            cell?.minusButton.tag = indexPath.row - 1
            cell?.titleLabel.text = item.basicInfo!.name!
            cell?.itemsCountLabel.text = String(modelsPrices[indexPath.row - 1].modelQuantity)
            cell?.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: item.priceInfo.price)
            cell?.subtotalLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: (item.priceInfo.price * Double(modelsPrices[indexPath.row - 1].modelQuantity)))
            let url = URL(string: modelsInCart[indexPath.row - 1].modelFile.thumbnail)
            cell?.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
    

            
            if modelsPrices[indexPath.row-1].isDiscounted {
                //discounted
                cell!.discountedPrice.isHidden = false
                cell!.discountedPrice.text = "$\(String(format: "%.2f",modelsPrices[indexPath.row-1].totalPrice))"
                cell!.discountedCutView.isHidden = false
            }else{
                cell!.discountedPrice.isHidden = true
                cell!.discountedCutView.isHidden = true
                
            }
            return cell!
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartDetailTableViewCell") as? CartDetailTableViewCell
            let index = indexPath.row - modelsInCart.count - 1
            cell?.titleLabel.text = modelsInCart[index].basicInfo.name
            cell?.quantityLabel.text = "\(String(modelsPrices[index].modelQuantity))"
            cell?.priceLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: modelsPrices[index].totalPrice)
            
            if modelsPrices[index].isDiscounted{
                cell?.priceLabel.text = "$\(String(format: "%.2f",modelsPrices[index].totalPrice))"
            }

            return cell!
        }
    }
    //Protocol Functions
    func deleteButtonTapped(cell: CartTableViewCell, sender: Any) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }
        modelsInCart.remove(at: indexPath.row - 1)
        modelsPrices.remove(at: indexPath.row - 1)
        DispatchQueue.main.async {
            if self.modelsInCart.count == 0{
                
                let alert = UIAlertController(title: "Cart is Empty", message: "Add Items to Cart", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: {(action:UIAlertAction!) in
                    self.cartDelegate?.cartHasBeenCleared()
                    if let navigator = self.navigationController {
                        navigator.popViewController(animated: true)
                    }
                }))
               
                self.present(alert, animated: true, completion: nil)
            }
            self.tableView.reloadData()
        }
        recalculateFinalPrice()
        
    }
    func plusButtonTapped(cell: CartTableViewCell, sender: Any) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }

        let modelPrice = modelsPrices[indexPath.row - 1]
        if modelPrice.modelQuantity < 100 {
            let modelQuantity = modelPrice.modelQuantity + 1
            let finalPrice = modelPrice.modelPrice * Double(modelQuantity)
            
            let newModelPriceStruct = ModelsQuantityStruct(modelIndex: modelPrice.modelIndex, modelQuantity: modelQuantity, modelPrice: modelPrice.modelPrice, totalPrice: finalPrice,isDiscounted:modelPrice.isDiscounted)
            
            modelsPrices[indexPath.row - 1] = newModelPriceStruct
            
           if isAnyDiscount{
            self.getCopounDetails()
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            recalculateFinalPrice()
        }else {
            self.view.makeToast("Total quantity should be less than 100")
        }
        
    }
    func minusButtonTapped(cell: CartTableViewCell, sender: Any) {
        guard let indexPath = self.tableView.indexPath(for: cell) else {
            return
        }
        
        let modelPrice = modelsPrices[indexPath.row - 1]
        
        if modelPrice.modelQuantity > 1 {
            let modelQuantity = modelPrice.modelQuantity - 1
            let finalPrice = modelPrice.modelPrice * Double(modelQuantity)
            
            let newModelPriceStruct = ModelsQuantityStruct(modelIndex: modelPrice.modelIndex, modelQuantity: modelQuantity, modelPrice: modelPrice.modelPrice, totalPrice: finalPrice,isDiscounted: modelPrice.isDiscounted)
            
            modelsPrices[indexPath.row - 1] = newModelPriceStruct
            
            if isAnyDiscount{
             self.getCopounDetails()
             }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            recalculateFinalPrice()
        }else {
            self.view.makeToast("Quantity cannot be less than 1")
        }
        
    }
}
