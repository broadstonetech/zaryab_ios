//
//  AnimationCollectionViewCell.swift
//  Parsl
//
//  Created by M Zaryab on 18/09/2021.
//

import UIKit

class AnimationCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var animatiomThumbnail: UIImageView!
        @IBOutlet weak var animationSelectedImg: UIImageView!
        var imagePath = ""
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
            self.mainView.backgroundColor = .lightGray
            self.mainView.layer.cornerRadius = 7
        }
}
