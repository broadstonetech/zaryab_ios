//
//  MyLiveAvatarsCollectionViewCell.swift
//  Parsl
//
//  Created by M Zaryab on 17/01/2022.
//

import UIKit

class MyLiveAvatarsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var animatiomThumbnail: UIImageView!
        @IBOutlet weak var animationSelectedImg: UIImageView!
        var imagePath = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.mainView.backgroundColor = .lightGray
        self.mainView.layer.cornerRadius = 7
    }
}
