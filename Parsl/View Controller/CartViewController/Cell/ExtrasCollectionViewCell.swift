//
//  ExtrasCollectionViewCell.swift
//  Parsl
//
//  Created by Billal on 24/02/2021.
//

import UIKit

protocol PreviewBtnDelegate {
    func previewBtnPressed(itemIndex: Int)
}

class ExtrasCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var thumbnail:       UIImageView!
    @IBOutlet weak var title:           UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var SelectedImage:   UIImageView!
    
    var cellIndex: Int?
    var delegate: PreviewBtnDelegate?
    
    var imagePath = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
//    @IBAction func previewBtnPressed(_ sender: UIButton) {
//        delegate?.previewBtnPressed(itemIndex: cellIndex!)
//    }
}

