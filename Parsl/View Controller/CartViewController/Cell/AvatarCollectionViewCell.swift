//
//  AvatarCollectionViewCell.swift
//  Parsl
//
//  Created by M Zaryab on 03/09/2021.
//

import UIKit

class AvatarCollectionViewCell: UICollectionViewCell {
 
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var characterThumbnail: UIImageView!
    
    @IBOutlet weak var avatarselectedImg: UIImageView!
    @IBOutlet weak var characterLbl: UILabel!
    var imagePath = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        self.mainView.backgroundColor = .lightGray
        self.mainView.layer.cornerRadius = 7
    }
}
