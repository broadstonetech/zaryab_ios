// Redeem Later.
// Redeem Now.
// This class allows the users to save PARSL otp for late redeem procedure as well as enable the user to do the redeem of the PARSL
/* always add strings in only constant file
 always seprate the urls in sperate file
 use alwasy debug print for testing purpose instead of the print statement.
 use dispatchqueue if using loading progress view etc.
 always make folder for view controller and make subfolder for its cell and extensions.
 always use enums in seprate file.
 add protocols always in last in the file.
 always use else if using if().
 */
import UIKit
import ContactsUI

class ReceiverModalViewController: BaseViewController,CNContactPickerDelegate, UITextFieldDelegate{
    
    // MARK: - Properties
    private let contactPicker = CNContactPickerViewController()
    var delegate : SendDataDelegate?
    let peoplePicker = CNContactPickerViewController()
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var redeemLater: UIButton!
    @IBOutlet weak var redeemNow: UIButton! // if it is a button then add btn in last camel case
    @IBOutlet weak var innerStackView: UIStackView!
    @IBOutlet weak var receiverNameTF: UITextField!
    @IBOutlet weak var urlTF: UITextField!
    @IBOutlet weak var receiverContactTF: UITextField!
    
    //MARK: -ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        receiverNameTF.delegate = self
        receiverContactTF.delegate = self
        self.redeemNow.backgroundColor = UIColor.lightGray
        self.redeemLater.backgroundColor = UIColor.lightGray
        self.redeemNow.isUserInteractionEnabled = false
        self.redeemLater.isUserInteractionEnabled = false
        self.redeemNow.layer.cornerRadius = self.redeemNow.frame.size.height/2
        self.redeemLater.layer.cornerRadius = self.redeemLater.frame.size.height/2
        urlTF.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.innerStackView.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //        self.delegate?.mayBelater()
        self.delegate?.showActionButton()
    }
    
    // MARK: - IBActions
    @IBAction func cancelButtonTapped(_ sender: Any) {
        if ParslUtils.sharedInstance.getIfuserLogin() {
            if Reachability.isConnectedToNetwork() {
                self.saveParslOtp()
            }
        }
        else {
            navigateToLoginVc()
        }
    }
    
    @IBAction func submitAction(_ sender: Any) {
        if urlTF.text == "" || urlTF.text!.isEmpty {
            self.view.makeToast(constantsStrings.enterOTP)
            return
        }
        receiverNameTF.text = ""
        receiverContactTF.text = ""
        
        
        if !urlTF.text!.contains("parsl.io") {
            let uppercasedText = urlTF.text?.uppercased()
            urlTF.text = uppercasedText
        }
        if Reachability.isConnectedToNetwork() {
            self.delegate?.sendData(self.receiverNameTF.text!,self.receiverContactTF.text!,self.urlTF.text!)
            dismiss(animated: true, completion: nil)
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    // MARK: - Class Funcions
    
    func navigateToLoginVc () {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func saveParslOtp() {
        ProgressHUD.show()
        let url = constantsUrl.saveParslOtpUrl
        let Data = ["parsl_otp": urlTF.text!] as [String: AnyObject]
        let params = ["app_id": "parsl_ios", "namespace": ParslUtils.sharedInstance.getUserNameSpace(), "data": Data] as [String: AnyObject]
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                print("...count is \(json)")
                let status = json["status"] as! Bool
                let message = json["message"] as! String
                if status == true {
                    DispatchQueue.main.async {
                        ProgressHUD.dismiss()
                        self.view.makeToast(message)
                        self.delegate?.mayBelater()
                        self.dismiss(animated: true, completion: nil)
                    }
                }
                else {
                    DispatchQueue.main.async {
                        ProgressHUD.dismiss()
                        self.showAlert(withMessage: message)
                    }
                }
                
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
            ProgressHUD.dismiss()
        })
    }
}

