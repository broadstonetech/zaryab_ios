//
//  Extension.swift
//  Parsl
//
//  Created by M Zaryab on 28/02/2022.
//

import Foundation
import UIKit

// MARK: - Extensions
// MARK: - textfield delegate method.
extension ReceiverModalViewController {
    // to change the oclor of the button before and after adding text.
    func textFieldDidChangeSelection(_ textField: UITextField) {
        let changeText = self.urlTF.text ?? ""
        if changeText == "" {
            self.redeemNow.backgroundColor = UIColor.lightGray
            self.redeemLater.backgroundColor = UIColor.lightGray
            self.redeemNow.isUserInteractionEnabled = false
            self.redeemLater.isUserInteractionEnabled = false
        }
        else {
            self.redeemNow.backgroundColor = UIColor.link
            self.redeemLater.backgroundColor = UIColor.link
            self.redeemNow.isUserInteractionEnabled = true
            self.redeemLater.isUserInteractionEnabled = true
        }
    }
}
