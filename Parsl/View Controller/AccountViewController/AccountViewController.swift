//
//  AccountViewController.swift
//  Parsl
//
//  Created by M Zaryab on 13/12/2021.
// This class gives access the users to visit multiple options of PARSL app to enhance user experience. Let user to change password, let user to watch notifications, let user to create, view avatars and parsls.

import UIKit
import StoreKit
import Toast_Swift
import SwiftUI

class AccountViewController:  BaseViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var createModelView: UIView!
    @IBOutlet weak var myParslsView: UIView!
    @IBOutlet weak var notificationsCountLbl: UILabel!
    @IBOutlet weak var termsAndConditions: UIView!
    @IBOutlet weak var legalHeight: NSLayoutConstraint!
    @IBOutlet weak var legalView: UIView!
    @IBOutlet weak var privacyView: UIView!
    @IBOutlet weak var registeredEmail: UILabel!
    @IBOutlet weak var signOutIcon: UIImageView!
    @IBOutlet weak var signOutLabel: UILabel!
    @IBOutlet weak var changePasswordView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var checkUpdateView: UIView!
    @IBOutlet weak var appSettingView: UIView!
    @IBOutlet weak var appSettingHeight: NSLayoutConstraint!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var clearCacheView: UIView!
    @IBOutlet weak var personalView: UIView!
    @IBOutlet weak var qvHeight: NSLayoutConstraint!
    @IBOutlet weak var avatarView: UIView!
    @IBOutlet weak var notificationView: UIView!
    @IBOutlet weak var queriesView: UIView!
    @IBOutlet weak var pViewHeight: NSLayoutConstraint!
    @IBOutlet weak var redeemParslView: UIView!
    @IBOutlet weak var senfParslview: UIView!
    @IBOutlet weak var generalQueryView: UIView!
    @IBOutlet weak var refundView: UIView!
    @IBOutlet weak var liveSupportView: UIView!
    @IBOutlet weak var signOutView: UIView!
    
    
    // MARK: - Properties
    var userEmail :String? = ""
    let deviceId = UIDevice.current.identifierForVendor?.uuidString
    
    
    // MARK: - LifeCycles
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationBarUI()
        setUp()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appVersion =  Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String? ?? "x.x"
        let buildNo = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
        self.versionLabel.text = "Version: \(appVersion) (\(buildNo))"
        scrollView.layer.cornerRadius = 15
        scrollView.clipsToBounds = true
        makeViewsTabable()
        createNotifications()
    }
    // MARK: - IBactions
    
    
    // MARK: - Methods
    func navigationBarUI() {
        self.navigationController?.isNavigationBarHidden = false
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "toastBackColor")
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "toastBackColor")
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func createNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(takeToAvatarScreen(_:)), name: Notification.Name(rawValue: "MyAvatarLoginCompleted"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(joinNowToSignOut(_:)), name: Notification.Name(rawValue: "joinNowLoginCompleted"), object: nil)
    }
    func makeViewsTabable () {
        let tapPersonal = UITapGestureRecognizer(target: self, action: #selector(self.showHidePersonalView(_:)))
        personalView.addGestureRecognizer(tapPersonal)
        let tapQuery = UITapGestureRecognizer(target: self, action: #selector(self.showQuesriesView(_:)))
        queriesView.addGestureRecognizer(tapQuery)
        let tapSetting = UITapGestureRecognizer(target: self, action: #selector(self.showAppSetting(_:)))
        appSettingView.addGestureRecognizer(tapSetting)
        let myAvatar = UITapGestureRecognizer(target: self, action: #selector(self.myAvatarPressed(_:)))
        avatarView.addGestureRecognizer(myAvatar)
        let notificationSummary = UITapGestureRecognizer(target: self, action: #selector(self.notificationSummaryPressed(_:)))
        notificationView.addGestureRecognizer(notificationSummary)
        let refund = UITapGestureRecognizer(target: self, action: #selector(self.refundpressed(_:)))
        refundView.addGestureRecognizer(refund)
        let query = UITapGestureRecognizer(target: self, action: #selector(self.queryPressed(_:)))
        generalQueryView.addGestureRecognizer(query)
        let sendParsl = UITapGestureRecognizer(target: self, action: #selector(self.sendParslPressed(_:)))
        senfParslview.addGestureRecognizer(sendParsl)
        let redeemparsl = UITapGestureRecognizer(target: self, action: #selector(self.redeemparslPressed(_:)))
        redeemParslView.addGestureRecognizer(redeemparsl)
        let liveSupport = UITapGestureRecognizer(target: self, action: #selector(self.liveSupportPressed(_:)))
        liveSupportView.addGestureRecognizer(liveSupport)
        let clearCache = UITapGestureRecognizer(target: self, action: #selector(self.clearCachePressed(_:)))
        clearCacheView.addGestureRecognizer(clearCache)
        let update = UITapGestureRecognizer(target: self, action: #selector(self.updatePressed(_:)))
        checkUpdateView.addGestureRecognizer(update)
        let changePassword = UITapGestureRecognizer(target: self, action: #selector(self.changePasswordPressed(_:)))
        changePasswordView.addGestureRecognizer(changePassword)
        let signOut = UITapGestureRecognizer(target: self, action: #selector(self.signOutPressed(_:)))
        signOutView.addGestureRecognizer(signOut)
        let legalTap = UITapGestureRecognizer(target: self, action: #selector(self.legalViewPressed(_:)))
        legalView.addGestureRecognizer(legalTap)
        let privacyTap = UITapGestureRecognizer(target: self, action: #selector(self.privacyTapPressed(_:)))
        privacyView.addGestureRecognizer(privacyTap)
        let tcTap = UITapGestureRecognizer(target: self, action: #selector(self.termsAndConditionsPressed(_:)))
        termsAndConditions.addGestureRecognizer(tcTap)
        let myParslTap = UITapGestureRecognizer(target: self, action: #selector(self.myParslsPressed(_:)))
        myParslsView.addGestureRecognizer(myParslTap)
        let createModel = UITapGestureRecognizer(target: self, action: #selector(self.createModelPressed(_:)))
        createModelView.addGestureRecognizer(createModel)
    }
    
    func signOut() {
        if ParslUtils.sharedInstance.getIfuserLogin() == true {
        ParslUtils.sharedInstance.saveUserNameSpace("")
        ParslUtils.sharedInstance.saveUserEmail("")
        ParslUtils.sharedInstance.saveIfLoginScreenShown(false)
        ParslUtils.sharedInstance.saveIfUserLogin(false)
        ParslUtils.sharedInstance.saveUserNumber("")
        
        self.registeredEmail.text = ""
            signOutLabel.text = constantsStrings.signOutSignIn
        } else { 
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
           vc.fromSendOrRedeem = "joinNow"
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func setUp() {
        UNUserNotificationCenter.current().getDeliveredNotifications { (notifications) in
                    DispatchQueue.main.async { [self] in
                        if notifications.count > 0 {
                            notificationsCountLbl.isHidden = false
                            notificationsCountLbl.text = String(notifications.count)
                        }else {
                            notificationsCountLbl.isHidden = true
                            notificationsCountLbl.text = ""
                        }
                    }
                }
        
        pViewHeight.constant = 40
        qvHeight.constant = 40
        appSettingHeight.constant = 40
        legalHeight.constant = 40
        privacyView.isHidden = true
        avatarView.isHidden = true
//        notificationView.isHidden = true
        generalQueryView.isHidden = true
        refundView.isHidden = true
        senfParslview.isHidden = true
        redeemParslView.isHidden = true
        clearCacheView.isHidden = true
        checkUpdateView.isHidden = true
        liveSupportView.isHidden = true
        termsAndConditions.isHidden = true
        myParslsView.isHidden = true
        
        if ParslUtils.sharedInstance.getIfuserLogin() == true {
            self.registeredEmail.text = ParslUtils.sharedInstance.getuserEmail()
            signOutLabel.text = constantsStrings.signOut
        }
        else {
            self.registeredEmail.text = ""
            signOutLabel.text = constantsStrings.signOutSignIn
        }
    }
    
    @objc func privacyTapPressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
            if let url = constantsUrl.pricacyPolicyUrl {
            UIApplication.shared.open(url)
        }
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    
    @objc func termsAndConditionsPressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
            if let url = constantsUrl.termsConditionsUrl {
            UIApplication.shared.open(url)
        }
        }
    }
    
    
    @objc func myParslsPressed(_ sender: UITapGestureRecognizer? = nil) {
        if ParslUtils.sharedInstance.getIfuserLogin() == true {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyParslsViewController")as! MyParslsViewController
        self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @objc func createModelPressed(_ sender: UITapGestureRecognizer? = nil) {
        @StateObject var model = CameraViewModel()
        if ParslUtils.sharedInstance.getifImagesUPloaded() == false {
            
            let alertController = UIAlertController(title: constantsStrings.appName, message: constantsStrings.folderNamelabel, preferredStyle: UIAlertController.Style.alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = constantsStrings.enterFolderName
                }
            let saveAction = UIAlertAction(title: constantsStrings.saveTitle, style: UIAlertAction.Style.default, handler: { alert -> Void in
                    let firstTextField = alertController.textFields![0] as UITextField
                print(firstTextField.text)
                ParslUtils.sharedInstance.saveFolderName(firstTextField.text ?? "")
                
                let swiftUIView = ContentView(model: model)
                    let hostingController = UIHostingController(rootView: swiftUIView)
        //        navigationController?.pushViewController(hostingController, animated: false)
                hostingController.modalPresentationStyle = .fullScreen
                self.present(hostingController, animated: true)
                
                })
            let cancelAction = UIAlertAction(title: constantsStrings.cancelLabel, style: UIAlertAction.Style.default, handler: {
                    (action : UIAlertAction!) -> Void in })
                alertController.addAction(saveAction)
                alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            
        } else {
            showAlert(withMessage: constantsStrings.imagesUPloading)
        }
       
    }
    
    
    
    @objc func signOutPressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
        signOut()
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
   
    @objc func legalViewPressed(_ sender: UITapGestureRecognizer? = nil) {
        if legalHeight.constant == 40 {
            legalHeight.constant = 120
            privacyView.isHidden = false
            termsAndConditions.isHidden = false
        } else {
            privacyView.isHidden = true
            termsAndConditions.isHidden = true
            legalHeight.constant = 40
        }
    }
    
    @objc func changePasswordPressed(_ sender: UITapGestureRecognizer? = nil) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "resetVC")as! resetVC
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func myAvatarPressed(_ sender: UITapGestureRecognizer? = nil) {
        if ParslUtils.sharedInstance.getIfuserLogin() == true{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyAvatarsViewController")as! MyAvatarsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
            vc.fromSendOrRedeem = "avatar"
            self.present(vc, animated: true, completion: nil)
        }

    }
    @objc func notificationSummaryPressed(_ sender: UITapGestureRecognizer? = nil) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationsSummryViewController") as! NotificationsSummryViewController
        self.navigationController?.pushViewController(vc, animated: true) 
        
    }
    @objc func refundpressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
            let email =  constantsUrl.refundEmailUrl
                            if let url = URL(string: "mailto:\(email)") {
                              if #available(iOS 10.0, *) {
                                UIApplication.shared.open(url)
                              } else {
                                UIApplication.shared.openURL(url)
                              }
                            }
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    @objc func queryPressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
            let email = constantsUrl.supportUrl
                           if let url = URL(string: "mailto:\(email)") {
                             if #available(iOS 10.0, *) {
                               UIApplication.shared.open(url)
                             } else {
                               UIApplication.shared.openURL(url)
                             }
                           }
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    @objc func sendParslPressed(_ sender: UITapGestureRecognizer? = nil) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                           vc.videoPath = "send_parsl"
                           self.navigationController!.present(vc, animated: true, completion: nil)
    }
    @objc func redeemparslPressed(_ sender: UITapGestureRecognizer? = nil) {
         let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VideoPlayerVC") as! VideoPlayerVC
                           vc.videoPath = "redeem_parsl"
                           self.navigationController!.present(vc, animated: true, completion: nil)

    }
    @objc func liveSupportPressed(_ sender: UITapGestureRecognizer? = nil) {
        self.userEmail = UserDefaults.standard.value(forKey: "userEmail") as! String? ?? ""
                                print("email is",self.userEmail)
                                if self.userEmail == nil  || self.userEmail == "" {
        
                                    let alertController = UIAlertController(title: constantsStrings.liveSupportTitle, message: constantsStrings.enterEmailtoTalkAgent, preferredStyle: .alert)
        
                                    alertController.addTextField { (textField : UITextField!) -> Void in
                                        textField.placeholder = constantsStrings.userEmail
                                    }
        
        
        
                                    let cancelAction = UIAlertAction(title: constantsStrings.cancelLabel, style: .default, handler: nil )
        
                                    alertController.addAction(cancelAction)
        
                                    let saveAction = UIAlertAction(title: constantsStrings.saveTitle, style: .default, handler: { alert -> Void in
                                        let firstTextField = alertController.textFields![0] as UITextField
                                        
                                        
                                        if !self.isValidEmail(firstTextField.text!) {
                                            self.view.makeToast(constantsStrings.enterValidEmail)
                                            return
                                        }else{
                                            UserDefaults.standard.set(firstTextField.text, forKey: "userEmail")
                                            self.userEmail = firstTextField.text
                                            if Reachability.isConnectedToNetwork() {
                                            self.openConversationWindow()
                                            } else {
                                                ProgressHUD.dismiss()
                                                self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
                                            }
                                        }
        
                                    })
                                    alertController.addAction(saveAction)
        
                                    self.present(alertController, animated: true, completion: nil)
                                } else {
                                    if Reachability.isConnectedToNetwork() {
                                    self.openConversationWindow()
                                    } else {
                                        ProgressHUD.dismiss()
                                        self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
                                    }
                                }
    }
    @objc func clearCachePressed(_ sender: UITapGestureRecognizer? = nil) {
        
        var style = ToastStyle()
        self.view.makeToast(constantsStrings.cacheCleared, duration: 3.0, position: .center, style: style)
        
        
    }
    @objc func updatePressed(_ sender: UITapGestureRecognizer? = nil) {
        if Reachability.isConnectedToNetwork() {
            guard let url = constantsUrl.checkUpdatedUrl else {
          return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }

    }
    
    @objc func showHidePersonalView(_ sender: UITapGestureRecognizer? = nil) {
        if ParslUtils.sharedInstance.getIfuserLogin() == true {
        if pViewHeight.constant == 200{
        avatarView.isHidden = true
            myParslsView.isHidden = true
//        notificationView.isHidden = true
        pViewHeight.constant = 40
        } else {
            avatarView.isHidden = false
            myParslsView.isHidden = false
//            notificationView.isHidden = false
            pViewHeight.constant = 200
        }
        } else {
            if pViewHeight.constant == 160{
            avatarView.isHidden = true
//            notificationView.isHidden = true
                myParslsView.isHidden = true
            pViewHeight.constant = 40
            } else {
                avatarView.isHidden = false
                myParslsView.isHidden = false
//                notificationView.isHidden = false
                pViewHeight.constant = 160
            }
        }
        
    }
    @objc func showQuesriesView(_ sender: UITapGestureRecognizer? = nil) {
        if qvHeight.constant == 200{
            generalQueryView.isHidden = true
            refundView.isHidden = true
            senfParslview.isHidden = true
            redeemParslView.isHidden = true
            liveSupportView.isHidden = true
            qvHeight.constant = 40
        } else {
            generalQueryView.isHidden = false
            refundView.isHidden = false
            senfParslview.isHidden = false
            redeemParslView.isHidden = false
            liveSupportView.isHidden = true
            qvHeight.constant = 200
        }
    }
    @objc func showAppSetting(_ sender: UITapGestureRecognizer? = nil) {
        if appSettingHeight.constant == 120{
            clearCacheView.isHidden = true
            checkUpdateView.isHidden = true
            appSettingHeight.constant = 40
        } else {
            appSettingHeight.constant = 120
            clearCacheView.isHidden = false
            checkUpdateView.isHidden = false
        }
    }
    @objc func takeToAvatarScreen (_ notification: NSNotification){
        if ParslUtils.sharedInstance.getIfuserLogin() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyAvatarsViewController")as! MyAvatarsViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        }
    }
    
    @objc func joinNowToSignOut (_ notification: NSNotification){
        
        self.signOutLabel.text = constantsStrings.signOut
        self.registeredEmail.text = ParslUtils.sharedInstance.getuserEmail()
    }
    func getAppVersionApi() {
        let url = constantsUrl.checkVersionUrl
        ProgressHUD.show()
        if Reachability.isConnectedToNetwork() {
        postRequest(serviceName: url, sendData: AppVersionCheck.sharedInstance.getCheckVersionApiParams(), success: { (response) in
            print(response)
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: response, options: .mutableContainers) as! [String: Any]
                
                let message = (jsonResponse["message"] ?? "") as! String
                let status = (jsonResponse["status"] ?? true) as!Bool
                if status == false {
                    if message.contains("App does not exisit") {
                        DispatchQueue.main.async {
                            ProgressHUD.dismiss()
                            let alert = UIAlertController(title: constantsStrings.appName, message: constantsStrings.appNotExist, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: constantsStrings.okLabel, style: .default) {
                                    UIAlertAction in
                                self.dismiss(animated: true, completion: nil)
                                }
                            alert.addAction(okAction)
                            self.present(alert, animated: true)
                        }
                    }else if message.contains("Invalid app type") {
                        DispatchQueue.main.async {
                            ProgressHUD.dismiss()
                            let alert = UIAlertController(title: constantsStrings.appName, message: constantsStrings.invalidType, preferredStyle: .alert)
                            let okAction = UIAlertAction(title: constantsStrings.okLabel, style: .default) {
                                    UIAlertAction in
                                self.dismiss(animated: true, completion: nil)
                                }
                            alert.addAction(okAction)
                            self.present(alert, animated: true)
                        }
                       
                    }
                    else{
                        DispatchQueue.main.async {
                            self.showAlertViewForVersionCheckAPI()
                        }
                    }
                }
            } catch let error {
                print(error)
                ProgressHUD.dismiss()
            }
            
        }, failure: { (data) in
            ProgressHUD.dismiss()
             
        })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
    
    func showAlertViewForVersionCheckAPI() {
        let optionMenu = UIAlertController(title: constantsStrings.appName, message: constantsStrings.newVersionAvailable, preferredStyle: .alert)

        let update = UIAlertAction(title: constantsStrings.updateLabel, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.openStoreProductWithiTunesItemIdentifier(constantsStrings.parslItunesIdentifier)   //Parsl identifier
        })

        let cancel = UIAlertAction(title: constantsStrings.cancelLabel, style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            self.navigationController?.dismiss(animated: true, completion: nil)
        })
        
        optionMenu.addAction(update)
        optionMenu.addAction(cancel)
        self.navigationController?.present(optionMenu, animated: true, completion: nil)
    }
    
    private func openStoreProductWithiTunesItemIdentifier(_ identifier: String) {
        let storeViewController = SKStoreProductViewController()
        storeViewController.delegate = self

        let parameters = [ SKStoreProductParameterITunesItemIdentifier : identifier]
        storeViewController.loadProduct(withParameters: parameters) { [weak self] (loaded, error) -> Void in
            if loaded {
                // Parent class of self is UIViewContorller
                self?.navigationController?.present(storeViewController, animated: true, completion: nil)
            }
        }
    }
    
 
    
    func saveFirebaseid(_ firebaseId: String) {
            UserDefaults.standard.set(firebaseId, forKey: "firebaseId")
            UserDefaults.standard.synchronize()
    }
    
    func openConversationWindow() {
        //check if email exists
        
          
          let sentDate = Date()
          var userName:String
          var email:String
          var phone:String?
          var namespace:String

              phone = ""
              userName = userEmail!
              email = userEmail!
              namespace = deviceId!
          
          var device_info : Dictionary <String, String> = [:]
  //        var receiver_info : Dictionary <String, String> = [:]

           device_info = [
              "device_manufacturar": "Apple" as String,
            "device_name":UIDevice.current.name as String,
            "device_os":UIDevice.current.systemName as String,
              "device_type": "iPhone" as String,
            "device_version":UIDevice.current.systemVersion as String
          ]

          var data: Dictionary<String, Any>

              data =
                  ["creator" :deviceId! as String,
                  "creator_name": userEmail! as String,
                  "phone":phone as Any,
                  "creator_email":email as String,
                  "creator_namespace":namespace as String,
                  "creater_device_info":device_info as Any,
                  "app_id":"parsl",
                  "creator_device_token":deviceToken as Any,
                  "msg_timestamp":  Int64((sentDate.timeIntervalSince1970).rounded())]
          

          print(data)
        if Reachability.isConnectedToNetwork() {
            postChatRequest(serviceName: constantsUrl.startChatUrl, sendData: data as [String : AnyObject], success: { (response) in
              print(response)
              let defaults = UserDefaults.standard
              let statusCode = defaults.string(forKey: "statusCode")

              if ((statusCode?.range(of: "200")) != nil){
                  DispatchQueue.main.async {
                     // self.showAlert("Success")
                      let status = response["status"] as! Bool
                      if status{
                          let data = response["data"] as! NSDictionary
                          let firebaseId = data["firebase_id"] as! String
                          print("firebase id is ......\(firebaseId)")
                          self.saveFirebaseid(firebaseId)
                          
                          let channel = Channel(id: firebaseId)
                          let vc = ChatViewController(channel: channel)
                          self.navigationController?.pushViewController(vc, animated: true)
                      }
                      else{
                          print(response["message"] as! String)
                      }
                      
                  }

              }
              
              DispatchQueue.main.async {
                  
              }

          }, failure: { (data) in
              print(data)
              
              DispatchQueue.main.async {
                  self.view.makeToast(constantsStrings.unableChat)
                  
              }
          })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }

        }

}


