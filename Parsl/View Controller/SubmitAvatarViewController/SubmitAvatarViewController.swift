//
//  SubmitAvatarViewController.swift
//  Parsl
//
//  Created by M Zaryab on 28/12/2021.
// This class helps the user to submit and eidt a new avatar

import UIKit
import Toast_Swift

class SubmitAvatarViewController: BaseViewController {
    
    // MARK: - Iboutlets
    @IBOutlet weak var mainview: UIView!
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var descriptionTV: UITextView!
    
    // MARK: - Properties
    var avatarUrl = ""
    var isfrom = ""
    var avatarId = ""
    var style = ToastStyle()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()   
    }
    // MARK: - Methods

    func setUI (){
        titleField.layer.cornerRadius = 5
        titleField.clipsToBounds = true
        descriptionTV.layer.cornerRadius = 5
        descriptionTV.clipsToBounds = true
        titleField.layer.borderColor = UIColor(named: "toastBackColor")?.cgColor
        descriptionTV.layer.borderColor = UIColor(named: "toastBackColor")?.cgColor
        titleField.layer.borderWidth = 1
        descriptionTV.layer.borderWidth = 1
        submitButton.layer.cornerRadius = submitButton.frame.size.height/2
        submitButton.setTitle(constantsStrings.submitButtonTitle, for: .normal)
        submitButton.backgroundColor = UIColor(named: "toastBackColor")
        titleField.delegate = self
        descriptionTV.delegate = self
        descriptionTV.text = constantsStrings.descriptionTextLabel
        descriptionTV.textColor = UIColor.lightGray
        mainview.layer.cornerRadius = 7
    }
    // MARK: - IBActions
    @IBAction func submit(_ sender: Any) {
        
        if isfrom == "edit" {
            self.editAvatar()
        }
        else {
            self.submitAvatar()
        }
        
    }
    
    func editAvatar() {
        ProgressHUD.show()
        let url = constantsUrl.editAvatarUrl
        var description = ""
        if descriptionTV.text == "" || descriptionTV.text == constantsStrings.descriptionTextLabel {
            description = ""
        }
        else {
            description = descriptionTV.text!
        }
        let params = ["namespace": ParslUtils.sharedInstance.getUserNameSpace(), "avatar_id": self.avatarId, "avatar_title": titleField.text!, "avatar_description": description] as [String: AnyObject]
        print(params)
        
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]

                print("...count is \(json)")
                let status = json["status"] as! Bool
                let message = json["message"] as! String
                if status == true {
                    DispatchQueue.main.async {
                        self.view.makeToast(constantsStrings.successfullyEdited, duration: 1.0, position: .bottom, style: self.style)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AvatarUpdated"), object: nil)
                            self.dismiss(animated: true, completion: nil)
                        }
                        ProgressHUD.dismiss()
                    }
                }
                else {
                    ProgressHUD.dismiss()
                    self.showAlert(withMessage: message)
                }
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
            ProgressHUD.dismiss()
        })
    }
    
    func submitAvatar() {
        
        ProgressHUD.show()
        let url = constantsUrl.createAvatarUrl
        var description = ""
        if descriptionTV.text == "" || descriptionTV.text == constantsStrings.descriptionTextLabel {
            description = ""
        }
        else {
            description = descriptionTV.text!
        }
        let params: [String: AnyObject] = ["app_id": "parsl_ios" as AnyObject, "namespace": ParslUtils.sharedInstance.getUserNameSpace() as AnyObject, "title": titleField.text! as AnyObject, "description": description as AnyObject, "url": avatarUrl as AnyObject]
        
        print(params)
        postRequest(serviceName: url, sendData: params, success: { (response) in
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                
                print("...count is \(json)")
                let status = json["status"] as! Bool
                let message = json["message"] as! String
                if status == true {
                    DispatchQueue.main.async {
                        self.view.makeToast(constantsStrings.submitSuccessfully, duration: 1.0, position: .bottom, style: self.style)
                        ProgressHUD.dismiss()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                            if let navigator = self.navigationController {
                                navigator.popViewController(animated: true)
                            }
                        }
                    }
                }
                else {
                    self.showAlert(withMessage: message)
                    ProgressHUD.dismiss()
                }
            } catch {
                ProgressHUD.dismiss()
            }
        }, failure: { (error) in
            print(error)
        })
    }
}
