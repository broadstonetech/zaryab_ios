//
//  SubmitAvatarExtension.swift
//  Parsl
//
//  Created by M Zaryab on 01/03/2022.
//
import UIKit
import Foundation

// MARK: - Extensionss
extension SubmitAvatarViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = 1
        if textField == titleField{
         maxLength = 20
        }
        //else if textField == descriptionField {
          //  maxLength = 40
        //}
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
// limit the textfield for number of characters and set ui on begin editing and stopping
extension SubmitAvatarViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descriptionTV.textColor == UIColor.lightGray {
            descriptionTV.text = nil
            descriptionTV.textColor = UIColor.black
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 50    // 10 Limit Value
    }
}

