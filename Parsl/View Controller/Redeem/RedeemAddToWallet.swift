////
////  RedeemAddToWallet.swift
////  Parsl
////
////  Created by Mufaza on 16/08/2021.
////
//
//import Foundation
//import UIKit
//import PassKit
//import Stripe
//import Alamofire
//
//class ViewController: UIViewController, PKAddPaymentPassViewControllerDelegate, STPIssuingCardEphemeralKeyProvider {
//
//    var pushProvisioningContext: STPPushProvisioningContext? = nil
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        // Do any additional setup after loading the view.
//
//
//    }
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(true)
//        beginPushProvisioning()
//    }
//
//
//    func beginPushProvisioning() {
//        let config = STPPushProvisioningContext.requestConfiguration(
//          withName: "Jenny Rosen", // the cardholder's name
//          description: "RocketRides Card", // optional; a description of your card
//          last4: "4242", // optional; the last 4 digits of the card
//          brand: .visa // optional; the brand of the card
//        )
//        let controller = STPFakeAddPaymentPassViewController(requestConfiguration: config, delegate: self)
//        self.present(controller!, animated: true, completion: nil)
//      }
//
//
//
//    func addPaymentPassViewController(_ controller: PKAddPaymentPassViewController, generateRequestWithCertificateChain certificates: [Data], nonce: Data, nonceSignature: Data, completionHandler handler: @escaping (PKAddPaymentPassRequest) -> Void) {
//
//        self.pushProvisioningContext = STPPushProvisioningContext(keyProvider: self)
//            // STPPushProvisioningContext implements this delegate method for you, by retrieving encrypted card details from the Stripe API.
//            self.pushProvisioningContext?.addPaymentPassViewController(controller, generateRequestWithCertificateChain: certificates, nonce: nonce, nonceSignature: nonceSignature, completionHandler: handler);
//
//    }
//
//    func addPaymentPassViewController(_ controller: PKAddPaymentPassViewController, didFinishAdding pass: PKPaymentPass?, error: Error?) {
//
//        self.dismiss(animated: true, completion: nil)
//
//    }
//
//    var dataRequest: DataRequest?
//
//    func createIssuingCardKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
//
//        let innerParams : [String : Any] = ["parsl" : "016DDE"]
//        let params : [String : Any] = ["data" : innerParams]
//        let url = "https://parsl.io/get/ephemeral/key"
//
//
//
//        dataRequest = Alamofire.Session.default.request(url, method: HTTPMethod.post , parameters: params, encoding: JSONEncoding.default, headers: [:]).response(completionHandler: { (responseData) in
//            guard let data = responseData.data  else{return}
//
////            do {
////
////                        let obj = try JSONSerialization.jsonObject(with: data, options: []) as! [AnyHashable: Any]
////                print(apiVersion)
////                        completion(obj, nil)
////                      } catch {
////                        completion(nil, error)
////                    }
////
//
//            do {
//                let dec = try JSONDecoder().decode(dataextractor.self, from: data)
//                let datakey = dec.data
//                var associatedObj : [[AnyHashable : Any]] = []
//                for obj in datakey.associated_objects{
//                    let innerParam : [AnyHashable : Any] = ["id" : obj.id , "type" : obj.type]
//                    associatedObj.append(innerParam)
//                }
//                let dataCompletion : [AnyHashable : Any] = ["id" : datakey.id , "object" : datakey.object , "created" : datakey.created ,"expires" : datakey.expires , "livemode" : datakey.livemode , "secret" : datakey.secret , "associated_objects" : associatedObj]
//                completion(dataCompletion,nil)
//
//
//            }catch {
//                print("Error")
//            }
//
////            do{
////            let decoder = try JSONDecoder().decode(dataextractor.self, from: data)
////                let idfetched = decoder.data.id
////             //   completion(idfetched, nil)
////                let pram : [AnyHashable : Any] = ["EphermalKey" : idfetched]
////                completion(pram,nil)
////                print(idfetched)
////            }
//
////            catch{
////                print("error")
////            }
//
//        })
//
//    }
//
//}
//
