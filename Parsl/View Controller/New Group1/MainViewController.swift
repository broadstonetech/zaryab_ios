//
//  ViewController.swift
//  Parsl
//
//  Created by Billal on 19/02/2021.
// This class enable the user to select one more multiple PARSLs for sending purpose. In this class user can also redeem PARSls by using PARSL otp. User can project different models and experience real time.
import Foundation
import UIKit
import SDWebImage
import AVFoundation
import AVKit
import MediaPlayer
import AudioToolbox
import ARKit
import SSZipArchive
import Stripe
import Alamofire
import Toast_Swift
import JJFloatingActionButton
import StoreKit
import FirebaseAuth
import FirebaseFirestore
import UnityFramework
import Lottie

var selectedCategoryDataList = [SubCategoryData]()
var giftModels: GiftModel!
var isOrderFlowCompleted = false

class MainViewController: BaseViewController, UITextFieldDelegate,AVPlayerViewControllerDelegate,SendDataDelegate,NativeCallsProtocol {
    
    // MARK: - Properties
    var prices:Double?
    var productIDs:String?
    var completeList : SubCategoryData?
    var isMessageShowing = false
    var isMenuShowing = false
    var downloadCount = 0
    var heightCount = 0
    var videoRepeatCount = 0
    var messageRepeatCount = 0
    var messageAR:String        = ""
    var quantity                = [Int]()
    var mainCategoryList:         MainCategory!
    var subCategoryList:          SubCategoryModel!
    var pricesList              = [Double]()
    var subCategoryArray        = [SubCategoryData]()
    var selectedItemsDataList   = [SubCategoryData]()
    var getParsl:                 GetParsl!
    var getParslList            = [ParslModels]()
    var getModelData:             ParslGiftsModel!
    var cartItemsArray          = [String]()
    var selectedIndex           = 0
    var cartCount :Int          = 0
    var searching               = false
    var isRedeem                = false
    var videoUrl:String         = ""
    var isFashionSelected = false
    var isfromMain = true
    var isInternetAvailable = true
    // var used for model projections
    var playVideo: SCNPlane?
    var videoNode: SCNNode?
    var modelUrl = ""
    var format:                 String?
    var downloadedModelURL:   URL?
    var isMultiNode:          Int?
    var textueresDownloadedCount = 0
    var destPathforTextures: String?
    var isAlltextureDownloaded: Bool = false
    var pricesListofCart = [Double]()
    var URLOTP:String = ""
    var isFirstTime:Bool = true
    var nameofSender = " "
    var isRedeemHalfViewShow = false
    var isAvatarDownloaded = false
    var isAvatarProjected = false
    var isShadowOff = true
    var isHorizontal = true
    
    var modelID:String?
    var selectedModelForDownload  = [String]()
    //add data to cart
    let configuration = ARWorldTrackingConfiguration()
    var dataList = [SubCategoryData]()
    var fileURL,modelTitle: String?
    let deviceId = UIDevice.current.identifierForVendor?.uuidString
    let actionButton = JJFloatingActionButton()
    
    var isVideoCompleted = false
    var isModelDownloaded = false
    var isModelProjected = false
    var isUnitysetup = false
    
    var vendersList: GetVendorsList!
    var vendors = [VendorsList]()
    
    var vendorSelectedIndex = 0
    var selectedModelIndex = 0
    var modelDataSturctArray = [ModelDataStruct]()
    var parslRedeemModel: RedeemModelStruct!
    
    var modelsInCart = [ModelDataStruct]()
    var modelsInView = [String : ModelDataStruct]()
    var downloadedModelsForRedeem = [ModelsDownloadedForRedeemStruct]()
    
    var searchResult = [ModelDataStruct]()
    var isSearching = false
    var selectedModelIndexForRedeem = -1
    var selectedFilter = ""
    var data  = [String:Any]()
    var downloadedData = [DownloadedDataForCatAndVendors]()
    var downloadedVendors = [DownloadedDataForVendors]()
    var modelsDownloadedForSend = [ModelsDownloadedForSendStruct]()
    var isModelAvailableInSapce = false
    var isGiftModelDownloaded = false
    var isArVideoPlayed = false
    var isRedeemMessage = false
    var isVideoMessage = false
    var redeemMessage = ""
    var isSendPressed = false
    var isSearchBarShow = false
    var isfrom = ""
    var forAvatar = ""
    var animatingCardView = false
    var tappedItem =  ModelDataStruct()
    var virtualCardModel: VirtualCard!
    var isSubcategoriesAPICalled = DownloadStatus.Downloading
    var userEmail :String? = ""
    var channelListener: ListenerRegistration?
    var documentID : String? = nil
    private let db = Firestore.firestore()
    var playerLayer: AVPlayerLayer?
    var unityView: UIView?
    var player: AVPlayer?
    var documentQueryObject: QueryDocumentSnapshot?
    private var messageReference: CollectionReference?
    let skipButton = UIButton(type: .system)
    private var channelReference: CollectionReference {
        return db.collection("channels")
    }
    
     //MARK: - IBOutlets
     //  For SceneKit
    @IBOutlet weak var subcategoriesHeight: NSLayoutConstraint!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    @IBOutlet weak var floatButtonView: UIView!
    @IBOutlet weak var redeemViewWalletBtn: UIButton!
    @IBOutlet weak var shadowBtn: UIButton!
    @IBOutlet weak var doneMainPView: UIButton!
    @IBOutlet weak var reloadBtn: UIButton!
    @IBOutlet weak var showMessageButton        : UIButton!
    @IBOutlet weak var leftButtonsView          : UIView!
    @IBOutlet weak var rightButtonsView         : UIView!
    @IBOutlet weak var senderTitle: UILabel!
    @IBOutlet weak var redeemVideoView: UIView!
    @IBOutlet weak var mainPresentedView: UIView!
    @IBOutlet weak var addToButton: RoundButton!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var presentedModelImage: UIImageView!
    @IBOutlet weak var presentedTextView: UITextView!
    
    //  For Basic View
    @IBOutlet weak var mainBottomView: UIView!
    @IBOutlet weak var MainCategoryStack:     UIStackView!
    @IBOutlet weak var cartMainView:         UIView!
    @IBOutlet weak var cartCountLabel:       UILabel!
    @IBOutlet weak var RedeemHalfView: UIView!
    
    //collection viewss
    @IBOutlet weak var mainCategoryCollectionView:      UICollectionView!
    @IBOutlet weak var subCategoryCollectionView:       UICollectionView!
    @IBOutlet weak var vendorsCollectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainCategoryView:        UIView!
    @IBOutlet weak var searchBar:               UISearchBar!
    @IBOutlet weak var subCategoryView:         UIView!
    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var redeemAnimatingView: UIView!
    @IBOutlet weak var videoView: VideoView!
    
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var collectionViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var modelsBgView: UIView!
    
    @IBOutlet weak var vendorDetailDialog: VendorsDetailsDialog!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    @IBOutlet weak var addToWalletBtn: UIButton!
    
    @IBOutlet weak var showRedeemHalfViewBtn: UIButton!
    @IBOutlet weak var redeemViewDone: UIButton!
    @IBOutlet weak var HVBtn: UIButton!
    @IBOutlet weak var BackgroundView: AnimationView!
    @IBOutlet weak var firstCardTextView: UITextView!
    @IBOutlet weak var secondCardTextView: UITextView!
    @IBOutlet weak var firstCardView: UIView!
    @IBOutlet weak var secondCardView: UIView!
    @IBOutlet weak var RedeemAnimatingBackgroundView: UIView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var animatingHeight: NSLayoutConstraint!
    @IBOutlet weak var animatingWidth: NSLayoutConstraint!
     
    //MARK: - View Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if ParslUtils.sharedInstance.parslOtp != "" {
            let otp = ParslUtils.sharedInstance.parslOtp ?? ""
            print(otp)
            sendData("", "", otp)
            ParslUtils.sharedInstance.parslOtp = ""
        } else {
            
        }
        
        if notificationNav.goToNotificationScreen == true {
            let vc = storyboard?.instantiateViewController(withIdentifier: "NotificationsSummryViewController") as! NotificationsSummryViewController
            navigationController?.pushViewController(vc, animated: true)
        } else {
            
        }
        
        if isfrom == "MyAvatar" {
            navigationController?.isNavigationBarHidden = false
            self.navigationItem.setHidesBackButton(false, animated: true)
            self.navigationController?.navigationBar.barTintColor = UIColor.green
        }
        else {
        navigationController?.isNavigationBarHidden = true
        self.navigationItem.setHidesBackButton(false, animated: true)
        self.navigationController?.navigationBar.barTintColor = UIColor.green
        addToButton.layer.cornerRadius = 5
        hideViews()
        self.videoView.isHidden = true
        
        if isVideoCompleted == false {
            skipButton.frame = CGRect(x: view.frame.size.width/1.2, y: 60, width: 50, height: 25)
            skipButton.backgroundColor = UIColor(named: "ColorForSkipBtn")
            skipButton.layer.cornerRadius = 5
            skipButton.clipsToBounds = true
            skipButton.setTitle(constantsStrings.skipLabel, for: .normal)
            skipButton.titleLabel!.font = UIFont.systemFont(ofSize: 18)
            
            skipButton.setTitleColor(UIColor.white, for: .normal)
            skipButton.addTarget(self, action: #selector(SkipVideo(_:)), for: .touchUpInside)
            if isfrom != "MyAvatars"{
            view.addSubview(skipButton)
            }
        }
        if self.modelsInCart.count != 0 {
            
        }
        else {
            cartCountLabel.text = " "
        }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isVideoCompleted {
           // sceneView.session.pause()
        }
        NotificationCenter.default.removeObserver(self)
        if !isRedeem{
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  NSClassFromString("FrameworkLibAPI")?.registerAPIforNativeCalls(self)
        FrameworkLibAPI.registerAPIforNativeCalls(self)
        if isfrom == "MyAvatars" {
            self.setUpUnityView()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                var id = self.forAvatar + "," + "true"
                UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "Activate", message: id)
            }
        } else {
        getGuestDeviceDetail()
        self.redeemViewWalletBtn.layer.cornerRadius = 10
        self.redeemViewWalletBtn.clipsToBounds = true
        self.cartMainView.layer.cornerRadius = 5
        self.cartMainView.clipsToBounds = true
        self.redeemAnimatingView.circularView()
        self.redeemAnimatingView.clipsToBounds = true
        searchBar.delegate = self
        searchBar.isHidden = true
        self.doneMainPView.layer.cornerRadius = doneMainPView.frame.size.height/2
        self.doneMainPView.backgroundColor = UIColor(named: "dark-custom-blue")
        self.doneMainPView.clipsToBounds = true
            self.doneMainPView.setTitle(constantsStrings.okLabel, for: .normal)
        self.doneMainPView.setTitleColor(UIColor.white, for: .normal)
        self.shadowBtn.setTitle(" ", for: .normal)
        self.shadowBtn.setImage(UIImage(named: "shadow off"), for: .normal)
        self.shadowBtn.setTitleColor(UIColor.white, for: .normal)
        self.HVBtn.setTitle(" ", for: .normal)
        self.HVBtn.setImage(UIImage(named: "horizontal"), for: .normal)
        self.HVBtn.setTitleColor(UIColor.white, for: .normal)
        getAppVersionApi()
        self.senderTitle.text =  "Greetings from \(nameofSender)"
        if traitCollection.userInterfaceStyle == .dark {
           debugPrint("dark mode enabled")
        } else {
            debugPrint("light mode enabled")
        }
        
        self.reloadBtn.setTitle("", for: .normal)
        self.tableView.tableFooterView = UIView()
        
        
            self.navigationItem.title = constantsStrings.appName
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.barTintColor = UIColor.systemGray6
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
       
        
        mainCategoryCollectionView.isUserInteractionEnabled = true
        mainCategoryCollectionView.allowsMultipleSelection              = true
        if #available(iOS 14.0, *) {
            mainCategoryCollectionView.allowsMultipleSelectionDuringEditing = true
        } else {
            // Fallback on earlier versions
        }
        
        mainCategoryCollectionView.delegate     = self
        mainCategoryCollectionView.dataSource   = self
        
        vendorsCollectionView.delegate = self
        vendorsCollectionView.dataSource = self
        vendorsCollectionView.contentInsetAdjustmentBehavior = .never
        
        subCategoryCollectionView.dataSource = self
        subCategoryCollectionView.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = true
         
        hideKeyboardTappedAround()
        
        DispatchQueue.main.async {
            self.getGiftModels()
        }
    
        NotificationCenter.default.addObserver(self, selector: #selector(setPlayerLayerToNil), name: UIApplication.didEnterBackgroundNotification, object: nil)
//
//          // foreground event
          NotificationCenter.default.addObserver(self, selector: #selector(reinitializePlayerLayer), name: UIApplication.willEnterForegroundNotification, object: nil)
//
//         // add these 2 notifications to prevent freeze on long Home button press and back
          NotificationCenter.default.addObserver(self, selector: #selector(setPlayerLayerToNil), name: UIApplication.willResignActiveNotification, object: nil)
//
          NotificationCenter.default.addObserver(self, selector: #selector(reinitializePlayerLayer), name: UIApplication.didBecomeActiveNotification, object: nil)
             
        prepareVideoViewAndLoadData()
         
        let blurViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(vendorDialogCloseBtnPressed))
        blurView.isUserInteractionEnabled = true
        blurView.addGestureRecognizer(blurViewTapGesture)
        
        vendorDetailDialog.cancelBtn.addTarget(self, action: #selector(vendorDialogCloseBtnPressed), for: .touchUpInside)
        
        vendorDetailDialog.visitStore.addTarget(self, action: #selector(visitStoreBtnPressed), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(showRedeemView(_:)), name: Notification.Name(rawValue: "ShowRedeemView"), object: nil)
        }
    }
     // set
    @objc func SkipVideo(_ sender:UIButton!) {
           DispatchQueue.main.async {
               self.playerLayer?.player = nil
               self.playerLayer?.removeFromSuperlayer()
               self.skipButton.removeFromSuperview()
           }
        if !isUnitysetup{
            isUnitysetup = true
            self.setUpUnityView()
            isVideoCompleted = true
        }
       }
    
    func hideViews() {
        messageView.isHidden            = (messageView.isHidden == true)
        MainCategoryStack.isHidden      = (MainCategoryStack.isHidden == true)
        cartMainView.isHidden           = (cartMainView.isHidden == true)
        showMessageButton.isHidden      = true
        mainPresentedView.isHidden      = true
        leftButtonsView.isHidden        = true
        rightButtonsView.isHidden       = true
        subCategoryView.isHidden        = (subCategoryView.isHidden == true)
        modelsBgView.isHidden = true
        sortButton.isHidden = true
    }
    
    private func prepareVideoViewAndLoadData() {
        isVideoCompleted = false
        videoView.isHidden = false
        
        guard let videoPath = Bundle.main.path(forResource: "parsl_video", ofType:"mp4") else {
           debugPrint("error return")
            return
        }
        let url = NSURL(fileURLWithPath: videoPath) as URL
        playVideo(url: url.absoluteString)

    }
    
    func playVideo(url: String){
        if let videoURL = URL(string: url) {
            player = AVPlayer(url: videoURL)
            playerLayer = AVPlayerLayer(player: player)
            playerLayer!.frame = self.view.bounds
            playerLayer!.videoGravity = .resizeAspectFill
            self.view.layer.addSublayer(playerLayer!)
            
            player!.play()
            
            NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoView.player?.currentItem)
        }
    }
    
    @objc func reachTheEndOfTheVideo(_ notification: Notification) {
        DispatchQueue.main.async {
            self.playerLayer?.player = nil
            self.playerLayer?.removeFromSuperlayer()
            self.skipButton.removeFromSuperview()
//            self.playerLayer?.isHidden = true
        }
        isVideoCompleted = true
        videoView.isHidden = true
        if !isUnitysetup{
            isUnitysetup = true
            self.setUpUnityView()

        }
    }
    @objc fileprivate func setPlayerLayerToNil() {
        // first pause the player before setting the playerLayer to nil. The pause works similar to a stop button
        player?.pause()

    }
//     // foreground event
    @objc fileprivate func reinitializePlayerLayer(){

        player?.play()
    }
    
    func modelDownloadingDone(_ message: String!) {
        
        //only when avatar projection has complete
        if isAvatarDownloaded == true{
            self.view.makeToast(constantsStrings.viewAvatar)
            isAvatarDownloaded = false
        }
        
        if isRedeem{
            selectedModelForDownload.append(message)
            tableView.reloadData()
        }
    }
    
    func avatarProjected(_ message: Bool){
        self.redeemAnimatingView.isHidden = false
    }
    
    func modelProjected(_ message: Bool) {
        debugPrint("model is projected from redeem screen")
        print(message)
        
        if isModelProjected == false{
            isModelProjected = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                if self.parslRedeemModel.data.avatarCharacter != "" {
                    self.loadAvatar()
                    self.isAvatarDownloaded = true
                }
                else{
                    self.redeemAnimatingView.isHidden = false
                }
               }
        }
    }
    
    func modelDownloaded(_ message: Bool) {
        print(message)
        debugPrint("projection done in model downloaded")
        if !animatingCardView{
            self.view.makeToast(constantsStrings.viewModel)
        }
        else {
            self.view.makeToast(constantsStrings.viewParsl)
        }
        if isRedeem{
            tableView.reloadData()
        }
    }
    

    func mayBelater() {
        if ParslUtils.sharedInstance.getIfuserLogin() {
                    self.cartMainView.isHidden = false
                    self.MainCategoryStack.isHidden = false
                    self.subCategoryView.isHidden  = false
                    self.subCategoryCollectionView.isHidden = false
                    self.modelsBgView.isHidden = false
                    self.vendorsCollectionView.isHidden = false
                    self.sortButton.isHidden = false
            self.isSendPressed = false
        } else {
            self.isSendPressed = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    func sendData(_ receiverName: String, _ receiverNumber: String,_ URLOTP: String) {
        print(URLOTP)
        self.isSendPressed = false
        DispatchQueue.main.async {
            self.cartMainView.isHidden = true
            self.searchBar.isHidden = true
            self.subcategoriesHeight.constant = 90
            self.actionButton.removeFromSuperview()
            self.view.addSubview(self.actionButton)
            self.actionButton.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
            self.actionButton.bottomAnchor.constraint(equalTo: self.MainCategoryStack.topAnchor, constant: -8).isActive = true
            self.URLOTP = URLOTP
            
            self.redeemGift(otp: URLOTP, name: receiverName, phoneNumber: receiverNumber)
            self.selectedModelForDownload.removeAll()
    }
}
    
    func sendRedeemData(_ receiverName: String, _ receiverNumber: String) {
       
        self.getVirtualGiftCard(otp: self.URLOTP, receiverName: receiverName, phoneNumber: receiverNumber)
        
        
        
    }
    
    func showActionButton(){
        self.actionButton.isHidden = false
    }
    
   
    
    @objc func showRedeemView (_ notification: NSNotification){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.showredeemScreen()
        }
    }
    
    @objc func vendorDialogCloseBtnPressed(){
        blurView.isHidden = true
        vendorDetailDialog.isHidden = true
    }
    
    @objc func visitStoreBtnPressed(){
        
        ParslUtils.sharedInstance.openUrl(url: vendersList.data.vendorsList[vendorSelectedIndex].otherInfo.website)
    }
    
    func addKeyboardListeners() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    //MARK: -Button Actions
    @IBAction func HVBtnPressed(_ sender: Any) {
        self.turnVerticalHorizontal()
        
    }
    @IBAction func reloadUnityScreen(_ sender: Any) {
        let alert = UIAlertController(title: constantsStrings.appName, message: constantsStrings.sureToClear, preferredStyle: .alert)
        let clearScreen = UIAlertAction(title: constantsStrings.clearLabel, style: .default, handler: { action in
            self.reloadUnity()
            
                })

        let cancel = UIAlertAction(title: constantsStrings.cancelLabel, style: .cancel, handler: { action in
            self.dismiss(animated: true, completion: nil)
        })
        alert.addAction(clearScreen)
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
        
        
    }
    @IBAction func sortButtonTapped(_ sender: Any) {
        openBottomActionSheetForDataSorting()
    }
    
    @IBAction func onOffShadow(_ sender: Any) {
        turnModelShadowsOffOn()
    }
    @IBAction func playVideoTapped(_ sender: Any) {
        let url = URL(string: parslRedeemModel.data.receiverData.videoMsg)!
        playServerVideo(url: url)
    }
    
    
    @IBAction func hideMainPresentedView(_ sender: Any) {
        mainPresentedView.isHidden = true
    }
    
    @IBAction func hideRedeemHalfView(_ sender: Any) {
        self.RedeemHalfView.isHidden = true
        self.redeemAnimatingView.isHidden = false
    }
    
    @IBAction func addToCartTapped(_ sender: Any) {
        
        if isItemAddedInCart(tappedItem){
            showAlertWithAction(withTitle: constantsStrings.appName, withMessage: constantsStrings.itemsAlreadyAdded)
        }else{
            modelsInCart.append(tappedItem)
            cartCountLabel.text = String(modelsInCart.count)
            mainPresentedView.isHidden = true
        }
    }
    
    private func isItemAddedInCart(_ selectedItem: ModelDataStruct) -> Bool {
        for item in modelsInCart {
            if item.productId == selectedItem.productId {
                return true
            }
        }
        return false
    }
    
    @IBAction func showMessageTapped(_ sender: Any) {
        messageRepeatCount += 1
    }
    
    @IBAction func cartTapped(_ sender: Any) {
        if modelsInCart.isEmpty {
            showAlert(withTitle:constantsStrings.appName, withMessage: constantsStrings.enterItemsInCart)
        }
        else{
            let vc = storyboard?.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
            isOrderFlowCompleted = true
            vc.quantity = quantity
            vc.selectedItemsArray = cartItemsArray
            vc.selectedItemsDataList = selectedItemsDataList
            vc.pricesList = pricesList
            vc.modelsInCart = modelsInCart
            vc.cartDelegate = self
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func showSearch(_ sender: Any) {
        if searchBar.isHidden == true{
            searchBar.isHidden = false
        }
        else{
            searchBar.isHidden = true
            searchResult.removeAll()
            isSearching = false
            DispatchQueue.main.async {
                self.subCategoryCollectionView.reloadData()
            }
        }
    }
    
    
    @IBAction func mainSearchBtnPressed(_ sender: Any) {
        
    }
    
    //MARK:- Floating Buttons
    func setUpFloatingActionbutton(){
        actionButton.buttonDiameter = 50
        actionButton.buttonImageColor = .black
        actionButton.buttonColor = .white
     
        actionButton.itemAnimationConfiguration = .circularPopUp(withRadius: 140)
 
        actionButton.configureDefaultItem { item in
            item.titlePosition = .bottom
            item.titleSpacing = .zero
            item.buttonColor = .white
            item.buttonImageColor = .black
            item.isSelected = true
        
            item.highlightedButtonColor = UIColor(named: "toastBackColor")
           // item.layer.si
            
        }
        
        actionButton.addItem(title:constantsStrings.MyaccountLabel,image: UIImage(named: "support")?.withRenderingMode(.alwaysTemplate)) { item in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        actionButton.addItem(title: constantsStrings.walletLabel, image: UIImage(named: "card")?.withRenderingMode(.alwaysTemplate)) { item in
            self.searchBar.isHidden = true
            self.actionButton.isHighlighted = true
            self.homeFunctionality()
            let url = constantsUrl.parslWalletUrl
            self.searchBar.isHidden = true
            self.isSendPressed = false
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.open(url!)
            }
            
        }
        actionButton.addItem(title: constantsStrings.redeemLabel, image: UIImage(named: "redeem")?.withRenderingMode(.alwaysTemplate)) { item in
            
            self.searchBar.isHidden = true
            self.showredeemScreen()
            
        }
        
        actionButton.addItem(title: constantsStrings.sendlabel ,image: UIImage(named: "send")?.withRenderingMode(.alwaysTemplate)) { item in
            if self.isSearchBarShow == true {
                self.searchBar.isHidden = false
            }
            if !self.isSendPressed {
                self.animatingCardView = false
                self.isSendPressed = true
                self.redeemAnimatingView.isHidden = true
                self.reloadUnity()
                self.hideViews()
                self.showMessageButton.isHidden = true
                self.cartMainView.isHidden = false
                self.MainCategoryStack.isHidden = false
                self.subCategoryView.isHidden  = false
                self.subCategoryCollectionView.isHidden = false
                if self.isInternetAvailable{
                self.modelsBgView.isHidden = false
                }
                self.vendorsCollectionView.isHidden = false
                self.sortButton.isHidden = false
                self.isRedeem = false
            }
            else{
                debugPrint("unable to press send")
                }
            }
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: MainCategoryStack.topAnchor, constant: -98).isActive = true
    }
    
    
    //MARK: -Functions for Projected Object
    
    func showredeemScreen(){
        self.searchBar.isHidden = true
        self.animatingCardView = true
        self.homeFunctionality()
        self.reloadUnity()
        self.actionButton.isHidden = true
        self.isModelProjected = false
        let vc  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "ReceiverModalViewController") as? ReceiverModalViewController
        vc?.delegate = self
        self.navigationController?.present(vc!, animated: true, completion: nil)
    }
    
    func homeFunctionality(){
        
        self.subCategoryView.isHidden = true
        self.messageView.isHidden = true
        self.MainCategoryStack.isHidden = true
        self.isModelAvailableInSapce = false
        self.cartMainView.isHidden = true
        self.redeemAnimatingView.isHidden = true
        self.isRedeem = false
        self.hideViews()
        
    }
    func addTapGestureToScreenView(){
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.didTap(withGestureRecognizer:)))
        if isRedeemHalfViewShow == true{
        self.unityView!.addGestureRecognizer(tapGestureRecognizer)
            debugPrint("added")
        }
        else{
            self.unityView!.removeGestureRecognizer(tapGestureRecognizer)
            debugPrint("removed......")
        }
        
        
    }
    
    func downloadModel(modelURL : String,modelTitle: String,format:String, modelIndex: Int) {
        
        let url = URL(string: modelURL)
        let modelTitle = modelTitle
        let request = URLRequest(url: url!)
        let session = URLSession.shared
        
        let downloadTask = session.downloadTask(with: request,completionHandler: { (location:URL?, response:URLResponse?, error:Error?) -> Void in
                var statusCode : Int = 100
            if let httpResponse = response as? HTTPURLResponse {
                debugPrint("StatusCode== \(httpResponse.statusCode)")
                let downloadedFile = httpResponse.suggestedFilename ?? url!.lastPathComponent
                debugPrint("filename==\(downloadedFile)")
                statusCode = httpResponse.statusCode
                
                if response != nil  && statusCode == 200 {
                    debugPrint("Response:\(String(describing: response))")
                    
                    // Get the document directory url
                    if location?.path != nil {
                        let locationPath = location!.path
                        if format == ".usdz" {
                            
                            let documentsDestPath:String = NSHomeDirectory() + "/Documents/\(modelTitle).usdz"//.zip
                            let fileManager = FileManager.default
                            if (fileManager.fileExists(atPath: documentsDestPath)){
                                try! fileManager.removeItem(atPath: documentsDestPath)
                            }
                            try! fileManager.moveItem(atPath: locationPath, toPath: documentsDestPath)
                            //get stored file path and url
                            let fileName = "\(modelTitle).usdz" // your image name here .zip
                            let filePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(fileName)"
                            let fileUrl: URL = URL(fileURLWithPath: filePath)
                            let destinationURL = fileUrl
                            print("destinatioURL is == \(destinationURL)")
                            self.modelUrl = String(describing: destinationURL)
                            DispatchQueue.main.async { [self] in
                                    ProgressHUD.dismiss()
                                //self.view.isUserInteractionEnabled = true
                                    if self.isRedeem == false{
                                        self.view.makeToast(constantsStrings.moveyourCamera, duration: 5.0, position: .center, completion: nil)
                                    }
                                    else{
                                        //self.drone.loadModel(url: destinationURL)
                                        let modelDownloaded = ModelsDownloadedForRedeemStruct(_3dModelFile: modelURL, destinationUrl: destinationURL)
                                        self.downloadedModelsForRedeem.append(modelDownloaded)
                                        
                                        self.view.makeToast(constantsStrings.moveyourCamera, duration: 5.0, position: .center, completion: nil)
                                        
//                                        self.addModelInScene(url: destinationURL)
                                        if selectedModelIndexForRedeem == modelIndex{
                                            //self.drone.loadModel(url: destinationURL)
                                            
                                        }
                                    }
                            }
                        }
                        else {
                            ProgressHUD.dismiss()
                            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.modelNotAvailable)
                            //self.view.isUserInteractionEnabled = true
                        }
                    }
                    else {
                        // show network error message and pop vc
                        DispatchQueue.main.async {
                            ProgressHUD.dismiss()
                            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.unableToDownloadModel)
                            //self.view.isUserInteractionEnabled = true
                        }
                    }
                }else {
                    //self.view.isUserInteractionEnabled = true
                    debugPrint("downlaoding failed")
                    
                }
            }
        })
        downloadTask.resume()
    }
    
    func downloadZippedModel(modelURL : String,modelTitle: String,format:String,videoURL:String,messageText:String) {
        ProgressHUD.show()
        let url = URL(string: modelURL)
        let modelTitle = modelTitle
        let request = URLRequest(url: url!)
        let session = URLSession.shared
        
        let downloadTask = session.downloadTask(with: request,completionHandler: { (location:URL?, response:URLResponse?, error:Error?) -> Void in
                var statusCode : Int = 100
            if let httpResponse = response as? HTTPURLResponse {
                debugPrint("StatusCode== \(httpResponse.statusCode)")
                let downloadedFile = httpResponse.suggestedFilename ?? url!.lastPathComponent
                debugPrint("filename==\(downloadedFile)")
                statusCode = httpResponse.statusCode
                
                if response != nil  && statusCode == 200 {
                    debugPrint("Response:\(String(describing: response))")
                    
                    // Get the document directory url
                    if location?.path != nil {
                        let locationPath = location!.path
                        if format == ".usdz" {
                            
                            let documentsDestPath:String = NSHomeDirectory() + "/Documents/\(modelTitle).usdz"//.zip
                            let fileManager = FileManager.default
                            if (fileManager.fileExists(atPath: documentsDestPath)){
                                try! fileManager.removeItem(atPath: documentsDestPath)
                            }
                            try! fileManager.moveItem(atPath: locationPath, toPath: documentsDestPath)
                            //get stored file path and url
                            let fileName = "\(modelTitle).usdz" // your image name here .zip
                            let filePath: String = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(fileName)"
                            let fileUrl: URL = URL(fileURLWithPath: filePath)
                            let destinationURL = fileUrl
                            print("destinatioURL is == \(destinationURL)")
                            self.modelUrl = String(describing: destinationURL)
                            DispatchQueue.main.async { [self] in
//                                    self.setUpSceneView()
                                    //self.view.isUserInteractionEnabled = true
                                    if self.isRedeem == false{
                                        let modelDownloaded = ModelsDownloadedForSendStruct(_3dModelFile: modelURL, destinationUrl: destinationURL)
                                        self.modelsDownloadedForSend.append(modelDownloaded)
                                        self.isModelAvailableInSapce = true
                                        
//                                        self.addModelInScene(url: destinationURL)
                                        //self.addModelInScene(url: destinationURL)
                                        self.view.makeToast(constantsStrings.moveyourCamera, duration: 5.0, position: .center, completion: nil)
                                    }
                                    else{
                                        self.isGiftModelDownloaded = true
                                        self.view.makeToast(constantsStrings.moveyourCamera, duration: 5.0, position: .center, completion: nil)
                                        //self.drone.loadModel(url: destinationURL)
//                                        self.addModelInScene(url: destinationURL)
                                        if self.isArVideoPlayed {
                                            
                                            
                                        }
                                    }
                                ProgressHUD.dismiss()
                            }
                        }
                        else {
                            ProgressHUD.dismiss()
                            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.modelNotAvailable)
                        }
                    }
                    else {
                        // show network error message and pop vc
                        DispatchQueue.main.async {
                            ProgressHUD.dismiss()
                            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.unableToDownloadModel)
                        }
                    }
                }else {
                   debugPrint("downlaoding failed")
                    
                }
            }
        })
        downloadTask.resume()
    }
    var modelPathURL:URL?
    
    var parentNode: String = ""
    
    @IBAction func messageDoneBtnPressed(_ sender: UIButton) {
        if messageView.isHidden == false {
            messageView.isHidden = true
        }
    }
    
    @objc func didTap(withGestureRecognizer recognizer: UIGestureRecognizer) {
        if isRedeem{
            self.RedeemHalfView.isHidden = true
            self.redeemAnimatingView.isHidden = false
        }
    }
    
    func getRoot(for node: SCNNode) -> SCNNode? {
        if var node = node.parent {
            node = getRoot(for: node)!
        }
        else {
            return node
        }
        return node
    }
  
    @IBAction func redeemViewWalletBtnTapped(_ sender: Any) {
        let vc  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "RedeemCustomModalViewController") as? RedeemCustomModalViewController
        vc?.delegate = self
        self.navigationController?.present(vc!, animated: true, completion: nil)
    }
    
    
    @IBAction func addToWalletBtnTapped(_ sender: UIButton) {
        
        let vc  = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(identifier: "RedeemCustomModalViewController") as? RedeemCustomModalViewController
        vc?.delegate = self
        self.navigationController?.present(vc!, animated: true, completion: nil)
      
    }
    
    @objc func videoDidEnd(note: NSNotification) {
        videoNode?.removeFromParentNode()
        if isRedeem {
            isArVideoPlayed = true
            if isGiftModelDownloaded {
                self.view.makeToast(constantsStrings.tapAnywhere, duration: 3.0, position: .center, completion: nil)
//                self.addModelInScene(url: URL(string: self.modelUrl)!)
                //self.drone.loadModel(url: URL(string: self.modelUrl)!)
            }else {
                ProgressHUD.show()
            }
        }else {
            DispatchQueue.main.async {
                if self.videoRepeatCount == 0 {
//                    self.drone.loadModel(url: self.modelPathURL!)
//                    self.sceneView.scene.rootNode.addChildNode(self.drone)
//                    self.subCategoryCollectionView.reloadData()
                    self.subCategoryView.isHidden = false
                }
            }
        }
        
    }


    @IBAction func showRedeemCustomModal(_ sender: Any) {
        redeemAnimatingView.isHidden = true
        self.senderTitle.text =  "Greetings from \(nameofSender)"
       
        UIView.transition(with: RedeemHalfView, duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
                            self.RedeemHalfView.isHidden = false
                            self.view.bringSubviewToFront(self.RedeemHalfView!)
                            self.tableView.reloadData()
                            
                      })
    }
    
    // MARK: - Functions Performing API Calls
    func addToWallet(name:String,phone:String,url:String){
        if Reachability.isConnectedToNetwork(){
            let data = ["parsl":url,"reciever_name":name,"reciever_phone_no":phone]
            let parameters = ["data":data]
            
            let url = URL(string: constantsUrl.getVirtualCardDetailUrl)!
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                        print(response!)
                    }
                }
                catch let error {
                    print(error)
                }
            })
            task.resume()
        }else{
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
        
    }
    
    func getGuestDeviceDetail() {
        if Reachability.isConnectedToNetwork(){
            let data = ["device_token":ParslUtils.sharedInstance.DEVICE_TOKEN,"device_id":deviceID] as [String : Any]
            let parameters = ["app_id":"parsl_ios",
                              "data":data] as [String : Any]
            
            let APIUrl = constantsUrl.saveGuestDeviceDetailUrl!
            let session = URLSession.shared
            var request = URLRequest(url: APIUrl)
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                print(error)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                        print(response!)
                    }
                } catch let error {
                    print(error)
                }
            })
            task.resume()
        }else{
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
        
    }
    func getCategory() {
        if Reachability.isConnectedToNetwork() {
            let parameters = ["app_id":"parsel_ios"] as [String: AnyObject]
//            let url = URL(string: "https://api.parsl.io/get/categories/list")!
            let url = constantsUrl.getCategoryListUrl
//            let session = URLSession.shared
//            var request = URLRequest(url: url)
//            request.httpMethod = "POST"
            ProgressHUD.show()
            
            postRequest(serviceName: url, sendData: parameters, success: { (response) in
                do {
                    self.mainCategoryList = try JSONDecoder().decode(MainCategory.self, from: response)
                    if !(self.mainCategoryList.data!.isEmpty) {
                        ProgressHUD.dismiss()
                        let firstCategoryName = self.mainCategoryList.data![0].categoryKey!
                        self.getCategoryVendors(categoryName: firstCategoryName)
                    }
                }
                        
                catch{
                    ProgressHUD.dismiss()
                }
                
            }, failure: { (data) in
                print(data)
                ProgressHUD.dismiss()
            })
             
        }else{
            self.isInternetAvailable = false
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
        
    }
    
    func playServerVideo(url: URL) {
            let player = AVPlayer(url: url)
            
            let vc = AVPlayerViewController()
            vc.player = player
            
            self.present(vc, animated: true) {
                vc.player?.play()
                
            }
    }

    func getSubCategories(key: String, vendor: String) {
        var vendors = [String]()
        vendors.append(vendor)
        let data: Dictionary<String, AnyObject> = ["category": key as AnyObject, "vendors": vendors as AnyObject]
        let parameters: Dictionary<String, AnyObject> = ["app_id": "parsl_ios" as AnyObject, "data": data as AnyObject]
        print(parameters)
        let url = constantsUrl.getVendorModelsurl
        
        postRequest(serviceName: url, sendData: parameters, success: { [self] (response) in
            print(response)
            do {
                let json = try JSONSerialization.jsonObject(with: response, options: []) as! [String: AnyObject]
                print(".....\(json)")
                fetchModelsApiData(data: json["data"] as? [AnyObject] ?? [AnyObject]())
                DispatchQueue.main.async {
//
                    if isfromMain {
                        self.vendorSelectedIndex = 0
                    }
                    self.isSubcategoriesAPICalled = .Downloaded
                    self.mainCategoryCollectionView.reloadData()
                    if vendersList.data.vendorsList.count == 1 {
                        self.vendorsCollectionView.reloadSections(IndexSet.init(integer: 0))
                    }
                    else{
                        self.vendorsCollectionView.reloadData()
                    }
                    self.subCategoryCollectionView.reloadData()
                    ProgressHUD.dismiss()
                    
                    
                }
                
            }catch {
                DispatchQueue.main.async {
                    ProgressHUD.dismiss()
                    self.isSubcategoriesAPICalled = .NotDownloadable
                }
            }
            
        }, failure: { (data) in
            print(data)
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
                self.isSubcategoriesAPICalled = .NotDownloadable
            }
        })
    }
    func getVendors(categoryName: String) {
        let url = constantsUrl.getCategoryVendorsUrl
        ProgressHUD.show()
        let data = ["category": categoryName]
        let params = ["app_id":"parsl_ios", "data": data] as [String : AnyObject]
        
        postRequest(serviceName: url, sendData: params, success: { (response) in
            
            do {
                self.vendersList = try JSONDecoder().decode(GetVendorsList.self, from: response)
                self.vendors = self.vendersList.data.vendorsList
                if !(self.vendersList.data.vendorsList.isEmpty) {
                    DispatchQueue.main.async{
                    self.downloadedVendors.append(DownloadedDataForVendors(categoryIndex: self.selectedIndex, vendorsList: self.vendersList))
                        self.vendorsCollectionView.reloadData()
                        self.mainCategoryCollectionView.reloadData()
                        ProgressHUD.dismiss()
                    }
                }
            }
                    
            catch{
                ProgressHUD.dismiss()
            }
            
        }, failure: { (data) in
            print(data)
            ProgressHUD.dismiss()
        })
    }
    
    func getCategoryVendors(categoryName: String) {
        let url = constantsUrl.getCategoryVendorsUrl
        
        let data = ["category": categoryName]
        let params = ["app_id":"parsl_ios", "data": data] as [String : AnyObject]
        print("Vendors api parameter: \(params)")
        ProgressHUD.show()
        postRequest(serviceName: url, sendData: params, success: { (response) in
            
            do {
                self.vendersList = try JSONDecoder().decode(GetVendorsList.self, from: response)
                self.vendors.removeAll()
                for j in 0...self.vendersList.data.vendorsList.count {
                    print("index is \(self.vendersList.data.vendorsList.count)")
                    if self.vendersList.data.vendorsList.count != j{
                    self.vendors.append(VendorsList(vendorID: "", vendorName: self.vendersList.data.vendorsList[j].vendorName, otherInfo: OtherInfo(slogan: "s", otherInfoDescription: "<#String#>dsd", website: "sdsd", logoURL: "sdsd")))
                    }
                    
                    else{
                        break
                    }
                }
               
                debugPrint("Vendor api response data \(self.vendersList.data)")
                if !(self.vendersList.data.vendorsList.isEmpty) {
                    DispatchQueue.main.async{
                    self.downloadedVendors.append(DownloadedDataForVendors(categoryIndex: self.selectedIndex, vendorsList: self.vendersList))
                        debugPrint("vendor name is......\(self.vendersList.data.vendorsList[0].vendorName)")
                    let vendorId = self.vendersList.data.vendorsList[0].vendorID
                        self.getSubCategories(key: categoryName, vendor: vendorId!)
                    }
                }
            }
                    
            catch{
                ProgressHUD.dismiss()
            }
            
        }, failure: { (data) in
            print(data)
            ProgressHUD.dismiss()
        })
    }
    
    
    
    func redeemGift(otp: String, name: String, phoneNumber: String) {
       // ProgressHUD.show("Please wait")
        self.view.isUserInteractionEnabled = true
          
        let data = ["parsl":otp as AnyObject,
                    "device_token":ParslUtils.sharedInstance.DEVICE_TOKEN as AnyObject,
                    "device_id":deviceID as AnyObject]
        let parameters = ["app_id":"parsl_ios" as AnyObject,
                          "data":data as AnyObject]
        
        
        print(parameters)
        let url = constantsUrl.getParslDetails
        
        postRequest(serviceName: url, sendData: parameters, success: { [self] (response) in
            print(response)
            do {
                let jsonResponse = try JSONSerialization.jsonObject(with: response, options: .mutableContainers) as! [String: AnyObject]
                parslRedeemModel = fetchRedeemModelStruct(response: jsonResponse)
                
                if parslRedeemModel != nil {
                    if self.parslRedeemModel.message == "Success"{
                        
                        
                        nameofSender = parslRedeemModel.data.senderData.senderName
                        debugPrint("name of sender is \(nameofSender)")
                        self.format = parslRedeemModel.data.giftModel.format
                        if parslRedeemModel.data.receiverData.videoMsg != ""{
                            DispatchQueue.main.async { [self] in
                                isVideoMessage = true
                            }
                        }else {
                            DispatchQueue.main.async {
                                self.isArVideoPlayed = true
                                isVideoMessage = false
                            }
                        }
//
                        redeemMessage = parslRedeemModel.data.receiverData.textMsg
                        
                        print(redeemMessage)
                        DispatchQueue.main.async {
                            self.isRedeem = true
                            self.MainCategoryStack.isHidden = true
                            self.rightButtonsView.isHidden       = true
                            self.leftButtonsView.isHidden        = true
                            self.subCategoryView.isHidden  = true
                            self.subCategoryCollectionView.isHidden = true
                            
                            var style = ToastStyle()
                            style.backgroundColor = UIColor(named: "toastBackColor")!
                            style.messageColor = .black
                            self.view.makeToast(self.redeemMessage, duration: 10.0, position: .center, style: style)
                            if parslRedeemModel.data.avatarAnimation != "" && parslRedeemModel.data.avatarCharacter != ""{
                                self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
                            }else{
                                self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
                            }
                            tableView.reloadData()
                        }
                        
                    }
                    else{
                        self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.inValidOtpUrl)
                        DispatchQueue.main.async {
                            //self.view.isUserInteractionEnabled = true
                            ProgressHUD.dismiss()
                        }
                    }
                }else {
                    self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.unableToFetchParsl)
                    DispatchQueue.main.async {
                        //self.view.isUserInteractionEnabled = true
                        ProgressHUD.dismiss()
                    }
                }
                
            } catch let error {
                print(error)
                DispatchQueue.main.async {
                    ProgressHUD.dismiss()
                    //self.view.isUserInteractionEnabled = true
                }
            }
            
            
            
        }, failure: { (data) in
            print(data)
            
            DispatchQueue.main.async {
                ProgressHUD.dismiss()
                //self.view.isUserInteractionEnabled = true
                self.showAlert(withMessage: constantsStrings.unableToFetch)
            }
        })
    }
    
    func getGiftModels() {
        if Reachability.isConnectedToNetwork(){
//            let parameters = ["app_id":"parsel_ios",
//                              "namespace":""] as [String : Any]
            let parameters = ["app_id": "parsl_ios"] as [String : Any]
            let url = constantsUrl.getGiftModels!
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            } catch let error {
                print(error)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
                
                guard error == nil else {
                    return
                }
                
                guard let data = data else {
                    return
                }
                
                do {
                    if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                        giftModels = try JSONDecoder().decode(GiftModel.self, from: data)
                    }
                } catch let error {
                    print(error)
                }
            })
            task.resume()
        }else{
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
        
    }
    
    func saveToServer() {
        var device_info : Dictionary <String, String> = [:]
        var receiver_info : Dictionary <String, String> = [:]

         device_info = [
          "device_name":UIDevice.current.name as String,
          "device_os":UIDevice.current.systemName as String,
            "device_version":UIDevice.current.systemVersion as String,
            "device_type":UIDevice.current.model,
            "device_manufacturar":"Apple"
        ]

        let data :Dictionary<String,Any> = ["creator" :userEmail! as String,"creator_name":userEmail! as String, "phone":"","creator_email":userEmail! as String,"creator_namespace":userEmail! as String,"creater_device_info":device_info as Any,"firebase_id":self.documentID! as String,"app_id":"parsl","creator_device_token":ParslUtils.sharedInstance.DEVICE_TOKEN! as String]
        
        
        print(data)
        
        postChatRequest(serviceName:constantsUrl.startChat, sendData: data as [String : AnyObject], success: { (data) in
            print("data is",data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
        
        let status = data["status"] as! Bool
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async {
                    if status{
                        print("successfuly saved to server")
                    }else{
                        self.showAlert(withMessage: constantsStrings.agentsAreBusy)
                    }
                   // self.showAlert("Success")
                    
                }
      
            }
  
        }, failure: { (data) in
            print(data)
            
        })
        
    }
    
    //MARK: -Redeem Animation Methods
    
    func loadRedeemAnimationBackGroundView() {
        RedeemAnimatingBackgroundView.isHidden = false
        actionButton.removeFromSuperview()

        BackgroundView.contentMode = .scaleAspectFill
        BackgroundView.loopMode = .loop
        BackgroundView.play()
        self.view.sendSubviewToBack(self.unityView!)
        self.view.bringSubviewToFront(RedeemAnimatingBackgroundView)
        BackgroundView.bringSubviewToFront(firstCardView)
        BackgroundView.bringSubviewToFront(secondCardView)
        BackgroundView.bringSubviewToFront(buttonView)
        
        animatingCardView = true
        
        self.roundButtonView()
        
        
        self.firstCardView.alpha = 0
        self.buttonView.alpha = 0
        self.firstCardView.isHidden = false
        
        UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
            self.firstCardView.transform = CGAffineTransform(translationX: -30, y: -200)
        }) { (_) in
            UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.firstCardView.alpha = 1
                self.firstCardTextView.text = constantsStrings.receivedParsl
                self.firstCardView.transform = self.firstCardView.transform.translatedBy(x: self.view.frame.width/2-self.firstCardView.frame.width/2, y: 200)
               
               
            }, completion: nil)
        }
        
        
        UIView.animate(withDuration: 1.3, delay: 0.7, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .transitionFlipFromLeft, animations: {
           
            self.buttonView.transform = CGAffineTransform(translationX: -30, y: -200)
        }) { (_) in
            
            UIView.animate(withDuration: 1, delay: 0.9, usingSpringWithDamping: 10, initialSpringVelocity: 1, options: .transitionFlipFromLeft, animations: {
                self.buttonView.alpha = 1
                self.buttonView.transform = self.firstCardView.transform.translatedBy(x: -32, y: 64)

            }, completion: nil)
            
        }
        
        isFirstTime = true
        
        
    }
    
    
    
    @IBAction func nxtBtnTapped(_ sender: Any) {
        
        if isFirstTime{
            
            UIView.animate(withDuration: 0){
                self.firstCardView.alpha = 0
                self.buttonView.alpha = 0
                self.firstCardView.isHidden = true
                
            }
             
            UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .curveEaseInOut, animations: {
                self.secondCardView.transform = CGAffineTransform(translationX: -30, y: -200)
            }) { (_) in
                UIView.animate(withDuration: 1, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                    self.secondCardView.alpha = 1
                    self.secondCardView.isHidden = false
                    // self.cardViewHeight.constant = 350
                    self.secondCardTextView.text = self.redeemMessage
                    self.secondCardView.transform = self.secondCardView.transform.translatedBy(x: self.view.frame.width/2-self.secondCardView.frame.width/2, y: 200)
                    
                    
                }, completion: nil)
                
                
            }
            
            UIView.animate(withDuration: 1.3, delay: 0.9, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .transitionFlipFromLeft, animations: {
                
                self.buttonView.transform = CGAffineTransform(translationX: -30, y: -200)
            }) { (_) in
                
                UIView.animate(withDuration: 1, delay: 0.7, usingSpringWithDamping: 10, initialSpringVelocity: 1, options: .transitionFlipFromLeft, animations: {
                    self.buttonView.alpha = 1
                    self.buttonView.transform = self.secondCardView.transform.translatedBy(x: -32, y: 264)
                    
                }, completion: nil)
                
                
            }
            
            
            self.isFirstTime = false
            
        }else{
            
            RedeemAnimatingBackgroundView.isHidden = true
            self.secondCardView.isHidden = true
            view.addSubview(actionButton)
            actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -12).isActive = true
            actionButton.bottomAnchor.constraint(equalTo: MainCategoryStack.topAnchor, constant: -8).isActive = true
            self.view.sendSubviewToBack(RedeemAnimatingBackgroundView)
            self.redeemAnimatingView.isHidden = false
            animatingCardView = false
            if parslRedeemModel.data.avatarAnimation != "" && parslRedeemModel.data.avatarCharacter != ""{
                self.loadAvatar()
            }else{
                self.sendRedeemMsg(ID: self.parslRedeemModel.data.giftModel.productId)
            }
            self.view.makeToast(constantsStrings.viewParsl)
        }
    }

        
func roundButtonView() {
    
    buttonView.layer.cornerRadius = buttonView.frame.size.width/2
    buttonView.clipsToBounds = true

    buttonView.layer.borderColor = UIColor.white.cgColor
    buttonView.layer.borderWidth = 5.0
    
    
    firstCardView.clipsToBounds = true
    firstCardView.layer.cornerRadius = 10.0
    
    secondCardView.clipsToBounds = true
    secondCardView.layer.cornerRadius = 10.0
    
    
}
    //MARK: -Unity Helper methods
     
    func destroyUnity(_ message: String!) {
        //destroying a model
        if !isRedeem{
            let parsed = message.replacingOccurrences(of: "(Clone)", with: "")
            modelsInView.removeValue(forKey: parsed)
            
            for (index, element) in modelsInCart.enumerated() {
              
                if element.productId == message{
                    modelsInCart.remove(at: index)
                    cartCountLabel.text = String(modelsInCart.count)
                    if cartCountLabel.text == "0" {
                        self.cartCountLabel.text = " "
                    }
                }
            }
        }
        self.view.sendSubviewToBack(self.unityView!)

    }
    
    func Redeem(boxID:String,modelID:String) {
       // var ID = boxID
         let ID = boxID + "," + modelID
        print("redeem id is",ID)
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "RedeemBox", message: ID)
    }
    
    func sendMSg(ID:String, defaultShadows: Bool) {
            var id = ID + "," + String(defaultShadows)
            print(id)
            UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "Activate", message: id)
            UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ShadowSwitchOff", message: "")
        
    }
    
    func sendRedeemMsg(ID:String) {
        var id = ID + "," + "true"
        print("model id is",ID)
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "Redeem", message: id)
    }
    
    func closeUnity() {
        
        print("closing...... ")
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "CloseUnity", message: "")
    }
    
    func turnModelShadowsOffOn() {
        var style = ToastStyle()
        if !isShadowOff{
            
            self.view.makeToast(constantsStrings.shadowsDisable, duration: 3.0, position: .center, style: style)
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ShadowSwitchOff", message: "")
            self.isShadowOff = true
            self.shadowBtn.setImage(UIImage(named: "shadow off"), for: .normal)
        }
        else{
            debugPrint("turning model shadows on...... ")
            if tappedItem.defaultShadow == false {
                self.view.makeToast(constantsStrings.noShadow, duration: 3.0, position: .bottom, style: style)
            }
            else {
                self.view.makeToast(constantsStrings.shadowEnable, duration: 3.0, position: .center, style: style)
            UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ShadowSwitchOn", message: "")
            self.isShadowOff = false
            self.shadowBtn.setImage(UIImage(named: "shadow on"), for: .normal)
            }
        }
    }
    
    func turnVerticalHorizontal(){
        var style = ToastStyle()
        if isHorizontal{
            UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "VertMovement", message: "")
            self.isHorizontal = false
            
            self.HVBtn.setImage(UIImage(named: "vertical"), for: .normal)
            self.view.makeToast(constantsStrings.horizontalDisable, duration: 3.0, position: .center, style: style)

        }
        else{
            UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "HoriMovement", message: "")
            self.isHorizontal = true
            self.HVBtn.setImage(UIImage(named: "horizontal"), for: .normal)
            self.view.makeToast(constantsStrings.horizontalEnable, duration: 3.0, position: .center, style: style)
        }
    }
    
  
     
    
    func saveFirebaseid(_ firebaseId: String){
            UserDefaults.standard.set(firebaseId, forKey: "firebaseId")
            UserDefaults.standard.synchronize()
    }
    
    func openConversationWindow() {
        //check if email exists
          let sentDate = Date()
          var userName:String
          var email:String
          var phone:String?
          var namespace:String

              phone = ""
              userName = userEmail!
              email = userEmail!
              namespace = deviceId!
          
          var device_info : Dictionary <String, String> = [:]
  //        var receiver_info : Dictionary <String, String> = [:]

           device_info = [
              "device_manufacturar": "Apple" as String,
            "device_name":UIDevice.current.name as String,
            "device_os":UIDevice.current.systemName as String,
              "device_type": "iPhone" as String,
            "device_version":UIDevice.current.systemVersion as String
          ]

          var data: Dictionary<String, Any>

              data =
                  ["creator" :deviceId! as String,
                  "creator_name": userEmail! as String,
                  "phone":phone as Any,
                  "creator_email":email as String,
                  "creator_namespace":namespace as String,
                  "creater_device_info":device_info as Any,
                  "app_id":"parsl",
                  "creator_device_token":deviceToken as Any,
                  "msg_timestamp":  Int64((sentDate.timeIntervalSince1970).rounded())]
          

          print(data)

        postChatRequest(serviceName:constantsUrl.startChat, sendData: data as [String : AnyObject], success: { (response) in
              print(response)
              let defaults = UserDefaults.standard
              let statusCode = defaults.string(forKey: "statusCode")

              if ((statusCode?.range(of: "200")) != nil){
                  DispatchQueue.main.async {
                      let status = response["status"] as! Bool
                      if status{
                          let data = response["data"] as! NSDictionary
                          let firebaseId = data["firebase_id"] as! String
                          debugPrint("firebase id is ......\(firebaseId)")
                          self.saveFirebaseid(firebaseId)
                          
                          let channel = Channel(id: firebaseId)
                          let vc = ChatViewController(channel: channel)
//
                          self.navigationController?.pushViewController(vc, animated: true)
                      }
                      else{
                          print(response["message"] as! String)
                      }
                      
                  }

              }
              
              DispatchQueue.main.async {
                  
              }

          }, failure: { (data) in
              print(data)
              
              DispatchQueue.main.async {
                  self.view.makeToast(constantsStrings.unableChat)
                  
              }
          })

        }
         
   
    func showAlertWithAction(withTitle title: String, withMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: constantsStrings.okLabel, style: .default, handler: { action in
       
            self.mainPresentedView.isHidden = true 
        })
        alert.addAction(ok)

        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
   
    
}

// MARK: - Protocols..
protocol SendDataDelegate: class {
    func sendData(_ receiverName: String, _ receiverNumber: String,_ URLOTP: String)
    func showActionButton()
    func sendRedeemData(_ receiverName: String, _ receiverNumber: String)
    func mayBelater()
}

protocol CartEmptyDelegate {
    func cartHasBeenCleared()
}
