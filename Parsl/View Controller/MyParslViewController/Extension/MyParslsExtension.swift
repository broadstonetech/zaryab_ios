//
//  Extension.swift
//  Parsl
//
//  Created by M Zaryab on 28/02/2022.
//

import Foundation
import UIKit


// MARK: - Extensions
// MARK: - TableView Delegates
extension MyParslsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyParslsTableViewCell", for: indexPath) as! MyParslsTableViewCell
        cell.descriptionLbl.text = "\(dataArray[indexPath.row].senderName ?? "") sent you PARSL \(dataArray[indexPath.row].parslOtp ?? "") \(dataArray[indexPath.row].amount)"
        let status = dataArray[indexPath.row].redeemedStatus ?? false
        var availStatus = ""
        if status == true {
            availStatus = "availed"
        }
        else {
            availStatus = "not availed"
        }
        let date = NSDate(timeIntervalSince1970: dataArray[indexPath.row].timestamp ?? Double("1643195387733")!)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let strDate = dateFormatter.string(from: date as Date)
        cell.timeStatus.text = "\(availStatus)   \(strDate)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let otp = dataArray[indexPath.row].parslOtp ?? ""
        ParslUtils.sharedInstance.parslOtp = otp
        debugPrint("parsl otp is \(String(describing: ParslUtils.sharedInstance.parslOtp).debugDescription)")
        if let navigator = navigationController{
            navigator.popViewController(animated: true)
            navigator.popViewController(animated: false)
        }
        
    }
     
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
