//
//  MyParslsViewController.swift
//  Parsl
//
//  Created by M Zaryab on 19/01/2022.
// this class is used to view the saved parsls otp and select parsls to redeem the
// parsls

import UIKit

class MyParslsViewController: BaseViewController {

    // MARK: - IBoutelts
    
    @IBOutlet weak var parslsTableview: UITableView!
    
    // MARK: - Properties
    var dataArray = [MyParslsData]()
    var delegate : SendDataDelegate?
    
    // MARK: - lifecycles
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationBarUi()
        self.getListofMyParsls()
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        parslsTableview.delegate = self
        parslsTableview.dataSource = self
        parslsTableview.rowHeight = UITableView.automaticDimension
        parslsTableview.reloadData()
        title = constantsStrings.myParslScreenTitle

        
    }
    // MARK: - Methods
    func setNavigationBarUi() {
        self.navigationController?.isNavigationBarHidden = false
        let appearance = UINavigationBarAppearance()
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor(named: "toastBackColor")
        self.navigationController?.navigationBar.barTintColor = UIColor(named: "toastBackColor")
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func getListofMyParsls() {
        ProgressHUD.show()
        let url = constantsUrl.myParslUrl
        let Data = ["namespace": ParslUtils.sharedInstance.getUserNameSpace()] as [String: AnyObject]
        let params = ["app_id": "parsl_ios", "data": Data] as [String: AnyObject]
        print(params)
        if Reachability.isConnectedToNetwork() {
        postChatRequest(serviceName: url, sendData: params, success: {(response) in
            
                let dictionary = response as [String:Any]
                print(response)
                print(dictionary)
                ProgressHUD.dismiss()
            let obj = MyParsls(fromDictionary: dictionary)
            self.dataArray = obj.data
            if obj.message == "Success" {
                DispatchQueue.main.async{
                    self.dataArray.reverse()
                    self.parslsTableview.reloadData()
                    ProgressHUD.dismiss()
                }
            } else {
                self.showAlert(withMessage: constantsStrings.unavailableParsls)
                ProgressHUD.dismiss()
            }
        }, failure: {(error) in
           print(error)
        })
        } else {
            ProgressHUD.dismiss()
            self.showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.noInternet)
        }
    }
}


