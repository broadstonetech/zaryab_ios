//
//  AlertTextField.swift
//  Parsl
//
//  Created by M Zaryab on 21/02/2022.
//

import SwiftUI

struct AlertTextField: View {
    @Binding var isShown: Bool
    @Binding var text: String
    
    let screenSize = UIScreen.main.bounds
    var title: String = "Add folder name"
    var body: some View {
        VStack {
            Text(title)
            TextField("", text: $text) .textFieldStyle(RoundedBorderTextFieldStyle())
            
            HStack {
                Button("done") {
                    
                }
                Button("cancel") {
                    
                }
            }
        }
        .padding()
        .frame(width: screenSize.width * 0.7, height: screenSize.height * 0.3)
        .offset(y: isShown ? 0 : screenSize.height)
        .animation(.spring())
        .background(Color.black)
        .clipShape(RoundedRectangle(cornerRadius: 20, style: .continuous))
        
       
    }
}

struct AlertTextField_Previews: PreviewProvider {
    static var previews: some View {
        AlertTextField(isShown: .constant(true), text: .constant(""))
    }
}
