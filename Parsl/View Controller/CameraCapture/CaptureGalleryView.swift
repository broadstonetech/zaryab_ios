/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A custom view that displays the contents of a capture directory.
*/
import Combine
import Foundation
import SwiftUI
import ZIPFoundation
import Alamofire
import ActivityIndicatorView
import AlertToast
import Combine
//import ProgressIndicatorView

 
/// This view displays the contents of a capture directory.
struct CaptureGalleryView: View {
    
    private let columnSpacing: CGFloat = 3
    @State public var presentAlert = false
    @State public var alertTitle = ""
    @State private var shouldAnimate = false
    @State private var showLoadingIndicator: Bool = false
    @State private var showToast = false
    @State private var bottomText = false
    @State private var isPresent = false
    @State private var textFieldAlertShown = false
    @State var totalImages = "hello"
    @State var lastCount = "hello"
    @State var changeInCount = false
    @State private var username: String = ""
    @State private var folderNameChange = false
    
    let method = ImageUPloadMethods()
    
     
    @Environment(\.presentationMode) private var presentation
    
    let pub = NotificationCenter.default
                .publisher(for: NSNotification.Name("UploadSuccess"))
    /// This is the data model the capture session uses.
    @ObservedObject var model: CameraViewModel
    
    /// This property holds the folder the user is currently viewing.
    @ObservedObject private var captureFolderState: CaptureFolderState
    
    /// When a thumbnail cell is tapped, this property holds the information about the tapped image.
    @State var zoomedCapture: CaptureInfo? = nil
    
    /// When this is set to `true`, the app displays a toolbar button to dispays the capture folder.
    @State private var showCaptureFolderView: Bool = false
    
    /// This property indicates whether the app is currently displaying the capture folder for the live session.
    let usingCurrentCaptureFolder: Bool
    
    /// This array defines the vertical layout for portrait orientation.
    let portraitLayout: [GridItem] = [ GridItem(.flexible()),
                                       GridItem(.flexible()),
                                       GridItem(.flexible()) ]
    
    
    
    /// This initializer creates a capture gallery view for the active capture session.
    init(model: CameraViewModel) {
        self.model = model
        self.captureFolderState = model.captureFolderState!
        usingCurrentCaptureFolder = true
        bottomText = ParslUtils.sharedInstance.getifImagesUPloaded()
        load()

    }
    
    /// This initializer creates a capture gallery view for a previously created capture folder.
    init(model: CameraViewModel, observing captureFolderState: CaptureFolderState) {
        self.model = model
        self.captureFolderState = captureFolderState
        usingCurrentCaptureFolder = (model.captureFolderState?.captureDir?.lastPathComponent
                                        == captureFolderState.captureDir?.lastPathComponent)
    }
    
   
    
    var body: some View {
        HStack() {
            ActivityIndicatorView(isVisible: $showLoadingIndicator, type: .flickeringDots)
                 .frame(width: 15.0, height: 15.0)
                 .foregroundColor(.white)
                 .padding()
            Spacer()
            Text(totalImages).font(.system(size: 12.0))
                .padding()
        }
          
        ZStack {
            Color(red: 0, green: 0, blue: 0.01, opacity: 1).ignoresSafeArea(.all)
            
            // Create a hidden navigation link for the toolbar item.
            NavigationLink(destination: CaptureFoldersView(model: model),
                           isActive: self.$showCaptureFolderView) {
                EmptyView()
            }
            .frame(width: 0, height: 0)
            .disabled(true)
//            ActivityIndicatorView(isVisible: $showLoadingIndicator, type: .flickeringDots)
//                 .frame(width: 25.0, height: 25.0)
//                 .foregroundColor(.white)
             
            GeometryReader { geometryReader in
                ScrollView() {
                    LazyVGrid(columns: portraitLayout, spacing: columnSpacing) {
                        ForEach(captureFolderState.captures, id: \.id) { captureInfo in
                            GalleryCell(captureInfo: captureInfo,
                                        cellWidth: geometryReader.size.width / 3,
                                        cellHeight: geometryReader.size.width / 3,
                                        zoomedCapture: $zoomedCapture)
                        }
                    }
                }
            }
            .blur(radius: zoomedCapture != nil ? 20 : 0)
            
            if zoomedCapture != nil {
                ZStack(alignment: .top) {
                    // Add a transluscent layer over the blur to make the text pop.
                    Color(red: 0.25, green: 0.25, blue: 0.25, opacity: 0.25)
                        .ignoresSafeArea(.all)
                    
                    VStack {
                        FullSizeImageView(captureInfo: zoomedCapture!)
                            .onTapGesture {
                                zoomedCapture = nil
                            }
                    }
                    
                    HStack {
                        Spacer()
                        Button(action: {
                            captureFolderState.removeCapture(captureInfo: zoomedCapture!,
                                                             deleteData: true)
                            zoomedCapture = nil
                        }) {
                            Text("Delete").foregroundColor(Color.red)
                        }.padding()
                    }
                }
            }
        }
        VStack(alignment: .center) {
            
            if bottomText == true {
                VStack(alignment: .center) {
                    Text("Don't quit the application until the upload is complete").font(.system(size: 10.0))
                    Text("However you may use the app or perform tasks").font(.system(size: 10.0))
                }
            }
            
        }
        .frame(width: 400, height: 20)
        .onReceive(pub, perform: { Output in
            print("notification observer fired")
//            showToast = true
            bottomText = false
            showLoadingIndicator = false
            alertTitle = "Images are uploaded successfully"
            presentAlert = true
//            lastCount = totalImages
            ParslUtils.sharedInstance.saveLastCount(totalImages)
        })
        
        .onAppear() {
            // total number of images
            let documentsPath = captureFolderState.captureDir?.path
            print(documentsPath)
            
            print("\(ParslUtils.sharedInstance.getIfDepthCameraAvailable())")
            // check if file path is empty or not
            let fileManagerD = FileManager.default
            if fileManagerD.fileExists(atPath: documentsPath!) {
                print("FILE AVAILABLE")
//                ParslUtils.sharedInstance.saveDirectoryPath(documentsPath!)
                do {
                    let numberOfItems = try fileManagerD.contentsOfDirectory(atPath: documentsPath!).count
                    print("no of items in path is \(numberOfItems)")
                    
                    if !model.isCameraAvailable {
//                    self.totalImages = String(numberOfItems / Int(2) + 1)
                        self.totalImages = String(numberOfItems)
                    }
                    else {
//                        self.totalImages = String(numberOfItems / Int(3))
                        self.totalImages = String(numberOfItems / Int(2))
                    }
                } catch {
                    
                }
            } else {
                print("FILE NOT AVAILABLE")
            }
            
            if ParslUtils.sharedInstance.getLastCount() != totalImages {
                changeInCount = true
            }
            
            bottomText = ParslUtils.sharedInstance.getifImagesUPloaded()
            showLoadingIndicator = ParslUtils.sharedInstance.getifImagesUPloaded()
        }
        .alert(isPresented: $presentAlert) { // 4
                    Alert(
                        title: Text("PARSL"),
                        message: Text(alertTitle)
                    )
                }.padding()
            .toast(isPresenting: $showToast, duration: 10, tapToDismiss: true, offsetY: 10, alert: {
                AlertToast(displayMode: .alert, type: .regular, title: "wait")
            }, onTap: {
                print("alert")
            }, completion: {
                debugPrint("completed")
            })
        
            .navigationTitle(Text("\(captureFolderState.captureDir?.lastPathComponent ?? "NONE")"))
//            .navigationTitle(Text("\(ParslUtils.sharedInstance.getIfDepthCameraAvailable() ?? "NONE")"))
        
        .navigationBarTitleDisplayMode(.inline)
        .navigationBarItems(trailing: HStack {
//            NewSessionButtonView(model: model, usingCurrentCaptureFolder: usingCurrentCaptureFolder)
//                .padding(.horizontal, 5)
            if usingCurrentCaptureFolder {
                Button(action: {
                    self.showCaptureFolderView = true
                }) {
                    Image(systemName: "folder")
                }
                   }
            if usingCurrentCaptureFolder {
                       Menu(content: {
                           Button(action: {
                               if ParslUtils.sharedInstance.getifImagesUPloaded() == false {
                                   debugPrint("creating new session")
                               model.requestNewCaptureFolder()
                               // Navigate back to the main scan page.
                               presentation.wrappedValue.dismiss()
                               }
                               else if ParslUtils.sharedInstance.getifImagesUPloaded() == true {
                                   debugPrint("creating toast")
                                   alertTitle = "Images are uploaded already. Please create new session"
                                   presentAlert = true
                               }
                           }) {
                               Label("New Session", systemImage: "camera")
                           }
                       }, label: {
                           Image(systemName: "plus.circle")
                       })
                   }
            
            if usingCurrentCaptureFolder {
                Button(action: {
                    //                    self.showCaptureFolderView = true
                    let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
                    print("pressed up arrow")
                     
                    let documentsPath = captureFolderState.captureDir?.path
                     
                    // check if file path is empty or not
                    let fileManagerD = FileManager.default
                    if fileManagerD.fileExists(atPath: documentsPath!) {
                        print("FILE AVAILABLE")
                        do {
                            let numberOfItems = try fileManagerD.contentsOfDirectory(atPath: documentsPath!).count
                            print("no of items in path is \(numberOfItems)")
                            if numberOfItems == 0 {
                                alertTitle = "Images folder is empty"
                                presentAlert = true
                                return
                            }
                            else {
                                print("images folder is not empty")
                            }
                        } catch {
                            
                        }
                    } else {
                        print("FILE NOT AVAILABLE")
                    }
                    // images must be greater then 20 and less then 30
//                    if Int(totalImages)! < 20 || Int(totalImages)! > 30 {
//                        alertTitle = "Images must be greater then 20 and less then 30"
//                        presentAlert = true
//                        return
//                    }
                    bottomText = true
                    var size = ""
                    print(captureFolderState.captureDir?.path)
                                 captureFolderState.captureDir?.path
                    let fileManager = FileManager()
                    let currentWorkingPath = captureFolderState.captureDir!.path
                    let sourceURL = captureFolderState.captureDir!
                    var destinationURL = URL(fileURLWithPath: currentWorkingPath)
                    destinationURL.appendPathComponent("\(captureFolderState.captureDir?.lastPathComponent ?? "NONE").zip")
                    print("destination url before zip \(destinationURL)")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    if ParslUtils.sharedInstance.getifImagesUPloaded() == false {
 
                        // If user add or delete the images after uploading and wants to upload again.
                        // check file if exist then delete it.
                        if changeInCount == true {
                            changeInCount = false
                            method.deleteZipFile()
                        }
                        // zip the folder
                    do {
                        let progress = Progress.init()
                        try fileManager.zipItem(at: sourceURL, to: destinationURL, shouldKeepParent: true, compressionMethod: .none, progress: progress)
                         
                        print(progress.fractionCompleted)
                        print("destination url is \(destinationURL)")
                        ParslUtils.sharedInstance.saveDirectoryPath(destinationURL.path)
                        // get the size of the image zip folder
                        do {
                            showLoadingIndicator = true

                            let resources = try destinationURL.resourceValues(forKeys:[.fileSizeKey])
                            let fileSize = resources.fileSize!
                            size = fileSize.description.sha256()
                            print("size ins shaw  \(size)")
                            ParslUtils.sharedInstance.saveSizeOfImages(size)
                            print ("\(fileSize)")

                            // upload zipfile to server
//                            showToast = false
                            self.shouldAnimate = true
                            appDelegate?.testMethod()

                        } catch {
                            showLoadingIndicator = false
                            print("Error: \(error)")
                        }
                    } catch {
                        showLoadingIndicator = false
                        print("Creation of ZIP archive failed with error:\(error)")
                        alertTitle = "Images are uploaded already. Please create new session"
                        bottomText = false
                        presentAlert = true
                    }
                    } else {
                        alertTitle = "Images are being uploaded. Do not quit the app"
                        presentAlert = true
                    }
                }
                    
                }) {
                    Image(systemName: "square.and.arrow.up")
                }
                
            }
        })
    }
    // saving last path component in shared preferences.
    private func load() {
        print("funtion loading")
//        ParslUtils.sharedInstance.saveDepthCameraAvailable(captureFolderState.captureDir?.lastPathComponent ?? "")
//        ParslUtils.sharedInstance.saveDirectoryPath(captureFolderState.captureDir!.path)
    }
}



/// This cell displays a single thumbnail image with its metadata annotated on it.
struct GalleryCell: View {
    /// This property stores the location of the image and its metadata.
    let captureInfo: CaptureInfo
    
    /// When the user taps a cell to zoom in, this property contains the tapped cell's `captureInfo`.
    @Binding var zoomedCapture: CaptureInfo?
    
    /// This property keeps track of whether the image's metadata files exist. The app populates this
    /// property asynchronously.
    @State private var existence: CaptureInfo.FileExistence = CaptureInfo.FileExistence()
    
    let cellWidth: CGFloat
    let cellHeight: CGFloat
    
    // This property holds a reference for a subscription to the `existence` future.
    var publisher: AnyPublisher<CaptureInfo.FileExistence, Never>
    
    init(captureInfo: CaptureInfo, cellWidth: CGFloat, cellHeight: CGFloat,
         zoomedCapture: Binding<CaptureInfo?>) {
        self.captureInfo = captureInfo
        self.cellWidth = cellWidth
        self.cellHeight = cellHeight
        self._zoomedCapture = zoomedCapture
        
        // Make a single publisher for the metadata future.
        publisher = captureInfo.checkFilesExist()
            .receive(on: DispatchQueue.main)
            .replaceError(with: CaptureInfo.FileExistence())
            .eraseToAnyPublisher()
    }
    
    var body : some View {
        ZStack {
            AsyncThumbnailView(url: captureInfo.imageUrl)
            
                .frame(width: cellWidth, height: cellHeight)
                .clipped()
                .onTapGesture {
                    withAnimation {
                        self.zoomedCapture = captureInfo
                    }
                }
                .onReceive(publisher, perform: { loadedExistence in
                    existence = loadedExistence
                })
//            print("capture url is ....\(captureInfo.imageUrl)")
            MetadataExistenceSummaryView(existence: existence)
                .font(.caption)
                .padding(.all, 2)
        } 
    }
}

struct AlertView: View {
    @State private var showingAlert = false

    var body: some View {
        if #available(iOS 15.0, *) {
            Button("Show Alert") {
                showingAlert = true
            }
            .alert("Important message", isPresented: $showingAlert) {
                Button("OK", role: .cancel) { }
            }
        } else {
            // Fallback on earlier versions
        }
    }
}

struct MetadataExistenceSummaryView: View {
    var existence: CaptureInfo.FileExistence
    private let paddingPixels: CGFloat = 2
    
    var body: some View {
        VStack {
            Spacer()
            HStack {
                if existence.depth && existence.gravity {
                    Image(systemName: "checkmark.circle.fill")
                        .foregroundColor(Color.green)
                        .padding(.all, paddingPixels)
                } else if existence.depth || existence.gravity {
                    Image(systemName: "exclamationmark.circle.fill")
                        .foregroundColor(Color.yellow)
                        .padding(.all, paddingPixels)
                } else {
                    Image(systemName: "xmark.circle.fill")
                        .foregroundColor(Color.red)
                        .padding(.all, paddingPixels)
                }
                Spacer()
            }
        }
    }
}

struct MetadataExistenceView: View {
    var existence: CaptureInfo.FileExistence
    var textLabels: Bool = false
    
    var body: some View {
        HStack {
            if existence.depth {
                Image(systemName: "square.3.stack.3d.top.fill")
                    .foregroundColor(Color.green)
            } else {
                Image(systemName: "xmark.circle")
                    .foregroundColor(Color.red)
            }
            if textLabels {
                Text("Depth")
                    .foregroundColor(.secondary)
            }
            
            Spacer()
            
            if existence.gravity {
                Image(systemName: "arrow.down.to.line.alt")
                    .foregroundColor(Color.green)
            } else {
                Image(systemName: "xmark.circle")
                    .foregroundColor(Color.red)
            }
            if textLabels {
                Text("Gravity")
                    .foregroundColor(.secondary)
            }
        }
    }
}

/// This is the view the app shows when the user taps on a `GalleryCell` thumbnail. It asynchronously
/// loads the full-size image and displays it full-screen.
struct FullSizeImageView: View {
    let captureInfo: CaptureInfo
    var publisher: AnyPublisher<CaptureInfo.FileExistence, Never>
    
    @State private var existence = CaptureInfo.FileExistence()
    
    init(captureInfo: CaptureInfo) {
        self.captureInfo = captureInfo
        publisher = captureInfo.checkFilesExist()
            .receive(on: DispatchQueue.main)
            .replaceError(with: CaptureInfo.FileExistence())
            .eraseToAnyPublisher()
    }
    
    var body: some View {
        VStack {
            Text(captureInfo.imageUrl.lastPathComponent)
                .font(.caption)
                .padding()
            GeometryReader { geometryReader in
                AsyncImageView(url: captureInfo.imageUrl)
                    .frame(width: geometryReader.size.width, height: geometryReader.size.height)
                    .aspectRatio(contentMode: .fit)
            }
            MetadataExistenceView(existence: existence, textLabels: true)
                .onReceive(publisher, perform: { loadedExistence in
                    existence = loadedExistence
                })
                .padding(.all)
        }
        .transition(.opacity)
    }
}
