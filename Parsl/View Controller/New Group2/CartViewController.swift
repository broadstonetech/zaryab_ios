//
//  CategoriesViewController.swift
//  Parsl
//
//  Created by Billal on 22/02/2021.
// this class helps the user to get the price of the selected items and do payment for the sleected PARSLs.

import UIKit
import AVKit
import MobileCoreServices
import PassKit
import Alamofire
import Lottie
import UnityFramework
import Toast_Swift
import Stripe

// MARK: - Structure
struct ModelsQuantityStruct {
    var modelIndex: Int!
    var modelQuantity: Int!
    var modelPrice: Double!
    var totalPrice: Double! 
    var isDiscounted:Bool!
}

class CartViewController: PaymentsBaseClass, UINavigationControllerDelegate, CartCellClickDelegate{
    
    // MARK: - Properties
    var isValidCoupon = false
    var arrColor = [(isSelected:Bool,color:UIColor)]()
    var couponResponse:                   CouponResponse!
    var taxPrice: Double            = 0.0
    var priceWithoutTax: Double     = 0.0
    var totalPrice: Double          = 0.0
    var senderName: String          = ""
    var senderEmail: String         = ""
    var senderAddress: String       = ""
    var senderNumber: String        = ""
    var receiverName: String        = ""
    var receiverEmail: String       = ""
    var receiverAddress: String     = ""
    var receiverNumber: String      = ""
    
    var deviceTokenString:String    = ""
    var pricesList                  = [Double]()
    var selectedItemsDataList       = [SubCategoryData]()
    //these arrays are used to zip into one dictionary
    var selectedItemsArray          = [String]()
    var quantity                    = [Int]()
    
    var videoURL: String            = ""
    var selectedModel:String        = ""
    var isVideo:Bool = false
    var selectedItem                = 0
    var selectIndex                 = 0
    var selectIndexSec              = 0
    var saveParsl                   : SaveParsl!
    var isVideoRecorded: Bool = false
    var extrasArray                     = [GiftModelMessage]()
 
    var animationView: AnimationView?
    
    var downloadedAnimationsFilePaths = [String]()
    
    var isAnyDiscount:Bool = false
    
    var avatarCharacter = ""
    var avatarAnimation = ""
    var applePayTapped = true
    var modelsInCart = [ModelDataStruct]()
    var modelsPrices = [ModelsQuantityStruct]()
    var giftWrapSelectedIndex = 0
    var animationSelectedIndex = -1
    var avatarSelectedIndex = -1
    
    var cartDelegate: CartEmptyDelegate?
    var unityView: UIView?
    var finalCharges = ""
    var appleDataKey = ""
    var pricesListCount:Int?
     
    
    //MARK: - IBOutlets
    @IBOutlet var backgroundView: UIView! 
    @IBOutlet weak var loginBtn: UILabel!
    @IBOutlet weak var loginTextView: UIView!
    @IBOutlet weak var myAvatarsView: UIView!
    @IBOutlet weak var myLiveAvatarCollectionView: UICollectionView!{
        didSet {
            myLiveAvatarCollectionView.delegate = self
            myLiveAvatarCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var extraStackView: UIStackView!
    @IBOutlet weak var extraLabelView: UIView!
    @IBOutlet weak var recordVideoView: UIView!
    @IBOutlet weak var personalizeView: UIView!
    @IBOutlet weak var mainAboveView: UIView!
    @IBOutlet weak var scrollHeight: UIView!
    @IBOutlet weak var textOuterView: IQPreviousNextView!
    @IBOutlet weak var scrolView: UIView!
    @IBOutlet weak var scroolView: UIScrollView!
    @IBOutlet weak var animationCollectionView: UICollectionView!{
        didSet {
            animationCollectionView.delegate = self
            animationCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var avatarCollectionView: UICollectionView!{
        didSet {
            avatarCollectionView.delegate = self
            avatarCollectionView.dataSource = self
            
        }
    }
    @IBOutlet weak var personalizeVM: UILabel!
    @IBOutlet weak var applyBtnOutlet: RoundButton!
    
    @IBOutlet weak var greetMsgOutlet: UIButton!
    @IBAction func greetMsgOutlet(_ sender: Any) {
    }
    @IBOutlet weak var personalizBtnOutlet: UIButton!
    @IBOutlet weak var skipVideoBtn: UIButton!
    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var interactionModelsView: UIView!
    @IBOutlet weak var avatarModelsView: UIView!
    @IBOutlet weak var giftModelsView: UIView!
    
    @IBOutlet weak var extraBtnOutlet: UIButton!
    @IBOutlet weak var CTPView: UIView!
    
    @IBOutlet weak var emailTFView: UIView!
    @IBOutlet weak var nameTFView: UIView!
    @IBOutlet weak var extraView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var ExtrasCollectionView: UICollectionView!
    
    @IBOutlet weak var messageTextView:             UITextView!
    @IBOutlet weak var totalCharges:                UILabel!
    
    @IBOutlet weak var applyCopoun:                 UIButton!
    @IBOutlet weak var copounTF:                    UITextField!
    @IBOutlet weak var addMessageButton:            RoundButton!
    @IBOutlet weak var paymentButton:               UIButton!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var recordVideoBtn: UIImageView!
    @IBOutlet weak var transactionalCostLabel: UILabel!
    
    //MARK: -View Lifecycles
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        setUIEachTime()
    }
      
    override func viewDidLoad() {
        super.viewDidLoad()
        calculateInitalModelPrices()
        SetUIOnce()
        setUPTapGestures()
        createNotifications()
        //delegates
        ExtrasCollectionView.delegate = self
        ExtrasCollectionView.dataSource = self
        tableView.delegate = self
        tableView.dataSource = self
        
        //loading gift models
        DispatchQueue.main.async {
            giftModels.data![0].isModelSelected = true
            self.ExtrasCollectionView.reloadData()
            self.avatarCollectionView.reloadData()
            self.animationCollectionView.reloadData()
        }
        
        for model in modelsInCart{
            selectedItemsArray.append(model.productId)
        }
        setViewOfTableView()
        
    }
    
    // MARK: - Mehtods
    func SetUIOnce() {
        self.mainAboveView.addShadowToView()
        self.mainAboveView.layer.cornerRadius = 5
        self.mainAboveView.clipsToBounds = true
        self.extraStackView.layer.cornerRadius = 5
        self.extraStackView.clipsToBounds = true
        extraLabelView.roundCornerss([.topLeft, .topRight], radius: 5)
        personalizeView.roundCornerss([.bottomLeft, .bottomRight], radius: 5)
        extraView.roundCornerss([.bottomLeft, .bottomRight], radius: 5)
        giftModelsView.layer.cornerRadius = 5
        giftModelsView.clipsToBounds = true
        avatarModelsView.layer.cornerRadius = 5
        avatarModelsView.clipsToBounds = true
        interactionModelsView.layer.cornerRadius = 5
        interactionModelsView.clipsToBounds = true
        messageTextView.textColor = .lightGray
        messageTextView.text = constantsStrings.cartScreenTextViewPlaceholder
        messageTextView.delegate = self
        copounTF.cornerRadiuss(radius: copounTF.frame.size.height/2, borderWidth: 1, color: UIColor.black.cgColor)
        nameTF.cornerRadiuss(radius: nameTF.frame.size.height/2, borderWidth: 1, color: UIColor.black.cgColor)
        emailTF.cornerRadiuss(radius: emailTF.frame.size.height/2, borderWidth: 1, color: UIColor.black.cgColor)
        applyBtnOutlet.layer.cornerRadius = paymentButton.frame.size.height/2
        applyBtnOutlet.clipsToBounds = true
        personalizBtnOutlet.roundCornerss([.bottomLeft, .topLeft], radius: personalizBtnOutlet.frame.size.height/2)
        extraBtnOutlet.roundCornerss([.bottomRight, .topRight], radius: extraBtnOutlet.frame.size.height/2)
    }
    
    func setUIEachTime() {
        paymentButton.layer.cornerRadius = paymentButton.frame.height / 2
        paymentButton.backgroundColor = UIColor(displayP3Red: 55/255, green: 176/255, blue: 131/255, alpha: 1)
        self.view.backgroundColor = UIColor.appColor(.CartViewBackgroundColor)
        self.extraBtnOutlet.backgroundColor =  UIColor(named: "light-custom-blue")
        self.extraBtnOutlet.setTitleColor(.black, for: .normal)
        self.personalizBtnOutlet.backgroundColor = UIColor(named: "dark-custom-blue")
        self.personalizBtnOutlet.setTitleColor(.white, for: .normal)
        self.extraView.isHidden = true
        self.messageTextView.isHidden = true
        self.recordVideoBtn.isHidden = true
        self.scrollViewHeight.constant = 300
        self.myLiveAvatarCollectionView.isHidden = true
        
        if ParslUtils.sharedInstance.getIfuserLogin() == true {
            emailTF.text = ParslUtils.sharedInstance.getuserEmail()
            loginTextView.isHidden = true
        }
        else {
            loginTextView.isHidden = false
        }
    }
    
    func setUPTapGestures() {
        let tapGestureRecoginzer = UITapGestureRecognizer(target: self, action: #selector(recordVideoBtnPressed))
        personalizeVM.isUserInteractionEnabled = true
        personalizeVM.addGestureRecognizer(tapGestureRecoginzer)
        
        let tapLogin = UITapGestureRecognizer(target: self, action: #selector(self.tapLoginPressed(_:)))
        loginBtn.addGestureRecognizer(tapLogin)
    }
    
    func setViewOfTableView() {
        let estimatedHeight = tableView.numberOfRows(inSection: 0) //You may need to modify as necessary
        let width = self.view.frame.size.width
        tableView.frame = CGRect(x: 0, y: 0, width: Int(width), height: estimatedHeight)
    }
    
    func createNotifications () {
        //payment token
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onPaymentTokenGenerated(_:)), name: NSNotification.Name(rawValue:PaymentObservers.paymentCompleteNotificationKey), object: nil)
        
        // keyboard hide and show
        NotificationCenter.default.addObserver(self, selector: #selector(CartViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
               NotificationCenter.default.addObserver(self, selector: #selector(CartViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
         
        NotificationCenter.default.addObserver(self, selector: #selector(showPaymenMethods(_:)), name: Notification.Name(rawValue: "ShowPaymentMehods"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(hideLoginDescription(_:)), name: Notification.Name(rawValue: "cartLoginSuccess"), object: nil)
    }
    // calculate prices before deleting or subtracting.
    private func calculateInitalModelPrices() {
        for i in 0..<modelsInCart.count {
            let modelIndex = i
            let modelQuantity = 1
            let modelPrice = modelsInCart[i].priceInfo.price ?? 0.0
            let finalPrice = modelPrice * Double(modelQuantity)
            
            let modelPriceStruct = ModelsQuantityStruct(modelIndex: modelIndex, modelQuantity: modelQuantity, modelPrice: modelPrice, totalPrice: finalPrice,isDiscounted: false)
            
            modelsPrices.append(modelPriceStruct)
            
            totalPrice += finalPrice
        }
        taxPrice = totalPrice * 0.15
        totalPrice = totalPrice + taxPrice
        totalCharges.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: totalPrice)
        self.finalCharges = ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: totalPrice)
        transactionalCostLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: taxPrice)
        
        
    }
    // calculate price after adding or deleting items.
     func recalculateFinalPrice(){
        totalPrice = 0
        for i in 0..<modelsPrices.count {
            totalPrice += modelsPrices[i].totalPrice
        }
        taxPrice = totalPrice * 0.15
        totalPrice = totalPrice + taxPrice
        totalCharges.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: totalPrice)
        transactionalCostLabel.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: taxPrice)
    }
    
    func avatarModule(_ message1: String!, forRedeem message2: String!) {
        print(message1)
        print(message2)
        avatarCharacter = message1
        avatarAnimation = message2
        
        self.view.sendSubviewToBack(self.unityView!)
        self.view.bringSubviewToFront(self.backgroundView)
    }
    
    @objc func tapLoginPressed(_ sender: UITapGestureRecognizer? = nil) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")as! LoginViewController
        vc.fromSendOrRedeem = "cart"
        self.present(vc, animated: true, completion: nil)
        
    }
    @objc func hideLoginDescription (_ notification: NSNotification){
        self.loginTextView.isHidden = true
        self.emailTF.text = ParslUtils.sharedInstance.getuserEmail()
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
            guard let userInfo = notification.userInfo else {return}
            guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
            let keyboardFrame = keyboardSize.cgRectValue
            if self.view.frame.origin.y == 0{
            self.view.frame.origin.y -= keyboardFrame.height-60
            }
        }
        @objc func keyboardWillHide(notification: NSNotification) {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y = 0
            }
        }
    
    @objc func showPaymenMethods (_ notification: NSNotification){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.paymentmethods()
        }
    }
    
    
    @objc private func onPaymentTokenGenerated(_ notification: NSNotification){
        ProgressHUD.dismiss()
        if let token = notification.userInfo?["token"] as? String {
            print("....\(token)")
            if let lastDigit = notification.userInfo?["lastFour"] as? String {
                if let cardType = notification.userInfo?["cardType"] as? String {
                    self.saveParslDetail(token: token,lastFour: lastDigit,cardType: cardType)
                }
                
            }
        }
        
    }
    
   func paymentmethods(){
       self.popupAlert(title: constantsStrings.chooseOptions, message: nil, actionTitles: ["Apple Pay","Pay with Stripe","Cancel"], actionStyle: [.default,.default,.cancel],
           actions: [
           {
           action in
               self.applePayTapped = true
               let paymentItem = PKPaymentSummaryItem.init(label: constantsStrings.appName, amount: NSDecimalNumber(value: Double(self.finalCharges)!))
                       let paymentNetworks = [PKPaymentNetwork.amex, .discover, .masterCard, .visa]
                       if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
                           let request = PKPaymentRequest()
                           request.currencyCode = "USD" // 1
                           request.countryCode = "US" // 2
                           request.merchantIdentifier = constantsStrings.appleMerchantIdentifier// 3
                           request.merchantCapabilities = PKMerchantCapability.capability3DS // 4
                           request.supportedNetworks = paymentNetworks // 5
                           request.paymentSummaryItems = [paymentItem] // 6
                           guard let paymentVC = PKPaymentAuthorizationViewController(paymentRequest: request) else {
                               self.displayDefaultAlert(title: constantsStrings.errorTitle, message: constantsStrings.errorMessage)

                               return
                           }
                           paymentVC.delegate = self
                           self.present(paymentVC, animated: true, completion: nil)
                       } else {
                           self.displayDefaultAlert(title: constantsStrings.errorTitle, message: constantsStrings.appleTransactionErrorMessage)
                       }
           },
           {
           action in
               self.applePayTapped = false
               let stripeVC = StipePaymentIntegration()
               self.navigationController?.present(stripeVC, animated: true, completion: nil)
           }
           ,nil], vc: self)
    }
    
    func setUpUnityView() {
        
        //load unity
        UnityEmbeddedSwift.showUnity()
        unityView = UnityEmbeddedSwift.getUnityView()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
          // self.setUpFloatingActionbutton()
            self.view.insertSubview(self.unityView!, at: 0)
      
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
                self.view.bringSubviewToFront(self.unityView!)
               

            })
        })
    }
    
    
    
    //MARK: -Functions
 
    @objc func video (
      _ videoPath: String,
      didFinishSavingWithError error: Error?,
      contextInfo info: AnyObject
    ) {
      let title = (error == nil) ? "Success" : "Error"
      let message = (error == nil) ? "Video was saved" : "Video failed to save"
      videoURL = videoPath
      let alert = UIAlertController(
        title: title,
        message: message,
        preferredStyle: .alert)
      alert.addAction(UIAlertAction(
        title: "OK",
        style: UIAlertAction.Style.cancel,
        handler: nil))
      present(alert, animated: true, completion: nil)
    }
    
    func dateTimeStatus(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yy hh:mm:ss a"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        if let dt = dateFormatter.date(from: date) {
            let userFormatter = DateFormatter()
            userFormatter.dateStyle = .medium // Set as desired
            userFormatter.timeStyle = .medium // Set as desired

            return userFormatter.string(from: dt)
        } else {
            return constantsStrings.unknownDate
        }
    }
    //device token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
    }
    func saveParslDetail(token: String,lastFour:String,cardType:String) {
        //Calculate tax price
        
        ProgressHUD.show()
        taxPrice = (totalPrice * 15) / 100
        priceWithoutTax = totalPrice - taxPrice
        
        //End Calculation of tax price
        
        var isVideoRecorded = 0
        
        if videoURL != "" {
            isVideoRecorded = 1
        }
        var itemDict = [AnyObject]()
        for i in 0..<modelsInCart.count {
            let item = ["product_id": modelsInCart[i].productId as AnyObject,
                        "quantity": modelsPrices[i].modelQuantity as AnyObject]
            
            itemDict.append(item as AnyObject)
        }
        print(itemDict)
        if ParslUtils.sharedInstance.getIfuserLogin() == false {
        if emailTF.text == ParslUtils.sharedInstance.getuserEmail(){
            ParslUtils.sharedInstance.saveUserEmail(emailTF.text!)
        
        } else {
            ParslUtils.sharedInstance.saveUserOldEmail(ParslUtils.sharedInstance.getuserEmail())
            ParslUtils.sharedInstance.saveUserEmail(self.emailTF.text!)
               }
              }
        let timestamp = Date().currentTimeMillis()
        var senderData = [String: Any]()
        if self.applePayTapped {
             senderData       = ["card_number":"6321",
                                    "card_type":"Apple Pay",
                                    "sender_device_token": ParslUtils.sharedInstance.DEVICE_TOKEN,
                                    "sender_device_id": deviceID,
                                    "email":emailTF.text ?? "",
                                    "sender_name": nameTF.text ?? ""
                                   ] as [String: Any]
        } else {
         senderData       = ["card_number":lastFour,
                                "card_type":cardType,
                                "sender_device_token": ParslUtils.sharedInstance.DEVICE_TOKEN,
                                "sender_device_id": deviceID,
                                "email":emailTF.text ?? "",
                                "sender_name": nameTF.text ?? ""
                               ] as [String: Any]
            }
        
        let priceRelated    = ["price": priceWithoutTax,
                               "currency": "usd",
                               "total_price": totalPrice] as [String : Any]
        
        
        if messageTextView.text == constantsStrings.cartScreenTextViewPlaceholder || messageTextView.text == "" {
            messageTextView.text = constantsStrings.cartScreenTextViewPlaceholder
        }else{
            messageTextView.text  = messageTextView.text
        }
        var characterkey = ""
        var animationKey = ""
        if avatarSelectedIndex != -1{
            characterkey = "avatar_\(giftModels.avatars![avatarSelectedIndex].chracter_key!)"
        }
        if animationSelectedIndex != -1{
            animationKey = giftModels.animations[animationSelectedIndex].animation_key!
        }
        
  
        let data :Dictionary<String,Any>           = ["selected_products":itemDict,
                               "gift_model":giftModels.data![giftWrapSelectedIndex].productID,
                               "sender_data":senderData,
                               "text_msg":messageTextView.text ?? "",
                               "parsl_timestamp":timestamp,
                               "parsl_stripe_token":token,
                               "have_video":isVideoRecorded,
                               "avatar_chracter": characterkey as String ,
                               "avatar_animation": animationKey as String,
                               "price_related": priceRelated] as [String : Any]
        let parameters      = ["app_id": "parsl_ios", "old_mail": ParslUtils.sharedInstance.getuserOldEmail(), "data": data] as [String : Any]
        print(parameters)
        let url = constantsUrl.saveParslDetailsUrl
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        do {
            
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = session.dataTask(with: request as URLRequest, completionHandler: { [self] data, response, error in
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            //applinks:parsl.io/604e127f66c4bc23831f1fcc
            do {
                ProgressHUD.dismiss()
                if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                    saveParsl = try JSONDecoder().decode(SaveParsl.self, from: data)
                    print(saveParsl!)
                    if self.videoURL != ""{
                        DispatchQueue.main.async {
                            self.uploadParslVideo()
                        }
                    }
                    DispatchQueue.main.async {
                        print(parameters)
                        print(saveParsl!)
                        guard let message = saveParsl.message else {
                            return
                        }
                        if message.contains("Price cannot be zero"){
                            showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.unsuccessfulTransactionMessage)
                        } else {
                            self.cartDelegate?.cartHasBeenCleared()
                            UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "ReLoadUnity", message: "")
                            isOrderFlowCompleted = true
                            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShareViewController") as! ShareViewController
                            vc.amount = "$" + String(saveParsl.data!.totalPrice!)
//                            vc.amount = self.finalCharges
                            vc.parslOTP = saveParsl.data!.parslOtp!
                            vc.parslURL = saveParsl.data!.parslURL!
                            vc.transaction = saveParsl.data!.transectionID!
                            vc.cardNumber = saveParsl.data!.cardNumber!
                            vc.transactionCost = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: taxPrice)
                            vc.modelsPrices = modelsPrices
                            vc.modelsInCart = modelsInCart
                            if messageTextView.text == constantsStrings.cartScreenTextViewPlaceholder || messageTextView.text == "" {
                                vc.msgText = constantsStrings.cartScreenTextViewPlaceholder + nameTF.text! + "."
                            }else{
                                vc.msgText = messageTextView.text
                            }
                            
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    
                }
            } catch let error {
                ProgressHUD.dismiss()
                print(error)
                showAlert(withTitle: constantsStrings.appName, withMessage: constantsStrings.unsuccessfulTransactionMessage)
            }
        })
        task.resume()
    }

    func uploadParslVideo(){
        let url = constantsUrl.uploadParslVideoUrl
        print(saveParsl.data!.parslID!)
        Alamofire.upload(multipartFormData: { multipart in
            multipart.append(NSURL(fileURLWithPath: self.videoURL) as URL, withName: "video_msg")

            multipart.append("\(self.saveParsl.data!.parslID!)".data(using: .utf8)!, withName :"parsl_id")
        }, to: url, method: .post, headers: nil) { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.response { answer in
                    print(answer.response!)
                    print("statusCode: \(String(describing: answer.response?.statusCode))")
                }
                upload.uploadProgress { progress in
                    //call progress callback here if you need it
                    print(progress)
                }
            case .failure(let encodingError):
                print("multipart upload encodingError: \(encodingError)")
            }
        }
    }
    // get the details of the copoun.
    func getCopounDetails(){
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        
        let data = ["coupon_code":self.copounTF.text!,
                    "selected_products":selectedItemsArray,
                    "date_time":dateString] as [String : Any]
        let parameters = ["app_id": "parsl_ios",
                          "data": data] as [String : Any]
        
        print(parameters)
        let url = constantsUrl.checkCopounDetailurl
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = session.dataTask(with: request as URLRequest, completionHandler: { [self] data, response, error in
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if (try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]) != nil {
                    
                    let decoder = JSONDecoder()
                    self.couponResponse = try? decoder.decode(CouponResponse.self, from: data)
                    dump(self.couponResponse)
                    let relief = couponResponse.data!.copounRelief
                    var discountedPrice = 0.0
                    var nonDiscountedPrice = 0.0
                    if couponResponse.data?.copounStatus == true{
                        
                        self.isAnyDiscount = true //coupon is valid
         
                            
                            let item = couponResponse.data!.approvedProducts!
                            if item.count > 0 {
                               
                                //discounted price
                                
                                for product in item{
                                    let index = modelsInCart.firstIndex(where: {$0.productId == product})
                                    var totalPrice = modelsPrices[index!].totalPrice
                                    
                                    if couponResponse.data?.copounReliefType == "price_relief"{
                                        totalPrice = totalPrice! -  (Double(relief!))
                                    
                                    }else{
                                        
                                        totalPrice = totalPrice! - (( (Double(relief!)) / 100) * totalPrice!)
                                    }
                                    
                                    modelsPrices[index!].totalPrice = totalPrice
                                    modelsPrices[index!].isDiscounted = true
                                    discountedPrice = totalPrice! + discountedPrice
                                   // self.calculateDiscountedPrice(indexPath: index!, discountedPrice:totalPrice!)
                    
                                }
                            }
                        
                        //denied products
                        let deniedItems = couponResponse.data!.deniedProducts!
                        if deniedItems.count > 0{
                            
                            for product in deniedItems{
                                let index = modelsInCart.firstIndex(where: {$0.productId == product})
                                let totalPrice = modelsPrices[index!].totalPrice
                                modelsPrices[index!].isDiscounted = false
                                nonDiscountedPrice = totalPrice! + nonDiscountedPrice
                                
                            }
                        }
                                    priceWithoutTax = discountedPrice + nonDiscountedPrice
                                   // priceWithoutTax = pricesList.reduce(0,+)
                                    taxPrice = ( 15.0 / 100) * priceWithoutTax
                                    totalPrice = priceWithoutTax + taxPrice
                                    DispatchQueue.main.async {
                                        self.transactionalCostLabel.text = "$\(String(format: "%.2f",taxPrice))"
                                        self.totalCharges.text = "$\(String(format: "%.2f",totalPrice))"
                                    }
                                    
                                }else{
                                    showAlert(withTitle: constantsStrings.copounErrorTitle, withMessage: constantsStrings.copounErrorMessage)
                                }
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                           
                        }
                    
                    else{
                        showAlert(withTitle: constantsStrings.copounErrorTitle, withMessage: constantsStrings.copounErrorMessage)
                    }
                    
                }
         catch let  error {
                print(error)
            }
        })
        task.resume()
    }
    
    //MARK: -IBActions
    // show message field
    @IBAction func showMsgView(_ sender: Any) {
        self.textOuterView.isHidden = false
        self.messageTextView.isHidden = false
        self.scrollViewHeight.constant = 800
       
    
    }
    
    // copoun apply
    @IBAction func applyCopounTapped(_ sender: Any) {
        isAnyDiscount = false
        if self.copounTF.text != ""{
            self.getCopounDetails()
        }
        else
        {
            showAlert(withTitle: constantsStrings.copounMissingFieldTitle, withMessage: constantsStrings.copounMissingFieldMessage)
        }
    }
    @IBAction func addMessageTapped(_ sender: Any) {
        self.popupAlert(title: constantsStrings.typeMessageOrRecordMessage, message: nil, actionTitles: ["Type Message","Record Video","Cancel"], actionStyle: [.default,.default,.cancel],
        actions: [{ action in
            self.messageTextView.isHidden = false
            
            
    },{ action in
        VideoHelper.startMediaBrowser(delegate: self, sourceType: .camera)
    },nil], vc: self)
    }
    @IBAction func paymentTapped(_ sender: Any) {
        if nameTF.text!.isEmpty || nameTF.text == "" || nameTF.text == " " {
            self.view.makeToast(constantsStrings.enterName)
            return
        }
        if emailTF.text!.isEmpty || emailTF.text == "" || emailTF.text == " " {
            self.view.makeToast(constantsStrings.enterEmail)
            return
        }
        if !isValidEmail(emailTF.text!) {
            self.view.makeToast(constantsStrings.enterValidEmail)
            return
        }
        self.paymentmethods()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func sendAvatarTapped(_ sender: Any) {
        //will open unity with avatar scene
        setUpUnityView()
        UnityEmbeddedSwift.sendUnityMessage("ObjectSpawner", methodName: "CreateAvatarScreen", message: "")
      
    }
    
    @IBAction func showExtraView(_ sender: Any) {
//        scrollHeight.heightAnchorshow.constraint(equalToConstant: 1100).isActive = true
        self.personalizeView.isHidden = true
        self.extraView.isHidden = false
        self.scrollViewHeight.constant = 700
        self.extraBtnOutlet.backgroundColor = UIColor(named: "dark-custom-blue")
        self.personalizBtnOutlet.backgroundColor = UIColor(named: "light-custom-blue")
        self.personalizBtnOutlet.setTitleColor(.black, for: .normal)
        self.extraBtnOutlet.setTitleColor(.white, for: .normal)
        if messageTextView.text == "" || messageTextView.text == constantsStrings.parsForYou{
            messageTextView.isHidden = true
            textOuterView.isHidden = true
            greetMsgOutlet.isHidden = false
        }
        else{
            messageTextView.isHidden = false
            self.scrollViewHeight.constant = 800
        }
        if isVideoRecorded == false{
            self.recordVideoView.isHidden = true
        }
        else{
            self.recordVideoView.isHidden = false
            self.scrollViewHeight.constant = 800
        }
        
        
    }
    
    @IBAction func showPDataView(_ sender: Any) {
//        scrollHeight.heightAnchor.constraint(equalToConstant: 250).isActive = true
        self.personalizeView.isHidden = false
        self.extraView.isHidden = true
        self.scrollViewHeight.constant = 300
        self.extraBtnOutlet.backgroundColor =  UIColor(named: "light-custom-blue")
        self.personalizBtnOutlet.backgroundColor = UIColor(named: "dark-custom-blue")
        self.extraBtnOutlet.setTitleColor(.black, for: .normal)
        self.personalizBtnOutlet.setTitleColor(.white, for: .normal)
    }
     
}
 

//MARK: -TableView Protocols
protocol CartCellClickDelegate: class {
    func plusButtonTapped(cell: CartTableViewCell, sender : Any)
    func minusButtonTapped(cell: CartTableViewCell, sender : Any)
    func deleteButtonTapped(cell: CartTableViewCell, sender : Any)
    
}
 

//video Message
extension CartViewController: UIImagePickerControllerDelegate { 
    func imagePickerController(
      _ picker: UIImagePickerController,
      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
    ) {
      dismiss(animated: true, completion: nil)

      guard
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as? String,
        mediaType == (kUTTypeMovie as String),
        let url = info[UIImagePickerController.InfoKey.mediaURL] as? URL,
        UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path)
        else { return }
 
        
        guard let data = try? Data(contentsOf: url) else {
            return
        }

        print("File size before compression: \(Double(data.count / 1048576)) mb")
        
        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + UUID().uuidString + ".mp4")
        compressVideo(inputURL: url as URL,
                      outputURL: compressedURL) { exportSession in
            guard let session = exportSession else {
                return
            }
//            let url = NSURL(string: compressedURL)
            let urll = compressedURL
            let parhUrl = urll.path
            if parhUrl == ""{
                print("url is empty......")
            }
            else{
                print("url path is \(parhUrl)")
            }
            
            switch session.status {
            case .unknown:
                break
            case .waiting:
                break
            case .exporting:
                break
            case .completed:
                guard let compressedData = try? Data(contentsOf: compressedURL) else {
                    return
                }
                self.isVideoRecorded = true
                self.videoURL = compressedURL.path
                DispatchQueue.main.async {
                    self.extraView.isHidden = false
                    self.recordVideoBtn.isHidden = false
                    self.recordVideoView.isHidden = false
                    self.isVideoRecorded = true
                    self.extraBtnOutlet.backgroundColor = UIColor(named: "dark-custom-blue")
                    self.extraBtnOutlet.setTitleColor(.white, for: .normal)
                    self.personalizBtnOutlet.backgroundColor = UIColor(named: "light-custom-blue")
                    self.personalizBtnOutlet.setTitleColor(.black, for: .normal)
                    self.personalizeView.isHidden = true
                    self.CTPView.isHidden = false
                    self.nameTFView.isHidden = false
                    self.emailTFView.isHidden = false
                    if self.messageTextView.text == "" || self.messageTextView.text == constantsStrings.parsForYou{
                        self.messageTextView.isHidden = true
                        self.textOuterView.isHidden = true
                        self.greetMsgOutlet.isHidden = false
                    }
                    else{
                        self.messageTextView.isHidden = false
                        self.textOuterView.isHidden = false
                        if self.scrollViewHeight.constant == Double("800"){
                            self.scrollViewHeight.constant = 900
                        }
                        else{
                            self.scrollViewHeight.constant = 800
                        }
                    }
                }
                print("File size after compression: \(Double(compressedData.count / 1048576)) mb")
            case .failed:
                break
            case .cancelled:
                DispatchQueue.main.async {
                    self.extraView.isHidden = false
                    self.recordVideoBtn.isHidden = true
                    self.recordVideoView.isHidden = true
                    self.extraBtnOutlet.backgroundColor = UIColor(named: "dark-custom-blue")
                    self.personalizBtnOutlet.backgroundColor = UIColor(named: "light-custom-blue")
                }
                break
            @unknown default:
                debugPrint("video canceled ....")
                break
            }
        }
        
    }
    
}

extension CartViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        let ph_TextColor: UIColor = .lightGray
        if messageTextView.textColor == ph_TextColor && messageTextView.isFirstResponder {
                self.messageTextView.text = nil
                self.messageTextView.textColor = .black
            }
    }
    
    func textViewDidEndEditing (_ textView: UITextView) {
        if messageTextView.text.isEmpty || messageTextView.text == "" {
            messageTextView.textColor = .lightGray
            messageTextView.text = constantsStrings.parsForYou
        }
    }
    
    @objc private func recordVideoBtnPressed(){
        VideoHelper.startMediaBrowser(delegate: self, sourceType: .camera)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.personalizeView.isHidden = false
            self.nameTFView.isHidden = false
            self.emailTFView.isHidden = false
            self.CTPView.isHidden = false
        })
    }
}
extension CartViewController: AVCaptureFileOutputRecordingDelegate {
    
    func fileOutput(_ output: AVCaptureFileOutput,
                        didFinishRecordingTo outputFileURL: URL,
                        from connections: [AVCaptureConnection],
                        error: Error?) {
            guard let data = try? Data(contentsOf: outputFileURL) else {
                return
            }

            print("File size before compression: \(data.count)")
            print("File size before compression: \(Double(data.count / 1048576)) mb")

            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + UUID().uuidString + ".mp4")
            compressVideo(inputURL: outputFileURL as URL,
                          outputURL: compressedURL) { exportSession in
                guard let session = exportSession else {
                    return
                }

                switch session.status {
                case .unknown:
                    break
                case .waiting:
                    break
                case .exporting:
                    break
                case .completed:
                    guard let compressedData = try? Data(contentsOf: compressedURL) else {
                        return
                    }
                    
                    print("File size after compression: \(compressedData.count)")

                    print("File size after compression: \(Double(compressedData.count / 1048576)) mb")
                case .failed:
                    break
                case .cancelled:
                   
                    break
                @unknown default:
                    break
                }
            }
        }


        func compressVideo(inputURL: URL,
                           outputURL: URL,
                           handler:@escaping (_ exportSession: AVAssetExportSession?) -> Void) {
            let urlAsset = AVURLAsset(url: inputURL, options: nil)
            guard let exportSession = AVAssetExportSession(asset: urlAsset,
                                                           presetName: AVAssetExportPresetMediumQuality) else {
                handler(nil)

                return
            }

            exportSession.outputURL = outputURL
            exportSession.outputFileType = .mp4
            exportSession.exportAsynchronously {
                handler(exportSession)
            }
        }
}



// apple pay payment delegates
extension CartViewController: PKPaymentAuthorizationViewControllerDelegate {
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        dismiss(animated: true, completion: nil)
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        dismiss(animated: true, completion: nil)
        STPAPIClient.shared().createToken(with: payment, completion: { (token, error) in
            guard let token = token else { 
                print(error)
            return
            }
            print(token.tokenId)
            self.saveParslDetail(token: token.tokenId, lastFour: "", cardType: "")
        })
}
}
