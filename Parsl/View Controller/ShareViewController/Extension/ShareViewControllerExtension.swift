//
//  ShareViewControllerExtension.swift
//  Parsl
//
//  Created by M Zaryab on 28/02/2022.
//

import Foundation
import UIKit


// MARK: - Extensions

// MARK: - TableVeiw
extension ShareViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + modelsInCart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeadingLabelsCell")!
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductItemsCell") as! ProductItemsCell
            let index = indexPath.row - 1
            cell.name.text = modelsInCart[index].basicInfo.name
            let url = URL(string: modelsInCart[index].modelFile.thumbnail)
            cell.thumbnail.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            cell.qty.text = String(modelsPrices[index].modelQuantity)
            cell.subTotal.text = "$" + ParslUtils.sharedInstance.doubleToRoundedOutput(doubleVal: modelsPrices[index].totalPrice)
            return cell
        }
    }
}
