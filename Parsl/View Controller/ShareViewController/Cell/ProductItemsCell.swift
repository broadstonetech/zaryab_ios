//
//  ProductItemsCell.swift
//  Parsl
//
//  Created by broadstone on 21/05/2021.
//
// products item tableview cell class.
import UIKit

class ProductItemsCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var qty: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
