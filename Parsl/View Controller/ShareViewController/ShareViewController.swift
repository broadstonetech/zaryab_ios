//
//  ShareViewController.swift
//  Parsl
//
//  Created by Billal on 08/03/2021.
// this class let the user to view the details of the bought PRASLs and enable user to share the PRASLs

import Foundation
import UIKit

class ShareViewController: PaymentsBaseClass{
    
    // MARK: - Properties
    var parslURL,parslOTP,transaction,amount,cardNumber,transactionCost, msgText:String!
    var modelsInCart = [ModelDataStruct]()
    var modelsPrices = [ModelsQuantityStruct]()
    
    var isShareBtnClicked = false
    // MARK: - IBoutlets
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var transactionID: UILabel!
    @IBOutlet weak var customizeButton: UIButton!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var msgForRecepient: UITextView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var transactionalCostLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var visitLinkLabel: UILabel!
    @IBOutlet weak var otpCodeLabel: UILabel!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        //msgForRecepient.text = "Hey, here is the Parsl visit link \(parslURL!) or use code \(parslOTP!) to unwrap your Parsl"
        transactionalCostLabel.text = transactionCost
        //msgLabel.text = "Visit link \(parslURL!) or use PARSL redeem code \(parslOTP!) to unwrap your PARSL."
        visitLinkLabel.text = parslURL
        otpCodeLabel.text = parslOTP
        transactionID.text = "\(transaction!)"
        amountLabel.text = "\(amount!)"

        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - IBoutlets
    @IBAction func shareButton(_ sender: Any) {
        if isShareBtnClicked {
            let alert = UIAlertController(title: constantsStrings.shareParslTitle, message: constantsStrings.shareParslMessage, preferredStyle: .alert)
            let share = UIAlertAction(title: constantsStrings.shareYes, style: .default, handler: { action in
                self.openShareActivity()
            })
            let cancel = UIAlertAction(title: constantsStrings.shareNo, style: .cancel, handler: { action in
                self.dismiss(animated: true, completion: nil)
                if let navigator = self.navigationController {
                    navigator.popViewController(animated: false)
                    navigator.popViewController(animated: true)
                }
            })
            alert.addAction(share)
            alert.addAction(cancel)
            //        alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
        }else {
            openShareActivity()
        }
        isShareBtnClicked = true
        
    }
    
    
    
    @IBAction func mainMenu(_ sender: Any) {
        print(parslOTP!)
        if let navigator = self.navigationController {
            navigator.popViewController(animated: false)
            navigator.popViewController(animated: true)
        }
    }
    // MARK: - Mehthods
    private func openShareActivity() {
        //let parslWebsite = "https://www.parsl.io"
        let parslWebsite = constantsUrl.ParslWebsite
        //let shareableText = msgText + "\n" + "Visit link \(parslWebsite) and use PARSL redeem code \(parslOTP!) to unwrap your PARSL."
        let shareableText = "\(constantsStrings.shareAbletext) \(parslOTP!) to unwrap your PARSL."
        let activityVC = UIActivityViewController(activityItems: [shareableText], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
}
