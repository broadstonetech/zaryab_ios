//
//  NativeCallProxy.m
//  UnityNativeIos
//
//  Created by Mufaza on 05/07/2021.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import "NativeCallProxy.h"

@implementation FrameworkLibAPI

id<NativeCallsProtocol> api = NULL;
+(void) registerAPIforNativeCalls:(id<NativeCallsProtocol>) aApi
{
    api = aApi;
}

@end

extern "C"
{   //when cart icon tapped in unity
    void sendMessageToMobileApp(const char* message)
    {
        return [api sendMessageToMobileApp:[NSString stringWithUTF8String:message]];
    }

    void destroyUnity(const char* message)
   {
        return [api destroyUnity:[NSString stringWithUTF8String:message]];
   }

    void showContainerView(const char* message)
   {
        return [api showContainerView:[NSString stringWithUTF8String:message]];
   }
    void sendMessageOnReload(const char* message)
   {
        return [api sendMessageOnReload:[NSString stringWithUTF8String:message]];
   }
    void modelDownloaded(const char* message)
   {
        return [api modelDownloaded:[NSNumber numberWithBool:message]];
   }
     void modelDownloadingDone(const char* message)
   {
        return [api modelDownloadingDone:[NSString stringWithUTF8String:message]];
   }
     void avatarModule(const char* message1,const char* message2)
   {
          return [api avatarModule:[NSString stringWithUTF8String:message1] forRedeem:[NSString stringWithUTF8String:message2]];
   }
   void modelProjected(const char* message)
   {
       return [api modelProjected:[NSNumber numberWithBool:message]];
   }
   void avatarProjected(const char* message)
   {
       return [api avatarProjected:[NSNumber numberWithBool:message]];
   }

}