﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.UI.Button SwitchPointCloudVisualizationMode::get_toggleButton()
extern void SwitchPointCloudVisualizationMode_get_toggleButton_m5FBFEDEBC9B371B76804637D7C8091E26C3C399F (void);
// 0x00000002 System.Void SwitchPointCloudVisualizationMode::set_toggleButton(UnityEngine.UI.Button)
extern void SwitchPointCloudVisualizationMode_set_toggleButton_mD9FC8EA7EAEE800F60DAFBCCAF19636BE29ECE77 (void);
// 0x00000003 UnityEngine.UI.Text SwitchPointCloudVisualizationMode::get_log()
extern void SwitchPointCloudVisualizationMode_get_log_m6E4EB81C73C81360C40431382CD6EB0AC2C59763 (void);
// 0x00000004 System.Void SwitchPointCloudVisualizationMode::set_log(UnityEngine.UI.Text)
extern void SwitchPointCloudVisualizationMode_set_log_m564736BD24430DB94CCF144DABD0B6036777832C (void);
// 0x00000005 UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode SwitchPointCloudVisualizationMode::get_mode()
extern void SwitchPointCloudVisualizationMode_get_mode_m20AA95EDFB2FFB095D258B2C38987A3BC1227616 (void);
// 0x00000006 System.Void SwitchPointCloudVisualizationMode::set_mode(UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode)
extern void SwitchPointCloudVisualizationMode_set_mode_m280F90EBF742145758E7D52D0C84CA5AC16F0075 (void);
// 0x00000007 System.Void SwitchPointCloudVisualizationMode::SwitchVisualizationMode()
extern void SwitchPointCloudVisualizationMode_SwitchVisualizationMode_m7CA339DEB4B8A40DFDFB8C6D79FC535048BAC278 (void);
// 0x00000008 System.Void SwitchPointCloudVisualizationMode::OnEnable()
extern void SwitchPointCloudVisualizationMode_OnEnable_m0AE60A40E7545E590E1ACDF4FFEA32912DC1C11B (void);
// 0x00000009 System.Void SwitchPointCloudVisualizationMode::OnPointCloudsChanged(UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs)
extern void SwitchPointCloudVisualizationMode_OnPointCloudsChanged_mDA9644178AA492F76221956ED387EF7BC7C5EE55 (void);
// 0x0000000A System.Void SwitchPointCloudVisualizationMode::SetMode(UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode)
extern void SwitchPointCloudVisualizationMode_SetMode_m4BC28FF39AF7600381B725286D7BCFBFE46F85AC (void);
// 0x0000000B System.Void SwitchPointCloudVisualizationMode::.ctor()
extern void SwitchPointCloudVisualizationMode__ctor_m02EAEB06B7F42B232A6280B8E7872F6596BF35B0 (void);
// 0x0000000C System.Void CSharpscaling::Update()
extern void CSharpscaling_Update_m522915C0C9FD444E0BFEAD25993C4C208260247C (void);
// 0x0000000D System.Void CSharpscaling::.ctor()
extern void CSharpscaling__ctor_mED682225400E498E39AE9C4EA60E4C4B7C770197 (void);
// 0x0000000E System.Void onClickForScaling::OnMouseDown()
extern void onClickForScaling_OnMouseDown_mA0759701B4EF289EB795C08A59E104DF6F436B6B (void);
// 0x0000000F System.Void onClickForScaling::.ctor()
extern void onClickForScaling__ctor_m5190794CA361BAA49FC175F6ACC0C60A0A29AB60 (void);
// 0x00000010 System.Void rotateController::Update()
extern void rotateController_Update_mB71E56B5BD55A3F24C437AECFD9242ED257BA1BF (void);
// 0x00000011 System.Void rotateController::OnMouseDown()
extern void rotateController_OnMouseDown_m284DC0D713B8D7AD1742183B753D6EC45B72A26D (void);
// 0x00000012 System.Void rotateController::OnMouseUp()
extern void rotateController_OnMouseUp_m6EF7A213E4D61D029634814A82DE1727BB0D0564 (void);
// 0x00000013 System.Void rotateController::.ctor()
extern void rotateController__ctor_mABBEC5D25C68F797F37D0C8F6C847FE9D1714078 (void);
// 0x00000014 System.Void ShowCanvasOnBuildingModels::Update()
extern void ShowCanvasOnBuildingModels_Update_mC7975878B85B48DDF5287D78C461375018D2910E (void);
// 0x00000015 System.Collections.IEnumerator ShowCanvasOnBuildingModels::WaitForPanelOff()
extern void ShowCanvasOnBuildingModels_WaitForPanelOff_m6D114493F85D3F312D4261DA99EB95B9A6809FD8 (void);
// 0x00000016 System.Void ShowCanvasOnBuildingModels::RemoveFromScene()
extern void ShowCanvasOnBuildingModels_RemoveFromScene_m7C323DBC67DA3AB34DBA23715AA91FA25248769F (void);
// 0x00000017 System.Void ShowCanvasOnBuildingModels::AddToCartPanelPop()
extern void ShowCanvasOnBuildingModels_AddToCartPanelPop_mEAC53C9A87BA80D5679F570B40152849E4693587 (void);
// 0x00000018 System.Void ShowCanvasOnBuildingModels::.ctor()
extern void ShowCanvasOnBuildingModels__ctor_m743D86C272E344B8CC84375E64AC9B5B2884950A (void);
// 0x00000019 System.Void ShowCanvasOnBuildingModels/NativeAPI::sendMessageToMobileApp(System.String)
extern void NativeAPI_sendMessageToMobileApp_m6D59B0C98E3CCB11CB49100B76FC514CA39DC8E1 (void);
// 0x0000001A System.Void ShowCanvasOnBuildingModels/NativeAPI::destroyUnity(System.String)
extern void NativeAPI_destroyUnity_mD4F74726CD21B7384C92910C19ABD7A217D230FE (void);
// 0x0000001B System.Void ShowCanvasOnBuildingModels/NativeAPI::.ctor()
extern void NativeAPI__ctor_mA377280AE0A5718846511B99AE5B2AF5B8940AFB (void);
// 0x0000001C System.Void ShowCanvasOnBuildingModels/<WaitForPanelOff>d__4::.ctor(System.Int32)
extern void U3CWaitForPanelOffU3Ed__4__ctor_m098F18885B7958CB1FE64B250D907C74CB508CF6 (void);
// 0x0000001D System.Void ShowCanvasOnBuildingModels/<WaitForPanelOff>d__4::System.IDisposable.Dispose()
extern void U3CWaitForPanelOffU3Ed__4_System_IDisposable_Dispose_m42924C65565FE2CEF1DFAC81769C3AFF6A66A90F (void);
// 0x0000001E System.Boolean ShowCanvasOnBuildingModels/<WaitForPanelOff>d__4::MoveNext()
extern void U3CWaitForPanelOffU3Ed__4_MoveNext_m161C7D218C0726D4B59BE03A0ED2AC01DFE29CB3 (void);
// 0x0000001F System.Object ShowCanvasOnBuildingModels/<WaitForPanelOff>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForPanelOffU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m176E4AD63DF89158686C677B760E62A087A42DD5 (void);
// 0x00000020 System.Void ShowCanvasOnBuildingModels/<WaitForPanelOff>d__4::System.Collections.IEnumerator.Reset()
extern void U3CWaitForPanelOffU3Ed__4_System_Collections_IEnumerator_Reset_m704C805470B5F9028E57EDABACBEE1FA01DE2E39 (void);
// 0x00000021 System.Object ShowCanvasOnBuildingModels/<WaitForPanelOff>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForPanelOffU3Ed__4_System_Collections_IEnumerator_get_Current_mE10B2223B7CC00859CAD1EE6E9924CC94A192C79 (void);
// 0x00000022 System.Void Activate::Start()
extern void Activate_Start_m08C8888DB0894B521E7A391FE19387E224C5C10A (void);
// 0x00000023 System.Void Activate::Update()
extern void Activate_Update_mA3BB4F4A0B7BBFE9B6F62B2FB0086021D43760D4 (void);
// 0x00000024 System.Void Activate::assss()
extern void Activate_assss_mA1AC697A6E9A852727107D362EEEF8DA3DCCE5FD (void);
// 0x00000025 System.Void Activate::.ctor()
extern void Activate__ctor_mFDE6E64A8AE313D7BCEC986811153A8443E2306C (void);
// 0x00000026 System.Void AddToCartScript::AddToCartItem()
extern void AddToCartScript_AddToCartItem_m69D02A77BE080494C9A091DB293A2E725AA3E65E (void);
// 0x00000027 System.Void AddToCartScript::.ctor()
extern void AddToCartScript__ctor_mDA5D0F9A8DAFE087715F080CB77A9C4CA8FF9D7B (void);
// 0x00000028 System.Void Anim::Update()
extern void Anim_Update_m246B9B6F3A5A51531603888DD2E0C56FDB496B46 (void);
// 0x00000029 System.Void Anim::playanimation()
extern void Anim_playanimation_m68C20969E30DCA86C059A75C74B6A62FFDF60A1B (void);
// 0x0000002A System.Void Anim::stand()
extern void Anim_stand_m9B3427C6CB70E7B4326709D1318868D5C66E9B21 (void);
// 0x0000002B System.Collections.IEnumerator Anim::delay()
extern void Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3 (void);
// 0x0000002C System.Void Anim::.ctor()
extern void Anim__ctor_mD06BEBFB08FB944CC36A2F70EDCD029A8B0DD0D8 (void);
// 0x0000002D System.Void Anim/<delay>d__3::.ctor(System.Int32)
extern void U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD (void);
// 0x0000002E System.Void Anim/<delay>d__3::System.IDisposable.Dispose()
extern void U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A (void);
// 0x0000002F System.Boolean Anim/<delay>d__3::MoveNext()
extern void U3CdelayU3Ed__3_MoveNext_mFDE630DACFE2D69CE5A5DE03FE60482B185AFECF (void);
// 0x00000030 System.Object Anim/<delay>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26 (void);
// 0x00000031 System.Void Anim/<delay>d__3::System.Collections.IEnumerator.Reset()
extern void U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521 (void);
// 0x00000032 System.Object Anim/<delay>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D (void);
// 0x00000033 System.Void AnimateModel::Start()
extern void AnimateModel_Start_m120DD4C0901DC917E52C9F1215768D3E5E6FD3BC (void);
// 0x00000034 System.Void AnimateModel::Update()
extern void AnimateModel_Update_m6E878217EC241361AFD3591C94B49632761387F5 (void);
// 0x00000035 System.Void AnimateModel::.ctor()
extern void AnimateModel__ctor_m828FACE086F16333DCEBEC48AD64F71755330922 (void);
// 0x00000036 System.Void API::GetBundleObject(System.String,UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>,UnityEngine.Transform)
extern void API_GetBundleObject_m1F40F48A9C8EC9F9A629DE6EF5DA168D632116A7 (void);
// 0x00000037 System.Collections.IEnumerator API::WaitForNetwork()
extern void API_WaitForNetwork_m5DDF6877C90ACCE6954C6C0ED29AB9F5464F5AA5 (void);
// 0x00000038 System.Collections.IEnumerator API::WaitForValidAsset()
extern void API_WaitForValidAsset_mC11999EEFDC00D721B2E162BB0F29083221E1B36 (void);
// 0x00000039 System.Collections.IEnumerator API::GetDisplayBundleRoutine(System.String,UnityEngine.Events.UnityAction`1<UnityEngine.GameObject>,UnityEngine.Transform)
extern void API_GetDisplayBundleRoutine_m8DB6214089AC5BDE847DB16A7D56D52AEA074546 (void);
// 0x0000003A System.Void API::save(System.Byte[],System.String)
extern void API_save_m5D33F677192B68C57048F97AB662736E4765F9FC (void);
// 0x0000003B System.Collections.IEnumerator API::LoadObject(System.String)
extern void API_LoadObject_m98069BC268D82CFCD7247117082B7F9BD7BBEE53 (void);
// 0x0000003C System.Void API::.ctor()
extern void API__ctor_m9FD4A0A4E81EA585D18E1B9B87805059B0A0C576 (void);
// 0x0000003D System.Void API/<WaitForNetwork>d__6::.ctor(System.Int32)
extern void U3CWaitForNetworkU3Ed__6__ctor_mF1412BE85CBC51E23909332D5EDC22CCCCD52617 (void);
// 0x0000003E System.Void API/<WaitForNetwork>d__6::System.IDisposable.Dispose()
extern void U3CWaitForNetworkU3Ed__6_System_IDisposable_Dispose_m63FC1A9D2AF19A2902FC6BEC5CC0C1195686BC3F (void);
// 0x0000003F System.Boolean API/<WaitForNetwork>d__6::MoveNext()
extern void U3CWaitForNetworkU3Ed__6_MoveNext_m2A3BD8B177FA7669DD1CA928E166FAD268D5C0C0 (void);
// 0x00000040 System.Object API/<WaitForNetwork>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForNetworkU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44A104805B8BD184D0F86FDE323E627B561D4361 (void);
// 0x00000041 System.Void API/<WaitForNetwork>d__6::System.Collections.IEnumerator.Reset()
extern void U3CWaitForNetworkU3Ed__6_System_Collections_IEnumerator_Reset_m1D06110E357866A98DDCFB00B2DD00A17ED85E52 (void);
// 0x00000042 System.Object API/<WaitForNetwork>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForNetworkU3Ed__6_System_Collections_IEnumerator_get_Current_m4775A515F0B7D9E7DBB553DDEC00990BA353B302 (void);
// 0x00000043 System.Void API/<WaitForValidAsset>d__7::.ctor(System.Int32)
extern void U3CWaitForValidAssetU3Ed__7__ctor_m6EA87D907FC80961BF4FB0E412A22CA79F5CF067 (void);
// 0x00000044 System.Void API/<WaitForValidAsset>d__7::System.IDisposable.Dispose()
extern void U3CWaitForValidAssetU3Ed__7_System_IDisposable_Dispose_m7315B6FBDDF7E7AECCC79C7393C979DEB24010EE (void);
// 0x00000045 System.Boolean API/<WaitForValidAsset>d__7::MoveNext()
extern void U3CWaitForValidAssetU3Ed__7_MoveNext_m76E6C7D3A5D8D6A729730E385869025585B94CED (void);
// 0x00000046 System.Object API/<WaitForValidAsset>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForValidAssetU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7A06703A12EEF4209975F7AAAE56C360B5CE6FC (void);
// 0x00000047 System.Void API/<WaitForValidAsset>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWaitForValidAssetU3Ed__7_System_Collections_IEnumerator_Reset_mBE7BDEF329CAAE9AE97662A752B5F6741D13BCDB (void);
// 0x00000048 System.Object API/<WaitForValidAsset>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForValidAssetU3Ed__7_System_Collections_IEnumerator_get_Current_m464AED68CC96B1511ECE779635DCF55CAF572825 (void);
// 0x00000049 System.Void API/<GetDisplayBundleRoutine>d__8::.ctor(System.Int32)
extern void U3CGetDisplayBundleRoutineU3Ed__8__ctor_mFA1757821B63C69B29593F24661176E67A6ACA0B (void);
// 0x0000004A System.Void API/<GetDisplayBundleRoutine>d__8::System.IDisposable.Dispose()
extern void U3CGetDisplayBundleRoutineU3Ed__8_System_IDisposable_Dispose_mF1E7197A2F2E0180E75F2815526E6636A519BB24 (void);
// 0x0000004B System.Boolean API/<GetDisplayBundleRoutine>d__8::MoveNext()
extern void U3CGetDisplayBundleRoutineU3Ed__8_MoveNext_m90F701AE4D8D754CEE26D32B43C105BD38D82157 (void);
// 0x0000004C System.Object API/<GetDisplayBundleRoutine>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D88626B7D5DA909122C176B1A58505180C12009 (void);
// 0x0000004D System.Void API/<GetDisplayBundleRoutine>d__8::System.Collections.IEnumerator.Reset()
extern void U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_IEnumerator_Reset_m3AA34E390021291ADF94076A955AA4C210BA594F (void);
// 0x0000004E System.Object API/<GetDisplayBundleRoutine>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_IEnumerator_get_Current_m578329969F9173AE09887DC96FFB02B77ED4CF14 (void);
// 0x0000004F System.Void API/<LoadObject>d__15::.ctor(System.Int32)
extern void U3CLoadObjectU3Ed__15__ctor_m2806C3D179D49546284BAF86438D104EB1DC67AC (void);
// 0x00000050 System.Void API/<LoadObject>d__15::System.IDisposable.Dispose()
extern void U3CLoadObjectU3Ed__15_System_IDisposable_Dispose_mDC6EFE309841F623F0CD99CA4FFA6C3428CF6122 (void);
// 0x00000051 System.Boolean API/<LoadObject>d__15::MoveNext()
extern void U3CLoadObjectU3Ed__15_MoveNext_mEFE04172295A1DF846635B26BE0BE804278CD569 (void);
// 0x00000052 System.Object API/<LoadObject>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadObjectU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6CEE352CD7F4696F21B9FF36651A779EBDCB1C28 (void);
// 0x00000053 System.Void API/<LoadObject>d__15::System.Collections.IEnumerator.Reset()
extern void U3CLoadObjectU3Ed__15_System_Collections_IEnumerator_Reset_m322BF612BBF0C1C308A0ECA2523930472B284D27 (void);
// 0x00000054 System.Object API/<LoadObject>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CLoadObjectU3Ed__15_System_Collections_IEnumerator_get_Current_m394C00B32D075F828FD28F3AA7571A7270A67C90 (void);
// 0x00000055 System.Void ArCanvas::Awake()
extern void ArCanvas_Awake_m9DA5F6E0B585FDB416AFE42E3AC7A49323E4CE29 (void);
// 0x00000056 System.Void ArCanvas::Update()
extern void ArCanvas_Update_m4CB4E509CE3E1FEA0B6A190C86B6144E873C5E34 (void);
// 0x00000057 System.Collections.IEnumerator ArCanvas::WaitForPanelOff()
extern void ArCanvas_WaitForPanelOff_m9F639AEE0EE30A52C3105E18747A176D724BB0E3 (void);
// 0x00000058 System.Void ArCanvas::Start()
extern void ArCanvas_Start_mEFBBCCF7FF62B5A813B12BC305686106788BB645 (void);
// 0x00000059 System.Void ArCanvas::RemoveFromScene()
extern void ArCanvas_RemoveFromScene_mEA7ECC175F7F9E44258C2100E18FDAF94AC13354 (void);
// 0x0000005A System.Collections.IEnumerator ArCanvas::WaitForDestruction()
extern void ArCanvas_WaitForDestruction_m751A2F37F6BE322997D8B104EEDA798750B8988B (void);
// 0x0000005B System.Void ArCanvas::AddToCartPanelPop()
extern void ArCanvas_AddToCartPanelPop_m41789DA41554D1AD4F1E26D4E925A5E3275E4CC2 (void);
// 0x0000005C System.Void ArCanvas::.ctor()
extern void ArCanvas__ctor_mB28079D2DF6AA059F08E3182D22070D36670D212 (void);
// 0x0000005D System.Void ArCanvas/NativeAPI::sendMessageToMobileApp(System.String)
extern void NativeAPI_sendMessageToMobileApp_m1E9509CED0236FABB9BCB35627FFBDD2707C662C (void);
// 0x0000005E System.Void ArCanvas/NativeAPI::destroyUnity(System.String)
extern void NativeAPI_destroyUnity_m447226448B75964021C0576B101B825FB832F999 (void);
// 0x0000005F System.Void ArCanvas/NativeAPI::.ctor()
extern void NativeAPI__ctor_m590217DC4297E301A3A81840213D71E04843D643 (void);
// 0x00000060 System.Void ArCanvas/<WaitForPanelOff>d__10::.ctor(System.Int32)
extern void U3CWaitForPanelOffU3Ed__10__ctor_m84F310F154772ECEC43AD769E92401549468716E (void);
// 0x00000061 System.Void ArCanvas/<WaitForPanelOff>d__10::System.IDisposable.Dispose()
extern void U3CWaitForPanelOffU3Ed__10_System_IDisposable_Dispose_m9634C463036E0D40A849B7FD28D2C4E418C09A02 (void);
// 0x00000062 System.Boolean ArCanvas/<WaitForPanelOff>d__10::MoveNext()
extern void U3CWaitForPanelOffU3Ed__10_MoveNext_m21430B861B473DA2FBFE4A3354B5F00E97863B2C (void);
// 0x00000063 System.Object ArCanvas/<WaitForPanelOff>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForPanelOffU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B1B663B02F7C7DFAE2DA6CC9CF39306E9B4DADA (void);
// 0x00000064 System.Void ArCanvas/<WaitForPanelOff>d__10::System.Collections.IEnumerator.Reset()
extern void U3CWaitForPanelOffU3Ed__10_System_Collections_IEnumerator_Reset_m6888027D1E67FCDA3F1E57665AFF7373D0AC83BB (void);
// 0x00000065 System.Object ArCanvas/<WaitForPanelOff>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForPanelOffU3Ed__10_System_Collections_IEnumerator_get_Current_mAB1A86583188C3DF7875F966ABB187D22A39FB3B (void);
// 0x00000066 System.Void ArCanvas/<WaitForDestruction>d__14::.ctor(System.Int32)
extern void U3CWaitForDestructionU3Ed__14__ctor_m04BB2BFC18D9F7077464DD0DFFA4356B73AA9609 (void);
// 0x00000067 System.Void ArCanvas/<WaitForDestruction>d__14::System.IDisposable.Dispose()
extern void U3CWaitForDestructionU3Ed__14_System_IDisposable_Dispose_m8DB33991297D578477C32147992BA67E11CFC767 (void);
// 0x00000068 System.Boolean ArCanvas/<WaitForDestruction>d__14::MoveNext()
extern void U3CWaitForDestructionU3Ed__14_MoveNext_m3CD7B807A7A0FBAF30E186A931D2455E9EDD8B96 (void);
// 0x00000069 System.Object ArCanvas/<WaitForDestruction>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForDestructionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DEA5FCC9FC6691C55C6BBB6B789397D70AC5612 (void);
// 0x0000006A System.Void ArCanvas/<WaitForDestruction>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitForDestructionU3Ed__14_System_Collections_IEnumerator_Reset_m1059529BDD33505776B838C83F953447FF0DD01D (void);
// 0x0000006B System.Object ArCanvas/<WaitForDestruction>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForDestructionU3Ed__14_System_Collections_IEnumerator_get_Current_mF4BF2E49A071D8349633F2BE5D34F9C6BA244E9E (void);
// 0x0000006C System.Void ARTapToPlace::Awake()
extern void ARTapToPlace_Awake_m749715AA31BC8E5EE2CFAF15537E12D247CA734C (void);
// 0x0000006D System.Boolean ARTapToPlace::TryGetTouchPosition(UnityEngine.Vector2&)
extern void ARTapToPlace_TryGetTouchPosition_m852168C24C6C48EFD8EBE81BF129AADE5B0C4CCC (void);
// 0x0000006E System.Void ARTapToPlace::Update()
extern void ARTapToPlace_Update_m556D3AD3B840B808FFFCB39191E4830192B5485F (void);
// 0x0000006F System.Collections.IEnumerator ARTapToPlace::WaitForBaseAnimation()
extern void ARTapToPlace_WaitForBaseAnimation_mFDA553B9F37AA0840CEFF4AD883DD960BDD8CD3C (void);
// 0x00000070 System.Void ARTapToPlace::.ctor()
extern void ARTapToPlace__ctor_mACE9DBBF058C946B4468A98EC63AF1DF7E7C43FF (void);
// 0x00000071 System.Void ARTapToPlace::.cctor()
extern void ARTapToPlace__cctor_m21790C08373A84D2B9B2453AB8162449818B8955 (void);
// 0x00000072 System.Void ARTapToPlace/NativeAPI::modelProjected(System.Boolean)
extern void NativeAPI_modelProjected_m898BDDD1C8ADBB5BC13AEA0BBC87542CCEF0E7E3 (void);
// 0x00000073 System.Void ARTapToPlace/NativeAPI::avatarProjected(System.Boolean)
extern void NativeAPI_avatarProjected_m45E3F715165A89CF20FDEF7809928AE54CC18EC2 (void);
// 0x00000074 System.Void ARTapToPlace/NativeAPI::.ctor()
extern void NativeAPI__ctor_m584B85BE5D8027FEDBFF657E21E0BF920DD8A3CF (void);
// 0x00000075 System.Void ARTapToPlace/<WaitForBaseAnimation>d__11::.ctor(System.Int32)
extern void U3CWaitForBaseAnimationU3Ed__11__ctor_m4FD68EED7240580AC65045FD4EEC9D2426E95267 (void);
// 0x00000076 System.Void ARTapToPlace/<WaitForBaseAnimation>d__11::System.IDisposable.Dispose()
extern void U3CWaitForBaseAnimationU3Ed__11_System_IDisposable_Dispose_mAAD9EE52AB324B4128B20087D8890EC4C8AC6337 (void);
// 0x00000077 System.Boolean ARTapToPlace/<WaitForBaseAnimation>d__11::MoveNext()
extern void U3CWaitForBaseAnimationU3Ed__11_MoveNext_m34004BF6D7B3BC4B849A3F52D3DB442E5F082019 (void);
// 0x00000078 System.Object ARTapToPlace/<WaitForBaseAnimation>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForBaseAnimationU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF46859E3EFC3BE349D906EB2DA9CA4ED3CA0268 (void);
// 0x00000079 System.Void ARTapToPlace/<WaitForBaseAnimation>d__11::System.Collections.IEnumerator.Reset()
extern void U3CWaitForBaseAnimationU3Ed__11_System_Collections_IEnumerator_Reset_mE91B962DB9EB29CF1117E5C319E2D967FD8125C5 (void);
// 0x0000007A System.Object ARTapToPlace/<WaitForBaseAnimation>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForBaseAnimationU3Ed__11_System_Collections_IEnumerator_get_Current_mE8187B7A5EE4119D5D9E4DFC9B90A26D5B21E454 (void);
// 0x0000007B System.Void ARTapToPlaceAvatar::Awake()
extern void ARTapToPlaceAvatar_Awake_m87AF3A614FB8CA4A5A42B4E39158BBB14B0C5493 (void);
// 0x0000007C System.Boolean ARTapToPlaceAvatar::TryGetTouchPosition(UnityEngine.Vector2&)
extern void ARTapToPlaceAvatar_TryGetTouchPosition_m7989AD5A9E5D9E7F0B78E11A45CBF91812C149B0 (void);
// 0x0000007D System.Void ARTapToPlaceAvatar::Update()
extern void ARTapToPlaceAvatar_Update_m51408E6611CE2E7331243B57C12F6E3EA3C32850 (void);
// 0x0000007E System.Void ARTapToPlaceAvatar::.ctor()
extern void ARTapToPlaceAvatar__ctor_m3E5DD5CB3D1730710E0918224BB165454CE34947 (void);
// 0x0000007F System.Void ARTapToPlaceAvatar::.cctor()
extern void ARTapToPlaceAvatar__cctor_m87D258890B7CA9FECDFF2EB8D3A4F95B97758FC6 (void);
// 0x00000080 System.Void ContentController::LoadContent(System.String)
extern void ContentController_LoadContent_m89D12A8DC1117E9DA3713D745572A20AD3AF0A1A (void);
// 0x00000081 System.Void ContentController::OnContentLoaded(UnityEngine.GameObject)
extern void ContentController_OnContentLoaded_m5BF69A7D1CE991B153CE5FD099EEA464DA70866F (void);
// 0x00000082 System.Void ContentController::DestroyAllChildren()
extern void ContentController_DestroyAllChildren_m1FFD90643F2B1719A51F88A717E300F588AC9BF9 (void);
// 0x00000083 System.Void ContentController::.ctor()
extern void ContentController__ctor_m00468CB1576EC1D2FB13CAF80FA88D8AE47E2FFF (void);
// 0x00000084 System.Void DragObject1::OnMouseDown()
extern void DragObject1_OnMouseDown_m4EDE2CBD6710F1D5A92CF284BC445D9CA169783B (void);
// 0x00000085 System.Void DragObject1::OnMouseDrag()
extern void DragObject1_OnMouseDrag_m0B316483CDEBF2D42FCBCAEF9FCF0F0C1E975C83 (void);
// 0x00000086 System.Void DragObject1::.ctor()
extern void DragObject1__ctor_m8030BF795D44667E18A6D43A020CE65078729ADD (void);
// 0x00000087 System.Void LookAtCamera::Start()
extern void LookAtCamera_Start_mCFE758FBB48D39BDD130AC7A6FAF30711DBFE8A6 (void);
// 0x00000088 System.Void LookAtCamera::Update()
extern void LookAtCamera_Update_m6D9202748A260CEBA5420C43FA50844A642FACFB (void);
// 0x00000089 System.Void LookAtCamera::.ctor()
extern void LookAtCamera__ctor_mCA03F526C152A3EE7381FC8F3E232BB791884238 (void);
// 0x0000008A System.Void MyDrag::Start()
extern void MyDrag_Start_m4A84FE502B25439D102526CE0A287002A0D6BE61 (void);
// 0x0000008B System.Void MyDrag::Update()
extern void MyDrag_Update_m83F87C06848FC0324F2F711559DC62C482FEB2C8 (void);
// 0x0000008C System.Void MyDrag::Move()
extern void MyDrag_Move_m990743428A7BA99499B38DEDC4FABBC548C876B5 (void);
// 0x0000008D System.Void MyDrag::.ctor()
extern void MyDrag__ctor_mDD8B0C6C518ADF555A75B5F63798C3F8ADEE7E6C (void);
// 0x0000008E System.Void ObjectSpawner::Start()
extern void ObjectSpawner_Start_mB5C9193D6076E2D9586CA1F5862D4C7D9AF008DE (void);
// 0x0000008F System.Void ObjectSpawner::Update()
extern void ObjectSpawner_Update_mBCDB36C4FF21AC8AF713E11BDB1FE504B0EC8D19 (void);
// 0x00000090 System.Void ObjectSpawner::ReLoadUnity()
extern void ObjectSpawner_ReLoadUnity_m6C86083ACC3A8BA0C4ED0AAB731B1B5A6021FD89 (void);
// 0x00000091 System.Void ObjectSpawner::Activate(System.String)
extern void ObjectSpawner_Activate_m34247E7E02036EBF855FE054FE1FFC3EC525CCA8 (void);
// 0x00000092 System.Collections.IEnumerator ObjectSpawner::WaitForDownload()
extern void ObjectSpawner_WaitForDownload_m5D57AD9DBB3ECE2AC348E376DEB7F7ECE1A9D79E (void);
// 0x00000093 System.Void ObjectSpawner::Redeem(System.String)
extern void ObjectSpawner_Redeem_m5BED17761A501AE11EAD3E0C68D452B096FE9E70 (void);
// 0x00000094 System.Collections.IEnumerator ObjectSpawner::WaitForRedeemDownload()
extern void ObjectSpawner_WaitForRedeemDownload_m04167A9FDCAA59DD8FA7A05CBBD420F01FAA8524 (void);
// 0x00000095 System.Void ObjectSpawner::RedeemCharacter(System.String)
extern void ObjectSpawner_RedeemCharacter_m85A125750ED5B2CC129A4BD56C3298D299AE7196 (void);
// 0x00000096 System.Collections.IEnumerator ObjectSpawner::WaitForCharacterDownload()
extern void ObjectSpawner_WaitForCharacterDownload_mDF68C0DFD2A2FC0F718190335D126DDA02CF5233 (void);
// 0x00000097 System.Void ObjectSpawner::ShadowSwitch()
extern void ObjectSpawner_ShadowSwitch_m36CCB43CCBE225DD0617D953F9646E5DFD225D20 (void);
// 0x00000098 System.Void ObjectSpawner::ShadowSwitchOn()
extern void ObjectSpawner_ShadowSwitchOn_m788F2C63F6021740EF10ECC05F068DCBDD27197F (void);
// 0x00000099 System.Void ObjectSpawner::ShadowSwitchOff()
extern void ObjectSpawner_ShadowSwitchOff_m00D1C076F5B14EA39EA1A4F587159E9518053B3F (void);
// 0x0000009A System.Void ObjectSpawner::MovementControl()
extern void ObjectSpawner_MovementControl_mDA7E5C4C4DAC392B3EF08EF75D6B107366FFFD24 (void);
// 0x0000009B System.Void ObjectSpawner::VertMovement()
extern void ObjectSpawner_VertMovement_m9D0B43B1F7C7403B751B44AEC2A189CB93EAB98B (void);
// 0x0000009C System.Void ObjectSpawner::HoriMovement()
extern void ObjectSpawner_HoriMovement_m8D8CECCC7BBAB9C22AC67D7612308975FF223C88 (void);
// 0x0000009D System.Void ObjectSpawner::ClearModelsCache()
extern void ObjectSpawner_ClearModelsCache_m08BB5C19D321B601AC455AF1344C10B38CB61A8D (void);
// 0x0000009E System.Void ObjectSpawner::.ctor()
extern void ObjectSpawner__ctor_mD103BAD86ABBCFC5726513C071E31158C1A150D3 (void);
// 0x0000009F System.Void ObjectSpawner/NativeAPI::modelDownloaded(System.Boolean)
extern void NativeAPI_modelDownloaded_m2CB305BC0327B63C44CFB00C1905881618AE07F6 (void);
// 0x000000A0 System.Void ObjectSpawner/NativeAPI::modelDownloadingDone(System.String)
extern void NativeAPI_modelDownloadingDone_mA22BB2476C75156FAF3A540FC40E485B7A6C1036 (void);
// 0x000000A1 System.Void ObjectSpawner/NativeAPI::.ctor()
extern void NativeAPI__ctor_m83D6870123E118D4FD44168A1A5F4D593D1D212C (void);
// 0x000000A2 System.Void ObjectSpawner/<WaitForDownload>d__21::.ctor(System.Int32)
extern void U3CWaitForDownloadU3Ed__21__ctor_m08197BFC5916856E72C8DCA01CB0543EA0FB6019 (void);
// 0x000000A3 System.Void ObjectSpawner/<WaitForDownload>d__21::System.IDisposable.Dispose()
extern void U3CWaitForDownloadU3Ed__21_System_IDisposable_Dispose_mF3BBF43CDA2CF2412196777C66C796AEAB8B3439 (void);
// 0x000000A4 System.Boolean ObjectSpawner/<WaitForDownload>d__21::MoveNext()
extern void U3CWaitForDownloadU3Ed__21_MoveNext_m0685F461800554B4F31CF4F5D09285CD47B7E699 (void);
// 0x000000A5 System.Object ObjectSpawner/<WaitForDownload>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForDownloadU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF099AAEF39D890D39B168DFF43F29620D763F84 (void);
// 0x000000A6 System.Void ObjectSpawner/<WaitForDownload>d__21::System.Collections.IEnumerator.Reset()
extern void U3CWaitForDownloadU3Ed__21_System_Collections_IEnumerator_Reset_m85C55B534E8A5F36E2345FCE848023E525B6708D (void);
// 0x000000A7 System.Object ObjectSpawner/<WaitForDownload>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForDownloadU3Ed__21_System_Collections_IEnumerator_get_Current_m28CC9EFB4156A6A1788012C10DE2AC3C6BEC4B92 (void);
// 0x000000A8 System.Void ObjectSpawner/<WaitForRedeemDownload>d__27::.ctor(System.Int32)
extern void U3CWaitForRedeemDownloadU3Ed__27__ctor_m4AB01FE9B37B424EE5FE5E50FF30BFEAA20F80A1 (void);
// 0x000000A9 System.Void ObjectSpawner/<WaitForRedeemDownload>d__27::System.IDisposable.Dispose()
extern void U3CWaitForRedeemDownloadU3Ed__27_System_IDisposable_Dispose_mD9665FF709FD9F71484B6692250C661DDFC7A022 (void);
// 0x000000AA System.Boolean ObjectSpawner/<WaitForRedeemDownload>d__27::MoveNext()
extern void U3CWaitForRedeemDownloadU3Ed__27_MoveNext_m0D750FD47DAC5AB2A6AFFBD69B56EED050C54F30 (void);
// 0x000000AB System.Object ObjectSpawner/<WaitForRedeemDownload>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForRedeemDownloadU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4CD0810E751CA61ECD341FA4BDBDC13FE16C7F0 (void);
// 0x000000AC System.Void ObjectSpawner/<WaitForRedeemDownload>d__27::System.Collections.IEnumerator.Reset()
extern void U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_Reset_m2335D3677F99A5261583F4DAEE2367AB55210015 (void);
// 0x000000AD System.Object ObjectSpawner/<WaitForRedeemDownload>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_get_Current_m5EBF4F30F9BD903BC0B1AD30C1F5F6C46BEE2A91 (void);
// 0x000000AE System.Void ObjectSpawner/<WaitForCharacterDownload>d__32::.ctor(System.Int32)
extern void U3CWaitForCharacterDownloadU3Ed__32__ctor_m9ADD7A4D652EDCA2711785138A1A88A0BCC3D5D4 (void);
// 0x000000AF System.Void ObjectSpawner/<WaitForCharacterDownload>d__32::System.IDisposable.Dispose()
extern void U3CWaitForCharacterDownloadU3Ed__32_System_IDisposable_Dispose_m17A5DCD395D4A509495D3DB61C87231C6E60678E (void);
// 0x000000B0 System.Boolean ObjectSpawner/<WaitForCharacterDownload>d__32::MoveNext()
extern void U3CWaitForCharacterDownloadU3Ed__32_MoveNext_m63617555B97894A1350F540D7536BB1E35D2AC24 (void);
// 0x000000B1 System.Object ObjectSpawner/<WaitForCharacterDownload>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForCharacterDownloadU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m083943FF8AF25E4A9A59AE96268F68B82B7BE85A (void);
// 0x000000B2 System.Void ObjectSpawner/<WaitForCharacterDownload>d__32::System.Collections.IEnumerator.Reset()
extern void U3CWaitForCharacterDownloadU3Ed__32_System_Collections_IEnumerator_Reset_m76215B01715E00247B1B570C5455B844F518637D (void);
// 0x000000B3 System.Object ObjectSpawner/<WaitForCharacterDownload>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForCharacterDownloadU3Ed__32_System_Collections_IEnumerator_get_Current_m0C3E05B73BF1B8890E6A147388C0B7D389818FC3 (void);
// 0x000000B4 System.Void ParslAvatarController::Start()
extern void ParslAvatarController_Start_m041B82DA2972CE14E6D62FDE7C77CF64C7A6742C (void);
// 0x000000B5 System.Void ParslAvatarController::ModelLoad(System.Int32)
extern void ParslAvatarController_ModelLoad_m64A42862A4C706201748ACFB7C5252BA48B34492 (void);
// 0x000000B6 System.Void ParslAvatarController::Emote(System.Int32)
extern void ParslAvatarController_Emote_mBCFA7715DD793D0FD92B38D934F5F3434B39C449 (void);
// 0x000000B7 System.Collections.IEnumerator ParslAvatarController::WaitForBaseAnimation()
extern void ParslAvatarController_WaitForBaseAnimation_m3425207E3455FDE6715B1A98CE72FD693F72EFE5 (void);
// 0x000000B8 System.Void ParslAvatarController::ResetScene()
extern void ParslAvatarController_ResetScene_mBAAC05A75FCB803B8376B8B309400F489160132C (void);
// 0x000000B9 System.Void ParslAvatarController::Back()
extern void ParslAvatarController_Back_m6EE03CC0CC8E42395AD6699CA1D9D97B0175244E (void);
// 0x000000BA System.Void ParslAvatarController::_cancleConfirmation()
extern void ParslAvatarController__cancleConfirmation_m697C1E42E74CB344782036B4ADF387E2BD0CF6B1 (void);
// 0x000000BB System.Void ParslAvatarController::_confirmConfirmation()
extern void ParslAvatarController__confirmConfirmation_m873AFAFA72BB21917917079FD0AAB17EEB43C097 (void);
// 0x000000BC System.Void ParslAvatarController::UploadCharacterValues()
extern void ParslAvatarController_UploadCharacterValues_m3E8E5C574E7BBD26922F30ED6E92338E2857D3F0 (void);
// 0x000000BD System.Void ParslAvatarController::MaleCharacterButton()
extern void ParslAvatarController_MaleCharacterButton_m79839D62D6B8A94760178FCF4455FD9782F7E523 (void);
// 0x000000BE System.Void ParslAvatarController::FemaleCharacterButton()
extern void ParslAvatarController_FemaleCharacterButton_m94C1C272FB4A6423751C4EB6DEAEA7CD1C7D24A4 (void);
// 0x000000BF System.Void ParslAvatarController::ChildCharacterButton()
extern void ParslAvatarController_ChildCharacterButton_mE994338D8C51860F2BAB23F5F79E3A90A5759758 (void);
// 0x000000C0 System.Void ParslAvatarController::.ctor()
extern void ParslAvatarController__ctor_mC0DBE4814F323F6F9B28828E1DA67849F2181508 (void);
// 0x000000C1 System.Void ParslAvatarController/NativeAPI::avatarModule(System.String,System.String)
extern void NativeAPI_avatarModule_m111BF0F2A32BB0C44F38FD36BC7A3B35742A5D4F (void);
// 0x000000C2 System.Void ParslAvatarController/NativeAPI::.ctor()
extern void NativeAPI__ctor_m0D84FDA583D2D7DDD542EC04BDCA079EC0AC2AD0 (void);
// 0x000000C3 System.Void ParslAvatarController/<WaitForBaseAnimation>d__13::.ctor(System.Int32)
extern void U3CWaitForBaseAnimationU3Ed__13__ctor_mDFD63E82A5F0AD0928F671E5A89CD7E774F38B3B (void);
// 0x000000C4 System.Void ParslAvatarController/<WaitForBaseAnimation>d__13::System.IDisposable.Dispose()
extern void U3CWaitForBaseAnimationU3Ed__13_System_IDisposable_Dispose_m0FDBAAE832C1D14359B2C3BACF061F3505049B76 (void);
// 0x000000C5 System.Boolean ParslAvatarController/<WaitForBaseAnimation>d__13::MoveNext()
extern void U3CWaitForBaseAnimationU3Ed__13_MoveNext_m6F2C1477856D8F875A4478284439B8BC8AF8E116 (void);
// 0x000000C6 System.Object ParslAvatarController/<WaitForBaseAnimation>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForBaseAnimationU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94E02502EA21E4E9180B10B040E77AF3BC80FBE2 (void);
// 0x000000C7 System.Void ParslAvatarController/<WaitForBaseAnimation>d__13::System.Collections.IEnumerator.Reset()
extern void U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_Reset_mC9611B0DDAFCE6CCAEAB6976ABABC7C2EBDB4BA3 (void);
// 0x000000C8 System.Object ParslAvatarController/<WaitForBaseAnimation>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_get_Current_m8D11DE91BAA64850F574F9601D7BEE2838FD7648 (void);
// 0x000000C9 System.Void PlaceContent::Update()
extern void PlaceContent_Update_mF3967E7E13C6EEABF0D7EB88616C57AE4E932781 (void);
// 0x000000CA System.Boolean PlaceContent::IsClickOverUI()
extern void PlaceContent_IsClickOverUI_m908B17F5232D4EEA755D4CD9506789DDDA480B5E (void);
// 0x000000CB System.Void PlaceContent::.ctor()
extern void PlaceContent__ctor_mF4A2CFE6250D7C4D33FF40C14C7AA78CB76B967E (void);
// 0x000000CC System.Void PlacementIndicator::Start()
extern void PlacementIndicator_Start_mC0F1E7B8F84514D2EDEA712785CDC9399A376200 (void);
// 0x000000CD System.Void PlacementIndicator::Update()
extern void PlacementIndicator_Update_m04B65B8903A034EA4C717E34BE6B8EF861382FE4 (void);
// 0x000000CE System.Void PlacementIndicator::.ctor()
extern void PlacementIndicator__ctor_mC5EF49C57C7F6519CA9E925EC1A0B13C7D3A08BE (void);
// 0x000000CF System.Boolean PlacementObject::get_Selected()
extern void PlacementObject_get_Selected_m016672B022C1F4823D54D5D6BD75BF99BFE5870A (void);
// 0x000000D0 System.Void PlacementObject::set_Selected(System.Boolean)
extern void PlacementObject_set_Selected_mB630BAA0B59D7CE262F123ABA59C77BA8A3CD47E (void);
// 0x000000D1 System.Boolean PlacementObject::get_Locked()
extern void PlacementObject_get_Locked_mC14FA7656C55BAB831982E7E1F076CA9F22B0431 (void);
// 0x000000D2 System.Void PlacementObject::set_Locked(System.Boolean)
extern void PlacementObject_set_Locked_m60DAD119123DDD5179F6A1C9B6771EC1D2F3845E (void);
// 0x000000D3 System.Void PlacementObject::SetOverlayText(System.String)
extern void PlacementObject_SetOverlayText_mF8D94744BC43A38F63855EC7A4E2229CAB983608 (void);
// 0x000000D4 System.Void PlacementObject::Awake()
extern void PlacementObject_Awake_mED0B25739C123D2B3EB209A59FE66CAA19F26E2B (void);
// 0x000000D5 System.Void PlacementObject::ToggleOverlay()
extern void PlacementObject_ToggleOverlay_m0D61DEBC8FD95937B8FE1C7EE641F301728E5D06 (void);
// 0x000000D6 System.Void PlacementObject::ToggleCanvas()
extern void PlacementObject_ToggleCanvas_mD425EA9347677BE51246528006BBE50FE96D1755 (void);
// 0x000000D7 System.Void PlacementObject::.ctor()
extern void PlacementObject__ctor_m424EC3A34F908843DA88EF32023510705ADB7534 (void);
// 0x000000D8 UnityEngine.GameObject PlacementWithMultipleDraggingDroppingController::get_PlacedPrefab()
extern void PlacementWithMultipleDraggingDroppingController_get_PlacedPrefab_m47A5436759E4FFA877926D91D10C3EA3E0443CA4 (void);
// 0x000000D9 System.Void PlacementWithMultipleDraggingDroppingController::set_PlacedPrefab(UnityEngine.GameObject)
extern void PlacementWithMultipleDraggingDroppingController_set_PlacedPrefab_m5A1DF8B13B979828211555EF49FE24E0BEC306C4 (void);
// 0x000000DA System.Void PlacementWithMultipleDraggingDroppingController::Awake()
extern void PlacementWithMultipleDraggingDroppingController_Awake_mEA748B763BFF99C724A27BECA9507A967A79D38C (void);
// 0x000000DB System.Void PlacementWithMultipleDraggingDroppingController::ChangePrefabSelection(System.String)
extern void PlacementWithMultipleDraggingDroppingController_ChangePrefabSelection_mCA3844D4E77D8203E8C62556847709F2B4640B64 (void);
// 0x000000DC System.Void PlacementWithMultipleDraggingDroppingController::Dismiss()
extern void PlacementWithMultipleDraggingDroppingController_Dismiss_m5AB0F7E5623981820F41C12A7961B2248676FE2B (void);
// 0x000000DD System.Void PlacementWithMultipleDraggingDroppingController::Update()
extern void PlacementWithMultipleDraggingDroppingController_Update_m84750C36697703609A9686184BC3369C275C4257 (void);
// 0x000000DE System.Void PlacementWithMultipleDraggingDroppingController::.ctor()
extern void PlacementWithMultipleDraggingDroppingController__ctor_mDB3EA5A58979804F0E380438760B4250B16125F2 (void);
// 0x000000DF System.Void PlacementWithMultipleDraggingDroppingController::.cctor()
extern void PlacementWithMultipleDraggingDroppingController__cctor_mAF496DA2B4A28200039E39F19FF8D22344651598 (void);
// 0x000000E0 System.Void PlacementWithMultipleDraggingDroppingController::<Awake>b__16_0()
extern void PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_0_m6D81DE0716636768E59D9208394BBC637BEEF144 (void);
// 0x000000E1 System.Void PlacementWithMultipleDraggingDroppingController::<Awake>b__16_1()
extern void PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_1_m0A272DC7C71DB834ADC4521180628482EF5DDD59 (void);
// 0x000000E2 System.Void PlacementWithMultipleDraggingDroppingController::<Awake>b__16_2()
extern void PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_2_m739E90DF149EDD32C71CB3BA24911CB6F25F348E (void);
// 0x000000E3 System.Void ReturnCartList::.ctor()
extern void ReturnCartList__ctor_mFBCE2C6653EFFEACC54DC0C6D65E0ED43609E58A (void);
// 0x000000E4 System.Void ToggleAR::OnValueChanged(System.Boolean)
extern void ToggleAR_OnValueChanged_m32E23F57B684D6AA4FF40229CF99D47F2A5EAE43 (void);
// 0x000000E5 System.Void ToggleAR::VisualizePlanes(System.Boolean)
extern void ToggleAR_VisualizePlanes_m64F47DB36EF2D3AA7A8E81F35BF1F5FF584F7073 (void);
// 0x000000E6 System.Void ToggleAR::VisualizePoints(System.Boolean)
extern void ToggleAR_VisualizePoints_m9362E15B317E73997AF22D5575F32F799D489E97 (void);
// 0x000000E7 System.Void ToggleAR::.ctor()
extern void ToggleAR__ctor_mA1CC296DD72AA9242A6D6153B5DD8336C03C3EF1 (void);
// 0x000000E8 UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::get_mode()
extern void ARAllPointCloudPointsParticleVisualizer_get_mode_mE2961E81075235D3BD24C54D522050A6A16ADF78 (void);
// 0x000000E9 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::set_mode(UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode)
extern void ARAllPointCloudPointsParticleVisualizer_set_mode_m93C09216743B5544F7EDF9C47FCDD9CD4E6E47C9 (void);
// 0x000000EA System.Int32 UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::get_totalPointCount()
extern void ARAllPointCloudPointsParticleVisualizer_get_totalPointCount_m82C8E588FD5467B4879E96DE8C1D8699A9FFC178 (void);
// 0x000000EB System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::OnPointCloudChanged(UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs)
extern void ARAllPointCloudPointsParticleVisualizer_OnPointCloudChanged_mAC37187B41FAA79B6084C2AE0DBE329AAA84E01E (void);
// 0x000000EC System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::SetParticlePosition(System.Int32,UnityEngine.Vector3)
extern void ARAllPointCloudPointsParticleVisualizer_SetParticlePosition_m296F1C3C6B0EA3C9D00B67776EB3A8E64B45B5AD (void);
// 0x000000ED System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::RenderPoints()
extern void ARAllPointCloudPointsParticleVisualizer_RenderPoints_m9308B52D42102E948AF53BE9D349175BE43E18CB (void);
// 0x000000EE System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::Awake()
extern void ARAllPointCloudPointsParticleVisualizer_Awake_m2039867B6B044168F1A26C1F2EF6D0F4E3354D2D (void);
// 0x000000EF System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::OnEnable()
extern void ARAllPointCloudPointsParticleVisualizer_OnEnable_m1F8B642B5E8F23CADAEDC9317FA62A1BE65ACB04 (void);
// 0x000000F0 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::OnDisable()
extern void ARAllPointCloudPointsParticleVisualizer_OnDisable_m482462736CDA9BAF6FF73CD40214E22B251FF77F (void);
// 0x000000F1 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::Update()
extern void ARAllPointCloudPointsParticleVisualizer_Update_m03E104CCAE3B121C955F9C228E16EDD7758896D8 (void);
// 0x000000F2 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::UpdateVisibility()
extern void ARAllPointCloudPointsParticleVisualizer_UpdateVisibility_m4C296E8EC0365D27C9C0BA909417BFB3A70F3F52 (void);
// 0x000000F3 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::SetVisible(System.Boolean)
extern void ARAllPointCloudPointsParticleVisualizer_SetVisible_mF075BA639C161AC2F4B4457D76FBA7392664B233 (void);
// 0x000000F4 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::.ctor()
extern void ARAllPointCloudPointsParticleVisualizer__ctor_m21F6F93A91B2CDD0C2FE8B7BE9BD38B6213B4088 (void);
static Il2CppMethodPointer s_methodPointers[244] = 
{
	SwitchPointCloudVisualizationMode_get_toggleButton_m5FBFEDEBC9B371B76804637D7C8091E26C3C399F,
	SwitchPointCloudVisualizationMode_set_toggleButton_mD9FC8EA7EAEE800F60DAFBCCAF19636BE29ECE77,
	SwitchPointCloudVisualizationMode_get_log_m6E4EB81C73C81360C40431382CD6EB0AC2C59763,
	SwitchPointCloudVisualizationMode_set_log_m564736BD24430DB94CCF144DABD0B6036777832C,
	SwitchPointCloudVisualizationMode_get_mode_m20AA95EDFB2FFB095D258B2C38987A3BC1227616,
	SwitchPointCloudVisualizationMode_set_mode_m280F90EBF742145758E7D52D0C84CA5AC16F0075,
	SwitchPointCloudVisualizationMode_SwitchVisualizationMode_m7CA339DEB4B8A40DFDFB8C6D79FC535048BAC278,
	SwitchPointCloudVisualizationMode_OnEnable_m0AE60A40E7545E590E1ACDF4FFEA32912DC1C11B,
	SwitchPointCloudVisualizationMode_OnPointCloudsChanged_mDA9644178AA492F76221956ED387EF7BC7C5EE55,
	SwitchPointCloudVisualizationMode_SetMode_m4BC28FF39AF7600381B725286D7BCFBFE46F85AC,
	SwitchPointCloudVisualizationMode__ctor_m02EAEB06B7F42B232A6280B8E7872F6596BF35B0,
	CSharpscaling_Update_m522915C0C9FD444E0BFEAD25993C4C208260247C,
	CSharpscaling__ctor_mED682225400E498E39AE9C4EA60E4C4B7C770197,
	onClickForScaling_OnMouseDown_mA0759701B4EF289EB795C08A59E104DF6F436B6B,
	onClickForScaling__ctor_m5190794CA361BAA49FC175F6ACC0C60A0A29AB60,
	rotateController_Update_mB71E56B5BD55A3F24C437AECFD9242ED257BA1BF,
	rotateController_OnMouseDown_m284DC0D713B8D7AD1742183B753D6EC45B72A26D,
	rotateController_OnMouseUp_m6EF7A213E4D61D029634814A82DE1727BB0D0564,
	rotateController__ctor_mABBEC5D25C68F797F37D0C8F6C847FE9D1714078,
	ShowCanvasOnBuildingModels_Update_mC7975878B85B48DDF5287D78C461375018D2910E,
	ShowCanvasOnBuildingModels_WaitForPanelOff_m6D114493F85D3F312D4261DA99EB95B9A6809FD8,
	ShowCanvasOnBuildingModels_RemoveFromScene_m7C323DBC67DA3AB34DBA23715AA91FA25248769F,
	ShowCanvasOnBuildingModels_AddToCartPanelPop_mEAC53C9A87BA80D5679F570B40152849E4693587,
	ShowCanvasOnBuildingModels__ctor_m743D86C272E344B8CC84375E64AC9B5B2884950A,
	NativeAPI_sendMessageToMobileApp_m6D59B0C98E3CCB11CB49100B76FC514CA39DC8E1,
	NativeAPI_destroyUnity_mD4F74726CD21B7384C92910C19ABD7A217D230FE,
	NativeAPI__ctor_mA377280AE0A5718846511B99AE5B2AF5B8940AFB,
	U3CWaitForPanelOffU3Ed__4__ctor_m098F18885B7958CB1FE64B250D907C74CB508CF6,
	U3CWaitForPanelOffU3Ed__4_System_IDisposable_Dispose_m42924C65565FE2CEF1DFAC81769C3AFF6A66A90F,
	U3CWaitForPanelOffU3Ed__4_MoveNext_m161C7D218C0726D4B59BE03A0ED2AC01DFE29CB3,
	U3CWaitForPanelOffU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m176E4AD63DF89158686C677B760E62A087A42DD5,
	U3CWaitForPanelOffU3Ed__4_System_Collections_IEnumerator_Reset_m704C805470B5F9028E57EDABACBEE1FA01DE2E39,
	U3CWaitForPanelOffU3Ed__4_System_Collections_IEnumerator_get_Current_mE10B2223B7CC00859CAD1EE6E9924CC94A192C79,
	Activate_Start_m08C8888DB0894B521E7A391FE19387E224C5C10A,
	Activate_Update_mA3BB4F4A0B7BBFE9B6F62B2FB0086021D43760D4,
	Activate_assss_mA1AC697A6E9A852727107D362EEEF8DA3DCCE5FD,
	Activate__ctor_mFDE6E64A8AE313D7BCEC986811153A8443E2306C,
	AddToCartScript_AddToCartItem_m69D02A77BE080494C9A091DB293A2E725AA3E65E,
	AddToCartScript__ctor_mDA5D0F9A8DAFE087715F080CB77A9C4CA8FF9D7B,
	Anim_Update_m246B9B6F3A5A51531603888DD2E0C56FDB496B46,
	Anim_playanimation_m68C20969E30DCA86C059A75C74B6A62FFDF60A1B,
	Anim_stand_m9B3427C6CB70E7B4326709D1318868D5C66E9B21,
	Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3,
	Anim__ctor_mD06BEBFB08FB944CC36A2F70EDCD029A8B0DD0D8,
	U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD,
	U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A,
	U3CdelayU3Ed__3_MoveNext_mFDE630DACFE2D69CE5A5DE03FE60482B185AFECF,
	U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26,
	U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521,
	U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D,
	AnimateModel_Start_m120DD4C0901DC917E52C9F1215768D3E5E6FD3BC,
	AnimateModel_Update_m6E878217EC241361AFD3591C94B49632761387F5,
	AnimateModel__ctor_m828FACE086F16333DCEBEC48AD64F71755330922,
	API_GetBundleObject_m1F40F48A9C8EC9F9A629DE6EF5DA168D632116A7,
	API_WaitForNetwork_m5DDF6877C90ACCE6954C6C0ED29AB9F5464F5AA5,
	API_WaitForValidAsset_mC11999EEFDC00D721B2E162BB0F29083221E1B36,
	API_GetDisplayBundleRoutine_m8DB6214089AC5BDE847DB16A7D56D52AEA074546,
	API_save_m5D33F677192B68C57048F97AB662736E4765F9FC,
	API_LoadObject_m98069BC268D82CFCD7247117082B7F9BD7BBEE53,
	API__ctor_m9FD4A0A4E81EA585D18E1B9B87805059B0A0C576,
	U3CWaitForNetworkU3Ed__6__ctor_mF1412BE85CBC51E23909332D5EDC22CCCCD52617,
	U3CWaitForNetworkU3Ed__6_System_IDisposable_Dispose_m63FC1A9D2AF19A2902FC6BEC5CC0C1195686BC3F,
	U3CWaitForNetworkU3Ed__6_MoveNext_m2A3BD8B177FA7669DD1CA928E166FAD268D5C0C0,
	U3CWaitForNetworkU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44A104805B8BD184D0F86FDE323E627B561D4361,
	U3CWaitForNetworkU3Ed__6_System_Collections_IEnumerator_Reset_m1D06110E357866A98DDCFB00B2DD00A17ED85E52,
	U3CWaitForNetworkU3Ed__6_System_Collections_IEnumerator_get_Current_m4775A515F0B7D9E7DBB553DDEC00990BA353B302,
	U3CWaitForValidAssetU3Ed__7__ctor_m6EA87D907FC80961BF4FB0E412A22CA79F5CF067,
	U3CWaitForValidAssetU3Ed__7_System_IDisposable_Dispose_m7315B6FBDDF7E7AECCC79C7393C979DEB24010EE,
	U3CWaitForValidAssetU3Ed__7_MoveNext_m76E6C7D3A5D8D6A729730E385869025585B94CED,
	U3CWaitForValidAssetU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7A06703A12EEF4209975F7AAAE56C360B5CE6FC,
	U3CWaitForValidAssetU3Ed__7_System_Collections_IEnumerator_Reset_mBE7BDEF329CAAE9AE97662A752B5F6741D13BCDB,
	U3CWaitForValidAssetU3Ed__7_System_Collections_IEnumerator_get_Current_m464AED68CC96B1511ECE779635DCF55CAF572825,
	U3CGetDisplayBundleRoutineU3Ed__8__ctor_mFA1757821B63C69B29593F24661176E67A6ACA0B,
	U3CGetDisplayBundleRoutineU3Ed__8_System_IDisposable_Dispose_mF1E7197A2F2E0180E75F2815526E6636A519BB24,
	U3CGetDisplayBundleRoutineU3Ed__8_MoveNext_m90F701AE4D8D754CEE26D32B43C105BD38D82157,
	U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D88626B7D5DA909122C176B1A58505180C12009,
	U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_IEnumerator_Reset_m3AA34E390021291ADF94076A955AA4C210BA594F,
	U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_IEnumerator_get_Current_m578329969F9173AE09887DC96FFB02B77ED4CF14,
	U3CLoadObjectU3Ed__15__ctor_m2806C3D179D49546284BAF86438D104EB1DC67AC,
	U3CLoadObjectU3Ed__15_System_IDisposable_Dispose_mDC6EFE309841F623F0CD99CA4FFA6C3428CF6122,
	U3CLoadObjectU3Ed__15_MoveNext_mEFE04172295A1DF846635B26BE0BE804278CD569,
	U3CLoadObjectU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6CEE352CD7F4696F21B9FF36651A779EBDCB1C28,
	U3CLoadObjectU3Ed__15_System_Collections_IEnumerator_Reset_m322BF612BBF0C1C308A0ECA2523930472B284D27,
	U3CLoadObjectU3Ed__15_System_Collections_IEnumerator_get_Current_m394C00B32D075F828FD28F3AA7571A7270A67C90,
	ArCanvas_Awake_m9DA5F6E0B585FDB416AFE42E3AC7A49323E4CE29,
	ArCanvas_Update_m4CB4E509CE3E1FEA0B6A190C86B6144E873C5E34,
	ArCanvas_WaitForPanelOff_m9F639AEE0EE30A52C3105E18747A176D724BB0E3,
	ArCanvas_Start_mEFBBCCF7FF62B5A813B12BC305686106788BB645,
	ArCanvas_RemoveFromScene_mEA7ECC175F7F9E44258C2100E18FDAF94AC13354,
	ArCanvas_WaitForDestruction_m751A2F37F6BE322997D8B104EEDA798750B8988B,
	ArCanvas_AddToCartPanelPop_m41789DA41554D1AD4F1E26D4E925A5E3275E4CC2,
	ArCanvas__ctor_mB28079D2DF6AA059F08E3182D22070D36670D212,
	NativeAPI_sendMessageToMobileApp_m1E9509CED0236FABB9BCB35627FFBDD2707C662C,
	NativeAPI_destroyUnity_m447226448B75964021C0576B101B825FB832F999,
	NativeAPI__ctor_m590217DC4297E301A3A81840213D71E04843D643,
	U3CWaitForPanelOffU3Ed__10__ctor_m84F310F154772ECEC43AD769E92401549468716E,
	U3CWaitForPanelOffU3Ed__10_System_IDisposable_Dispose_m9634C463036E0D40A849B7FD28D2C4E418C09A02,
	U3CWaitForPanelOffU3Ed__10_MoveNext_m21430B861B473DA2FBFE4A3354B5F00E97863B2C,
	U3CWaitForPanelOffU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B1B663B02F7C7DFAE2DA6CC9CF39306E9B4DADA,
	U3CWaitForPanelOffU3Ed__10_System_Collections_IEnumerator_Reset_m6888027D1E67FCDA3F1E57665AFF7373D0AC83BB,
	U3CWaitForPanelOffU3Ed__10_System_Collections_IEnumerator_get_Current_mAB1A86583188C3DF7875F966ABB187D22A39FB3B,
	U3CWaitForDestructionU3Ed__14__ctor_m04BB2BFC18D9F7077464DD0DFFA4356B73AA9609,
	U3CWaitForDestructionU3Ed__14_System_IDisposable_Dispose_m8DB33991297D578477C32147992BA67E11CFC767,
	U3CWaitForDestructionU3Ed__14_MoveNext_m3CD7B807A7A0FBAF30E186A931D2455E9EDD8B96,
	U3CWaitForDestructionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DEA5FCC9FC6691C55C6BBB6B789397D70AC5612,
	U3CWaitForDestructionU3Ed__14_System_Collections_IEnumerator_Reset_m1059529BDD33505776B838C83F953447FF0DD01D,
	U3CWaitForDestructionU3Ed__14_System_Collections_IEnumerator_get_Current_mF4BF2E49A071D8349633F2BE5D34F9C6BA244E9E,
	ARTapToPlace_Awake_m749715AA31BC8E5EE2CFAF15537E12D247CA734C,
	ARTapToPlace_TryGetTouchPosition_m852168C24C6C48EFD8EBE81BF129AADE5B0C4CCC,
	ARTapToPlace_Update_m556D3AD3B840B808FFFCB39191E4830192B5485F,
	ARTapToPlace_WaitForBaseAnimation_mFDA553B9F37AA0840CEFF4AD883DD960BDD8CD3C,
	ARTapToPlace__ctor_mACE9DBBF058C946B4468A98EC63AF1DF7E7C43FF,
	ARTapToPlace__cctor_m21790C08373A84D2B9B2453AB8162449818B8955,
	NativeAPI_modelProjected_m898BDDD1C8ADBB5BC13AEA0BBC87542CCEF0E7E3,
	NativeAPI_avatarProjected_m45E3F715165A89CF20FDEF7809928AE54CC18EC2,
	NativeAPI__ctor_m584B85BE5D8027FEDBFF657E21E0BF920DD8A3CF,
	U3CWaitForBaseAnimationU3Ed__11__ctor_m4FD68EED7240580AC65045FD4EEC9D2426E95267,
	U3CWaitForBaseAnimationU3Ed__11_System_IDisposable_Dispose_mAAD9EE52AB324B4128B20087D8890EC4C8AC6337,
	U3CWaitForBaseAnimationU3Ed__11_MoveNext_m34004BF6D7B3BC4B849A3F52D3DB442E5F082019,
	U3CWaitForBaseAnimationU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF46859E3EFC3BE349D906EB2DA9CA4ED3CA0268,
	U3CWaitForBaseAnimationU3Ed__11_System_Collections_IEnumerator_Reset_mE91B962DB9EB29CF1117E5C319E2D967FD8125C5,
	U3CWaitForBaseAnimationU3Ed__11_System_Collections_IEnumerator_get_Current_mE8187B7A5EE4119D5D9E4DFC9B90A26D5B21E454,
	ARTapToPlaceAvatar_Awake_m87AF3A614FB8CA4A5A42B4E39158BBB14B0C5493,
	ARTapToPlaceAvatar_TryGetTouchPosition_m7989AD5A9E5D9E7F0B78E11A45CBF91812C149B0,
	ARTapToPlaceAvatar_Update_m51408E6611CE2E7331243B57C12F6E3EA3C32850,
	ARTapToPlaceAvatar__ctor_m3E5DD5CB3D1730710E0918224BB165454CE34947,
	ARTapToPlaceAvatar__cctor_m87D258890B7CA9FECDFF2EB8D3A4F95B97758FC6,
	ContentController_LoadContent_m89D12A8DC1117E9DA3713D745572A20AD3AF0A1A,
	ContentController_OnContentLoaded_m5BF69A7D1CE991B153CE5FD099EEA464DA70866F,
	ContentController_DestroyAllChildren_m1FFD90643F2B1719A51F88A717E300F588AC9BF9,
	ContentController__ctor_m00468CB1576EC1D2FB13CAF80FA88D8AE47E2FFF,
	DragObject1_OnMouseDown_m4EDE2CBD6710F1D5A92CF284BC445D9CA169783B,
	DragObject1_OnMouseDrag_m0B316483CDEBF2D42FCBCAEF9FCF0F0C1E975C83,
	DragObject1__ctor_m8030BF795D44667E18A6D43A020CE65078729ADD,
	LookAtCamera_Start_mCFE758FBB48D39BDD130AC7A6FAF30711DBFE8A6,
	LookAtCamera_Update_m6D9202748A260CEBA5420C43FA50844A642FACFB,
	LookAtCamera__ctor_mCA03F526C152A3EE7381FC8F3E232BB791884238,
	MyDrag_Start_m4A84FE502B25439D102526CE0A287002A0D6BE61,
	MyDrag_Update_m83F87C06848FC0324F2F711559DC62C482FEB2C8,
	MyDrag_Move_m990743428A7BA99499B38DEDC4FABBC548C876B5,
	MyDrag__ctor_mDD8B0C6C518ADF555A75B5F63798C3F8ADEE7E6C,
	ObjectSpawner_Start_mB5C9193D6076E2D9586CA1F5862D4C7D9AF008DE,
	ObjectSpawner_Update_mBCDB36C4FF21AC8AF713E11BDB1FE504B0EC8D19,
	ObjectSpawner_ReLoadUnity_m6C86083ACC3A8BA0C4ED0AAB731B1B5A6021FD89,
	ObjectSpawner_Activate_m34247E7E02036EBF855FE054FE1FFC3EC525CCA8,
	ObjectSpawner_WaitForDownload_m5D57AD9DBB3ECE2AC348E376DEB7F7ECE1A9D79E,
	ObjectSpawner_Redeem_m5BED17761A501AE11EAD3E0C68D452B096FE9E70,
	ObjectSpawner_WaitForRedeemDownload_m04167A9FDCAA59DD8FA7A05CBBD420F01FAA8524,
	ObjectSpawner_RedeemCharacter_m85A125750ED5B2CC129A4BD56C3298D299AE7196,
	ObjectSpawner_WaitForCharacterDownload_mDF68C0DFD2A2FC0F718190335D126DDA02CF5233,
	ObjectSpawner_ShadowSwitch_m36CCB43CCBE225DD0617D953F9646E5DFD225D20,
	ObjectSpawner_ShadowSwitchOn_m788F2C63F6021740EF10ECC05F068DCBDD27197F,
	ObjectSpawner_ShadowSwitchOff_m00D1C076F5B14EA39EA1A4F587159E9518053B3F,
	ObjectSpawner_MovementControl_mDA7E5C4C4DAC392B3EF08EF75D6B107366FFFD24,
	ObjectSpawner_VertMovement_m9D0B43B1F7C7403B751B44AEC2A189CB93EAB98B,
	ObjectSpawner_HoriMovement_m8D8CECCC7BBAB9C22AC67D7612308975FF223C88,
	ObjectSpawner_ClearModelsCache_m08BB5C19D321B601AC455AF1344C10B38CB61A8D,
	ObjectSpawner__ctor_mD103BAD86ABBCFC5726513C071E31158C1A150D3,
	NativeAPI_modelDownloaded_m2CB305BC0327B63C44CFB00C1905881618AE07F6,
	NativeAPI_modelDownloadingDone_mA22BB2476C75156FAF3A540FC40E485B7A6C1036,
	NativeAPI__ctor_m83D6870123E118D4FD44168A1A5F4D593D1D212C,
	U3CWaitForDownloadU3Ed__21__ctor_m08197BFC5916856E72C8DCA01CB0543EA0FB6019,
	U3CWaitForDownloadU3Ed__21_System_IDisposable_Dispose_mF3BBF43CDA2CF2412196777C66C796AEAB8B3439,
	U3CWaitForDownloadU3Ed__21_MoveNext_m0685F461800554B4F31CF4F5D09285CD47B7E699,
	U3CWaitForDownloadU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF099AAEF39D890D39B168DFF43F29620D763F84,
	U3CWaitForDownloadU3Ed__21_System_Collections_IEnumerator_Reset_m85C55B534E8A5F36E2345FCE848023E525B6708D,
	U3CWaitForDownloadU3Ed__21_System_Collections_IEnumerator_get_Current_m28CC9EFB4156A6A1788012C10DE2AC3C6BEC4B92,
	U3CWaitForRedeemDownloadU3Ed__27__ctor_m4AB01FE9B37B424EE5FE5E50FF30BFEAA20F80A1,
	U3CWaitForRedeemDownloadU3Ed__27_System_IDisposable_Dispose_mD9665FF709FD9F71484B6692250C661DDFC7A022,
	U3CWaitForRedeemDownloadU3Ed__27_MoveNext_m0D750FD47DAC5AB2A6AFFBD69B56EED050C54F30,
	U3CWaitForRedeemDownloadU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4CD0810E751CA61ECD341FA4BDBDC13FE16C7F0,
	U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_Reset_m2335D3677F99A5261583F4DAEE2367AB55210015,
	U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_get_Current_m5EBF4F30F9BD903BC0B1AD30C1F5F6C46BEE2A91,
	U3CWaitForCharacterDownloadU3Ed__32__ctor_m9ADD7A4D652EDCA2711785138A1A88A0BCC3D5D4,
	U3CWaitForCharacterDownloadU3Ed__32_System_IDisposable_Dispose_m17A5DCD395D4A509495D3DB61C87231C6E60678E,
	U3CWaitForCharacterDownloadU3Ed__32_MoveNext_m63617555B97894A1350F540D7536BB1E35D2AC24,
	U3CWaitForCharacterDownloadU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m083943FF8AF25E4A9A59AE96268F68B82B7BE85A,
	U3CWaitForCharacterDownloadU3Ed__32_System_Collections_IEnumerator_Reset_m76215B01715E00247B1B570C5455B844F518637D,
	U3CWaitForCharacterDownloadU3Ed__32_System_Collections_IEnumerator_get_Current_m0C3E05B73BF1B8890E6A147388C0B7D389818FC3,
	ParslAvatarController_Start_m041B82DA2972CE14E6D62FDE7C77CF64C7A6742C,
	ParslAvatarController_ModelLoad_m64A42862A4C706201748ACFB7C5252BA48B34492,
	ParslAvatarController_Emote_mBCFA7715DD793D0FD92B38D934F5F3434B39C449,
	ParslAvatarController_WaitForBaseAnimation_m3425207E3455FDE6715B1A98CE72FD693F72EFE5,
	ParslAvatarController_ResetScene_mBAAC05A75FCB803B8376B8B309400F489160132C,
	ParslAvatarController_Back_m6EE03CC0CC8E42395AD6699CA1D9D97B0175244E,
	ParslAvatarController__cancleConfirmation_m697C1E42E74CB344782036B4ADF387E2BD0CF6B1,
	ParslAvatarController__confirmConfirmation_m873AFAFA72BB21917917079FD0AAB17EEB43C097,
	ParslAvatarController_UploadCharacterValues_m3E8E5C574E7BBD26922F30ED6E92338E2857D3F0,
	ParslAvatarController_MaleCharacterButton_m79839D62D6B8A94760178FCF4455FD9782F7E523,
	ParslAvatarController_FemaleCharacterButton_m94C1C272FB4A6423751C4EB6DEAEA7CD1C7D24A4,
	ParslAvatarController_ChildCharacterButton_mE994338D8C51860F2BAB23F5F79E3A90A5759758,
	ParslAvatarController__ctor_mC0DBE4814F323F6F9B28828E1DA67849F2181508,
	NativeAPI_avatarModule_m111BF0F2A32BB0C44F38FD36BC7A3B35742A5D4F,
	NativeAPI__ctor_m0D84FDA583D2D7DDD542EC04BDCA079EC0AC2AD0,
	U3CWaitForBaseAnimationU3Ed__13__ctor_mDFD63E82A5F0AD0928F671E5A89CD7E774F38B3B,
	U3CWaitForBaseAnimationU3Ed__13_System_IDisposable_Dispose_m0FDBAAE832C1D14359B2C3BACF061F3505049B76,
	U3CWaitForBaseAnimationU3Ed__13_MoveNext_m6F2C1477856D8F875A4478284439B8BC8AF8E116,
	U3CWaitForBaseAnimationU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94E02502EA21E4E9180B10B040E77AF3BC80FBE2,
	U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_Reset_mC9611B0DDAFCE6CCAEAB6976ABABC7C2EBDB4BA3,
	U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_get_Current_m8D11DE91BAA64850F574F9601D7BEE2838FD7648,
	PlaceContent_Update_mF3967E7E13C6EEABF0D7EB88616C57AE4E932781,
	PlaceContent_IsClickOverUI_m908B17F5232D4EEA755D4CD9506789DDDA480B5E,
	PlaceContent__ctor_mF4A2CFE6250D7C4D33FF40C14C7AA78CB76B967E,
	PlacementIndicator_Start_mC0F1E7B8F84514D2EDEA712785CDC9399A376200,
	PlacementIndicator_Update_m04B65B8903A034EA4C717E34BE6B8EF861382FE4,
	PlacementIndicator__ctor_mC5EF49C57C7F6519CA9E925EC1A0B13C7D3A08BE,
	PlacementObject_get_Selected_m016672B022C1F4823D54D5D6BD75BF99BFE5870A,
	PlacementObject_set_Selected_mB630BAA0B59D7CE262F123ABA59C77BA8A3CD47E,
	PlacementObject_get_Locked_mC14FA7656C55BAB831982E7E1F076CA9F22B0431,
	PlacementObject_set_Locked_m60DAD119123DDD5179F6A1C9B6771EC1D2F3845E,
	PlacementObject_SetOverlayText_mF8D94744BC43A38F63855EC7A4E2229CAB983608,
	PlacementObject_Awake_mED0B25739C123D2B3EB209A59FE66CAA19F26E2B,
	PlacementObject_ToggleOverlay_m0D61DEBC8FD95937B8FE1C7EE641F301728E5D06,
	PlacementObject_ToggleCanvas_mD425EA9347677BE51246528006BBE50FE96D1755,
	PlacementObject__ctor_m424EC3A34F908843DA88EF32023510705ADB7534,
	PlacementWithMultipleDraggingDroppingController_get_PlacedPrefab_m47A5436759E4FFA877926D91D10C3EA3E0443CA4,
	PlacementWithMultipleDraggingDroppingController_set_PlacedPrefab_m5A1DF8B13B979828211555EF49FE24E0BEC306C4,
	PlacementWithMultipleDraggingDroppingController_Awake_mEA748B763BFF99C724A27BECA9507A967A79D38C,
	PlacementWithMultipleDraggingDroppingController_ChangePrefabSelection_mCA3844D4E77D8203E8C62556847709F2B4640B64,
	PlacementWithMultipleDraggingDroppingController_Dismiss_m5AB0F7E5623981820F41C12A7961B2248676FE2B,
	PlacementWithMultipleDraggingDroppingController_Update_m84750C36697703609A9686184BC3369C275C4257,
	PlacementWithMultipleDraggingDroppingController__ctor_mDB3EA5A58979804F0E380438760B4250B16125F2,
	PlacementWithMultipleDraggingDroppingController__cctor_mAF496DA2B4A28200039E39F19FF8D22344651598,
	PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_0_m6D81DE0716636768E59D9208394BBC637BEEF144,
	PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_1_m0A272DC7C71DB834ADC4521180628482EF5DDD59,
	PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_2_m739E90DF149EDD32C71CB3BA24911CB6F25F348E,
	ReturnCartList__ctor_mFBCE2C6653EFFEACC54DC0C6D65E0ED43609E58A,
	ToggleAR_OnValueChanged_m32E23F57B684D6AA4FF40229CF99D47F2A5EAE43,
	ToggleAR_VisualizePlanes_m64F47DB36EF2D3AA7A8E81F35BF1F5FF584F7073,
	ToggleAR_VisualizePoints_m9362E15B317E73997AF22D5575F32F799D489E97,
	ToggleAR__ctor_mA1CC296DD72AA9242A6D6153B5DD8336C03C3EF1,
	ARAllPointCloudPointsParticleVisualizer_get_mode_mE2961E81075235D3BD24C54D522050A6A16ADF78,
	ARAllPointCloudPointsParticleVisualizer_set_mode_m93C09216743B5544F7EDF9C47FCDD9CD4E6E47C9,
	ARAllPointCloudPointsParticleVisualizer_get_totalPointCount_m82C8E588FD5467B4879E96DE8C1D8699A9FFC178,
	ARAllPointCloudPointsParticleVisualizer_OnPointCloudChanged_mAC37187B41FAA79B6084C2AE0DBE329AAA84E01E,
	ARAllPointCloudPointsParticleVisualizer_SetParticlePosition_m296F1C3C6B0EA3C9D00B67776EB3A8E64B45B5AD,
	ARAllPointCloudPointsParticleVisualizer_RenderPoints_m9308B52D42102E948AF53BE9D349175BE43E18CB,
	ARAllPointCloudPointsParticleVisualizer_Awake_m2039867B6B044168F1A26C1F2EF6D0F4E3354D2D,
	ARAllPointCloudPointsParticleVisualizer_OnEnable_m1F8B642B5E8F23CADAEDC9317FA62A1BE65ACB04,
	ARAllPointCloudPointsParticleVisualizer_OnDisable_m482462736CDA9BAF6FF73CD40214E22B251FF77F,
	ARAllPointCloudPointsParticleVisualizer_Update_m03E104CCAE3B121C955F9C228E16EDD7758896D8,
	ARAllPointCloudPointsParticleVisualizer_UpdateVisibility_m4C296E8EC0365D27C9C0BA909417BFB3A70F3F52,
	ARAllPointCloudPointsParticleVisualizer_SetVisible_mF075BA639C161AC2F4B4457D76FBA7392664B233,
	ARAllPointCloudPointsParticleVisualizer__ctor_m21F6F93A91B2CDD0C2FE8B7BE9BD38B6213B4088,
};
static const int32_t s_InvokerIndices[244] = 
{
	4235,
	3418,
	4235,
	3418,
	4211,
	3394,
	4324,
	4324,
	3318,
	3394,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4235,
	4324,
	4324,
	4324,
	6792,
	6792,
	4324,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4235,
	4324,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	4324,
	4324,
	4324,
	903,
	4235,
	4235,
	724,
	1799,
	2458,
	4324,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	4324,
	4324,
	4235,
	4324,
	4324,
	4235,
	4324,
	4324,
	6792,
	6792,
	4324,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	4324,
	2706,
	4324,
	4235,
	4324,
	6922,
	6794,
	6794,
	4324,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	4324,
	2706,
	4324,
	4324,
	6922,
	3418,
	3418,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	3418,
	4235,
	3418,
	4235,
	3418,
	4235,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	6794,
	6792,
	4324,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	4324,
	3394,
	3394,
	4235,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	6316,
	4324,
	3394,
	4324,
	4272,
	4235,
	4324,
	4235,
	4324,
	4272,
	4324,
	4324,
	4324,
	4324,
	4272,
	3451,
	4272,
	3451,
	3418,
	4324,
	4324,
	4324,
	4324,
	4235,
	3418,
	4324,
	3418,
	4324,
	4324,
	4324,
	6922,
	4324,
	4324,
	4324,
	4324,
	3451,
	3451,
	3451,
	4324,
	4211,
	3394,
	4211,
	3319,
	1666,
	4324,
	4324,
	4324,
	4324,
	4324,
	4324,
	3451,
	4324,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	244,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
