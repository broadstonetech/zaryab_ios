﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARPointCloud_t7801A20C710FCBFDF1A589FA3ED53C7C9DF9222A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF_0_0_0_var), NULL);
	}
}
static void SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE_CustomAttributesCacheGenerator_m_ToggleButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE_CustomAttributesCacheGenerator_m_Log(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
}
static void ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA_CustomAttributesCacheGenerator_ShowCanvasOnBuildingModels_WaitForPanelOff_m6D114493F85D3F312D4261DA99EB95B9A6809FD8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_0_0_0_var), NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__4__ctor_m098F18885B7958CB1FE64B250D907C74CB508CF6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__4_System_IDisposable_Dispose_m42924C65565FE2CEF1DFAC81769C3AFF6A66A90F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m176E4AD63DF89158686C677B760E62A087A42DD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__4_System_Collections_IEnumerator_Reset_m704C805470B5F9028E57EDABACBEE1FA01DE2E39(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__4_System_Collections_IEnumerator_get_Current_mE10B2223B7CC00859CAD1EE6E9924CC94A192C79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Anim_tF3468CFBF078A3F63800C8B0BEA4A5119743F8D4_CustomAttributesCacheGenerator_Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_0_0_0_var), NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_WaitForNetwork_m5DDF6877C90ACCE6954C6C0ED29AB9F5464F5AA5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_0_0_0_var), NULL);
	}
}
static void API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_WaitForValidAsset_mC11999EEFDC00D721B2E162BB0F29083221E1B36(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_0_0_0_var), NULL);
	}
}
static void API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_GetDisplayBundleRoutine_m8DB6214089AC5BDE847DB16A7D56D52AEA074546(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_0_0_0_var), NULL);
	}
}
static void API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_LoadObject_m98069BC268D82CFCD7247117082B7F9BD7BBEE53(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_0_0_0_var), NULL);
	}
}
static void U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__6__ctor_mF1412BE85CBC51E23909332D5EDC22CCCCD52617(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__6_System_IDisposable_Dispose_m63FC1A9D2AF19A2902FC6BEC5CC0C1195686BC3F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44A104805B8BD184D0F86FDE323E627B561D4361(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__6_System_Collections_IEnumerator_Reset_m1D06110E357866A98DDCFB00B2DD00A17ED85E52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__6_System_Collections_IEnumerator_get_Current_m4775A515F0B7D9E7DBB553DDEC00990BA353B302(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__7__ctor_m6EA87D907FC80961BF4FB0E412A22CA79F5CF067(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__7_System_IDisposable_Dispose_m7315B6FBDDF7E7AECCC79C7393C979DEB24010EE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7A06703A12EEF4209975F7AAAE56C360B5CE6FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__7_System_Collections_IEnumerator_Reset_mBE7BDEF329CAAE9AE97662A752B5F6741D13BCDB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__7_System_Collections_IEnumerator_get_Current_m464AED68CC96B1511ECE779635DCF55CAF572825(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__8__ctor_mFA1757821B63C69B29593F24661176E67A6ACA0B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__8_System_IDisposable_Dispose_mF1E7197A2F2E0180E75F2815526E6636A519BB24(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D88626B7D5DA909122C176B1A58505180C12009(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_IEnumerator_Reset_m3AA34E390021291ADF94076A955AA4C210BA594F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_IEnumerator_get_Current_m578329969F9173AE09887DC96FFB02B77ED4CF14(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator_U3CLoadObjectU3Ed__15__ctor_m2806C3D179D49546284BAF86438D104EB1DC67AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator_U3CLoadObjectU3Ed__15_System_IDisposable_Dispose_mDC6EFE309841F623F0CD99CA4FFA6C3428CF6122(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator_U3CLoadObjectU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6CEE352CD7F4696F21B9FF36651A779EBDCB1C28(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator_U3CLoadObjectU3Ed__15_System_Collections_IEnumerator_Reset_m322BF612BBF0C1C308A0ECA2523930472B284D27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator_U3CLoadObjectU3Ed__15_System_Collections_IEnumerator_get_Current_m394C00B32D075F828FD28F3AA7571A7270A67C90(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B_CustomAttributesCacheGenerator_ArCanvas_WaitForPanelOff_m9F639AEE0EE30A52C3105E18747A176D724BB0E3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_0_0_0_var), NULL);
	}
}
static void ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B_CustomAttributesCacheGenerator_ArCanvas_WaitForDestruction_m751A2F37F6BE322997D8B104EEDA798750B8988B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_0_0_0_var), NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__10__ctor_m84F310F154772ECEC43AD769E92401549468716E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__10_System_IDisposable_Dispose_m9634C463036E0D40A849B7FD28D2C4E418C09A02(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B1B663B02F7C7DFAE2DA6CC9CF39306E9B4DADA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__10_System_Collections_IEnumerator_Reset_m6888027D1E67FCDA3F1E57665AFF7373D0AC83BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__10_System_Collections_IEnumerator_get_Current_mAB1A86583188C3DF7875F966ABB187D22A39FB3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__14__ctor_m04BB2BFC18D9F7077464DD0DFFA4356B73AA9609(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__14_System_IDisposable_Dispose_m8DB33991297D578477C32147992BA67E11CFC767(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DEA5FCC9FC6691C55C6BBB6B789397D70AC5612(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__14_System_Collections_IEnumerator_Reset_m1059529BDD33505776B838C83F953447FF0DD01D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__14_System_Collections_IEnumerator_get_Current_mF4BF2E49A071D8349633F2BE5D34F9C6BA244E9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
}
static void ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_CustomAttributesCacheGenerator_ARTapToPlace_WaitForBaseAnimation_mFDA553B9F37AA0840CEFF4AD883DD960BDD8CD3C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_0_0_0_var), NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__11__ctor_m4FD68EED7240580AC65045FD4EEC9D2426E95267(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__11_System_IDisposable_Dispose_mAAD9EE52AB324B4128B20087D8890EC4C8AC6337(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF46859E3EFC3BE349D906EB2DA9CA4ED3CA0268(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__11_System_Collections_IEnumerator_Reset_mE91B962DB9EB29CF1117E5C319E2D967FD8125C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__11_System_Collections_IEnumerator_get_Current_mE8187B7A5EE4119D5D9E4DFC9B90A26D5B21E454(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
}
static void ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForDownload_m5D57AD9DBB3ECE2AC348E376DEB7F7ECE1A9D79E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_0_0_0_var), NULL);
	}
}
static void ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForRedeemDownload_m04167A9FDCAA59DD8FA7A05CBBD420F01FAA8524(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_0_0_0_var), NULL);
	}
}
static void ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForCharacterDownload_mDF68C0DFD2A2FC0F718190335D126DDA02CF5233(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_0_0_0_var), NULL);
	}
}
static void U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__21__ctor_m08197BFC5916856E72C8DCA01CB0543EA0FB6019(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__21_System_IDisposable_Dispose_mF3BBF43CDA2CF2412196777C66C796AEAB8B3439(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF099AAEF39D890D39B168DFF43F29620D763F84(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__21_System_Collections_IEnumerator_Reset_m85C55B534E8A5F36E2345FCE848023E525B6708D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__21_System_Collections_IEnumerator_get_Current_m28CC9EFB4156A6A1788012C10DE2AC3C6BEC4B92(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27__ctor_m4AB01FE9B37B424EE5FE5E50FF30BFEAA20F80A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_IDisposable_Dispose_mD9665FF709FD9F71484B6692250C661DDFC7A022(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4CD0810E751CA61ECD341FA4BDBDC13FE16C7F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_Reset_m2335D3677F99A5261583F4DAEE2367AB55210015(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_get_Current_m5EBF4F30F9BD903BC0B1AD30C1F5F6C46BEE2A91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator_U3CWaitForCharacterDownloadU3Ed__32__ctor_m9ADD7A4D652EDCA2711785138A1A88A0BCC3D5D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator_U3CWaitForCharacterDownloadU3Ed__32_System_IDisposable_Dispose_m17A5DCD395D4A509495D3DB61C87231C6E60678E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator_U3CWaitForCharacterDownloadU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m083943FF8AF25E4A9A59AE96268F68B82B7BE85A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator_U3CWaitForCharacterDownloadU3Ed__32_System_Collections_IEnumerator_Reset_m76215B01715E00247B1B570C5455B844F518637D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator_U3CWaitForCharacterDownloadU3Ed__32_System_Collections_IEnumerator_get_Current_m0C3E05B73BF1B8890E6A147388C0B7D389818FC3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3_CustomAttributesCacheGenerator_ParslAvatarController_WaitForBaseAnimation_m3425207E3455FDE6715B1A98CE72FD693F72EFE5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_0_0_0_var), NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13__ctor_mDFD63E82A5F0AD0928F671E5A89CD7E774F38B3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_IDisposable_Dispose_m0FDBAAE832C1D14359B2C3BACF061F3505049B76(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94E02502EA21E4E9180B10B040E77AF3BC80FBE2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_Reset_mC9611B0DDAFCE6CCAEAB6976ABABC7C2EBDB4BA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_get_Current_m8D11DE91BAA64850F574F9601D7BEE2838FD7648(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_IsSelected(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_IsLocked(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_OverlayText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_canvasComponent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_OverlayDisplayText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_placedPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_welcomePanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_dismissButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_arCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_redButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_greenButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_blueButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_0_m6D81DE0716636768E59D9208394BBC637BEEF144(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_1_m0A272DC7C71DB834ADC4521180628482EF5DDD59(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_2_m739E90DF149EDD32C71CB3BA24911CB6F25F348E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPointCloud_t7801A20C710FCBFDF1A589FA3ED53C7C9DF9222A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARPointCloud_t7801A20C710FCBFDF1A589FA3ED53C7C9DF9222A_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_0_0_0_var), NULL);
	}
}
static void ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B_CustomAttributesCacheGenerator_m_Mode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x74\x68\x65\x72\x20\x74\x6F\x20\x64\x72\x61\x77\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x66\x65\x61\x74\x75\x72\x65\x20\x70\x6F\x69\x6E\x74\x73\x20\x6F\x72\x20\x6F\x6E\x6C\x79\x20\x74\x68\x65\x20\x6F\x6E\x65\x73\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x66\x72\x61\x6D\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[117] = 
{
	SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE_CustomAttributesCacheGenerator,
	rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24_CustomAttributesCacheGenerator,
	U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator,
	U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator,
	U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator,
	U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator,
	U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator,
	U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator,
	U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator,
	ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_CustomAttributesCacheGenerator,
	U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator,
	ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3_CustomAttributesCacheGenerator,
	U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator,
	U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator,
	ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B_CustomAttributesCacheGenerator,
	SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE_CustomAttributesCacheGenerator_m_ToggleButton,
	SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE_CustomAttributesCacheGenerator_m_Log,
	SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE_CustomAttributesCacheGenerator_m_Mode,
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_IsSelected,
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_IsLocked,
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_OverlayText,
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_canvasComponent,
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7_CustomAttributesCacheGenerator_OverlayDisplayText,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_placedPrefab,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_welcomePanel,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_dismissButton,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_arCamera,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_redButton,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_greenButton,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_blueButton,
	ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B_CustomAttributesCacheGenerator_m_Mode,
	ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA_CustomAttributesCacheGenerator_ShowCanvasOnBuildingModels_WaitForPanelOff_m6D114493F85D3F312D4261DA99EB95B9A6809FD8,
	U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__4__ctor_m098F18885B7958CB1FE64B250D907C74CB508CF6,
	U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__4_System_IDisposable_Dispose_m42924C65565FE2CEF1DFAC81769C3AFF6A66A90F,
	U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m176E4AD63DF89158686C677B760E62A087A42DD5,
	U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__4_System_Collections_IEnumerator_Reset_m704C805470B5F9028E57EDABACBEE1FA01DE2E39,
	U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__4_System_Collections_IEnumerator_get_Current_mE10B2223B7CC00859CAD1EE6E9924CC94A192C79,
	Anim_tF3468CFBF078A3F63800C8B0BEA4A5119743F8D4_CustomAttributesCacheGenerator_Anim_delay_m49B92C3EC4783A88BB915F7B9E5828E35676DFE3,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3__ctor_mB41968B0DEEA004DDB90B2E10A791EB96E48D2CD,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_IDisposable_Dispose_m1F06D74D5874EB191FBFA69957B9C26470ADAC4A,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m96FF82B4AFBECC14FD6D4D385BD51F77F6B96E26,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_Reset_mDDF68C47CED558B3AB02F86D903391C0BED82521,
	U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79_CustomAttributesCacheGenerator_U3CdelayU3Ed__3_System_Collections_IEnumerator_get_Current_m9E064515BE7020B0F9EF09EA13C41705F94DE64D,
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_WaitForNetwork_m5DDF6877C90ACCE6954C6C0ED29AB9F5464F5AA5,
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_WaitForValidAsset_mC11999EEFDC00D721B2E162BB0F29083221E1B36,
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_GetDisplayBundleRoutine_m8DB6214089AC5BDE847DB16A7D56D52AEA074546,
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2_CustomAttributesCacheGenerator_API_LoadObject_m98069BC268D82CFCD7247117082B7F9BD7BBEE53,
	U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__6__ctor_mF1412BE85CBC51E23909332D5EDC22CCCCD52617,
	U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__6_System_IDisposable_Dispose_m63FC1A9D2AF19A2902FC6BEC5CC0C1195686BC3F,
	U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44A104805B8BD184D0F86FDE323E627B561D4361,
	U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__6_System_Collections_IEnumerator_Reset_m1D06110E357866A98DDCFB00B2DD00A17ED85E52,
	U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8_CustomAttributesCacheGenerator_U3CWaitForNetworkU3Ed__6_System_Collections_IEnumerator_get_Current_m4775A515F0B7D9E7DBB553DDEC00990BA353B302,
	U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__7__ctor_m6EA87D907FC80961BF4FB0E412A22CA79F5CF067,
	U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__7_System_IDisposable_Dispose_m7315B6FBDDF7E7AECCC79C7393C979DEB24010EE,
	U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7A06703A12EEF4209975F7AAAE56C360B5CE6FC,
	U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__7_System_Collections_IEnumerator_Reset_mBE7BDEF329CAAE9AE97662A752B5F6741D13BCDB,
	U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F_CustomAttributesCacheGenerator_U3CWaitForValidAssetU3Ed__7_System_Collections_IEnumerator_get_Current_m464AED68CC96B1511ECE779635DCF55CAF572825,
	U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__8__ctor_mFA1757821B63C69B29593F24661176E67A6ACA0B,
	U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__8_System_IDisposable_Dispose_mF1E7197A2F2E0180E75F2815526E6636A519BB24,
	U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D88626B7D5DA909122C176B1A58505180C12009,
	U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_IEnumerator_Reset_m3AA34E390021291ADF94076A955AA4C210BA594F,
	U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07_CustomAttributesCacheGenerator_U3CGetDisplayBundleRoutineU3Ed__8_System_Collections_IEnumerator_get_Current_m578329969F9173AE09887DC96FFB02B77ED4CF14,
	U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator_U3CLoadObjectU3Ed__15__ctor_m2806C3D179D49546284BAF86438D104EB1DC67AC,
	U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator_U3CLoadObjectU3Ed__15_System_IDisposable_Dispose_mDC6EFE309841F623F0CD99CA4FFA6C3428CF6122,
	U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator_U3CLoadObjectU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6CEE352CD7F4696F21B9FF36651A779EBDCB1C28,
	U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator_U3CLoadObjectU3Ed__15_System_Collections_IEnumerator_Reset_m322BF612BBF0C1C308A0ECA2523930472B284D27,
	U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3_CustomAttributesCacheGenerator_U3CLoadObjectU3Ed__15_System_Collections_IEnumerator_get_Current_m394C00B32D075F828FD28F3AA7571A7270A67C90,
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B_CustomAttributesCacheGenerator_ArCanvas_WaitForPanelOff_m9F639AEE0EE30A52C3105E18747A176D724BB0E3,
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B_CustomAttributesCacheGenerator_ArCanvas_WaitForDestruction_m751A2F37F6BE322997D8B104EEDA798750B8988B,
	U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__10__ctor_m84F310F154772ECEC43AD769E92401549468716E,
	U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__10_System_IDisposable_Dispose_m9634C463036E0D40A849B7FD28D2C4E418C09A02,
	U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5B1B663B02F7C7DFAE2DA6CC9CF39306E9B4DADA,
	U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__10_System_Collections_IEnumerator_Reset_m6888027D1E67FCDA3F1E57665AFF7373D0AC83BB,
	U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1_CustomAttributesCacheGenerator_U3CWaitForPanelOffU3Ed__10_System_Collections_IEnumerator_get_Current_mAB1A86583188C3DF7875F966ABB187D22A39FB3B,
	U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__14__ctor_m04BB2BFC18D9F7077464DD0DFFA4356B73AA9609,
	U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__14_System_IDisposable_Dispose_m8DB33991297D578477C32147992BA67E11CFC767,
	U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2DEA5FCC9FC6691C55C6BBB6B789397D70AC5612,
	U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__14_System_Collections_IEnumerator_Reset_m1059529BDD33505776B838C83F953447FF0DD01D,
	U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED_CustomAttributesCacheGenerator_U3CWaitForDestructionU3Ed__14_System_Collections_IEnumerator_get_Current_mF4BF2E49A071D8349633F2BE5D34F9C6BA244E9E,
	ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_CustomAttributesCacheGenerator_ARTapToPlace_WaitForBaseAnimation_mFDA553B9F37AA0840CEFF4AD883DD960BDD8CD3C,
	U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__11__ctor_m4FD68EED7240580AC65045FD4EEC9D2426E95267,
	U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__11_System_IDisposable_Dispose_mAAD9EE52AB324B4128B20087D8890EC4C8AC6337,
	U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF46859E3EFC3BE349D906EB2DA9CA4ED3CA0268,
	U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__11_System_Collections_IEnumerator_Reset_mE91B962DB9EB29CF1117E5C319E2D967FD8125C5,
	U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__11_System_Collections_IEnumerator_get_Current_mE8187B7A5EE4119D5D9E4DFC9B90A26D5B21E454,
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForDownload_m5D57AD9DBB3ECE2AC348E376DEB7F7ECE1A9D79E,
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForRedeemDownload_m04167A9FDCAA59DD8FA7A05CBBD420F01FAA8524,
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388_CustomAttributesCacheGenerator_ObjectSpawner_WaitForCharacterDownload_mDF68C0DFD2A2FC0F718190335D126DDA02CF5233,
	U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__21__ctor_m08197BFC5916856E72C8DCA01CB0543EA0FB6019,
	U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__21_System_IDisposable_Dispose_mF3BBF43CDA2CF2412196777C66C796AEAB8B3439,
	U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCF099AAEF39D890D39B168DFF43F29620D763F84,
	U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__21_System_Collections_IEnumerator_Reset_m85C55B534E8A5F36E2345FCE848023E525B6708D,
	U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F_CustomAttributesCacheGenerator_U3CWaitForDownloadU3Ed__21_System_Collections_IEnumerator_get_Current_m28CC9EFB4156A6A1788012C10DE2AC3C6BEC4B92,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27__ctor_m4AB01FE9B37B424EE5FE5E50FF30BFEAA20F80A1,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_IDisposable_Dispose_mD9665FF709FD9F71484B6692250C661DDFC7A022,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4CD0810E751CA61ECD341FA4BDBDC13FE16C7F0,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_Reset_m2335D3677F99A5261583F4DAEE2367AB55210015,
	U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76_CustomAttributesCacheGenerator_U3CWaitForRedeemDownloadU3Ed__27_System_Collections_IEnumerator_get_Current_m5EBF4F30F9BD903BC0B1AD30C1F5F6C46BEE2A91,
	U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator_U3CWaitForCharacterDownloadU3Ed__32__ctor_m9ADD7A4D652EDCA2711785138A1A88A0BCC3D5D4,
	U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator_U3CWaitForCharacterDownloadU3Ed__32_System_IDisposable_Dispose_m17A5DCD395D4A509495D3DB61C87231C6E60678E,
	U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator_U3CWaitForCharacterDownloadU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m083943FF8AF25E4A9A59AE96268F68B82B7BE85A,
	U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator_U3CWaitForCharacterDownloadU3Ed__32_System_Collections_IEnumerator_Reset_m76215B01715E00247B1B570C5455B844F518637D,
	U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE_CustomAttributesCacheGenerator_U3CWaitForCharacterDownloadU3Ed__32_System_Collections_IEnumerator_get_Current_m0C3E05B73BF1B8890E6A147388C0B7D389818FC3,
	ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3_CustomAttributesCacheGenerator_ParslAvatarController_WaitForBaseAnimation_m3425207E3455FDE6715B1A98CE72FD693F72EFE5,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13__ctor_mDFD63E82A5F0AD0928F671E5A89CD7E774F38B3B,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_IDisposable_Dispose_m0FDBAAE832C1D14359B2C3BACF061F3505049B76,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m94E02502EA21E4E9180B10B040E77AF3BC80FBE2,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_Reset_mC9611B0DDAFCE6CCAEAB6976ABABC7C2EBDB4BA3,
	U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9_CustomAttributesCacheGenerator_U3CWaitForBaseAnimationU3Ed__13_System_Collections_IEnumerator_get_Current_m8D11DE91BAA64850F574F9601D7BEE2838FD7648,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_0_m6D81DE0716636768E59D9208394BBC637BEEF144,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_1_m0A272DC7C71DB834ADC4521180628482EF5DDD59,
	PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_CustomAttributesCacheGenerator_PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_2_m739E90DF149EDD32C71CB3BA24911CB6F25F348E,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
