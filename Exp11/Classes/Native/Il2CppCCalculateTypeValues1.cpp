﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Action`1<System.IntPtr>
struct Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914;
// System.Action`1<UnityEngine.VFX.VFXOutputEventArgs>
struct Action_1_t5C8C9298698F95A378E73C4584F33A97EF82A064;
// System.Action`2<System.IntPtr,UnityEngine.XR.ARKit.NSError>
struct Action_2_tF10732C74E0C1A51A43D2ADBE57C998FDD67583D;
// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.ISubsystem>
struct Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B;
// System.Collections.Generic.Dictionary`2<System.UInt64,UnityEngine.Vector3>
struct Dictionary_2_t921FF2EF21AD8B2BCFAD3713C351119422CE55FB;
// System.Collections.Generic.HashSet`1<UnityEngine.TextureFormat>
struct HashSet_1_t7981318DC35BC53AD64634B2D643DD6055A39054;
// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>
struct List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor>
struct List_1_tDED98C236097B36F9015B396398179A6F8A62E50;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor>
struct List_1_t5ACA7E75885D8B9D7B85548B84BF43976A5038DC;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor>
struct List_1_t449C61AAF5F71828F78F12D50C77AE776AF0E330;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor>
struct List_1_tCD8BCD5191A621FA7E8E3F2D67BA5D2BCE0FC04F;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor>
struct List_1_tA1D273638689FBC5ED4F3CAF82F64C158000481E;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor>
struct List_1_tB23F14817387B6E0CF6BC3F698BE74D4321CBBD4;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor>
struct List_1_t179969632C4CD7B25BEBAA7DA12BA4C8F82F69F2;
// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystemDescriptor>
struct List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0;
// System.Collections.Generic.List`1<UnityEngine.XR.XRMeshSubsystemDescriptor>
struct List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor>
struct List_1_t7B38C168F61005A6F1570A6CB78D9FBC8916E16D;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor>
struct List_1_t9AA280C4698A976F6616D552D829D1609D4A65BC;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor>
struct List_1_tA457BDB78534FD0C67280F0D299F21C6BB7C23BF;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor>
struct List_1_t6D435690E62CDB3645DB1A76F3B7B8B6069BDB4F;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor>
struct List_1_tAE84735071B78277703DB9996DE2E5C4456317C5;
// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor>
struct List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673;
// System.Collections.Generic.List`1<AddToCartScript/ItemsData>
struct List_1_t5A371EF4F5E6A6D6CD6E33ECF11BBAC748F84D8F;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// UnityEngine.Material[]
struct MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492;
// PlacementObject[]
struct PlacementObjectU5BU5D_t4FA713DF94452422EB8FD49580571D344722A54E;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.TextureFormat[]
struct TextureFormatU5BU5D_tC081C559AF0CE6678E9673429ADDC9B93B6B45CE;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C;
// API
struct API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2;
// UnityEngine.XR.ARKit.ARKitSessionDelegate
struct ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE;
// UnityEngine.XR.ARFoundation.ARPlaneManager
struct ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4;
// UnityEngine.XR.ARFoundation.ARPointCloud
struct ARPointCloud_t7801A20C710FCBFDF1A589FA3ED53C7C9DF9222A;
// UnityEngine.XR.ARFoundation.ARPointCloudManager
struct ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF;
// UnityEngine.XR.ARFoundation.ARRaycastManager
struct ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F;
// ARTapToPlace
struct ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E;
// ArCanvas
struct ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B;
// UnityEngine.AssetBundle
struct AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_tBCF59D1FD408125E4C2C937EC23AB0ABB7E4051A;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E;
// UnityEngine.XR.ARSubsystems.ConfigurationChooser
struct ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// ObjectSpawner
struct ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388;
// ParslAvatarController
struct ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E;
// PlacementIndicator
struct PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275;
// PlacementObject
struct PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50;
// UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary
struct RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B;
// UnityEngine.Shader
struct Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39;
// ShowCanvasOnBuildingModels
struct ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// UnityEngine.SubsystemsImplementation.SubsystemProvider
struct SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// TMPro.TextMeshPro
struct TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA;
// System.Uri
struct Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612;
// UnityEngine.VFX.VFXEventAttribute
struct VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86;
// UnityEngine.VFX.VisualEffectAsset
struct VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor
struct XRAnchorSubsystemDescriptor_t3BD7F9922EF5C04185D59349C76D625BC1E44E3B;
// UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor
struct XRCameraSubsystemDescriptor_t1F8A45C69031E2981B1863518C43793D26E2C5E5;
// UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor
struct XRDepthSubsystemDescriptor_t745DBB7D313FB52F756E0B7AA993FBBC26B412C2;
// UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor
struct XREnvironmentProbeSubsystemDescriptor_t7C10519F545418330347AC7434FBB10F39DD4243;
// UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor
struct XRHumanBodySubsystemDescriptor_t00E75DD05B03BCC1BF5A794547615692B7A55C04;
// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor
struct XRImageTrackingSubsystemDescriptor_t3EC191739B144A8AA00CEEE03E8F7FF01D7F833B;
// UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor
struct XRObjectTrackingSubsystemDescriptor_t831B568A9BE175A6A79BB87E25DEFAC519A6C472;
// UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor
struct XROcclusionSubsystemDescriptor_tC9C8F2EFB7768358C203968CA71D353F0DD234FB;
// UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor
struct XRParticipantSubsystemDescriptor_t533EE70DC8D4B105B9C87B76D35A7D59A84BCA55;
// UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor
struct XRPlaneSubsystemDescriptor_t98B66B6D99804656DDDB45C9BDA61C2EE4EDB483;
// UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor
struct XRRaycastSubsystemDescriptor_tB9891F63FC4871797BCD04DB7167142BE2049B2C;
// UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary
struct XRReferenceObjectLibrary_t07704B2996E507F23EE3C99CFC3BB73A83C99A7C;
// UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor
struct XRSessionSubsystemDescriptor_tC45A49D1179090D5C6D3B3DC1DC31CAB5A627B1C;
// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct ErrorEventHandler_tD47781EBB7CF0CC4C111496024BD59B1D1A6A1F2;
// UnityEngine.Video.VideoPlayer/EventHandler
struct EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD;
// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct FrameReadyEventHandler_t9529BD5A34E9C8BE7D8A39D46A6C4ABC673374EC;
// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct TimeEventHandler_t7CA131EB85E0FFCBE8660E030698BD83D3994DD8;
// UnityEngine.XR.ARSubsystems.XRAnchorSubsystem/Provider
struct Provider_t9F286D20EB73EBBA4B6E7203C7A9051BE673C2E2;
// UnityEngine.XR.ARSubsystems.XRCameraSubsystem/Provider
struct Provider_t55916B0D2766C320DCA36A0C870BA2FD80F8B6D1;
// UnityEngine.XR.ARSubsystems.XRDepthSubsystem/Provider
struct Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6;
// UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/Provider
struct Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190;
// UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem/Provider
struct Provider_t055C90C34B2BCE8D134DF44C12823E320519168C;
// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/Provider
struct Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E;
// UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem/Provider
struct Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E;
// UnityEngine.XR.ARSubsystems.XROcclusionSubsystem/Provider
struct Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB;
// UnityEngine.XR.ARSubsystems.XRParticipantSubsystem/Provider
struct Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E;
// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/Provider
struct Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12;
// UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/Provider
struct Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227;
// UnityEngine.XR.ARSubsystems.XRSessionSubsystem/Provider
struct Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A;
// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate
struct OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF;

struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com;
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com;
struct VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF_marshaled_com;
struct VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_tEB1A1F441D3E4E8A35F3B9EE4147916810B1DD3B 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_tE7281C0E269ACF224AB82F00FE8D46ED8C621032 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_t9BC900AC115CCA160074141915B355D8F694FB1F 
{
public:

public:
};


// <Module>
struct U3CModuleU3E_t7757219A6D4DF3F0E2950E860119AEA621C68AF1 
{
public:

public:
};


// System.Object


// UnityEngine.XR.ARKit.ARKitLoaderConstants
struct ARKitLoaderConstants_t1A46CABB17835CC85326A66CF3921059BB927B13  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitMeshSubsystemExtensions
struct ARKitMeshSubsystemExtensions_tA52932FCB3EF1A3607716616D0217B10B222329D  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitSessionDelegate
struct ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE  : public RuntimeObject
{
public:

public:
};

struct ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields
{
public:
	// System.Action`2<System.IntPtr,UnityEngine.XR.ARKit.NSError> UnityEngine.XR.ARKit.ARKitSessionDelegate::s_SessionDidFailWithError
	Action_2_tF10732C74E0C1A51A43D2ADBE57C998FDD67583D * ___s_SessionDidFailWithError_0;
	// System.Action`1<System.IntPtr> UnityEngine.XR.ARKit.ARKitSessionDelegate::s_CoachingOverlayViewWillActivate
	Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * ___s_CoachingOverlayViewWillActivate_1;
	// System.Action`1<System.IntPtr> UnityEngine.XR.ARKit.ARKitSessionDelegate::s_CoachingOverlayViewDidDeactivate
	Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * ___s_CoachingOverlayViewDidDeactivate_2;
	// System.Action`1<System.IntPtr> UnityEngine.XR.ARKit.ARKitSessionDelegate::s_ConfigurationChanged
	Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * ___s_ConfigurationChanged_3;

public:
	inline static int32_t get_offset_of_s_SessionDidFailWithError_0() { return static_cast<int32_t>(offsetof(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields, ___s_SessionDidFailWithError_0)); }
	inline Action_2_tF10732C74E0C1A51A43D2ADBE57C998FDD67583D * get_s_SessionDidFailWithError_0() const { return ___s_SessionDidFailWithError_0; }
	inline Action_2_tF10732C74E0C1A51A43D2ADBE57C998FDD67583D ** get_address_of_s_SessionDidFailWithError_0() { return &___s_SessionDidFailWithError_0; }
	inline void set_s_SessionDidFailWithError_0(Action_2_tF10732C74E0C1A51A43D2ADBE57C998FDD67583D * value)
	{
		___s_SessionDidFailWithError_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SessionDidFailWithError_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_CoachingOverlayViewWillActivate_1() { return static_cast<int32_t>(offsetof(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields, ___s_CoachingOverlayViewWillActivate_1)); }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * get_s_CoachingOverlayViewWillActivate_1() const { return ___s_CoachingOverlayViewWillActivate_1; }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 ** get_address_of_s_CoachingOverlayViewWillActivate_1() { return &___s_CoachingOverlayViewWillActivate_1; }
	inline void set_s_CoachingOverlayViewWillActivate_1(Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * value)
	{
		___s_CoachingOverlayViewWillActivate_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_CoachingOverlayViewWillActivate_1), (void*)value);
	}

	inline static int32_t get_offset_of_s_CoachingOverlayViewDidDeactivate_2() { return static_cast<int32_t>(offsetof(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields, ___s_CoachingOverlayViewDidDeactivate_2)); }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * get_s_CoachingOverlayViewDidDeactivate_2() const { return ___s_CoachingOverlayViewDidDeactivate_2; }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 ** get_address_of_s_CoachingOverlayViewDidDeactivate_2() { return &___s_CoachingOverlayViewDidDeactivate_2; }
	inline void set_s_CoachingOverlayViewDidDeactivate_2(Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * value)
	{
		___s_CoachingOverlayViewDidDeactivate_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_CoachingOverlayViewDidDeactivate_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_ConfigurationChanged_3() { return static_cast<int32_t>(offsetof(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields, ___s_ConfigurationChanged_3)); }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * get_s_ConfigurationChanged_3() const { return ___s_ConfigurationChanged_3; }
	inline Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 ** get_address_of_s_ConfigurationChanged_3() { return &___s_ConfigurationChanged_3; }
	inline void set_s_ConfigurationChanged_3(Action_1_t35A46FAEE6B0A26D311444DF75B6EAFC59EBD914 * value)
	{
		___s_ConfigurationChanged_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ConfigurationChanged_3), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARWorldMapRequestStatusExtensions
struct ARWorldMapRequestStatusExtensions_tBA8B558AE95AA57A9FB348869D8F63494FDB5C1A  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.Api
struct Api_tA818DF9363CED72D41D212D7B479C32A793F1D9C  : public RuntimeObject
{
public:

public:
};


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.EnvironmentProbeApi
struct EnvironmentProbeApi_t2B36D33D812E48CB4615B2FB0B41BD22D184BB19  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.InputLayoutLoader
struct InputLayoutLoader_t792860969FB135A9DDEF032345D0AE0E6C4988A0  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.NSErrorExtensions
struct NSErrorExtensions_t6DCF646D8E7E93886A929C22BC31AD2F85E5BDAB  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.NSObject
struct NSObject_tDE23F4BBB8E43766E6795164F9B80137E83E96C0  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary
struct RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B  : public RuntimeObject
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider
struct SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.SubsystemsImplementation.SubsystemProvider::m_Running
	bool ___m_Running_0;

public:
	inline static int32_t get_offset_of_m_Running_0() { return static_cast<int32_t>(offsetof(SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9, ___m_Running_0)); }
	inline bool get_m_Running_0() const { return ___m_Running_0; }
	inline bool* get_address_of_m_Running_0() { return &___m_Running_0; }
	inline void set_m_Running_0(bool value)
	{
		___m_Running_0 = value;
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider
struct SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.SubsystemsImplementation.SubsystemWithProvider::<running>k__BackingField
	bool ___U3CrunningU3Ek__BackingField_0;
	// UnityEngine.SubsystemsImplementation.SubsystemProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider::<providerBase>k__BackingField
	SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9 * ___U3CproviderBaseU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CrunningU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E, ___U3CrunningU3Ek__BackingField_0)); }
	inline bool get_U3CrunningU3Ek__BackingField_0() const { return ___U3CrunningU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CrunningU3Ek__BackingField_0() { return &___U3CrunningU3Ek__BackingField_0; }
	inline void set_U3CrunningU3Ek__BackingField_0(bool value)
	{
		___U3CrunningU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CproviderBaseU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E, ___U3CproviderBaseU3Ek__BackingField_1)); }
	inline SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9 * get_U3CproviderBaseU3Ek__BackingField_1() const { return ___U3CproviderBaseU3Ek__BackingField_1; }
	inline SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9 ** get_address_of_U3CproviderBaseU3Ek__BackingField_1() { return &___U3CproviderBaseU3Ek__BackingField_1; }
	inline void set_U3CproviderBaseU3Ek__BackingField_1(SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9 * value)
	{
		___U3CproviderBaseU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderBaseU3Ek__BackingField_1), (void*)value);
	}
};


// UnityEngine.VFX.VFXManager
struct VFXManager_tE525803FFE4F59D87D5B5E61AF8433037F226340  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// API/<GetDisplayBundleRoutine>d__8
struct U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07  : public RuntimeObject
{
public:
	// System.Int32 API/<GetDisplayBundleRoutine>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object API/<GetDisplayBundleRoutine>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String API/<GetDisplayBundleRoutine>d__8::assetName
	String_t* ___assetName_2;
	// API API/<GetDisplayBundleRoutine>d__8::<>4__this
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * ___U3CU3E4__this_3;
	// UnityEngine.Networking.UnityWebRequest API/<GetDisplayBundleRoutine>d__8::<www>5__2
	UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * ___U3CwwwU3E5__2_4;
	// UnityEngine.Networking.DownloadHandler API/<GetDisplayBundleRoutine>d__8::<handle>5__3
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * ___U3ChandleU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_assetName_2() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07, ___assetName_2)); }
	inline String_t* get_assetName_2() const { return ___assetName_2; }
	inline String_t** get_address_of_assetName_2() { return &___assetName_2; }
	inline void set_assetName_2(String_t* value)
	{
		___assetName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assetName_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07, ___U3CU3E4__this_3)); }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07, ___U3CwwwU3E5__2_4)); }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CwwwU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3ChandleU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07, ___U3ChandleU3E5__3_5)); }
	inline DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * get_U3ChandleU3E5__3_5() const { return ___U3ChandleU3E5__3_5; }
	inline DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB ** get_address_of_U3ChandleU3E5__3_5() { return &___U3ChandleU3E5__3_5; }
	inline void set_U3ChandleU3E5__3_5(DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * value)
	{
		___U3ChandleU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3ChandleU3E5__3_5), (void*)value);
	}
};


// API/<LoadObject>d__15
struct U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3  : public RuntimeObject
{
public:
	// System.Int32 API/<LoadObject>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object API/<LoadObject>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String API/<LoadObject>d__15::path
	String_t* ___path_2;
	// API API/<LoadObject>d__15::<>4__this
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * ___U3CU3E4__this_3;
	// UnityEngine.AssetBundleRequest API/<LoadObject>d__15::<assetBundleRequest>5__2
	AssetBundleRequest_tBCF59D1FD408125E4C2C937EC23AB0ABB7E4051A * ___U3CassetBundleRequestU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3, ___U3CU3E4__this_3)); }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CassetBundleRequestU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3, ___U3CassetBundleRequestU3E5__2_4)); }
	inline AssetBundleRequest_tBCF59D1FD408125E4C2C937EC23AB0ABB7E4051A * get_U3CassetBundleRequestU3E5__2_4() const { return ___U3CassetBundleRequestU3E5__2_4; }
	inline AssetBundleRequest_tBCF59D1FD408125E4C2C937EC23AB0ABB7E4051A ** get_address_of_U3CassetBundleRequestU3E5__2_4() { return &___U3CassetBundleRequestU3E5__2_4; }
	inline void set_U3CassetBundleRequestU3E5__2_4(AssetBundleRequest_tBCF59D1FD408125E4C2C937EC23AB0ABB7E4051A * value)
	{
		___U3CassetBundleRequestU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CassetBundleRequestU3E5__2_4), (void*)value);
	}
};


// API/<WaitForNetwork>d__6
struct U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8  : public RuntimeObject
{
public:
	// System.Int32 API/<WaitForNetwork>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object API/<WaitForNetwork>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// API API/<WaitForNetwork>d__6::<>4__this
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8, ___U3CU3E4__this_2)); }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// API/<WaitForValidAsset>d__7
struct U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F  : public RuntimeObject
{
public:
	// System.Int32 API/<WaitForValidAsset>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object API/<WaitForValidAsset>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// API API/<WaitForValidAsset>d__7::<>4__this
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F, ___U3CU3E4__this_2)); }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitCameraSubsystem/NativeApi
struct NativeApi_tA8D79572A35E338F74C186C460AA1780DD18222A  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitCpuImageApi/Native
struct Native_tF04A9C18AA77B29062CE0428C310D081D0D35E77  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitHumanBodySubsystem/NativeApi
struct NativeApi_tD8D7F6FC5A365EA4D3189425623AC3D23A7ED03B  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitMeshSubsystemExtensions/NativeApi
struct NativeApi_tDB232E5FEE2CD4783CEB5C809F129E4A2A4E7EB5  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/NativeApi
struct NativeApi_tFF41350E0941271CB10B461CA357ABF510ACB223  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitRaycastSubsystem/NativeApi
struct NativeApi_t62CE96BA0CF6D47C5FD80B91223590E60F948070  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi
struct NativeApi_t14734856858FD58426CAE2F6A09FA522E550C226  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/NativeApi
struct NativeApi_tB65E4D304D37EA8B8ED6329919AEE7845A85C55E  : public RuntimeObject
{
public:

public:
};


// ARTapToPlace/<WaitForBaseAnimation>d__11
struct U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA  : public RuntimeObject
{
public:
	// System.Int32 ARTapToPlace/<WaitForBaseAnimation>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ARTapToPlace/<WaitForBaseAnimation>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ARTapToPlace ARTapToPlace/<WaitForBaseAnimation>d__11::<>4__this
	ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA, ___U3CU3E4__this_2)); }
	inline ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ARTapToPlace/NativeAPI
struct NativeAPI_t6DB0B44081D67DC7D8297AAC58876922A63CDB03  : public RuntimeObject
{
public:

public:
};


// Anim/<delay>d__3
struct U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79  : public RuntimeObject
{
public:
	// System.Int32 Anim/<delay>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Anim/<delay>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}
};


// ArCanvas/<WaitForDestruction>d__14
struct U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED  : public RuntimeObject
{
public:
	// System.Int32 ArCanvas/<WaitForDestruction>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ArCanvas/<WaitForDestruction>d__14::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ArCanvas ArCanvas/<WaitForDestruction>d__14::<>4__this
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED, ___U3CU3E4__this_2)); }
	inline ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ArCanvas/<WaitForPanelOff>d__10
struct U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1  : public RuntimeObject
{
public:
	// System.Int32 ArCanvas/<WaitForPanelOff>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ArCanvas/<WaitForPanelOff>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ArCanvas ArCanvas/<WaitForPanelOff>d__10::<>4__this
	ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1, ___U3CU3E4__this_2)); }
	inline ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ArCanvas/NativeAPI
struct NativeAPI_t0C0CF7D8A37491A6EE905628D637573B751EFBA9  : public RuntimeObject
{
public:

public:
};


// ObjectSpawner/<WaitForCharacterDownload>d__32
struct U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE  : public RuntimeObject
{
public:
	// System.Int32 ObjectSpawner/<WaitForCharacterDownload>d__32::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ObjectSpawner/<WaitForCharacterDownload>d__32::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ObjectSpawner ObjectSpawner/<WaitForCharacterDownload>d__32::<>4__this
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE, ___U3CU3E4__this_2)); }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ObjectSpawner/<WaitForDownload>d__21
struct U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F  : public RuntimeObject
{
public:
	// System.Int32 ObjectSpawner/<WaitForDownload>d__21::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ObjectSpawner/<WaitForDownload>d__21::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ObjectSpawner ObjectSpawner/<WaitForDownload>d__21::<>4__this
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F, ___U3CU3E4__this_2)); }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ObjectSpawner/<WaitForRedeemDownload>d__27
struct U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76  : public RuntimeObject
{
public:
	// System.Int32 ObjectSpawner/<WaitForRedeemDownload>d__27::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ObjectSpawner/<WaitForRedeemDownload>d__27::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ObjectSpawner ObjectSpawner/<WaitForRedeemDownload>d__27::<>4__this
	ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76, ___U3CU3E4__this_2)); }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ObjectSpawner/NativeAPI
struct NativeAPI_tA82FCF52A5EFF585B109939C30419CF8D4259A65  : public RuntimeObject
{
public:

public:
};


// ParslAvatarController/<WaitForBaseAnimation>d__13
struct U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9  : public RuntimeObject
{
public:
	// System.Int32 ParslAvatarController/<WaitForBaseAnimation>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ParslAvatarController/<WaitForBaseAnimation>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ParslAvatarController ParslAvatarController/<WaitForBaseAnimation>d__13::<>4__this
	ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9, ___U3CU3E4__this_2)); }
	inline ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ParslAvatarController/NativeAPI
struct NativeAPI_t8130043C884E3E066EA5BD75C35CFD0662C8B501  : public RuntimeObject
{
public:

public:
};


// ShowCanvasOnBuildingModels/<WaitForPanelOff>d__4
struct U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2  : public RuntimeObject
{
public:
	// System.Int32 ShowCanvasOnBuildingModels/<WaitForPanelOff>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object ShowCanvasOnBuildingModels/<WaitForPanelOff>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// ShowCanvasOnBuildingModels ShowCanvasOnBuildingModels/<WaitForPanelOff>d__4::<>4__this
	ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2, ___U3CU3E4__this_2)); }
	inline ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// ShowCanvasOnBuildingModels/NativeAPI
struct NativeAPI_t315FBF47DE43B17EAD085102FBAA4F7286D06A69  : public RuntimeObject
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRCpuImage/Api
struct Api_t7C92F00C6416A2C636A44AAC833C3773C567DC3E  : public RuntimeObject
{
public:

public:
};


// Unity.Collections.NativeSlice`1<System.Byte>
struct NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B 
{
public:
	// System.Byte* Unity.Collections.NativeSlice`1::m_Buffer
	uint8_t* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeSlice`1::m_Stride
	int32_t ___m_Stride_1;
	// System.Int32 Unity.Collections.NativeSlice`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B, ___m_Buffer_0)); }
	inline uint8_t* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline uint8_t** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(uint8_t* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Stride_1() { return static_cast<int32_t>(offsetof(NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B, ___m_Stride_1)); }
	inline int32_t get_m_Stride_1() const { return ___m_Stride_1; }
	inline int32_t* get_address_of_m_Stride_1() { return &___m_Stride_1; }
	inline void set_m_Stride_1(int32_t value)
	{
		___m_Stride_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// Unity.Collections.NativeSlice`1<System.UInt32>
struct NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A 
{
public:
	// System.Byte* Unity.Collections.NativeSlice`1::m_Buffer
	uint8_t* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeSlice`1::m_Stride
	int32_t ___m_Stride_1;
	// System.Int32 Unity.Collections.NativeSlice`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A, ___m_Buffer_0)); }
	inline uint8_t* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline uint8_t** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(uint8_t* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Stride_1() { return static_cast<int32_t>(offsetof(NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A, ___m_Stride_1)); }
	inline int32_t get_m_Stride_1() const { return ___m_Stride_1; }
	inline int32_t* get_address_of_m_Stride_1() { return &___m_Stride_1; }
	inline void set_m_Stride_1(int32_t value)
	{
		___m_Stride_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRAnchorSubsystem>
struct SubsystemProvider_1_t302358330269847780327C2298A4FFA7D79AF2BF  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRCameraSubsystem>
struct SubsystemProvider_1_t3B6396AEE76B5D8268802608E3593AA3D48DB307  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRDepthSubsystem>
struct SubsystemProvider_1_tBB539901FE99992CAA10A1EFDFA610E048498E98  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem>
struct SubsystemProvider_1_tC3DB99A11F9F3210CE2ABA9FE09C127C5B13FF80  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem>
struct SubsystemProvider_1_tBF37BFFB47314B7D87E24F4C4903C90930C0302C  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem>
struct SubsystemProvider_1_t3086BC462E1384FBB8137E64FA6C513FC002E440  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem>
struct SubsystemProvider_1_t71FE677A1A2F32123FE4C7868D5943892028AE12  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XROcclusionSubsystem>
struct SubsystemProvider_1_t57D5C398A7A30AC3C3674CA126FAE612BC00F597  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRParticipantSubsystem>
struct SubsystemProvider_1_t3D6D4D936F16F3264F75D1BAB46A4A398F18F204  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRPlaneSubsystem>
struct SubsystemProvider_1_tF2F3B0C041BDD07A00CD49B25AE6016B61F24816  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRRaycastSubsystem>
struct SubsystemProvider_1_t7ACBE98539B067B19E2D5BCC2B852277F4A38875  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemProvider`1<UnityEngine.XR.ARSubsystems.XRSessionSubsystem>
struct SubsystemProvider_1_tFA56F133FD9BCE90A1C4C7D15FFE2571963D8DE4  : public SubsystemProvider_t39E89FB8DB1EF1D2B0AF93796AEDB19D76A665F9
{
public:

public:
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRAnchorSubsystem,UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRAnchorSubsystem/Provider>
struct SubsystemWithProvider_3_tD91EB2F57F19DA2CDB9A5E0011978CA1EA351BA2  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRAnchorSubsystemDescriptor_t3BD7F9922EF5C04185D59349C76D625BC1E44E3B * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t9F286D20EB73EBBA4B6E7203C7A9051BE673C2E2 * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_tD91EB2F57F19DA2CDB9A5E0011978CA1EA351BA2, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRAnchorSubsystemDescriptor_t3BD7F9922EF5C04185D59349C76D625BC1E44E3B * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRAnchorSubsystemDescriptor_t3BD7F9922EF5C04185D59349C76D625BC1E44E3B ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRAnchorSubsystemDescriptor_t3BD7F9922EF5C04185D59349C76D625BC1E44E3B * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_tD91EB2F57F19DA2CDB9A5E0011978CA1EA351BA2, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t9F286D20EB73EBBA4B6E7203C7A9051BE673C2E2 * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t9F286D20EB73EBBA4B6E7203C7A9051BE673C2E2 ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t9F286D20EB73EBBA4B6E7203C7A9051BE673C2E2 * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRCameraSubsystem,UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRCameraSubsystem/Provider>
struct SubsystemWithProvider_3_tA938665692EBC0CA746A276F8413E462E8930FD4  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRCameraSubsystemDescriptor_t1F8A45C69031E2981B1863518C43793D26E2C5E5 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t55916B0D2766C320DCA36A0C870BA2FD80F8B6D1 * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_tA938665692EBC0CA746A276F8413E462E8930FD4, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRCameraSubsystemDescriptor_t1F8A45C69031E2981B1863518C43793D26E2C5E5 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRCameraSubsystemDescriptor_t1F8A45C69031E2981B1863518C43793D26E2C5E5 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRCameraSubsystemDescriptor_t1F8A45C69031E2981B1863518C43793D26E2C5E5 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_tA938665692EBC0CA746A276F8413E462E8930FD4, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t55916B0D2766C320DCA36A0C870BA2FD80F8B6D1 * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t55916B0D2766C320DCA36A0C870BA2FD80F8B6D1 ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t55916B0D2766C320DCA36A0C870BA2FD80F8B6D1 * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRDepthSubsystem,UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRDepthSubsystem/Provider>
struct SubsystemWithProvider_3_tD436D6BE4AA164ED727D09EFDE50FF8DCAA50D98  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRDepthSubsystemDescriptor_t745DBB7D313FB52F756E0B7AA993FBBC26B412C2 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6 * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_tD436D6BE4AA164ED727D09EFDE50FF8DCAA50D98, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRDepthSubsystemDescriptor_t745DBB7D313FB52F756E0B7AA993FBBC26B412C2 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRDepthSubsystemDescriptor_t745DBB7D313FB52F756E0B7AA993FBBC26B412C2 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRDepthSubsystemDescriptor_t745DBB7D313FB52F756E0B7AA993FBBC26B412C2 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_tD436D6BE4AA164ED727D09EFDE50FF8DCAA50D98, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6 * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6 ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6 * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/Provider>
struct SubsystemWithProvider_3_t8B33A21A2B183DB3F429FD3F0A899D7ED1BB4DEA  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XREnvironmentProbeSubsystemDescriptor_t7C10519F545418330347AC7434FBB10F39DD4243 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190 * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t8B33A21A2B183DB3F429FD3F0A899D7ED1BB4DEA, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XREnvironmentProbeSubsystemDescriptor_t7C10519F545418330347AC7434FBB10F39DD4243 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XREnvironmentProbeSubsystemDescriptor_t7C10519F545418330347AC7434FBB10F39DD4243 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XREnvironmentProbeSubsystemDescriptor_t7C10519F545418330347AC7434FBB10F39DD4243 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t8B33A21A2B183DB3F429FD3F0A899D7ED1BB4DEA, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190 * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190 ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190 * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem/Provider>
struct SubsystemWithProvider_3_t152AEC9946809B23BD9A7DE32A2113E12B8CE2C2  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRHumanBodySubsystemDescriptor_t00E75DD05B03BCC1BF5A794547615692B7A55C04 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t055C90C34B2BCE8D134DF44C12823E320519168C * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t152AEC9946809B23BD9A7DE32A2113E12B8CE2C2, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRHumanBodySubsystemDescriptor_t00E75DD05B03BCC1BF5A794547615692B7A55C04 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRHumanBodySubsystemDescriptor_t00E75DD05B03BCC1BF5A794547615692B7A55C04 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRHumanBodySubsystemDescriptor_t00E75DD05B03BCC1BF5A794547615692B7A55C04 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t152AEC9946809B23BD9A7DE32A2113E12B8CE2C2, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t055C90C34B2BCE8D134DF44C12823E320519168C * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t055C90C34B2BCE8D134DF44C12823E320519168C ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t055C90C34B2BCE8D134DF44C12823E320519168C * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/Provider>
struct SubsystemWithProvider_3_t249D82EF0730E7FF15F2B19C4BB45B2E08CF620B  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRImageTrackingSubsystemDescriptor_t3EC191739B144A8AA00CEEE03E8F7FF01D7F833B * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t249D82EF0730E7FF15F2B19C4BB45B2E08CF620B, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRImageTrackingSubsystemDescriptor_t3EC191739B144A8AA00CEEE03E8F7FF01D7F833B * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRImageTrackingSubsystemDescriptor_t3EC191739B144A8AA00CEEE03E8F7FF01D7F833B ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRImageTrackingSubsystemDescriptor_t3EC191739B144A8AA00CEEE03E8F7FF01D7F833B * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t249D82EF0730E7FF15F2B19C4BB45B2E08CF620B, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem/Provider>
struct SubsystemWithProvider_3_t2622D99FF6F2A6B95B2E82547A76A7E7712AA7DB  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRObjectTrackingSubsystemDescriptor_t831B568A9BE175A6A79BB87E25DEFAC519A6C472 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2622D99FF6F2A6B95B2E82547A76A7E7712AA7DB, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRObjectTrackingSubsystemDescriptor_t831B568A9BE175A6A79BB87E25DEFAC519A6C472 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRObjectTrackingSubsystemDescriptor_t831B568A9BE175A6A79BB87E25DEFAC519A6C472 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRObjectTrackingSubsystemDescriptor_t831B568A9BE175A6A79BB87E25DEFAC519A6C472 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2622D99FF6F2A6B95B2E82547A76A7E7712AA7DB, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XROcclusionSubsystem,UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XROcclusionSubsystem/Provider>
struct SubsystemWithProvider_3_t2838D413336061A31AFDEA49065AD29BD1EB3A1B  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XROcclusionSubsystemDescriptor_tC9C8F2EFB7768358C203968CA71D353F0DD234FB * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2838D413336061A31AFDEA49065AD29BD1EB3A1B, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XROcclusionSubsystemDescriptor_tC9C8F2EFB7768358C203968CA71D353F0DD234FB * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XROcclusionSubsystemDescriptor_tC9C8F2EFB7768358C203968CA71D353F0DD234FB ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XROcclusionSubsystemDescriptor_tC9C8F2EFB7768358C203968CA71D353F0DD234FB * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2838D413336061A31AFDEA49065AD29BD1EB3A1B, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRParticipantSubsystem,UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRParticipantSubsystem/Provider>
struct SubsystemWithProvider_3_t0293B6FD1251DCA5DC0D3396C57B87118ECE01DF  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRParticipantSubsystemDescriptor_t533EE70DC8D4B105B9C87B76D35A7D59A84BCA55 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t0293B6FD1251DCA5DC0D3396C57B87118ECE01DF, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRParticipantSubsystemDescriptor_t533EE70DC8D4B105B9C87B76D35A7D59A84BCA55 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRParticipantSubsystemDescriptor_t533EE70DC8D4B105B9C87B76D35A7D59A84BCA55 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRParticipantSubsystemDescriptor_t533EE70DC8D4B105B9C87B76D35A7D59A84BCA55 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t0293B6FD1251DCA5DC0D3396C57B87118ECE01DF, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRPlaneSubsystem,UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/Provider>
struct SubsystemWithProvider_3_t2D48685843F3C8CD4AE71F1303F357DCAE9FD683  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRPlaneSubsystemDescriptor_t98B66B6D99804656DDDB45C9BDA61C2EE4EDB483 * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12 * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2D48685843F3C8CD4AE71F1303F357DCAE9FD683, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRPlaneSubsystemDescriptor_t98B66B6D99804656DDDB45C9BDA61C2EE4EDB483 * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRPlaneSubsystemDescriptor_t98B66B6D99804656DDDB45C9BDA61C2EE4EDB483 ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRPlaneSubsystemDescriptor_t98B66B6D99804656DDDB45C9BDA61C2EE4EDB483 * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t2D48685843F3C8CD4AE71F1303F357DCAE9FD683, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12 * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12 ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12 * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRRaycastSubsystem,UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/Provider>
struct SubsystemWithProvider_3_t6C72A4BB6DC4A9CC6B00E99D4A5EF1E1C9BBAF1E  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRRaycastSubsystemDescriptor_tB9891F63FC4871797BCD04DB7167142BE2049B2C * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227 * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t6C72A4BB6DC4A9CC6B00E99D4A5EF1E1C9BBAF1E, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRRaycastSubsystemDescriptor_tB9891F63FC4871797BCD04DB7167142BE2049B2C * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRRaycastSubsystemDescriptor_tB9891F63FC4871797BCD04DB7167142BE2049B2C ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRRaycastSubsystemDescriptor_tB9891F63FC4871797BCD04DB7167142BE2049B2C * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t6C72A4BB6DC4A9CC6B00E99D4A5EF1E1C9BBAF1E, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227 * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227 ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227 * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3<UnityEngine.XR.ARSubsystems.XRSessionSubsystem,UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRSessionSubsystem/Provider>
struct SubsystemWithProvider_3_t646DFCE31181130FB557E4AFA37198CF3170977F  : public SubsystemWithProvider_t1C1868CF8676F5596C1AD20A7CE69BDF7C7DE73E
{
public:
	// TSubsystemDescriptor UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<subsystemDescriptor>k__BackingField
	XRSessionSubsystemDescriptor_tC45A49D1179090D5C6D3B3DC1DC31CAB5A627B1C * ___U3CsubsystemDescriptorU3Ek__BackingField_2;
	// TProvider UnityEngine.SubsystemsImplementation.SubsystemWithProvider`3::<provider>k__BackingField
	Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A * ___U3CproviderU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t646DFCE31181130FB557E4AFA37198CF3170977F, ___U3CsubsystemDescriptorU3Ek__BackingField_2)); }
	inline XRSessionSubsystemDescriptor_tC45A49D1179090D5C6D3B3DC1DC31CAB5A627B1C * get_U3CsubsystemDescriptorU3Ek__BackingField_2() const { return ___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline XRSessionSubsystemDescriptor_tC45A49D1179090D5C6D3B3DC1DC31CAB5A627B1C ** get_address_of_U3CsubsystemDescriptorU3Ek__BackingField_2() { return &___U3CsubsystemDescriptorU3Ek__BackingField_2; }
	inline void set_U3CsubsystemDescriptorU3Ek__BackingField_2(XRSessionSubsystemDescriptor_tC45A49D1179090D5C6D3B3DC1DC31CAB5A627B1C * value)
	{
		___U3CsubsystemDescriptorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsubsystemDescriptorU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CproviderU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SubsystemWithProvider_3_t646DFCE31181130FB557E4AFA37198CF3170977F, ___U3CproviderU3Ek__BackingField_3)); }
	inline Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A * get_U3CproviderU3Ek__BackingField_3() const { return ___U3CproviderU3Ek__BackingField_3; }
	inline Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A ** get_address_of_U3CproviderU3Ek__BackingField_3() { return &___U3CproviderU3Ek__BackingField_3; }
	inline void set_U3CproviderU3Ek__BackingField_3(Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A * value)
	{
		___U3CproviderU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CproviderU3Ek__BackingField_3), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitCpuImageApi
struct ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59  : public Api_t7C92F00C6416A2C636A44AAC833C3773C567DC3E
{
public:

public:
};

struct ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59_StaticFields
{
public:
	// UnityEngine.XR.ARKit.ARKitCpuImageApi UnityEngine.XR.ARKit.ARKitCpuImageApi::<instance>k__BackingField
	ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59 * ___U3CinstanceU3Ek__BackingField_0;
	// System.Collections.Generic.HashSet`1<UnityEngine.TextureFormat> UnityEngine.XR.ARKit.ARKitCpuImageApi::s_SupportedVideoConversionFormats
	HashSet_1_t7981318DC35BC53AD64634B2D643DD6055A39054 * ___s_SupportedVideoConversionFormats_1;

public:
	inline static int32_t get_offset_of_U3CinstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59_StaticFields, ___U3CinstanceU3Ek__BackingField_0)); }
	inline ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59 * get_U3CinstanceU3Ek__BackingField_0() const { return ___U3CinstanceU3Ek__BackingField_0; }
	inline ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59 ** get_address_of_U3CinstanceU3Ek__BackingField_0() { return &___U3CinstanceU3Ek__BackingField_0; }
	inline void set_U3CinstanceU3Ek__BackingField_0(ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59 * value)
	{
		___U3CinstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CinstanceU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_SupportedVideoConversionFormats_1() { return static_cast<int32_t>(offsetof(ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59_StaticFields, ___s_SupportedVideoConversionFormats_1)); }
	inline HashSet_1_t7981318DC35BC53AD64634B2D643DD6055A39054 * get_s_SupportedVideoConversionFormats_1() const { return ___s_SupportedVideoConversionFormats_1; }
	inline HashSet_1_t7981318DC35BC53AD64634B2D643DD6055A39054 ** get_address_of_s_SupportedVideoConversionFormats_1() { return &___s_SupportedVideoConversionFormats_1; }
	inline void set_s_SupportedVideoConversionFormats_1(HashSet_1_t7981318DC35BC53AD64634B2D643DD6055A39054 * value)
	{
		___s_SupportedVideoConversionFormats_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SupportedVideoConversionFormats_1), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARWorldMap
struct ARWorldMap_t90151C78B15487234BE2E76D169DA191819704A2 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMap::<nativeHandle>k__BackingField
	int32_t ___U3CnativeHandleU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnativeHandleU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ARWorldMap_t90151C78B15487234BE2E76D169DA191819704A2, ___U3CnativeHandleU3Ek__BackingField_1)); }
	inline int32_t get_U3CnativeHandleU3Ek__BackingField_1() const { return ___U3CnativeHandleU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CnativeHandleU3Ek__BackingField_1() { return &___U3CnativeHandleU3Ek__BackingField_1; }
	inline void set_U3CnativeHandleU3Ek__BackingField_1(int32_t value)
	{
		___U3CnativeHandleU3Ek__BackingField_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARWorldMapRequest
struct ARWorldMapRequest_tAF3FCBE9900DC7D9563E7D5D3B59B3EA9398E9A4 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequest::m_RequestId
	int32_t ___m_RequestId_0;

public:
	inline static int32_t get_offset_of_m_RequestId_0() { return static_cast<int32_t>(offsetof(ARWorldMapRequest_tAF3FCBE9900DC7D9563E7D5D3B59B3EA9398E9A4, ___m_RequestId_0)); }
	inline int32_t get_m_RequestId_0() const { return ___m_RequestId_0; }
	inline int32_t* get_address_of_m_RequestId_0() { return &___m_RequestId_0; }
	inline void set_m_RequestId_0(int32_t value)
	{
		___m_RequestId_0 = value;
	}
};


// UnityEngine.XR.ARKit.DefaultARKitSessionDelegate
struct DefaultARKitSessionDelegate_t7C6D81321CB37E084745A9EEA026C154ED2DB639  : public ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE
{
public:
	// System.Int32 UnityEngine.XR.ARKit.DefaultARKitSessionDelegate::<retriesRemaining>k__BackingField
	int32_t ___U3CretriesRemainingU3Ek__BackingField_4;
	// System.Int32 UnityEngine.XR.ARKit.DefaultARKitSessionDelegate::<maxRetryCount>k__BackingField
	int32_t ___U3CmaxRetryCountU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CretriesRemainingU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DefaultARKitSessionDelegate_t7C6D81321CB37E084745A9EEA026C154ED2DB639, ___U3CretriesRemainingU3Ek__BackingField_4)); }
	inline int32_t get_U3CretriesRemainingU3Ek__BackingField_4() const { return ___U3CretriesRemainingU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CretriesRemainingU3Ek__BackingField_4() { return &___U3CretriesRemainingU3Ek__BackingField_4; }
	inline void set_U3CretriesRemainingU3Ek__BackingField_4(int32_t value)
	{
		___U3CretriesRemainingU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CmaxRetryCountU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DefaultARKitSessionDelegate_t7C6D81321CB37E084745A9EEA026C154ED2DB639, ___U3CmaxRetryCountU3Ek__BackingField_5)); }
	inline int32_t get_U3CmaxRetryCountU3Ek__BackingField_5() const { return ___U3CmaxRetryCountU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CmaxRetryCountU3Ek__BackingField_5() { return &___U3CmaxRetryCountU3Ek__BackingField_5; }
	inline void set_U3CmaxRetryCountU3Ek__BackingField_5(int32_t value)
	{
		___U3CmaxRetryCountU3Ek__BackingField_5 = value;
	}
};


// System.Double
struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// Microsoft.CodeAnalysis.EmbeddedAttribute
struct EmbeddedAttribute_t05629E57A0E96598455481383D6D826715328F7C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.InteropServices.GCHandle
struct GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603 
{
public:
	// System.Int32 System.Runtime.InteropServices.GCHandle::handle
	int32_t ___handle_0;

public:
	inline static int32_t get_offset_of_handle_0() { return static_cast<int32_t>(offsetof(GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603, ___handle_0)); }
	inline int32_t get_handle_0() const { return ___handle_0; }
	inline int32_t* get_address_of_handle_0() { return &___handle_0; }
	inline void set_handle_0(int32_t value)
	{
		___handle_0 = value;
	}
};


// System.Guid
struct Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rngAccess_12), (void*)value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rng_13), (void*)value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t2CB5440F189986116A2FA9F907AE52644047AC50 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fastRng_14), (void*)value);
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int64
struct Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3 
{
public:
	// System.Int64 System.Int64::m_value
	int64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int64_t378EE0D608BD3107E77238E85F30D2BBD46981F3, ___m_value_0)); }
	inline int64_t get_m_value_0() const { return ___m_value_0; }
	inline int64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int64_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_t49E064784AB175290B77B6E6137930BE53B124B9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.XR.ARKit.MemoryLayout
struct MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.MemoryLayout::size
	int32_t ___size_0;
	// System.Int32 UnityEngine.XR.ARKit.MemoryLayout::stride
	int32_t ___stride_1;
	// System.Int32 UnityEngine.XR.ARKit.MemoryLayout::alignment
	int32_t ___alignment_2;

public:
	inline static int32_t get_offset_of_size_0() { return static_cast<int32_t>(offsetof(MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D, ___size_0)); }
	inline int32_t get_size_0() const { return ___size_0; }
	inline int32_t* get_address_of_size_0() { return &___size_0; }
	inline void set_size_0(int32_t value)
	{
		___size_0 = value;
	}

	inline static int32_t get_offset_of_stride_1() { return static_cast<int32_t>(offsetof(MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D, ___stride_1)); }
	inline int32_t get_stride_1() const { return ___stride_1; }
	inline int32_t* get_address_of_stride_1() { return &___stride_1; }
	inline void set_stride_1(int32_t value)
	{
		___stride_1 = value;
	}

	inline static int32_t get_offset_of_alignment_2() { return static_cast<int32_t>(offsetof(MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D, ___alignment_2)); }
	inline int32_t get_alignment_2() const { return ___alignment_2; }
	inline int32_t* get_address_of_alignment_2() { return &___alignment_2; }
	inline void set_alignment_2(int32_t value)
	{
		___alignment_2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.MutableRuntimeReferenceImageLibrary
struct MutableRuntimeReferenceImageLibrary_t887376CE46B48DEEC6E8655D429BADCA6E3C7EAA  : public RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B
{
public:

public:
};


// UnityEngine.XR.ARKit.OSVersion
struct OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.OSVersion::<major>k__BackingField
	int32_t ___U3CmajorU3Ek__BackingField_0;
	// System.Int32 UnityEngine.XR.ARKit.OSVersion::<minor>k__BackingField
	int32_t ___U3CminorU3Ek__BackingField_1;
	// System.Int32 UnityEngine.XR.ARKit.OSVersion::<point>k__BackingField
	int32_t ___U3CpointU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmajorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C, ___U3CmajorU3Ek__BackingField_0)); }
	inline int32_t get_U3CmajorU3Ek__BackingField_0() const { return ___U3CmajorU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CmajorU3Ek__BackingField_0() { return &___U3CmajorU3Ek__BackingField_0; }
	inline void set_U3CmajorU3Ek__BackingField_0(int32_t value)
	{
		___U3CmajorU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CminorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C, ___U3CminorU3Ek__BackingField_1)); }
	inline int32_t get_U3CminorU3Ek__BackingField_1() const { return ___U3CminorU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CminorU3Ek__BackingField_1() { return &___U3CminorU3Ek__BackingField_1; }
	inline void set_U3CminorU3Ek__BackingField_1(int32_t value)
	{
		___U3CminorU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CpointU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C, ___U3CpointU3Ek__BackingField_2)); }
	inline int32_t get_U3CpointU3Ek__BackingField_2() const { return ___U3CpointU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CpointU3Ek__BackingField_2() { return &___U3CpointU3Ek__BackingField_2; }
	inline void set_U3CpointU3Ek__BackingField_2(int32_t value)
	{
		___U3CpointU3Ek__BackingField_2 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.VFX.VFXOutputEventArgs
struct VFXOutputEventArgs_tE7E97EDFD67E4561E4412D2E4B1C999F95850BF5 
{
public:
	// System.Int32 UnityEngine.VFX.VFXOutputEventArgs::<nameId>k__BackingField
	int32_t ___U3CnameIdU3Ek__BackingField_0;
	// UnityEngine.VFX.VFXEventAttribute UnityEngine.VFX.VFXOutputEventArgs::<eventAttribute>k__BackingField
	VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF * ___U3CeventAttributeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameIdU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VFXOutputEventArgs_tE7E97EDFD67E4561E4412D2E4B1C999F95850BF5, ___U3CnameIdU3Ek__BackingField_0)); }
	inline int32_t get_U3CnameIdU3Ek__BackingField_0() const { return ___U3CnameIdU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CnameIdU3Ek__BackingField_0() { return &___U3CnameIdU3Ek__BackingField_0; }
	inline void set_U3CnameIdU3Ek__BackingField_0(int32_t value)
	{
		___U3CnameIdU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CeventAttributeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(VFXOutputEventArgs_tE7E97EDFD67E4561E4412D2E4B1C999F95850BF5, ___U3CeventAttributeU3Ek__BackingField_1)); }
	inline VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF * get_U3CeventAttributeU3Ek__BackingField_1() const { return ___U3CeventAttributeU3Ek__BackingField_1; }
	inline VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF ** get_address_of_U3CeventAttributeU3Ek__BackingField_1() { return &___U3CeventAttributeU3Ek__BackingField_1; }
	inline void set_U3CeventAttributeU3Ek__BackingField_1(VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF * value)
	{
		___U3CeventAttributeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CeventAttributeU3Ek__BackingField_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.VFX.VFXOutputEventArgs
struct VFXOutputEventArgs_tE7E97EDFD67E4561E4412D2E4B1C999F95850BF5_marshaled_pinvoke
{
	int32_t ___U3CnameIdU3Ek__BackingField_0;
	VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF_marshaled_pinvoke* ___U3CeventAttributeU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.VFX.VFXOutputEventArgs
struct VFXOutputEventArgs_tE7E97EDFD67E4561E4412D2E4B1C999F95850BF5_marshaled_com
{
	int32_t ___U3CnameIdU3Ek__BackingField_0;
	VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF_marshaled_com* ___U3CeventAttributeU3Ek__BackingField_1;
};

// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32
struct __StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C__padding[32];
	};

public:
};


// AddToCartScript/ItemsData
struct ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27 
{
public:
	// System.String AddToCartScript/ItemsData::Name
	String_t* ___Name_0;
	// System.Int32 AddToCartScript/ItemsData::Quantity
	int32_t ___Quantity_1;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_Quantity_1() { return static_cast<int32_t>(offsetof(ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27, ___Quantity_1)); }
	inline int32_t get_Quantity_1() const { return ___Quantity_1; }
	inline int32_t* get_address_of_Quantity_1() { return &___Quantity_1; }
	inline void set_Quantity_1(int32_t value)
	{
		___Quantity_1 = value;
	}
};

// Native definition for P/Invoke marshalling of AddToCartScript/ItemsData
struct ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___Quantity_1;
};
// Native definition for COM marshalling of AddToCartScript/ItemsData
struct ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___Quantity_1;
};

// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.BoundedPlane,UnityEngine.XR.ARSubsystems.XRPlaneSubsystem,UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/Provider>
struct TrackingSubsystem_4_t4CF696722E0C05A2C0234E78E673F4F17EEC1C94  : public SubsystemWithProvider_3_t2D48685843F3C8CD4AE71F1303F357DCAE9FD683
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRAnchor,UnityEngine.XR.ARSubsystems.XRAnchorSubsystem,UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRAnchorSubsystem/Provider>
struct TrackingSubsystem_4_t5C7E2B8B7A9943DF8B9FF5B46FB5AFA71E9826F1  : public SubsystemWithProvider_3_tD91EB2F57F19DA2CDB9A5E0011978CA1EA351BA2
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XREnvironmentProbe,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/Provider>
struct TrackingSubsystem_4_t3D5C3B3749ABE82CC258AD552288C51FAE67DA1A  : public SubsystemWithProvider_3_t8B33A21A2B183DB3F429FD3F0A899D7ED1BB4DEA
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRHumanBody,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem/Provider>
struct TrackingSubsystem_4_tB0BB38AE7B56DA9BE6D8463DD64E4766AD686B86  : public SubsystemWithProvider_3_t152AEC9946809B23BD9A7DE32A2113E12B8CE2C2
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRParticipant,UnityEngine.XR.ARSubsystems.XRParticipantSubsystem,UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRParticipantSubsystem/Provider>
struct TrackingSubsystem_4_tF2C9DD677702042D71E5050214FE516389400277  : public SubsystemWithProvider_3_t0293B6FD1251DCA5DC0D3396C57B87118ECE01DF
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRPointCloud,UnityEngine.XR.ARSubsystems.XRDepthSubsystem,UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRDepthSubsystem/Provider>
struct TrackingSubsystem_4_t52B43FDBB6E641E351193D790222EA1C68B2984E  : public SubsystemWithProvider_3_tD436D6BE4AA164ED727D09EFDE50FF8DCAA50D98
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRRaycast,UnityEngine.XR.ARSubsystems.XRRaycastSubsystem,UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/Provider>
struct TrackingSubsystem_4_t87A57AE1E1117ED73BBD3B84DD595F36FA975077  : public SubsystemWithProvider_3_t6C72A4BB6DC4A9CC6B00E99D4A5EF1E1C9BBAF1E
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRTrackedImage,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/Provider>
struct TrackingSubsystem_4_tCE5EA1B7325877FD88C7CF41F681F25B1FC1717A  : public SubsystemWithProvider_3_t249D82EF0730E7FF15F2B19C4BB45B2E08CF620B
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.TrackingSubsystem`4<UnityEngine.XR.ARSubsystems.XRTrackedObject,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor,UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem/Provider>
struct TrackingSubsystem_4_t1E41FDFF37B1529EED554D89481040B067E300EB  : public SubsystemWithProvider_3_t2622D99FF6F2A6B95B2E82547A76A7E7712AA7DB
{
public:

public:
};


// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t52768071AC670B242659CF974483AF846D6828BF  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t52768071AC670B242659CF974483AF846D6828BF_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=32 <PrivateImplementationDetails>::E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50
	__StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C  ___E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0;

public:
	inline static int32_t get_offset_of_E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t52768071AC670B242659CF974483AF846D6828BF_StaticFields, ___E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0)); }
	inline __StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C  get_E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0() const { return ___E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0; }
	inline __StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C * get_address_of_E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0() { return &___E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0; }
	inline void set_E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0(__StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C  value)
	{
		___E8CF6E95EA720603B480AF38C38D806DF25A1A1623EB6551AA9285B2F4C83C50_0 = value;
	}
};


// UnityEngine.XR.ARKit.ARCoachingGoal
struct ARCoachingGoal_t1B0E9451A645B81235667827947BC3196ED1A6AC 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARCoachingGoal::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARCoachingGoal_t1B0E9451A645B81235667827947BC3196ED1A6AC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARCoachingOverlayTransition
struct ARCoachingOverlayTransition_tFCB8AFB8BA3D335E98404B8BEE0433F8FCE84449 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARCoachingOverlayTransition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARCoachingOverlayTransition_tFCB8AFB8BA3D335E98404B8BEE0433F8FCE84449, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARCollaborationData
struct ARCollaborationData_t46F3E05335DC9EDF98A04CF88AE6394540B9D8DB 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARCollaborationData::m_NativePtr
	intptr_t ___m_NativePtr_0;

public:
	inline static int32_t get_offset_of_m_NativePtr_0() { return static_cast<int32_t>(offsetof(ARCollaborationData_t46F3E05335DC9EDF98A04CF88AE6394540B9D8DB, ___m_NativePtr_0)); }
	inline intptr_t get_m_NativePtr_0() const { return ___m_NativePtr_0; }
	inline intptr_t* get_address_of_m_NativePtr_0() { return &___m_NativePtr_0; }
	inline void set_m_NativePtr_0(intptr_t value)
	{
		___m_NativePtr_0 = value;
	}
};


// UnityEngine.XR.ARKit.ARCollaborationDataPriority
struct ARCollaborationDataPriority_t49B334CCC35BF3BB49565E91A2241B10AEABAC6B 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARCollaborationDataPriority::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARCollaborationDataPriority_t49B334CCC35BF3BB49565E91A2241B10AEABAC6B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitErrorCode
struct ARKitErrorCode_t3236EDBBC228C5D3F07F79560500BDE1ACA03535 
{
public:
	// System.Int64 UnityEngine.XR.ARKit.ARKitErrorCode::value__
	int64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARKitErrorCode_t3236EDBBC228C5D3F07F79560500BDE1ACA03535, ___value___2)); }
	inline int64_t get_value___2() const { return ___value___2; }
	inline int64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int64_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitImageDatabase
struct ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71  : public MutableRuntimeReferenceImageLibrary_t887376CE46B48DEEC6E8655D429BADCA6E3C7EAA
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARKitImageDatabase::<nativePtr>k__BackingField
	intptr_t ___U3CnativePtrU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CnativePtrU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71, ___U3CnativePtrU3Ek__BackingField_0)); }
	inline intptr_t get_U3CnativePtrU3Ek__BackingField_0() const { return ___U3CnativePtrU3Ek__BackingField_0; }
	inline intptr_t* get_address_of_U3CnativePtrU3Ek__BackingField_0() { return &___U3CnativePtrU3Ek__BackingField_0; }
	inline void set_U3CnativePtrU3Ek__BackingField_0(intptr_t value)
	{
		___U3CnativePtrU3Ek__BackingField_0 = value;
	}
};

struct ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71_StaticFields
{
public:
	// UnityEngine.TextureFormat[] UnityEngine.XR.ARKit.ARKitImageDatabase::k_SupportedFormats
	TextureFormatU5BU5D_tC081C559AF0CE6678E9673429ADDC9B93B6B45CE* ___k_SupportedFormats_1;

public:
	inline static int32_t get_offset_of_k_SupportedFormats_1() { return static_cast<int32_t>(offsetof(ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71_StaticFields, ___k_SupportedFormats_1)); }
	inline TextureFormatU5BU5D_tC081C559AF0CE6678E9673429ADDC9B93B6B45CE* get_k_SupportedFormats_1() const { return ___k_SupportedFormats_1; }
	inline TextureFormatU5BU5D_tC081C559AF0CE6678E9673429ADDC9B93B6B45CE** get_address_of_k_SupportedFormats_1() { return &___k_SupportedFormats_1; }
	inline void set_k_SupportedFormats_1(TextureFormatU5BU5D_tC081C559AF0CE6678E9673429ADDC9B93B6B45CE* value)
	{
		___k_SupportedFormats_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_SupportedFormats_1), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARMeshClassification
struct ARMeshClassification_t848441DB74FC412CD7B765DD8BE3CE8EAF9EE0D8 
{
public:
	// System.Byte UnityEngine.XR.ARKit.ARMeshClassification::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARMeshClassification_t848441DB74FC412CD7B765DD8BE3CE8EAF9EE0D8, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARWorldAlignment
struct ARWorldAlignment_tF4BB107DD485C641CF563BBF31FEE5B5045FE81D 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldAlignment::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARWorldAlignment_tF4BB107DD485C641CF563BBF31FEE5B5045FE81D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARWorldMapRequestStatus
struct ARWorldMapRequestStatus_t9A2ABBA3D64593F57CCFC959D7D294CE5E54E73C 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequestStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARWorldMapRequestStatus_t9A2ABBA3D64593F57CCFC959D7D294CE5E54E73C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARWorldMappingStatus
struct ARWorldMappingStatus_t64E63DB3361506D91570796A834A9F47DA563805 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMappingStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARWorldMappingStatus_t64E63DB3361506D91570796A834A9F47DA563805, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Collections.Allocator
struct Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05 
{
public:
	// System.Int32 Unity.Collections.Allocator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Allocator_t9888223DEF4F46F3419ECFCCD0753599BEE52A05, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.XR.ARKit.CoreLocationErrorCode
struct CoreLocationErrorCode_t1772BC652A434E0A4C551CD089CD7C10EF42125E 
{
public:
	// System.Int64 UnityEngine.XR.ARKit.CoreLocationErrorCode::value__
	int64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CoreLocationErrorCode_t1772BC652A434E0A4C551CD089CD7C10EF42125E, ___value___2)); }
	inline int64_t get_value___2() const { return ___value___2; }
	inline int64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int64_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.XR.ARSubsystems.Feature
struct Feature_t079F5923A4893A9E07B968C27F44AC5FCAC87C83 
{
public:
	// System.UInt64 UnityEngine.XR.ARSubsystems.Feature::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Feature_t079F5923A4893A9E07B968C27F44AC5FCAC87C83, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ManagedReferenceImage
struct ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1 
{
public:
	// System.Guid UnityEngine.XR.ARKit.ManagedReferenceImage::guid
	Guid_t  ___guid_0;
	// System.Guid UnityEngine.XR.ARKit.ManagedReferenceImage::textureGuid
	Guid_t  ___textureGuid_1;
	// UnityEngine.Vector2 UnityEngine.XR.ARKit.ManagedReferenceImage::size
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___size_2;
	// System.IntPtr UnityEngine.XR.ARKit.ManagedReferenceImage::name
	intptr_t ___name_3;
	// System.IntPtr UnityEngine.XR.ARKit.ManagedReferenceImage::texture
	intptr_t ___texture_4;

public:
	inline static int32_t get_offset_of_guid_0() { return static_cast<int32_t>(offsetof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1, ___guid_0)); }
	inline Guid_t  get_guid_0() const { return ___guid_0; }
	inline Guid_t * get_address_of_guid_0() { return &___guid_0; }
	inline void set_guid_0(Guid_t  value)
	{
		___guid_0 = value;
	}

	inline static int32_t get_offset_of_textureGuid_1() { return static_cast<int32_t>(offsetof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1, ___textureGuid_1)); }
	inline Guid_t  get_textureGuid_1() const { return ___textureGuid_1; }
	inline Guid_t * get_address_of_textureGuid_1() { return &___textureGuid_1; }
	inline void set_textureGuid_1(Guid_t  value)
	{
		___textureGuid_1 = value;
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1, ___size_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_size_2() const { return ___size_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1, ___name_3)); }
	inline intptr_t get_name_3() const { return ___name_3; }
	inline intptr_t* get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(intptr_t value)
	{
		___name_3 = value;
	}

	inline static int32_t get_offset_of_texture_4() { return static_cast<int32_t>(offsetof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1, ___texture_4)); }
	inline intptr_t get_texture_4() const { return ___texture_4; }
	inline intptr_t* get_address_of_texture_4() { return &___texture_4; }
	inline void set_texture_4(intptr_t value)
	{
		___texture_4 = value;
	}
};


// UnityEngine.XR.ARKit.NSData
struct NSData_t17EC9EAA93403854EE122C95559948C024C209D0 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.NSData::m_NativePtr
	intptr_t ___m_NativePtr_0;

public:
	inline static int32_t get_offset_of_m_NativePtr_0() { return static_cast<int32_t>(offsetof(NSData_t17EC9EAA93403854EE122C95559948C024C209D0, ___m_NativePtr_0)); }
	inline intptr_t get_m_NativePtr_0() const { return ___m_NativePtr_0; }
	inline intptr_t* get_address_of_m_NativePtr_0() { return &___m_NativePtr_0; }
	inline void set_m_NativePtr_0(intptr_t value)
	{
		___m_NativePtr_0 = value;
	}
};


// UnityEngine.XR.ARKit.NSError
struct NSError_t2F84A3F44A97C98D4782E39A8052CCC879098DE9 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.NSError::m_Self
	intptr_t ___m_Self_0;

public:
	inline static int32_t get_offset_of_m_Self_0() { return static_cast<int32_t>(offsetof(NSError_t2F84A3F44A97C98D4782E39A8052CCC879098DE9, ___m_Self_0)); }
	inline intptr_t get_m_Self_0() const { return ___m_Self_0; }
	inline intptr_t* get_address_of_m_Self_0() { return &___m_Self_0; }
	inline void set_m_Self_0(intptr_t value)
	{
		___m_Self_0 = value;
	}
};


// UnityEngine.XR.ARKit.NSErrorDomain
struct NSErrorDomain_tA7AC985B6B3ABA9CEA0E75DF868AD647A30F1B29 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.NSErrorDomain::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NSErrorDomain_tA7AC985B6B3ABA9CEA0E75DF868AD647A30F1B29, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.NSMutableData
struct NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.NSMutableData::m_NativePtr
	intptr_t ___m_NativePtr_0;

public:
	inline static int32_t get_offset_of_m_NativePtr_0() { return static_cast<int32_t>(offsetof(NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F, ___m_NativePtr_0)); }
	inline intptr_t get_m_NativePtr_0() const { return ___m_NativePtr_0; }
	inline intptr_t* get_address_of_m_NativePtr_0() { return &___m_NativePtr_0; }
	inline void set_m_NativePtr_0(intptr_t value)
	{
		___m_NativePtr_0 = value;
	}
};


// UnityEngine.XR.ARKit.NSString
struct NSString_t73752BE746684D6BB8F70FE48A833CE2C9001350 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.NSString::m_Self
	intptr_t ___m_Self_0;

public:
	inline static int32_t get_offset_of_m_Self_0() { return static_cast<int32_t>(offsetof(NSString_t73752BE746684D6BB8F70FE48A833CE2C9001350, ___m_Self_0)); }
	inline intptr_t get_m_Self_0() const { return ___m_Self_0; }
	inline intptr_t* get_address_of_m_Self_0() { return &___m_Self_0; }
	inline void set_m_Self_0(intptr_t value)
	{
		___m_Self_0 = value;
	}
};


// UnityEngine.XR.ARKit.NativeChanges
struct NativeChanges_t2AB4CBD7DD7F23F421DFD0CE4D4CDB26402D6D76 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.NativeChanges::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(NativeChanges_t2AB4CBD7DD7F23F421DFD0CE4D4CDB26402D6D76, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.XR.ARSubsystems.OcclusionPreferenceMode
struct OcclusionPreferenceMode_tB85530C1AF1BD2CD83770B19A90C6D3F781EADC1 
{
public:
	// System.Int32 UnityEngine.XR.ARSubsystems.OcclusionPreferenceMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OcclusionPreferenceMode_tB85530C1AF1BD2CD83770B19A90C6D3F781EADC1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Playables.PlayableHandle
struct PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	intptr_t ___m_Handle_0;
	// System.UInt32 UnityEngine.Playables.PlayableHandle::m_Version
	uint32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A, ___m_Handle_0)); }
	inline intptr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline intptr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(intptr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A, ___m_Version_1)); }
	inline uint32_t get_m_Version_1() const { return ___m_Version_1; }
	inline uint32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(uint32_t value)
	{
		___m_Version_1 = value;
	}
};

struct PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A_StaticFields
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::m_Null
	PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  ___m_Null_2;

public:
	inline static int32_t get_offset_of_m_Null_2() { return static_cast<int32_t>(offsetof(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A_StaticFields, ___m_Null_2)); }
	inline PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  get_m_Null_2() const { return ___m_Null_2; }
	inline PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A * get_address_of_m_Null_2() { return &___m_Null_2; }
	inline void set_m_Null_2(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  value)
	{
		___m_Null_2 = value;
	}
};


// UnityEngine.Pose
struct Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A, ___position_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_0() const { return ___position_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A, ___rotation_1)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A_StaticFields, ___k_Identity_2)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___k_Identity_2 = value;
	}
};


// UnityEngine.XR.ARKit.SetReferenceLibraryResult
struct SetReferenceLibraryResult_t5F5FC87DC43CB92C6332217BE1C755F59E8E5303 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.SetReferenceLibraryResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SetReferenceLibraryResult_t5F5FC87DC43CB92C6332217BE1C755F59E8E5303, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureFormat
struct TextureFormat_tBED5388A0445FE978F97B41D247275B036407932 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_tBED5388A0445FE978F97B41D247275B036407932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.VFX.VFXEventAttribute
struct VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.VFX.VFXEventAttribute::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Boolean UnityEngine.VFX.VFXEventAttribute::m_Owner
	bool ___m_Owner_1;
	// UnityEngine.VFX.VisualEffectAsset UnityEngine.VFX.VFXEventAttribute::m_VfxAsset
	VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50 * ___m_VfxAsset_2;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Owner_1() { return static_cast<int32_t>(offsetof(VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF, ___m_Owner_1)); }
	inline bool get_m_Owner_1() const { return ___m_Owner_1; }
	inline bool* get_address_of_m_Owner_1() { return &___m_Owner_1; }
	inline void set_m_Owner_1(bool value)
	{
		___m_Owner_1 = value;
	}

	inline static int32_t get_offset_of_m_VfxAsset_2() { return static_cast<int32_t>(offsetof(VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF, ___m_VfxAsset_2)); }
	inline VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50 * get_m_VfxAsset_2() const { return ___m_VfxAsset_2; }
	inline VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50 ** get_address_of_m_VfxAsset_2() { return &___m_VfxAsset_2; }
	inline void set_m_VfxAsset_2(VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50 * value)
	{
		___m_VfxAsset_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_VfxAsset_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.VFX.VFXEventAttribute
struct VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	int32_t ___m_Owner_1;
	VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50 * ___m_VfxAsset_2;
};
// Native definition for COM marshalling of UnityEngine.VFX.VFXEventAttribute
struct VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF_marshaled_com
{
	intptr_t ___m_Ptr_0;
	int32_t ___m_Owner_1;
	VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50 * ___m_VfxAsset_2;
};

// UnityEngine.VFX.VFXExpressionValues
struct VFXExpressionValues_tFB46D1CD053E9CD5BD04CBE4DB1B0ED24C9C0883  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.VFX.VFXExpressionValues::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(VFXExpressionValues_tFB46D1CD053E9CD5BD04CBE4DB1B0ED24C9C0883, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.VFX.VFXExpressionValues
struct VFXExpressionValues_tFB46D1CD053E9CD5BD04CBE4DB1B0ED24C9C0883_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.VFX.VFXExpressionValues
struct VFXExpressionValues_tFB46D1CD053E9CD5BD04CBE4DB1B0ED24C9C0883_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.VFX.VFXSpawnerState
struct VFXSpawnerState_t5879CC401019E9C9D4F81128147AE52AAED167CD  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.VFX.VFXSpawnerState::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Boolean UnityEngine.VFX.VFXSpawnerState::m_Owner
	bool ___m_Owner_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(VFXSpawnerState_t5879CC401019E9C9D4F81128147AE52AAED167CD, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Owner_1() { return static_cast<int32_t>(offsetof(VFXSpawnerState_t5879CC401019E9C9D4F81128147AE52AAED167CD, ___m_Owner_1)); }
	inline bool get_m_Owner_1() const { return ___m_Owner_1; }
	inline bool* get_address_of_m_Owner_1() { return &___m_Owner_1; }
	inline void set_m_Owner_1(bool value)
	{
		___m_Owner_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.VFX.VFXSpawnerState
struct VFXSpawnerState_t5879CC401019E9C9D4F81128147AE52AAED167CD_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	int32_t ___m_Owner_1;
};
// Native definition for COM marshalling of UnityEngine.VFX.VFXSpawnerState
struct VFXSpawnerState_t5879CC401019E9C9D4F81128147AE52AAED167CD_marshaled_com
{
	intptr_t ___m_Ptr_0;
	int32_t ___m_Owner_1;
};

// UnityEngine.Video.Video3DLayout
struct Video3DLayout_t128A1265A65BE3B41138D19C5A827986A2F22F45 
{
public:
	// System.Int32 UnityEngine.Video.Video3DLayout::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Video3DLayout_t128A1265A65BE3B41138D19C5A827986A2F22F45, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Video.VideoAspectRatio
struct VideoAspectRatio_tB3C11859B0FA98E77D62BE7E1BD59084E7919B5E 
{
public:
	// System.Int32 UnityEngine.Video.VideoAspectRatio::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoAspectRatio_tB3C11859B0FA98E77D62BE7E1BD59084E7919B5E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Video.VideoAudioOutputMode
struct VideoAudioOutputMode_tDD6B846B9A65F1C53DA4D4D8117CDB223BE3DE56 
{
public:
	// System.Int32 UnityEngine.Video.VideoAudioOutputMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoAudioOutputMode_tDD6B846B9A65F1C53DA4D4D8117CDB223BE3DE56, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Video.VideoRenderMode
struct VideoRenderMode_tB2F8E98B2EBB3216E6322E55C246CE0587CC0A7B 
{
public:
	// System.Int32 UnityEngine.Video.VideoRenderMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoRenderMode_tB2F8E98B2EBB3216E6322E55C246CE0587CC0A7B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Video.VideoSource
struct VideoSource_t66E8298534E5BB7DFD28A7D8ADE397E328CD8896 
{
public:
	// System.Int32 UnityEngine.Video.VideoSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoSource_t66E8298534E5BB7DFD28A7D8ADE397E328CD8896, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Video.VideoTimeReference
struct VideoTimeReference_tDF02822B01320D3B0ADBE75452C8FA6B5FE96F1E 
{
public:
	// System.Int32 UnityEngine.Video.VideoTimeReference::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoTimeReference_tDF02822B01320D3B0ADBE75452C8FA6B5FE96F1E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Video.VideoTimeSource
struct VideoTimeSource_t881900D70589FDDD1C7471CB8C7FEA132B98038F 
{
public:
	// System.Int32 UnityEngine.Video.VideoTimeSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoTimeSource_t881900D70589FDDD1C7471CB8C7FEA132B98038F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRCameraSubsystem
struct XRCameraSubsystem_t3B32F6EA8A2E4D23AF240B5D21C34759D2613AC9  : public SubsystemWithProvider_3_tA938665692EBC0CA746A276F8413E462E8930FD4
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XROcclusionSubsystem
struct XROcclusionSubsystem_t7546B929F9B5B6EB13B975FE4DB1F4099EE533B8  : public SubsystemWithProvider_3_t2838D413336061A31AFDEA49065AD29BD1EB3A1B
{
public:

public:
};


// UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode
struct Mode_t3F065752D98A1D2AC6961F2183A645E20299CBE6 
{
public:
	// System.Int32 UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3F065752D98A1D2AC6961F2183A645E20299CBE6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitCameraSubsystem/CameraConfigurationResult
struct CameraConfigurationResult_t88B2CEBCA2C7C5310468B805A6C738A5ED5D04E9 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARKitCameraSubsystem/CameraConfigurationResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraConfigurationResult_t88B2CEBCA2C7C5310468B805A6C738A5ED5D04E9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitCpuImageApi/ImageType
struct ImageType_tA6813318BDBCD6CA15A1A64D300FD2A32911CEE4 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARKitCpuImageApi/ImageType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ImageType_tA6813318BDBCD6CA15A1A64D300FD2A32911CEE4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitImageDatabase/ConvertRGBA32ToARGB32Job
struct ConvertRGBA32ToARGB32Job_t993F9DEAC7565A1537923428323E86A8B0506D78 
{
public:
	// Unity.Collections.NativeSlice`1<System.UInt32> UnityEngine.XR.ARKit.ARKitImageDatabase/ConvertRGBA32ToARGB32Job::rgbaImage
	NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  ___rgbaImage_0;
	// Unity.Collections.NativeSlice`1<System.UInt32> UnityEngine.XR.ARKit.ARKitImageDatabase/ConvertRGBA32ToARGB32Job::argbImage
	NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  ___argbImage_1;

public:
	inline static int32_t get_offset_of_rgbaImage_0() { return static_cast<int32_t>(offsetof(ConvertRGBA32ToARGB32Job_t993F9DEAC7565A1537923428323E86A8B0506D78, ___rgbaImage_0)); }
	inline NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  get_rgbaImage_0() const { return ___rgbaImage_0; }
	inline NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A * get_address_of_rgbaImage_0() { return &___rgbaImage_0; }
	inline void set_rgbaImage_0(NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  value)
	{
		___rgbaImage_0 = value;
	}

	inline static int32_t get_offset_of_argbImage_1() { return static_cast<int32_t>(offsetof(ConvertRGBA32ToARGB32Job_t993F9DEAC7565A1537923428323E86A8B0506D78, ___argbImage_1)); }
	inline NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  get_argbImage_1() const { return ___argbImage_1; }
	inline NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A * get_address_of_argbImage_1() { return &___argbImage_1; }
	inline void set_argbImage_1(NativeSlice_1_t2093A56F394B534880A6F90CD910C6FCA0F9958A  value)
	{
		___argbImage_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitImageDatabase/ReleaseDatabaseJob
struct ReleaseDatabaseJob_t2712527A976BD231EF32DCEAAAF276FC834E8DA1 
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARKitImageDatabase/ReleaseDatabaseJob::database
	intptr_t ___database_0;

public:
	inline static int32_t get_offset_of_database_0() { return static_cast<int32_t>(offsetof(ReleaseDatabaseJob_t2712527A976BD231EF32DCEAAAF276FC834E8DA1, ___database_0)); }
	inline intptr_t get_database_0() const { return ___database_0; }
	inline intptr_t* get_address_of_database_0() { return &___database_0; }
	inline void set_database_0(intptr_t value)
	{
		___database_0 = value;
	}
};


// UnityEngine.Networking.UnityWebRequest/Result
struct Result_t3233C0F690EC3844C8E0C4649568659679AFBE75 
{
public:
	// System.Int32 UnityEngine.Networking.UnityWebRequest/Result::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Result_t3233C0F690EC3844C8E0C4649568659679AFBE75, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.UnityWebRequest/UnityWebRequestError
struct UnityWebRequestError_t01C779C192877A58EBDB44371C42F9A5831EB9F6 
{
public:
	// System.Int32 UnityEngine.Networking.UnityWebRequest/UnityWebRequestError::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityWebRequestError_t01C779C192877A58EBDB44371C42F9A5831EB9F6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod
struct UnityWebRequestMethod_tF538D9A75B76FFC81710E65697E38C1B12E4F7E5 
{
public:
	// System.Int32 UnityEngine.Networking.UnityWebRequest/UnityWebRequestMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnityWebRequestMethod_tF538D9A75B76FFC81710E65697E38C1B12E4F7E5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.XR.ARSubsystems.XRAnchorSubsystem/Provider
struct Provider_t9F286D20EB73EBBA4B6E7203C7A9051BE673C2E2  : public SubsystemProvider_1_t302358330269847780327C2298A4FFA7D79AF2BF
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRCameraSubsystem/Provider
struct Provider_t55916B0D2766C320DCA36A0C870BA2FD80F8B6D1  : public SubsystemProvider_1_t3B6396AEE76B5D8268802608E3593AA3D48DB307
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRDepthSubsystem/Provider
struct Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6  : public SubsystemProvider_1_tBB539901FE99992CAA10A1EFDFA610E048498E98
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem/Provider
struct Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190  : public SubsystemProvider_1_tC3DB99A11F9F3210CE2ABA9FE09C127C5B13FF80
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem/Provider
struct Provider_t055C90C34B2BCE8D134DF44C12823E320519168C  : public SubsystemProvider_1_tBF37BFFB47314B7D87E24F4C4903C90930C0302C
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem/Provider
struct Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E  : public SubsystemProvider_1_t3086BC462E1384FBB8137E64FA6C513FC002E440
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem/Provider
struct Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E  : public SubsystemProvider_1_t71FE677A1A2F32123FE4C7868D5943892028AE12
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XROcclusionSubsystem/Provider
struct Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB  : public SubsystemProvider_1_t57D5C398A7A30AC3C3674CA126FAE612BC00F597
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRParticipantSubsystem/Provider
struct Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E  : public SubsystemProvider_1_t3D6D4D936F16F3264F75D1BAB46A4A398F18F204
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem/Provider
struct Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12  : public SubsystemProvider_1_tF2F3B0C041BDD07A00CD49B25AE6016B61F24816
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRRaycastSubsystem/Provider
struct Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227  : public SubsystemProvider_1_t7ACBE98539B067B19E2D5BCC2B852277F4A38875
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRSessionSubsystem/Provider
struct Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A  : public SubsystemProvider_1_tFA56F133FD9BCE90A1C4C7D15FFE2571963D8DE4
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/Availability
struct Availability_tC71B9EC590C2DBAC608742646F563CEC07C89E79 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/Availability::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Availability_tC71B9EC590C2DBAC608742646F563CEC07C89E79, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.Collections.NativeArray`1<System.Byte>
struct NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Quaternion>
struct NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Vector2>
struct NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Vector3>
struct NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// Unity.Collections.NativeArray`1<UnityEngine.Vector4>
struct NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156 
{
public:
	// System.Void* Unity.Collections.NativeArray`1::m_Buffer
	void* ___m_Buffer_0;
	// System.Int32 Unity.Collections.NativeArray`1::m_Length
	int32_t ___m_Length_1;
	// Unity.Collections.Allocator Unity.Collections.NativeArray`1::m_AllocatorLabel
	int32_t ___m_AllocatorLabel_2;

public:
	inline static int32_t get_offset_of_m_Buffer_0() { return static_cast<int32_t>(offsetof(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156, ___m_Buffer_0)); }
	inline void* get_m_Buffer_0() const { return ___m_Buffer_0; }
	inline void** get_address_of_m_Buffer_0() { return &___m_Buffer_0; }
	inline void set_m_Buffer_0(void* value)
	{
		___m_Buffer_0 = value;
	}

	inline static int32_t get_offset_of_m_Length_1() { return static_cast<int32_t>(offsetof(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156, ___m_Length_1)); }
	inline int32_t get_m_Length_1() const { return ___m_Length_1; }
	inline int32_t* get_address_of_m_Length_1() { return &___m_Length_1; }
	inline void set_m_Length_1(int32_t value)
	{
		___m_Length_1 = value;
	}

	inline static int32_t get_offset_of_m_AllocatorLabel_2() { return static_cast<int32_t>(offsetof(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156, ___m_AllocatorLabel_2)); }
	inline int32_t get_m_AllocatorLabel_2() const { return ___m_AllocatorLabel_2; }
	inline int32_t* get_address_of_m_AllocatorLabel_2() { return &___m_AllocatorLabel_2; }
	inline void set_m_AllocatorLabel_2(int32_t value)
	{
		___m_AllocatorLabel_2 = value;
	}
};


// UnityEngine.XR.ARKit.ARCollaborationDataBuilder
struct ARCollaborationDataBuilder_t292F9D3CA1D26159C77A9C69F8E0FD2CB298F293 
{
public:
	// UnityEngine.XR.ARKit.NSMutableData UnityEngine.XR.ARKit.ARCollaborationDataBuilder::m_NSMutableData
	NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F  ___m_NSMutableData_0;

public:
	inline static int32_t get_offset_of_m_NSMutableData_0() { return static_cast<int32_t>(offsetof(ARCollaborationDataBuilder_t292F9D3CA1D26159C77A9C69F8E0FD2CB298F293, ___m_NSMutableData_0)); }
	inline NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F  get_m_NSMutableData_0() const { return ___m_NSMutableData_0; }
	inline NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F * get_address_of_m_NSMutableData_0() { return &___m_NSMutableData_0; }
	inline void set_m_NSMutableData_0(NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F  value)
	{
		___m_NSMutableData_0 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitCameraSubsystem
struct ARKitCameraSubsystem_tE327D2A1BE7A0BD90A9353C8EDBDE9A882315245  : public XRCameraSubsystem_t3B32F6EA8A2E4D23AF240B5D21C34759D2613AC9
{
public:

public:
};

struct ARKitCameraSubsystem_tE327D2A1BE7A0BD90A9353C8EDBDE9A882315245_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitCameraSubsystem::k_BackgroundShaderKeywordsToNotCompile
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___k_BackgroundShaderKeywordsToNotCompile_8;

public:
	inline static int32_t get_offset_of_k_BackgroundShaderKeywordsToNotCompile_8() { return static_cast<int32_t>(offsetof(ARKitCameraSubsystem_tE327D2A1BE7A0BD90A9353C8EDBDE9A882315245_StaticFields, ___k_BackgroundShaderKeywordsToNotCompile_8)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_k_BackgroundShaderKeywordsToNotCompile_8() const { return ___k_BackgroundShaderKeywordsToNotCompile_8; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_k_BackgroundShaderKeywordsToNotCompile_8() { return &___k_BackgroundShaderKeywordsToNotCompile_8; }
	inline void set_k_BackgroundShaderKeywordsToNotCompile_8(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___k_BackgroundShaderKeywordsToNotCompile_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_BackgroundShaderKeywordsToNotCompile_8), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitOcclusionSubsystem
struct ARKitOcclusionSubsystem_tE15296E017A597561D0C3B399ED52577B138F343  : public XROcclusionSubsystem_t7546B929F9B5B6EB13B975FE4DB1F4099EE533B8
{
public:

public:
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.ConfigurationDescriptor
struct ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78 
{
public:
	// System.IntPtr UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::m_Identifier
	intptr_t ___m_Identifier_0;
	// UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::m_Capabilities
	uint64_t ___m_Capabilities_1;
	// System.Int32 UnityEngine.XR.ARSubsystems.ConfigurationDescriptor::m_Rank
	int32_t ___m_Rank_2;

public:
	inline static int32_t get_offset_of_m_Identifier_0() { return static_cast<int32_t>(offsetof(ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78, ___m_Identifier_0)); }
	inline intptr_t get_m_Identifier_0() const { return ___m_Identifier_0; }
	inline intptr_t* get_address_of_m_Identifier_0() { return &___m_Identifier_0; }
	inline void set_m_Identifier_0(intptr_t value)
	{
		___m_Identifier_0 = value;
	}

	inline static int32_t get_offset_of_m_Capabilities_1() { return static_cast<int32_t>(offsetof(ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78, ___m_Capabilities_1)); }
	inline uint64_t get_m_Capabilities_1() const { return ___m_Capabilities_1; }
	inline uint64_t* get_address_of_m_Capabilities_1() { return &___m_Capabilities_1; }
	inline void set_m_Capabilities_1(uint64_t value)
	{
		___m_Capabilities_1 = value;
	}

	inline static int32_t get_offset_of_m_Rank_2() { return static_cast<int32_t>(offsetof(ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78, ___m_Rank_2)); }
	inline int32_t get_m_Rank_2() const { return ___m_Rank_2; }
	inline int32_t* get_address_of_m_Rank_2() { return &___m_Rank_2; }
	inline void set_m_Rank_2(int32_t value)
	{
		___m_Rank_2 = value;
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.XR.ARKit.SerializedARCollaborationData
struct SerializedARCollaborationData_t21AF44A6BD46EF7C15CB5558B47AD839E14F7AC3 
{
public:
	// UnityEngine.XR.ARKit.NSData UnityEngine.XR.ARKit.SerializedARCollaborationData::m_NSData
	NSData_t17EC9EAA93403854EE122C95559948C024C209D0  ___m_NSData_0;

public:
	inline static int32_t get_offset_of_m_NSData_0() { return static_cast<int32_t>(offsetof(SerializedARCollaborationData_t21AF44A6BD46EF7C15CB5558B47AD839E14F7AC3, ___m_NSData_0)); }
	inline NSData_t17EC9EAA93403854EE122C95559948C024C209D0  get_m_NSData_0() const { return ___m_NSData_0; }
	inline NSData_t17EC9EAA93403854EE122C95559948C024C209D0 * get_address_of_m_NSData_0() { return &___m_NSData_0; }
	inline void set_m_NSData_0(NSData_t17EC9EAA93403854EE122C95559948C024C209D0  value)
	{
		___m_NSData_0 = value;
	}
};


// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_DownloadHandler_1() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_DownloadHandler_1)); }
	inline DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * get_m_DownloadHandler_1() const { return ___m_DownloadHandler_1; }
	inline DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB ** get_address_of_m_DownloadHandler_1() { return &___m_DownloadHandler_1; }
	inline void set_m_DownloadHandler_1(DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB * value)
	{
		___m_DownloadHandler_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DownloadHandler_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_UploadHandler_2() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_UploadHandler_2)); }
	inline UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * get_m_UploadHandler_2() const { return ___m_UploadHandler_2; }
	inline UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA ** get_address_of_m_UploadHandler_2() { return &___m_UploadHandler_2; }
	inline void set_m_UploadHandler_2(UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA * value)
	{
		___m_UploadHandler_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_UploadHandler_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_CertificateHandler_3() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_CertificateHandler_3)); }
	inline CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * get_m_CertificateHandler_3() const { return ___m_CertificateHandler_3; }
	inline CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E ** get_address_of_m_CertificateHandler_3() { return &___m_CertificateHandler_3; }
	inline void set_m_CertificateHandler_3(CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E * value)
	{
		___m_CertificateHandler_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CertificateHandler_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uri_4() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___m_Uri_4)); }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * get_m_Uri_4() const { return ___m_Uri_4; }
	inline Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 ** get_address_of_m_Uri_4() { return &___m_Uri_4; }
	inline void set_m_Uri_4(Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * value)
	{
		___m_Uri_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uri_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5)); }
	inline bool get_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() const { return ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5() { return &___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5; }
	inline void set_U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5(bool value)
	{
		___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6)); }
	inline bool get_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() const { return ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6() { return &___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6; }
	inline void set_U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6(bool value)
	{
		___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E, ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7)); }
	inline bool get_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() const { return ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7() { return &___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7; }
	inline void set_U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7(bool value)
	{
		___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_com* ___m_CertificateHandler_3;
	Uri_t4A915E1CC15B2C650F478099AD448E9466CBF612 * ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};

// UnityEngine.Video.VideoClip
struct VideoClip_tA8C2507553BEE394C46B7A876D6F56DD09F6C90F  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Experimental.Video.VideoClipPlayable
struct VideoClipPlayable_tC49201F6C8E1AB1CC8F4E31EFC12C7E1C03BC2E1 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Video.VideoClipPlayable::m_Handle
	PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(VideoClipPlayable_tC49201F6C8E1AB1CC8F4E31EFC12C7E1C03BC2E1, ___m_Handle_0)); }
	inline PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t50DCD240B0400DDAD0822C13E5DBC7AD64DC027A  value)
	{
		___m_Handle_0 = value;
	}
};


// UnityEngine.VFX.VisualEffectObject
struct VisualEffectObject_tC7804AFDC2B4F2F0CE6833AC467ABC177A1617DB  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRAnchorSubsystem
struct XRAnchorSubsystem_t625D9B76C590AB601CF85525DB9396BE84425AA7  : public TrackingSubsystem_4_t5C7E2B8B7A9943DF8B9FF5B46FB5AFA71E9826F1
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRDepthSubsystem
struct XRDepthSubsystem_t808E21F0192095B08FA03AC535314FB5EF3B7E28  : public TrackingSubsystem_4_t52B43FDBB6E641E351193D790222EA1C68B2984E
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystem
struct XREnvironmentProbeSubsystem_t0C60258F565400E7C5AF0E0B7FA933F2BCF83CB6  : public TrackingSubsystem_4_t3D5C3B3749ABE82CC258AD552288C51FAE67DA1A
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRHumanBodySubsystem
struct XRHumanBodySubsystem_t71FBF94503DCE781657FA4F362464EA389CD9F2B  : public TrackingSubsystem_4_tB0BB38AE7B56DA9BE6D8463DD64E4766AD686B86
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem
struct XRImageTrackingSubsystem_tBC68AD21C11D8D67F3343844E129DF505FF705CE  : public TrackingSubsystem_4_tCE5EA1B7325877FD88C7CF41F681F25B1FC1717A
{
public:
	// UnityEngine.XR.ARSubsystems.RuntimeReferenceImageLibrary UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystem::m_ImageLibrary
	RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B * ___m_ImageLibrary_4;

public:
	inline static int32_t get_offset_of_m_ImageLibrary_4() { return static_cast<int32_t>(offsetof(XRImageTrackingSubsystem_tBC68AD21C11D8D67F3343844E129DF505FF705CE, ___m_ImageLibrary_4)); }
	inline RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B * get_m_ImageLibrary_4() const { return ___m_ImageLibrary_4; }
	inline RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B ** get_address_of_m_ImageLibrary_4() { return &___m_ImageLibrary_4; }
	inline void set_m_ImageLibrary_4(RuntimeReferenceImageLibrary_t76072EC5637B1F0F8FD0A1BFD3AEAF954D6F8D6B * value)
	{
		___m_ImageLibrary_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ImageLibrary_4), (void*)value);
	}
};


// UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem
struct XRObjectTrackingSubsystem_t3F31F4C8BCA868FE69BD8EC75BA5A1116026C881  : public TrackingSubsystem_4_t1E41FDFF37B1529EED554D89481040B067E300EB
{
public:
	// UnityEngine.XR.ARSubsystems.XRReferenceObjectLibrary UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystem::m_Library
	XRReferenceObjectLibrary_t07704B2996E507F23EE3C99CFC3BB73A83C99A7C * ___m_Library_4;

public:
	inline static int32_t get_offset_of_m_Library_4() { return static_cast<int32_t>(offsetof(XRObjectTrackingSubsystem_t3F31F4C8BCA868FE69BD8EC75BA5A1116026C881, ___m_Library_4)); }
	inline XRReferenceObjectLibrary_t07704B2996E507F23EE3C99CFC3BB73A83C99A7C * get_m_Library_4() const { return ___m_Library_4; }
	inline XRReferenceObjectLibrary_t07704B2996E507F23EE3C99CFC3BB73A83C99A7C ** get_address_of_m_Library_4() { return &___m_Library_4; }
	inline void set_m_Library_4(XRReferenceObjectLibrary_t07704B2996E507F23EE3C99CFC3BB73A83C99A7C * value)
	{
		___m_Library_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Library_4), (void*)value);
	}
};


// UnityEngine.XR.ARSubsystems.XRParticipantSubsystem
struct XRParticipantSubsystem_t7F710E46FC5A17967E7CAE126DE9443C752C36FC  : public TrackingSubsystem_4_tF2C9DD677702042D71E5050214FE516389400277
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRPlaneSubsystem
struct XRPlaneSubsystem_t7C76F9D2C993B0DC38F0A7CDCE745EA7C12417EE  : public TrackingSubsystem_4_t4CF696722E0C05A2C0234E78E673F4F17EEC1C94
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRRaycastSubsystem
struct XRRaycastSubsystem_t62FDAC9AA1BD44C4557AEE3FEF3D2FA24C71B6B8  : public TrackingSubsystem_4_t87A57AE1E1117ED73BBD3B84DD595F36FA975077
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitAnchorSubsystem/ARKitProvider
struct ARKitProvider_tE10DC6803776ECE42E55F127E68C47E50319202E  : public Provider_t9F286D20EB73EBBA4B6E7203C7A9051BE673C2E2
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitCameraSubsystem/ARKitProvider
struct ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252  : public Provider_t55916B0D2766C320DCA36A0C870BA2FD80F8B6D1
{
public:
	// UnityEngine.Material UnityEngine.XR.ARKit.ARKitCameraSubsystem/ARKitProvider::m_CameraMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_CameraMaterial_9;

public:
	inline static int32_t get_offset_of_m_CameraMaterial_9() { return static_cast<int32_t>(offsetof(ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252, ___m_CameraMaterial_9)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_CameraMaterial_9() const { return ___m_CameraMaterial_9; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_CameraMaterial_9() { return &___m_CameraMaterial_9; }
	inline void set_m_CameraMaterial_9(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_CameraMaterial_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CameraMaterial_9), (void*)value);
	}
};

struct ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252_StaticFields
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARKitCameraSubsystem/ARKitProvider::k_TextureYPropertyNameId
	int32_t ___k_TextureYPropertyNameId_3;
	// System.Int32 UnityEngine.XR.ARKit.ARKitCameraSubsystem/ARKitProvider::k_TextureCbCrPropertyNameId
	int32_t ___k_TextureCbCrPropertyNameId_4;
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitCameraSubsystem/ARKitProvider::k_LegacyRPEnabledMaterialKeywords
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___k_LegacyRPEnabledMaterialKeywords_5;
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitCameraSubsystem/ARKitProvider::k_LegacyRPDisabledMaterialKeywords
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___k_LegacyRPDisabledMaterialKeywords_6;
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitCameraSubsystem/ARKitProvider::k_URPEnabledMaterialKeywords
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___k_URPEnabledMaterialKeywords_7;
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitCameraSubsystem/ARKitProvider::k_URPDisabledMaterialKeywords
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___k_URPDisabledMaterialKeywords_8;

public:
	inline static int32_t get_offset_of_k_TextureYPropertyNameId_3() { return static_cast<int32_t>(offsetof(ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252_StaticFields, ___k_TextureYPropertyNameId_3)); }
	inline int32_t get_k_TextureYPropertyNameId_3() const { return ___k_TextureYPropertyNameId_3; }
	inline int32_t* get_address_of_k_TextureYPropertyNameId_3() { return &___k_TextureYPropertyNameId_3; }
	inline void set_k_TextureYPropertyNameId_3(int32_t value)
	{
		___k_TextureYPropertyNameId_3 = value;
	}

	inline static int32_t get_offset_of_k_TextureCbCrPropertyNameId_4() { return static_cast<int32_t>(offsetof(ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252_StaticFields, ___k_TextureCbCrPropertyNameId_4)); }
	inline int32_t get_k_TextureCbCrPropertyNameId_4() const { return ___k_TextureCbCrPropertyNameId_4; }
	inline int32_t* get_address_of_k_TextureCbCrPropertyNameId_4() { return &___k_TextureCbCrPropertyNameId_4; }
	inline void set_k_TextureCbCrPropertyNameId_4(int32_t value)
	{
		___k_TextureCbCrPropertyNameId_4 = value;
	}

	inline static int32_t get_offset_of_k_LegacyRPEnabledMaterialKeywords_5() { return static_cast<int32_t>(offsetof(ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252_StaticFields, ___k_LegacyRPEnabledMaterialKeywords_5)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_k_LegacyRPEnabledMaterialKeywords_5() const { return ___k_LegacyRPEnabledMaterialKeywords_5; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_k_LegacyRPEnabledMaterialKeywords_5() { return &___k_LegacyRPEnabledMaterialKeywords_5; }
	inline void set_k_LegacyRPEnabledMaterialKeywords_5(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___k_LegacyRPEnabledMaterialKeywords_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_LegacyRPEnabledMaterialKeywords_5), (void*)value);
	}

	inline static int32_t get_offset_of_k_LegacyRPDisabledMaterialKeywords_6() { return static_cast<int32_t>(offsetof(ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252_StaticFields, ___k_LegacyRPDisabledMaterialKeywords_6)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_k_LegacyRPDisabledMaterialKeywords_6() const { return ___k_LegacyRPDisabledMaterialKeywords_6; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_k_LegacyRPDisabledMaterialKeywords_6() { return &___k_LegacyRPDisabledMaterialKeywords_6; }
	inline void set_k_LegacyRPDisabledMaterialKeywords_6(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___k_LegacyRPDisabledMaterialKeywords_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_LegacyRPDisabledMaterialKeywords_6), (void*)value);
	}

	inline static int32_t get_offset_of_k_URPEnabledMaterialKeywords_7() { return static_cast<int32_t>(offsetof(ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252_StaticFields, ___k_URPEnabledMaterialKeywords_7)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_k_URPEnabledMaterialKeywords_7() const { return ___k_URPEnabledMaterialKeywords_7; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_k_URPEnabledMaterialKeywords_7() { return &___k_URPEnabledMaterialKeywords_7; }
	inline void set_k_URPEnabledMaterialKeywords_7(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___k_URPEnabledMaterialKeywords_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_URPEnabledMaterialKeywords_7), (void*)value);
	}

	inline static int32_t get_offset_of_k_URPDisabledMaterialKeywords_8() { return static_cast<int32_t>(offsetof(ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252_StaticFields, ___k_URPDisabledMaterialKeywords_8)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_k_URPDisabledMaterialKeywords_8() const { return ___k_URPDisabledMaterialKeywords_8; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_k_URPDisabledMaterialKeywords_8() { return &___k_URPDisabledMaterialKeywords_8; }
	inline void set_k_URPDisabledMaterialKeywords_8(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___k_URPDisabledMaterialKeywords_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___k_URPDisabledMaterialKeywords_8), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem/ARKitProvider
struct ARKitProvider_tBED347A85C19012D5B5F124BC8C0FB4876FBEACC  : public Provider_tAF87FE3E906FDBF14F06488A1AA5E80400EFE190
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitHumanBodySubsystem/ARKitProvider
struct ARKitProvider_t5CC80E0D6F9A6EFFA70F54E8CDBEDB36D0CC4ED0  : public Provider_t055C90C34B2BCE8D134DF44C12823E320519168C
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob
struct AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5 
{
public:
	// Unity.Collections.NativeSlice`1<System.Byte> UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::image
	NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B  ___image_0;
	// System.IntPtr UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::database
	intptr_t ___database_1;
	// System.IntPtr UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::validator
	intptr_t ___validator_2;
	// System.Int32 UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::width
	int32_t ___width_3;
	// System.Int32 UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::height
	int32_t ___height_4;
	// System.Single UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::physicalWidth
	float ___physicalWidth_5;
	// UnityEngine.TextureFormat UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::format
	int32_t ___format_6;
	// UnityEngine.XR.ARKit.ManagedReferenceImage UnityEngine.XR.ARKit.ARKitImageDatabase/AddImageJob::managedReferenceImage
	ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1  ___managedReferenceImage_7;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___image_0)); }
	inline NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B  get_image_0() const { return ___image_0; }
	inline NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B * get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(NativeSlice_1_tA05D3BC7EB042685CBFD3369ECB6140D114C911B  value)
	{
		___image_0 = value;
	}

	inline static int32_t get_offset_of_database_1() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___database_1)); }
	inline intptr_t get_database_1() const { return ___database_1; }
	inline intptr_t* get_address_of_database_1() { return &___database_1; }
	inline void set_database_1(intptr_t value)
	{
		___database_1 = value;
	}

	inline static int32_t get_offset_of_validator_2() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___validator_2)); }
	inline intptr_t get_validator_2() const { return ___validator_2; }
	inline intptr_t* get_address_of_validator_2() { return &___validator_2; }
	inline void set_validator_2(intptr_t value)
	{
		___validator_2 = value;
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___width_3)); }
	inline int32_t get_width_3() const { return ___width_3; }
	inline int32_t* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(int32_t value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_height_4() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___height_4)); }
	inline int32_t get_height_4() const { return ___height_4; }
	inline int32_t* get_address_of_height_4() { return &___height_4; }
	inline void set_height_4(int32_t value)
	{
		___height_4 = value;
	}

	inline static int32_t get_offset_of_physicalWidth_5() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___physicalWidth_5)); }
	inline float get_physicalWidth_5() const { return ___physicalWidth_5; }
	inline float* get_address_of_physicalWidth_5() { return &___physicalWidth_5; }
	inline void set_physicalWidth_5(float value)
	{
		___physicalWidth_5 = value;
	}

	inline static int32_t get_offset_of_format_6() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___format_6)); }
	inline int32_t get_format_6() const { return ___format_6; }
	inline int32_t* get_address_of_format_6() { return &___format_6; }
	inline void set_format_6(int32_t value)
	{
		___format_6 = value;
	}

	inline static int32_t get_offset_of_managedReferenceImage_7() { return static_cast<int32_t>(offsetof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5, ___managedReferenceImage_7)); }
	inline ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1  get_managedReferenceImage_7() const { return ___managedReferenceImage_7; }
	inline ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1 * get_address_of_managedReferenceImage_7() { return &___managedReferenceImage_7; }
	inline void set_managedReferenceImage_7(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1  value)
	{
		___managedReferenceImage_7 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem/ARKitProvider
struct ARKitProvider_t6838013201F7369104C65DD65159D910B116E8AC  : public Provider_tA7CEF856C3BC486ADEBD656F5535E24262AAAE9E
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider
struct ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611  : public Provider_t5B60C630FB68EFEAB6FA2F3D9A732C144003B7FB
{
public:
	// UnityEngine.XR.ARSubsystems.OcclusionPreferenceMode UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::m_OcclusionPreferenceMode
	int32_t ___m_OcclusionPreferenceMode_14;

public:
	inline static int32_t get_offset_of_m_OcclusionPreferenceMode_14() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611, ___m_OcclusionPreferenceMode_14)); }
	inline int32_t get_m_OcclusionPreferenceMode_14() const { return ___m_OcclusionPreferenceMode_14; }
	inline int32_t* get_address_of_m_OcclusionPreferenceMode_14() { return &___m_OcclusionPreferenceMode_14; }
	inline void set_m_OcclusionPreferenceMode_14(int32_t value)
	{
		___m_OcclusionPreferenceMode_14 = value;
	}
};

struct ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::k_TextureHumanStencilPropertyId
	int32_t ___k_TextureHumanStencilPropertyId_7;
	// System.Int32 UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::k_TextureHumanDepthPropertyId
	int32_t ___k_TextureHumanDepthPropertyId_8;
	// System.Int32 UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::k_TextureEnvironmentDepthPropertyId
	int32_t ___k_TextureEnvironmentDepthPropertyId_9;
	// System.Int32 UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::k_TextureEnvironmentDepthConfidencePropertyId
	int32_t ___k_TextureEnvironmentDepthConfidencePropertyId_10;
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::m_HumanEnabledMaterialKeywords
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___m_HumanEnabledMaterialKeywords_11;
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::m_EnvironmentDepthEnabledMaterialKeywords
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___m_EnvironmentDepthEnabledMaterialKeywords_12;
	// System.Collections.Generic.List`1<System.String> UnityEngine.XR.ARKit.ARKitOcclusionSubsystem/ARKitProvider::m_AllDisabledMaterialKeywords
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ___m_AllDisabledMaterialKeywords_13;

public:
	inline static int32_t get_offset_of_k_TextureHumanStencilPropertyId_7() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___k_TextureHumanStencilPropertyId_7)); }
	inline int32_t get_k_TextureHumanStencilPropertyId_7() const { return ___k_TextureHumanStencilPropertyId_7; }
	inline int32_t* get_address_of_k_TextureHumanStencilPropertyId_7() { return &___k_TextureHumanStencilPropertyId_7; }
	inline void set_k_TextureHumanStencilPropertyId_7(int32_t value)
	{
		___k_TextureHumanStencilPropertyId_7 = value;
	}

	inline static int32_t get_offset_of_k_TextureHumanDepthPropertyId_8() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___k_TextureHumanDepthPropertyId_8)); }
	inline int32_t get_k_TextureHumanDepthPropertyId_8() const { return ___k_TextureHumanDepthPropertyId_8; }
	inline int32_t* get_address_of_k_TextureHumanDepthPropertyId_8() { return &___k_TextureHumanDepthPropertyId_8; }
	inline void set_k_TextureHumanDepthPropertyId_8(int32_t value)
	{
		___k_TextureHumanDepthPropertyId_8 = value;
	}

	inline static int32_t get_offset_of_k_TextureEnvironmentDepthPropertyId_9() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___k_TextureEnvironmentDepthPropertyId_9)); }
	inline int32_t get_k_TextureEnvironmentDepthPropertyId_9() const { return ___k_TextureEnvironmentDepthPropertyId_9; }
	inline int32_t* get_address_of_k_TextureEnvironmentDepthPropertyId_9() { return &___k_TextureEnvironmentDepthPropertyId_9; }
	inline void set_k_TextureEnvironmentDepthPropertyId_9(int32_t value)
	{
		___k_TextureEnvironmentDepthPropertyId_9 = value;
	}

	inline static int32_t get_offset_of_k_TextureEnvironmentDepthConfidencePropertyId_10() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___k_TextureEnvironmentDepthConfidencePropertyId_10)); }
	inline int32_t get_k_TextureEnvironmentDepthConfidencePropertyId_10() const { return ___k_TextureEnvironmentDepthConfidencePropertyId_10; }
	inline int32_t* get_address_of_k_TextureEnvironmentDepthConfidencePropertyId_10() { return &___k_TextureEnvironmentDepthConfidencePropertyId_10; }
	inline void set_k_TextureEnvironmentDepthConfidencePropertyId_10(int32_t value)
	{
		___k_TextureEnvironmentDepthConfidencePropertyId_10 = value;
	}

	inline static int32_t get_offset_of_m_HumanEnabledMaterialKeywords_11() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___m_HumanEnabledMaterialKeywords_11)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_m_HumanEnabledMaterialKeywords_11() const { return ___m_HumanEnabledMaterialKeywords_11; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_m_HumanEnabledMaterialKeywords_11() { return &___m_HumanEnabledMaterialKeywords_11; }
	inline void set_m_HumanEnabledMaterialKeywords_11(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___m_HumanEnabledMaterialKeywords_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HumanEnabledMaterialKeywords_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_EnvironmentDepthEnabledMaterialKeywords_12() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___m_EnvironmentDepthEnabledMaterialKeywords_12)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_m_EnvironmentDepthEnabledMaterialKeywords_12() const { return ___m_EnvironmentDepthEnabledMaterialKeywords_12; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_m_EnvironmentDepthEnabledMaterialKeywords_12() { return &___m_EnvironmentDepthEnabledMaterialKeywords_12; }
	inline void set_m_EnvironmentDepthEnabledMaterialKeywords_12(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___m_EnvironmentDepthEnabledMaterialKeywords_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EnvironmentDepthEnabledMaterialKeywords_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_AllDisabledMaterialKeywords_13() { return static_cast<int32_t>(offsetof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields, ___m_AllDisabledMaterialKeywords_13)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get_m_AllDisabledMaterialKeywords_13() const { return ___m_AllDisabledMaterialKeywords_13; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of_m_AllDisabledMaterialKeywords_13() { return &___m_AllDisabledMaterialKeywords_13; }
	inline void set_m_AllDisabledMaterialKeywords_13(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		___m_AllDisabledMaterialKeywords_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AllDisabledMaterialKeywords_13), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitParticipantSubsystem/ARKitProvider
struct ARKitProvider_t9908B32D1E5398BA2FD0D825E9343F8CA0277B4C  : public Provider_t1D0BC515976D24FD30341AC456ACFCB2DE4A344E
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARKitParticipantSubsystem/ARKitProvider::m_Ptr
	intptr_t ___m_Ptr_1;

public:
	inline static int32_t get_offset_of_m_Ptr_1() { return static_cast<int32_t>(offsetof(ARKitProvider_t9908B32D1E5398BA2FD0D825E9343F8CA0277B4C, ___m_Ptr_1)); }
	inline intptr_t get_m_Ptr_1() const { return ___m_Ptr_1; }
	inline intptr_t* get_address_of_m_Ptr_1() { return &___m_Ptr_1; }
	inline void set_m_Ptr_1(intptr_t value)
	{
		___m_Ptr_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitRaycastSubsystem/ARKitProvider
struct ARKitProvider_t3FB2A65F0CF8BCD91C4CC54F3841159E7F7DAA88  : public Provider_tF185BE0541D2066CD242583CEFE7709DD22DD227
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARKitRaycastSubsystem/ARKitProvider::m_Self
	intptr_t ___m_Self_1;

public:
	inline static int32_t get_offset_of_m_Self_1() { return static_cast<int32_t>(offsetof(ARKitProvider_t3FB2A65F0CF8BCD91C4CC54F3841159E7F7DAA88, ___m_Self_1)); }
	inline intptr_t get_m_Self_1() const { return ___m_Self_1; }
	inline intptr_t* get_address_of_m_Self_1() { return &___m_Self_1; }
	inline void set_m_Self_1(intptr_t value)
	{
		___m_Self_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem/ARKitProvider
struct ARKitProvider_t9AF751FC81DD6B052A462DBB30F46C946CE9E7CF  : public Provider_t4C3675997BB8AF3A6A32C3EC3C93C10E4D3E8D1A
{
public:
	// System.IntPtr UnityEngine.XR.ARKit.ARKitSessionSubsystem/ARKitProvider::m_Self
	intptr_t ___m_Self_1;
	// System.Runtime.InteropServices.GCHandle UnityEngine.XR.ARKit.ARKitSessionSubsystem/ARKitProvider::m_SubsystemHandle
	GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  ___m_SubsystemHandle_2;

public:
	inline static int32_t get_offset_of_m_Self_1() { return static_cast<int32_t>(offsetof(ARKitProvider_t9AF751FC81DD6B052A462DBB30F46C946CE9E7CF, ___m_Self_1)); }
	inline intptr_t get_m_Self_1() const { return ___m_Self_1; }
	inline intptr_t* get_address_of_m_Self_1() { return &___m_Self_1; }
	inline void set_m_Self_1(intptr_t value)
	{
		___m_Self_1 = value;
	}

	inline static int32_t get_offset_of_m_SubsystemHandle_2() { return static_cast<int32_t>(offsetof(ARKitProvider_t9AF751FC81DD6B052A462DBB30F46C946CE9E7CF, ___m_SubsystemHandle_2)); }
	inline GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  get_m_SubsystemHandle_2() const { return ___m_SubsystemHandle_2; }
	inline GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603 * get_address_of_m_SubsystemHandle_2() { return &___m_SubsystemHandle_2; }
	inline void set_m_SubsystemHandle_2(GCHandle_t757890BC4BBBEDE5A623A3C110013EDD24613603  value)
	{
		___m_SubsystemHandle_2 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/ARKitProvider
struct ARKitProvider_tC6BBA25F076F6D64FA8E6336E6860D7DBD41A7A1  : public Provider_t8E88C17A70269CD3E96909AFCCA952AAA7DEC0B6
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRObjectTrackingSubsystem/ARKitProvider
struct ARKitProvider_tDDBF5C474A3543A0F5FA7FB07A058B223F6DA8EC  : public Provider_t35977A2A0AA6C338BC9893668DD32F0294A9C01E
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider
struct ARKitProvider_tF9F620C89E671C028D10768C5300D3539661A2C1  : public Provider_t6CB5B1036B0AAED1379F3828D695A6942B72BA12
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitAnchorSubsystem
struct ARKitAnchorSubsystem_t721F48C4B69507D7E38664A68AA42DD1DF75B3DF  : public XRAnchorSubsystem_t625D9B76C590AB601CF85525DB9396BE84425AA7
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitEnvironmentProbeSubsystem
struct ARKitEnvironmentProbeSubsystem_t628136AE15CA16280BA838B37E2B7CDF0473E9D2  : public XREnvironmentProbeSubsystem_t0C60258F565400E7C5AF0E0B7FA933F2BCF83CB6
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitHumanBodySubsystem
struct ARKitHumanBodySubsystem_t8BA2C528FDE109FBDDBD0ED2FF8BC1FD081E92B7  : public XRHumanBodySubsystem_t71FBF94503DCE781657FA4F362464EA389CD9F2B
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitImageTrackingSubsystem
struct ARKitImageTrackingSubsystem_t0F98C12C3E36B9F3819FC43494A883B600401237  : public XRImageTrackingSubsystem_tBC68AD21C11D8D67F3343844E129DF505FF705CE
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitLoaderSettings
struct ARKitLoaderSettings_t97CD3CA8B21183EC5C61B94BB7299FD217479B24  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitParticipantSubsystem
struct ARKitParticipantSubsystem_tFBFB76108F5069693F4F40F8DAA4B6A907A709C3  : public XRParticipantSubsystem_t7F710E46FC5A17967E7CAE126DE9443C752C36FC
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitRaycastSubsystem
struct ARKitRaycastSubsystem_t8DF7A7F21C4610FBBEF6A240FD1790CD47BE9900  : public XRRaycastSubsystem_t62FDAC9AA1BD44C4557AEE3FEF3D2FA24C71B6B8
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem
struct ARKitXRDepthSubsystem_t50D82DC2115EED29E5F0EEBA8A0F4FA7876BD2D5  : public XRDepthSubsystem_t808E21F0192095B08FA03AC535314FB5EF3B7E28
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRObjectTrackingSubsystem
struct ARKitXRObjectTrackingSubsystem_tB611337EA1668BBCB3A7123F14117CB536707A6D  : public XRObjectTrackingSubsystem_t3F31F4C8BCA868FE69BD8EC75BA5A1116026C881
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem
struct ARKitXRPlaneSubsystem_t03042D64756D81EC6B4657CD1323D7404EC7530B  : public XRPlaneSubsystem_t7C76F9D2C993B0DC38F0A7CDCE745EA7C12417EE
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.Configuration
struct Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0 
{
public:
	// UnityEngine.XR.ARSubsystems.ConfigurationDescriptor UnityEngine.XR.ARSubsystems.Configuration::<descriptor>k__BackingField
	ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78  ___U3CdescriptorU3Ek__BackingField_0;
	// UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARSubsystems.Configuration::<features>k__BackingField
	uint64_t ___U3CfeaturesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CdescriptorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0, ___U3CdescriptorU3Ek__BackingField_0)); }
	inline ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78  get_U3CdescriptorU3Ek__BackingField_0() const { return ___U3CdescriptorU3Ek__BackingField_0; }
	inline ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78 * get_address_of_U3CdescriptorU3Ek__BackingField_0() { return &___U3CdescriptorU3Ek__BackingField_0; }
	inline void set_U3CdescriptorU3Ek__BackingField_0(ConfigurationDescriptor_t5CD12F017FCCF861DC33D7C0D6F0121015DEEA78  value)
	{
		___U3CdescriptorU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CfeaturesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0, ___U3CfeaturesU3Ek__BackingField_1)); }
	inline uint64_t get_U3CfeaturesU3Ek__BackingField_1() const { return ___U3CfeaturesU3Ek__BackingField_1; }
	inline uint64_t* get_address_of_U3CfeaturesU3Ek__BackingField_1() { return &___U3CfeaturesU3Ek__BackingField_1; }
	inline void set_U3CfeaturesU3Ek__BackingField_1(uint64_t value)
	{
		___U3CfeaturesU3Ek__BackingField_1 = value;
	}
};


// UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D  : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB
{
public:
	// Unity.Collections.NativeArray`1<System.Byte> UnityEngine.Networking.DownloadHandlerBuffer::m_NativeData
	NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  ___m_NativeData_1;

public:
	inline static int32_t get_offset_of_m_NativeData_1() { return static_cast<int32_t>(offsetof(DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D, ___m_NativeData_1)); }
	inline NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  get_m_NativeData_1() const { return ___m_NativeData_1; }
	inline NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785 * get_address_of_m_NativeData_1() { return &___m_NativeData_1; }
	inline void set_m_NativeData_1(NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  value)
	{
		___m_NativeData_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D_marshaled_pinvoke : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke
{
	NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  ___m_NativeData_1;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandlerBuffer
struct DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D_marshaled_com : public DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_com
{
	NativeArray_1_t3C2855C5B238E8C6739D4C17833F244B95C0F785  ___m_NativeData_1;
};

// UnityEngine.VFX.VFXSpawnerCallbacks
struct VFXSpawnerCallbacks_t62128B7E3ADA64EBEA4705691DE0F045104801CA  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.VFX.VisualEffectAsset
struct VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50  : public VisualEffectObject_tC7804AFDC2B4F2F0CE6833AC467ABC177A1617DB
{
public:

public:
};

struct VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50_StaticFields
{
public:
	// System.Int32 UnityEngine.VFX.VisualEffectAsset::PlayEventID
	int32_t ___PlayEventID_4;
	// System.Int32 UnityEngine.VFX.VisualEffectAsset::StopEventID
	int32_t ___StopEventID_5;

public:
	inline static int32_t get_offset_of_PlayEventID_4() { return static_cast<int32_t>(offsetof(VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50_StaticFields, ___PlayEventID_4)); }
	inline int32_t get_PlayEventID_4() const { return ___PlayEventID_4; }
	inline int32_t* get_address_of_PlayEventID_4() { return &___PlayEventID_4; }
	inline void set_PlayEventID_4(int32_t value)
	{
		___PlayEventID_4 = value;
	}

	inline static int32_t get_offset_of_StopEventID_5() { return static_cast<int32_t>(offsetof(VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50_StaticFields, ___StopEventID_5)); }
	inline int32_t get_StopEventID_5() const { return ___StopEventID_5; }
	inline int32_t* get_address_of_StopEventID_5() { return &___StopEventID_5; }
	inline void set_StopEventID_5(int32_t value)
	{
		___StopEventID_5 = value;
	}
};


// UnityEngine.XR.Management.XRLoader
struct XRLoader_tE37B92C6B9CDD944DDF7AFF5704E9EB342D62F6B  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.XR.ARSubsystems.XRReferenceObjectEntry
struct XRReferenceObjectEntry_t873762C96E954D47B49C3B36CABD423408DC72D2  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob
struct TransformPositionsJob_t76836F51EABF9ECE4503F3ECAC1DA2C32D0DA468 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Quaternion> UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob::positionsIn
	NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  ___positionsIn_0;
	// Unity.Collections.NativeArray`1<UnityEngine.Vector3> UnityEngine.XR.ARKit.ARKitXRDepthSubsystem/TransformPositionsJob::positionsOut
	NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  ___positionsOut_1;

public:
	inline static int32_t get_offset_of_positionsIn_0() { return static_cast<int32_t>(offsetof(TransformPositionsJob_t76836F51EABF9ECE4503F3ECAC1DA2C32D0DA468, ___positionsIn_0)); }
	inline NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  get_positionsIn_0() const { return ___positionsIn_0; }
	inline NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60 * get_address_of_positionsIn_0() { return &___positionsIn_0; }
	inline void set_positionsIn_0(NativeArray_1_t2672EA3A09E3FE76419439DC581905D460584A60  value)
	{
		___positionsIn_0 = value;
	}

	inline static int32_t get_offset_of_positionsOut_1() { return static_cast<int32_t>(offsetof(TransformPositionsJob_t76836F51EABF9ECE4503F3ECAC1DA2C32D0DA468, ___positionsOut_1)); }
	inline NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  get_positionsOut_1() const { return ___positionsOut_1; }
	inline NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52 * get_address_of_positionsOut_1() { return &___positionsOut_1; }
	inline void set_positionsOut_1(NativeArray_1_t2577BCA09CA248EFB78BD7FDA7F09D5C395DFF52  value)
	{
		___positionsOut_1 = value;
	}
};


// UnityEngine.Video.VideoPlayer/ErrorEventHandler
struct ErrorEventHandler_tD47781EBB7CF0CC4C111496024BD59B1D1A6A1F2  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Video.VideoPlayer/EventHandler
struct EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler
struct FrameReadyEventHandler_t9529BD5A34E9C8BE7D8A39D46A6C4ABC673374EC  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Video.VideoPlayer/TimeEventHandler
struct TimeEventHandler_t7CA131EB85E0FFCBE8660E030698BD83D3994DD8  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate
struct OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider/FlipBoundaryWindingJob
struct FlipBoundaryWindingJob_t726CFFAB953CC7C841EE74365C27322E57A2A5DA 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider/FlipBoundaryWindingJob::positions
	NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  ___positions_0;

public:
	inline static int32_t get_offset_of_positions_0() { return static_cast<int32_t>(offsetof(FlipBoundaryWindingJob_t726CFFAB953CC7C841EE74365C27322E57A2A5DA, ___positions_0)); }
	inline NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  get_positions_0() const { return ___positions_0; }
	inline NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * get_address_of_positions_0() { return &___positions_0; }
	inline void set_positions_0(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  value)
	{
		___positions_0 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider/TransformBoundaryPositionsJob
struct TransformBoundaryPositionsJob_t538BF69F9F744EBC94AA0487075746AD113A64FE 
{
public:
	// Unity.Collections.NativeArray`1<UnityEngine.Vector4> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider/TransformBoundaryPositionsJob::positionsIn
	NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  ___positionsIn_0;
	// Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARKit.ARKitXRPlaneSubsystem/ARKitProvider/TransformBoundaryPositionsJob::positionsOut
	NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  ___positionsOut_1;

public:
	inline static int32_t get_offset_of_positionsIn_0() { return static_cast<int32_t>(offsetof(TransformBoundaryPositionsJob_t538BF69F9F744EBC94AA0487075746AD113A64FE, ___positionsIn_0)); }
	inline NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  get_positionsIn_0() const { return ___positionsIn_0; }
	inline NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156 * get_address_of_positionsIn_0() { return &___positionsIn_0; }
	inline void set_positionsIn_0(NativeArray_1_t381CA7D92591F9C96DB84965C31AF8713A546156  value)
	{
		___positionsIn_0 = value;
	}

	inline static int32_t get_offset_of_positionsOut_1() { return static_cast<int32_t>(offsetof(TransformBoundaryPositionsJob_t538BF69F9F744EBC94AA0487075746AD113A64FE, ___positionsOut_1)); }
	inline NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  get_positionsOut_1() const { return ___positionsOut_1; }
	inline NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0 * get_address_of_positionsOut_1() { return &___positionsOut_1; }
	inline void set_positionsOut_1(NativeArray_1_t431C85F30275831D1F5D458AB73DFCE050693BC0  value)
	{
		___positionsOut_1 = value;
	}
};


// System.Nullable`1<UnityEngine.XR.ARSubsystems.Configuration>
struct Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494 
{
public:
	// T System.Nullable`1::value
	Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494, ___value_0)); }
	inline Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0  get_value_0() const { return ___value_0; }
	inline Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Configuration_t29C11C7E576D64F717543048BDA1EBCEF0CF60C0  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitReferenceObjectEntry
struct ARKitReferenceObjectEntry_t43AE0F7071569F012C44B59BAF26D1398ADEDE47  : public XRReferenceObjectEntry_t873762C96E954D47B49C3B36CABD423408DC72D2
{
public:
	// UnityEngine.Pose UnityEngine.XR.ARKit.ARKitReferenceObjectEntry::m_ReferenceOrigin
	Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  ___m_ReferenceOrigin_4;

public:
	inline static int32_t get_offset_of_m_ReferenceOrigin_4() { return static_cast<int32_t>(offsetof(ARKitReferenceObjectEntry_t43AE0F7071569F012C44B59BAF26D1398ADEDE47, ___m_ReferenceOrigin_4)); }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  get_m_ReferenceOrigin_4() const { return ___m_ReferenceOrigin_4; }
	inline Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A * get_address_of_m_ReferenceOrigin_4() { return &___m_ReferenceOrigin_4; }
	inline void set_m_ReferenceOrigin_4(Pose_t9F30358E65733E60A1DC8682FDB7104F40C9434A  value)
	{
		___m_ReferenceOrigin_4 = value;
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::prepareCompleted
	EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * ___prepareCompleted_4;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::loopPointReached
	EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * ___loopPointReached_5;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::started
	EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * ___started_6;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::frameDropped
	EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * ___frameDropped_7;
	// UnityEngine.Video.VideoPlayer/ErrorEventHandler UnityEngine.Video.VideoPlayer::errorReceived
	ErrorEventHandler_tD47781EBB7CF0CC4C111496024BD59B1D1A6A1F2 * ___errorReceived_8;
	// UnityEngine.Video.VideoPlayer/EventHandler UnityEngine.Video.VideoPlayer::seekCompleted
	EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * ___seekCompleted_9;
	// UnityEngine.Video.VideoPlayer/TimeEventHandler UnityEngine.Video.VideoPlayer::clockResyncOccurred
	TimeEventHandler_t7CA131EB85E0FFCBE8660E030698BD83D3994DD8 * ___clockResyncOccurred_10;
	// UnityEngine.Video.VideoPlayer/FrameReadyEventHandler UnityEngine.Video.VideoPlayer::frameReady
	FrameReadyEventHandler_t9529BD5A34E9C8BE7D8A39D46A6C4ABC673374EC * ___frameReady_11;

public:
	inline static int32_t get_offset_of_prepareCompleted_4() { return static_cast<int32_t>(offsetof(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86, ___prepareCompleted_4)); }
	inline EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * get_prepareCompleted_4() const { return ___prepareCompleted_4; }
	inline EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD ** get_address_of_prepareCompleted_4() { return &___prepareCompleted_4; }
	inline void set_prepareCompleted_4(EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * value)
	{
		___prepareCompleted_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prepareCompleted_4), (void*)value);
	}

	inline static int32_t get_offset_of_loopPointReached_5() { return static_cast<int32_t>(offsetof(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86, ___loopPointReached_5)); }
	inline EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * get_loopPointReached_5() const { return ___loopPointReached_5; }
	inline EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD ** get_address_of_loopPointReached_5() { return &___loopPointReached_5; }
	inline void set_loopPointReached_5(EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * value)
	{
		___loopPointReached_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loopPointReached_5), (void*)value);
	}

	inline static int32_t get_offset_of_started_6() { return static_cast<int32_t>(offsetof(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86, ___started_6)); }
	inline EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * get_started_6() const { return ___started_6; }
	inline EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD ** get_address_of_started_6() { return &___started_6; }
	inline void set_started_6(EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * value)
	{
		___started_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___started_6), (void*)value);
	}

	inline static int32_t get_offset_of_frameDropped_7() { return static_cast<int32_t>(offsetof(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86, ___frameDropped_7)); }
	inline EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * get_frameDropped_7() const { return ___frameDropped_7; }
	inline EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD ** get_address_of_frameDropped_7() { return &___frameDropped_7; }
	inline void set_frameDropped_7(EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * value)
	{
		___frameDropped_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frameDropped_7), (void*)value);
	}

	inline static int32_t get_offset_of_errorReceived_8() { return static_cast<int32_t>(offsetof(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86, ___errorReceived_8)); }
	inline ErrorEventHandler_tD47781EBB7CF0CC4C111496024BD59B1D1A6A1F2 * get_errorReceived_8() const { return ___errorReceived_8; }
	inline ErrorEventHandler_tD47781EBB7CF0CC4C111496024BD59B1D1A6A1F2 ** get_address_of_errorReceived_8() { return &___errorReceived_8; }
	inline void set_errorReceived_8(ErrorEventHandler_tD47781EBB7CF0CC4C111496024BD59B1D1A6A1F2 * value)
	{
		___errorReceived_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___errorReceived_8), (void*)value);
	}

	inline static int32_t get_offset_of_seekCompleted_9() { return static_cast<int32_t>(offsetof(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86, ___seekCompleted_9)); }
	inline EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * get_seekCompleted_9() const { return ___seekCompleted_9; }
	inline EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD ** get_address_of_seekCompleted_9() { return &___seekCompleted_9; }
	inline void set_seekCompleted_9(EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD * value)
	{
		___seekCompleted_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___seekCompleted_9), (void*)value);
	}

	inline static int32_t get_offset_of_clockResyncOccurred_10() { return static_cast<int32_t>(offsetof(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86, ___clockResyncOccurred_10)); }
	inline TimeEventHandler_t7CA131EB85E0FFCBE8660E030698BD83D3994DD8 * get_clockResyncOccurred_10() const { return ___clockResyncOccurred_10; }
	inline TimeEventHandler_t7CA131EB85E0FFCBE8660E030698BD83D3994DD8 ** get_address_of_clockResyncOccurred_10() { return &___clockResyncOccurred_10; }
	inline void set_clockResyncOccurred_10(TimeEventHandler_t7CA131EB85E0FFCBE8660E030698BD83D3994DD8 * value)
	{
		___clockResyncOccurred_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clockResyncOccurred_10), (void*)value);
	}

	inline static int32_t get_offset_of_frameReady_11() { return static_cast<int32_t>(offsetof(VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86, ___frameReady_11)); }
	inline FrameReadyEventHandler_t9529BD5A34E9C8BE7D8A39D46A6C4ABC673374EC * get_frameReady_11() const { return ___frameReady_11; }
	inline FrameReadyEventHandler_t9529BD5A34E9C8BE7D8A39D46A6C4ABC673374EC ** get_address_of_frameReady_11() { return &___frameReady_11; }
	inline void set_frameReady_11(FrameReadyEventHandler_t9529BD5A34E9C8BE7D8A39D46A6C4ABC673374EC * value)
	{
		___frameReady_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___frameReady_11), (void*)value);
	}
};


// UnityEngine.VFX.VisualEffect
struct VisualEffect_t7C6E2AAA4DB4F47960AF2029EA96D4B579B3A4CA  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:
	// UnityEngine.VFX.VFXEventAttribute UnityEngine.VFX.VisualEffect::m_cachedEventAttribute
	VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF * ___m_cachedEventAttribute_4;
	// System.Action`1<UnityEngine.VFX.VFXOutputEventArgs> UnityEngine.VFX.VisualEffect::outputEventReceived
	Action_1_t5C8C9298698F95A378E73C4584F33A97EF82A064 * ___outputEventReceived_5;

public:
	inline static int32_t get_offset_of_m_cachedEventAttribute_4() { return static_cast<int32_t>(offsetof(VisualEffect_t7C6E2AAA4DB4F47960AF2029EA96D4B579B3A4CA, ___m_cachedEventAttribute_4)); }
	inline VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF * get_m_cachedEventAttribute_4() const { return ___m_cachedEventAttribute_4; }
	inline VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF ** get_address_of_m_cachedEventAttribute_4() { return &___m_cachedEventAttribute_4; }
	inline void set_m_cachedEventAttribute_4(VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF * value)
	{
		___m_cachedEventAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_cachedEventAttribute_4), (void*)value);
	}

	inline static int32_t get_offset_of_outputEventReceived_5() { return static_cast<int32_t>(offsetof(VisualEffect_t7C6E2AAA4DB4F47960AF2029EA96D4B579B3A4CA, ___outputEventReceived_5)); }
	inline Action_1_t5C8C9298698F95A378E73C4584F33A97EF82A064 * get_outputEventReceived_5() const { return ___outputEventReceived_5; }
	inline Action_1_t5C8C9298698F95A378E73C4584F33A97EF82A064 ** get_address_of_outputEventReceived_5() { return &___outputEventReceived_5; }
	inline void set_outputEventReceived_5(Action_1_t5C8C9298698F95A378E73C4584F33A97EF82A064 * value)
	{
		___outputEventReceived_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___outputEventReceived_5), (void*)value);
	}
};


// UnityEngine.XR.Management.XRLoaderHelper
struct XRLoaderHelper_t37A55C343AC31D25BB3EBD203DABFA357F51C013  : public XRLoader_tE37B92C6B9CDD944DDF7AFF5704E9EB342D62F6B
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,UnityEngine.ISubsystem> UnityEngine.XR.Management.XRLoaderHelper::m_SubsystemInstanceMap
	Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B * ___m_SubsystemInstanceMap_4;

public:
	inline static int32_t get_offset_of_m_SubsystemInstanceMap_4() { return static_cast<int32_t>(offsetof(XRLoaderHelper_t37A55C343AC31D25BB3EBD203DABFA357F51C013, ___m_SubsystemInstanceMap_4)); }
	inline Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B * get_m_SubsystemInstanceMap_4() const { return ___m_SubsystemInstanceMap_4; }
	inline Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B ** get_address_of_m_SubsystemInstanceMap_4() { return &___m_SubsystemInstanceMap_4; }
	inline void set_m_SubsystemInstanceMap_4(Dictionary_2_t4F3B5B526335E16355EDBC766052AEAB07B1777B * value)
	{
		___m_SubsystemInstanceMap_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SubsystemInstanceMap_4), (void*)value);
	}
};


// API
struct API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject API::ObjSpawner
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ObjSpawner_4;
	// System.String API::ProductIDForSaving
	String_t* ___ProductIDForSaving_5;
	// System.String API::PathSaved
	String_t* ___PathSaved_6;
	// UnityEngine.GameObject API::TapSpawner
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TapSpawner_7;
	// UnityEngine.UI.Text API::text
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___text_9;
	// UnityEngine.AssetBundle API::assetBundle
	AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * ___assetBundle_10;
	// System.Boolean API::isAvailable
	bool ___isAvailable_11;
	// UnityEngine.GameObject API::obj2
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj2_12;
	// UnityEngine.GameObject API::obj
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___obj_13;

public:
	inline static int32_t get_offset_of_ObjSpawner_4() { return static_cast<int32_t>(offsetof(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2, ___ObjSpawner_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ObjSpawner_4() const { return ___ObjSpawner_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ObjSpawner_4() { return &___ObjSpawner_4; }
	inline void set_ObjSpawner_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ObjSpawner_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ObjSpawner_4), (void*)value);
	}

	inline static int32_t get_offset_of_ProductIDForSaving_5() { return static_cast<int32_t>(offsetof(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2, ___ProductIDForSaving_5)); }
	inline String_t* get_ProductIDForSaving_5() const { return ___ProductIDForSaving_5; }
	inline String_t** get_address_of_ProductIDForSaving_5() { return &___ProductIDForSaving_5; }
	inline void set_ProductIDForSaving_5(String_t* value)
	{
		___ProductIDForSaving_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ProductIDForSaving_5), (void*)value);
	}

	inline static int32_t get_offset_of_PathSaved_6() { return static_cast<int32_t>(offsetof(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2, ___PathSaved_6)); }
	inline String_t* get_PathSaved_6() const { return ___PathSaved_6; }
	inline String_t** get_address_of_PathSaved_6() { return &___PathSaved_6; }
	inline void set_PathSaved_6(String_t* value)
	{
		___PathSaved_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PathSaved_6), (void*)value);
	}

	inline static int32_t get_offset_of_TapSpawner_7() { return static_cast<int32_t>(offsetof(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2, ___TapSpawner_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TapSpawner_7() const { return ___TapSpawner_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TapSpawner_7() { return &___TapSpawner_7; }
	inline void set_TapSpawner_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TapSpawner_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TapSpawner_7), (void*)value);
	}

	inline static int32_t get_offset_of_text_9() { return static_cast<int32_t>(offsetof(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2, ___text_9)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_text_9() const { return ___text_9; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_text_9() { return &___text_9; }
	inline void set_text_9(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___text_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___text_9), (void*)value);
	}

	inline static int32_t get_offset_of_assetBundle_10() { return static_cast<int32_t>(offsetof(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2, ___assetBundle_10)); }
	inline AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * get_assetBundle_10() const { return ___assetBundle_10; }
	inline AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 ** get_address_of_assetBundle_10() { return &___assetBundle_10; }
	inline void set_assetBundle_10(AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * value)
	{
		___assetBundle_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assetBundle_10), (void*)value);
	}

	inline static int32_t get_offset_of_isAvailable_11() { return static_cast<int32_t>(offsetof(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2, ___isAvailable_11)); }
	inline bool get_isAvailable_11() const { return ___isAvailable_11; }
	inline bool* get_address_of_isAvailable_11() { return &___isAvailable_11; }
	inline void set_isAvailable_11(bool value)
	{
		___isAvailable_11 = value;
	}

	inline static int32_t get_offset_of_obj2_12() { return static_cast<int32_t>(offsetof(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2, ___obj2_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_obj2_12() const { return ___obj2_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_obj2_12() { return &___obj2_12; }
	inline void set_obj2_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___obj2_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___obj2_12), (void*)value);
	}

	inline static int32_t get_offset_of_obj_13() { return static_cast<int32_t>(offsetof(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2, ___obj_13)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_obj_13() const { return ___obj_13; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_obj_13() { return &___obj_13; }
	inline void set_obj_13(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___obj_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___obj_13), (void*)value);
	}
};


// UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer
struct ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::m_Mode
	int32_t ___m_Mode_4;
	// UnityEngine.XR.ARFoundation.ARPointCloud UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::m_PointCloud
	ARPointCloud_t7801A20C710FCBFDF1A589FA3ED53C7C9DF9222A * ___m_PointCloud_5;
	// UnityEngine.ParticleSystem UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_6;
	// UnityEngine.ParticleSystem/Particle[] UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::m_Particles
	ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* ___m_Particles_7;
	// System.Int32 UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::m_NumParticles
	int32_t ___m_NumParticles_8;
	// System.Collections.Generic.Dictionary`2<System.UInt64,UnityEngine.Vector3> UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::m_Points
	Dictionary_2_t921FF2EF21AD8B2BCFAD3713C351119422CE55FB * ___m_Points_9;

public:
	inline static int32_t get_offset_of_m_Mode_4() { return static_cast<int32_t>(offsetof(ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B, ___m_Mode_4)); }
	inline int32_t get_m_Mode_4() const { return ___m_Mode_4; }
	inline int32_t* get_address_of_m_Mode_4() { return &___m_Mode_4; }
	inline void set_m_Mode_4(int32_t value)
	{
		___m_Mode_4 = value;
	}

	inline static int32_t get_offset_of_m_PointCloud_5() { return static_cast<int32_t>(offsetof(ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B, ___m_PointCloud_5)); }
	inline ARPointCloud_t7801A20C710FCBFDF1A589FA3ED53C7C9DF9222A * get_m_PointCloud_5() const { return ___m_PointCloud_5; }
	inline ARPointCloud_t7801A20C710FCBFDF1A589FA3ED53C7C9DF9222A ** get_address_of_m_PointCloud_5() { return &___m_PointCloud_5; }
	inline void set_m_PointCloud_5(ARPointCloud_t7801A20C710FCBFDF1A589FA3ED53C7C9DF9222A * value)
	{
		___m_PointCloud_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PointCloud_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParticleSystem_6() { return static_cast<int32_t>(offsetof(ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B, ___m_ParticleSystem_6)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_6() const { return ___m_ParticleSystem_6; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_6() { return &___m_ParticleSystem_6; }
	inline void set_m_ParticleSystem_6(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Particles_7() { return static_cast<int32_t>(offsetof(ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B, ___m_Particles_7)); }
	inline ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* get_m_Particles_7() const { return ___m_Particles_7; }
	inline ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C** get_address_of_m_Particles_7() { return &___m_Particles_7; }
	inline void set_m_Particles_7(ParticleU5BU5D_tF02F4854575E99F3004B58B6CC6BB2373BAEB04C* value)
	{
		___m_Particles_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Particles_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_NumParticles_8() { return static_cast<int32_t>(offsetof(ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B, ___m_NumParticles_8)); }
	inline int32_t get_m_NumParticles_8() const { return ___m_NumParticles_8; }
	inline int32_t* get_address_of_m_NumParticles_8() { return &___m_NumParticles_8; }
	inline void set_m_NumParticles_8(int32_t value)
	{
		___m_NumParticles_8 = value;
	}

	inline static int32_t get_offset_of_m_Points_9() { return static_cast<int32_t>(offsetof(ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B, ___m_Points_9)); }
	inline Dictionary_2_t921FF2EF21AD8B2BCFAD3713C351119422CE55FB * get_m_Points_9() const { return ___m_Points_9; }
	inline Dictionary_2_t921FF2EF21AD8B2BCFAD3713C351119422CE55FB ** get_address_of_m_Points_9() { return &___m_Points_9; }
	inline void set_m_Points_9(Dictionary_2_t921FF2EF21AD8B2BCFAD3713C351119422CE55FB * value)
	{
		___m_Points_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Points_9), (void*)value);
	}
};


// UnityEngine.XR.ARKit.ARKitLoader
struct ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27  : public XRLoaderHelper_t37A55C343AC31D25BB3EBD203DABFA357F51C013
{
public:

public:
};

struct ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRSessionSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_SessionSubsystemDescriptors
	List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 * ___s_SessionSubsystemDescriptors_5;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRCameraSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_CameraSubsystemDescriptors
	List_1_t5ACA7E75885D8B9D7B85548B84BF43976A5038DC * ___s_CameraSubsystemDescriptors_6;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRDepthSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_DepthSubsystemDescriptors
	List_1_t449C61AAF5F71828F78F12D50C77AE776AF0E330 * ___s_DepthSubsystemDescriptors_7;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRPlaneSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_PlaneSubsystemDescriptors
	List_1_t6D435690E62CDB3645DB1A76F3B7B8B6069BDB4F * ___s_PlaneSubsystemDescriptors_8;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRAnchorSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_AnchorSubsystemDescriptors
	List_1_tDED98C236097B36F9015B396398179A6F8A62E50 * ___s_AnchorSubsystemDescriptors_9;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRRaycastSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_RaycastSubsystemDescriptors
	List_1_tAE84735071B78277703DB9996DE2E5C4456317C5 * ___s_RaycastSubsystemDescriptors_10;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRHumanBodySubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_HumanBodySubsystemDescriptors
	List_1_tB23F14817387B6E0CF6BC3F698BE74D4321CBBD4 * ___s_HumanBodySubsystemDescriptors_11;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XREnvironmentProbeSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_EnvironmentProbeSubsystemDescriptors
	List_1_tCD8BCD5191A621FA7E8E3F2D67BA5D2BCE0FC04F * ___s_EnvironmentProbeSubsystemDescriptors_12;
	// System.Collections.Generic.List`1<UnityEngine.XR.XRInputSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_InputSubsystemDescriptors
	List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 * ___s_InputSubsystemDescriptors_13;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRImageTrackingSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_ImageTrackingSubsystemDescriptors
	List_1_t179969632C4CD7B25BEBAA7DA12BA4C8F82F69F2 * ___s_ImageTrackingSubsystemDescriptors_14;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRObjectTrackingSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_ObjectTrackingSubsystemDescriptors
	List_1_t7B38C168F61005A6F1570A6CB78D9FBC8916E16D * ___s_ObjectTrackingSubsystemDescriptors_15;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRFaceSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_FaceSubsystemDescriptors
	List_1_tA1D273638689FBC5ED4F3CAF82F64C158000481E * ___s_FaceSubsystemDescriptors_16;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XROcclusionSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_OcclusionSubsystemDescriptors
	List_1_t9AA280C4698A976F6616D552D829D1609D4A65BC * ___s_OcclusionSubsystemDescriptors_17;
	// System.Collections.Generic.List`1<UnityEngine.XR.ARSubsystems.XRParticipantSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_ParticipantSubsystemDescriptors
	List_1_tA457BDB78534FD0C67280F0D299F21C6BB7C23BF * ___s_ParticipantSubsystemDescriptors_18;
	// System.Collections.Generic.List`1<UnityEngine.XR.XRMeshSubsystemDescriptor> UnityEngine.XR.ARKit.ARKitLoader::s_MeshSubsystemDescriptors
	List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 * ___s_MeshSubsystemDescriptors_19;

public:
	inline static int32_t get_offset_of_s_SessionSubsystemDescriptors_5() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_SessionSubsystemDescriptors_5)); }
	inline List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 * get_s_SessionSubsystemDescriptors_5() const { return ___s_SessionSubsystemDescriptors_5; }
	inline List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 ** get_address_of_s_SessionSubsystemDescriptors_5() { return &___s_SessionSubsystemDescriptors_5; }
	inline void set_s_SessionSubsystemDescriptors_5(List_1_tDA25459A722F5B00FFE84463D9952660BC3F6673 * value)
	{
		___s_SessionSubsystemDescriptors_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SessionSubsystemDescriptors_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_CameraSubsystemDescriptors_6() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_CameraSubsystemDescriptors_6)); }
	inline List_1_t5ACA7E75885D8B9D7B85548B84BF43976A5038DC * get_s_CameraSubsystemDescriptors_6() const { return ___s_CameraSubsystemDescriptors_6; }
	inline List_1_t5ACA7E75885D8B9D7B85548B84BF43976A5038DC ** get_address_of_s_CameraSubsystemDescriptors_6() { return &___s_CameraSubsystemDescriptors_6; }
	inline void set_s_CameraSubsystemDescriptors_6(List_1_t5ACA7E75885D8B9D7B85548B84BF43976A5038DC * value)
	{
		___s_CameraSubsystemDescriptors_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_CameraSubsystemDescriptors_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_DepthSubsystemDescriptors_7() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_DepthSubsystemDescriptors_7)); }
	inline List_1_t449C61AAF5F71828F78F12D50C77AE776AF0E330 * get_s_DepthSubsystemDescriptors_7() const { return ___s_DepthSubsystemDescriptors_7; }
	inline List_1_t449C61AAF5F71828F78F12D50C77AE776AF0E330 ** get_address_of_s_DepthSubsystemDescriptors_7() { return &___s_DepthSubsystemDescriptors_7; }
	inline void set_s_DepthSubsystemDescriptors_7(List_1_t449C61AAF5F71828F78F12D50C77AE776AF0E330 * value)
	{
		___s_DepthSubsystemDescriptors_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DepthSubsystemDescriptors_7), (void*)value);
	}

	inline static int32_t get_offset_of_s_PlaneSubsystemDescriptors_8() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_PlaneSubsystemDescriptors_8)); }
	inline List_1_t6D435690E62CDB3645DB1A76F3B7B8B6069BDB4F * get_s_PlaneSubsystemDescriptors_8() const { return ___s_PlaneSubsystemDescriptors_8; }
	inline List_1_t6D435690E62CDB3645DB1A76F3B7B8B6069BDB4F ** get_address_of_s_PlaneSubsystemDescriptors_8() { return &___s_PlaneSubsystemDescriptors_8; }
	inline void set_s_PlaneSubsystemDescriptors_8(List_1_t6D435690E62CDB3645DB1A76F3B7B8B6069BDB4F * value)
	{
		___s_PlaneSubsystemDescriptors_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_PlaneSubsystemDescriptors_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_AnchorSubsystemDescriptors_9() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_AnchorSubsystemDescriptors_9)); }
	inline List_1_tDED98C236097B36F9015B396398179A6F8A62E50 * get_s_AnchorSubsystemDescriptors_9() const { return ___s_AnchorSubsystemDescriptors_9; }
	inline List_1_tDED98C236097B36F9015B396398179A6F8A62E50 ** get_address_of_s_AnchorSubsystemDescriptors_9() { return &___s_AnchorSubsystemDescriptors_9; }
	inline void set_s_AnchorSubsystemDescriptors_9(List_1_tDED98C236097B36F9015B396398179A6F8A62E50 * value)
	{
		___s_AnchorSubsystemDescriptors_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_AnchorSubsystemDescriptors_9), (void*)value);
	}

	inline static int32_t get_offset_of_s_RaycastSubsystemDescriptors_10() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_RaycastSubsystemDescriptors_10)); }
	inline List_1_tAE84735071B78277703DB9996DE2E5C4456317C5 * get_s_RaycastSubsystemDescriptors_10() const { return ___s_RaycastSubsystemDescriptors_10; }
	inline List_1_tAE84735071B78277703DB9996DE2E5C4456317C5 ** get_address_of_s_RaycastSubsystemDescriptors_10() { return &___s_RaycastSubsystemDescriptors_10; }
	inline void set_s_RaycastSubsystemDescriptors_10(List_1_tAE84735071B78277703DB9996DE2E5C4456317C5 * value)
	{
		___s_RaycastSubsystemDescriptors_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_RaycastSubsystemDescriptors_10), (void*)value);
	}

	inline static int32_t get_offset_of_s_HumanBodySubsystemDescriptors_11() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_HumanBodySubsystemDescriptors_11)); }
	inline List_1_tB23F14817387B6E0CF6BC3F698BE74D4321CBBD4 * get_s_HumanBodySubsystemDescriptors_11() const { return ___s_HumanBodySubsystemDescriptors_11; }
	inline List_1_tB23F14817387B6E0CF6BC3F698BE74D4321CBBD4 ** get_address_of_s_HumanBodySubsystemDescriptors_11() { return &___s_HumanBodySubsystemDescriptors_11; }
	inline void set_s_HumanBodySubsystemDescriptors_11(List_1_tB23F14817387B6E0CF6BC3F698BE74D4321CBBD4 * value)
	{
		___s_HumanBodySubsystemDescriptors_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_HumanBodySubsystemDescriptors_11), (void*)value);
	}

	inline static int32_t get_offset_of_s_EnvironmentProbeSubsystemDescriptors_12() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_EnvironmentProbeSubsystemDescriptors_12)); }
	inline List_1_tCD8BCD5191A621FA7E8E3F2D67BA5D2BCE0FC04F * get_s_EnvironmentProbeSubsystemDescriptors_12() const { return ___s_EnvironmentProbeSubsystemDescriptors_12; }
	inline List_1_tCD8BCD5191A621FA7E8E3F2D67BA5D2BCE0FC04F ** get_address_of_s_EnvironmentProbeSubsystemDescriptors_12() { return &___s_EnvironmentProbeSubsystemDescriptors_12; }
	inline void set_s_EnvironmentProbeSubsystemDescriptors_12(List_1_tCD8BCD5191A621FA7E8E3F2D67BA5D2BCE0FC04F * value)
	{
		___s_EnvironmentProbeSubsystemDescriptors_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EnvironmentProbeSubsystemDescriptors_12), (void*)value);
	}

	inline static int32_t get_offset_of_s_InputSubsystemDescriptors_13() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_InputSubsystemDescriptors_13)); }
	inline List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 * get_s_InputSubsystemDescriptors_13() const { return ___s_InputSubsystemDescriptors_13; }
	inline List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 ** get_address_of_s_InputSubsystemDescriptors_13() { return &___s_InputSubsystemDescriptors_13; }
	inline void set_s_InputSubsystemDescriptors_13(List_1_t885BD663DFFEB6C32E74934BE1CE00D566657BA0 * value)
	{
		___s_InputSubsystemDescriptors_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_InputSubsystemDescriptors_13), (void*)value);
	}

	inline static int32_t get_offset_of_s_ImageTrackingSubsystemDescriptors_14() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_ImageTrackingSubsystemDescriptors_14)); }
	inline List_1_t179969632C4CD7B25BEBAA7DA12BA4C8F82F69F2 * get_s_ImageTrackingSubsystemDescriptors_14() const { return ___s_ImageTrackingSubsystemDescriptors_14; }
	inline List_1_t179969632C4CD7B25BEBAA7DA12BA4C8F82F69F2 ** get_address_of_s_ImageTrackingSubsystemDescriptors_14() { return &___s_ImageTrackingSubsystemDescriptors_14; }
	inline void set_s_ImageTrackingSubsystemDescriptors_14(List_1_t179969632C4CD7B25BEBAA7DA12BA4C8F82F69F2 * value)
	{
		___s_ImageTrackingSubsystemDescriptors_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ImageTrackingSubsystemDescriptors_14), (void*)value);
	}

	inline static int32_t get_offset_of_s_ObjectTrackingSubsystemDescriptors_15() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_ObjectTrackingSubsystemDescriptors_15)); }
	inline List_1_t7B38C168F61005A6F1570A6CB78D9FBC8916E16D * get_s_ObjectTrackingSubsystemDescriptors_15() const { return ___s_ObjectTrackingSubsystemDescriptors_15; }
	inline List_1_t7B38C168F61005A6F1570A6CB78D9FBC8916E16D ** get_address_of_s_ObjectTrackingSubsystemDescriptors_15() { return &___s_ObjectTrackingSubsystemDescriptors_15; }
	inline void set_s_ObjectTrackingSubsystemDescriptors_15(List_1_t7B38C168F61005A6F1570A6CB78D9FBC8916E16D * value)
	{
		___s_ObjectTrackingSubsystemDescriptors_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ObjectTrackingSubsystemDescriptors_15), (void*)value);
	}

	inline static int32_t get_offset_of_s_FaceSubsystemDescriptors_16() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_FaceSubsystemDescriptors_16)); }
	inline List_1_tA1D273638689FBC5ED4F3CAF82F64C158000481E * get_s_FaceSubsystemDescriptors_16() const { return ___s_FaceSubsystemDescriptors_16; }
	inline List_1_tA1D273638689FBC5ED4F3CAF82F64C158000481E ** get_address_of_s_FaceSubsystemDescriptors_16() { return &___s_FaceSubsystemDescriptors_16; }
	inline void set_s_FaceSubsystemDescriptors_16(List_1_tA1D273638689FBC5ED4F3CAF82F64C158000481E * value)
	{
		___s_FaceSubsystemDescriptors_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_FaceSubsystemDescriptors_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_OcclusionSubsystemDescriptors_17() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_OcclusionSubsystemDescriptors_17)); }
	inline List_1_t9AA280C4698A976F6616D552D829D1609D4A65BC * get_s_OcclusionSubsystemDescriptors_17() const { return ___s_OcclusionSubsystemDescriptors_17; }
	inline List_1_t9AA280C4698A976F6616D552D829D1609D4A65BC ** get_address_of_s_OcclusionSubsystemDescriptors_17() { return &___s_OcclusionSubsystemDescriptors_17; }
	inline void set_s_OcclusionSubsystemDescriptors_17(List_1_t9AA280C4698A976F6616D552D829D1609D4A65BC * value)
	{
		___s_OcclusionSubsystemDescriptors_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_OcclusionSubsystemDescriptors_17), (void*)value);
	}

	inline static int32_t get_offset_of_s_ParticipantSubsystemDescriptors_18() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_ParticipantSubsystemDescriptors_18)); }
	inline List_1_tA457BDB78534FD0C67280F0D299F21C6BB7C23BF * get_s_ParticipantSubsystemDescriptors_18() const { return ___s_ParticipantSubsystemDescriptors_18; }
	inline List_1_tA457BDB78534FD0C67280F0D299F21C6BB7C23BF ** get_address_of_s_ParticipantSubsystemDescriptors_18() { return &___s_ParticipantSubsystemDescriptors_18; }
	inline void set_s_ParticipantSubsystemDescriptors_18(List_1_tA457BDB78534FD0C67280F0D299F21C6BB7C23BF * value)
	{
		___s_ParticipantSubsystemDescriptors_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ParticipantSubsystemDescriptors_18), (void*)value);
	}

	inline static int32_t get_offset_of_s_MeshSubsystemDescriptors_19() { return static_cast<int32_t>(offsetof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields, ___s_MeshSubsystemDescriptors_19)); }
	inline List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 * get_s_MeshSubsystemDescriptors_19() const { return ___s_MeshSubsystemDescriptors_19; }
	inline List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 ** get_address_of_s_MeshSubsystemDescriptors_19() { return &___s_MeshSubsystemDescriptors_19; }
	inline void set_s_MeshSubsystemDescriptors_19(List_1_tA4CB3CC063D44B52D336C5DDA258EF7CE9B98A94 * value)
	{
		___s_MeshSubsystemDescriptors_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_MeshSubsystemDescriptors_19), (void*)value);
	}
};


// ARTapToPlace
struct ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean ARTapToPlace::ProjectionDone
	bool ___ProjectionDone_4;
	// UnityEngine.GameObject ARTapToPlace::gameObjectToInstantiate
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gameObjectToInstantiate_5;
	// UnityEngine.GameObject ARTapToPlace::spawnedObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___spawnedObject_6;
	// UnityEngine.GameObject ARTapToPlace::_ObjSpawner
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____ObjSpawner_7;
	// UnityEngine.XR.ARFoundation.ARRaycastManager ARTapToPlace::_arRaycastManager
	ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * ____arRaycastManager_8;
	// UnityEngine.Vector2 ARTapToPlace::touchPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___touchPosition_9;

public:
	inline static int32_t get_offset_of_ProjectionDone_4() { return static_cast<int32_t>(offsetof(ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51, ___ProjectionDone_4)); }
	inline bool get_ProjectionDone_4() const { return ___ProjectionDone_4; }
	inline bool* get_address_of_ProjectionDone_4() { return &___ProjectionDone_4; }
	inline void set_ProjectionDone_4(bool value)
	{
		___ProjectionDone_4 = value;
	}

	inline static int32_t get_offset_of_gameObjectToInstantiate_5() { return static_cast<int32_t>(offsetof(ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51, ___gameObjectToInstantiate_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_gameObjectToInstantiate_5() const { return ___gameObjectToInstantiate_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_gameObjectToInstantiate_5() { return &___gameObjectToInstantiate_5; }
	inline void set_gameObjectToInstantiate_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___gameObjectToInstantiate_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameObjectToInstantiate_5), (void*)value);
	}

	inline static int32_t get_offset_of_spawnedObject_6() { return static_cast<int32_t>(offsetof(ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51, ___spawnedObject_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_spawnedObject_6() const { return ___spawnedObject_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_spawnedObject_6() { return &___spawnedObject_6; }
	inline void set_spawnedObject_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___spawnedObject_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spawnedObject_6), (void*)value);
	}

	inline static int32_t get_offset_of__ObjSpawner_7() { return static_cast<int32_t>(offsetof(ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51, ____ObjSpawner_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__ObjSpawner_7() const { return ____ObjSpawner_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__ObjSpawner_7() { return &____ObjSpawner_7; }
	inline void set__ObjSpawner_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____ObjSpawner_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ObjSpawner_7), (void*)value);
	}

	inline static int32_t get_offset_of__arRaycastManager_8() { return static_cast<int32_t>(offsetof(ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51, ____arRaycastManager_8)); }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * get__arRaycastManager_8() const { return ____arRaycastManager_8; }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F ** get_address_of__arRaycastManager_8() { return &____arRaycastManager_8; }
	inline void set__arRaycastManager_8(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * value)
	{
		____arRaycastManager_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____arRaycastManager_8), (void*)value);
	}

	inline static int32_t get_offset_of_touchPosition_9() { return static_cast<int32_t>(offsetof(ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51, ___touchPosition_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_touchPosition_9() const { return ___touchPosition_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_touchPosition_9() { return &___touchPosition_9; }
	inline void set_touchPosition_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___touchPosition_9 = value;
	}
};

struct ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> ARTapToPlace::hits
	List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * ___hits_10;

public:
	inline static int32_t get_offset_of_hits_10() { return static_cast<int32_t>(offsetof(ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_StaticFields, ___hits_10)); }
	inline List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * get_hits_10() const { return ___hits_10; }
	inline List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D ** get_address_of_hits_10() { return &___hits_10; }
	inline void set_hits_10(List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * value)
	{
		___hits_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hits_10), (void*)value);
	}
};


// ARTapToPlaceAvatar
struct ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject ARTapToPlaceAvatar::gameObjectToInstantiate
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___gameObjectToInstantiate_4;
	// UnityEngine.GameObject ARTapToPlaceAvatar::spawnedObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___spawnedObject_5;
	// UnityEngine.XR.ARFoundation.ARRaycastManager ARTapToPlaceAvatar::_arRaycastManager
	ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * ____arRaycastManager_6;
	// UnityEngine.Vector2 ARTapToPlaceAvatar::touchPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___touchPosition_7;
	// UnityEngine.GameObject ARTapToPlaceAvatar::AvatarController
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___AvatarController_9;

public:
	inline static int32_t get_offset_of_gameObjectToInstantiate_4() { return static_cast<int32_t>(offsetof(ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3, ___gameObjectToInstantiate_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_gameObjectToInstantiate_4() const { return ___gameObjectToInstantiate_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_gameObjectToInstantiate_4() { return &___gameObjectToInstantiate_4; }
	inline void set_gameObjectToInstantiate_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___gameObjectToInstantiate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gameObjectToInstantiate_4), (void*)value);
	}

	inline static int32_t get_offset_of_spawnedObject_5() { return static_cast<int32_t>(offsetof(ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3, ___spawnedObject_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_spawnedObject_5() const { return ___spawnedObject_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_spawnedObject_5() { return &___spawnedObject_5; }
	inline void set_spawnedObject_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___spawnedObject_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spawnedObject_5), (void*)value);
	}

	inline static int32_t get_offset_of__arRaycastManager_6() { return static_cast<int32_t>(offsetof(ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3, ____arRaycastManager_6)); }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * get__arRaycastManager_6() const { return ____arRaycastManager_6; }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F ** get_address_of__arRaycastManager_6() { return &____arRaycastManager_6; }
	inline void set__arRaycastManager_6(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * value)
	{
		____arRaycastManager_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____arRaycastManager_6), (void*)value);
	}

	inline static int32_t get_offset_of_touchPosition_7() { return static_cast<int32_t>(offsetof(ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3, ___touchPosition_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_touchPosition_7() const { return ___touchPosition_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_touchPosition_7() { return &___touchPosition_7; }
	inline void set_touchPosition_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___touchPosition_7 = value;
	}

	inline static int32_t get_offset_of_AvatarController_9() { return static_cast<int32_t>(offsetof(ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3, ___AvatarController_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_AvatarController_9() const { return ___AvatarController_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_AvatarController_9() { return &___AvatarController_9; }
	inline void set_AvatarController_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___AvatarController_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AvatarController_9), (void*)value);
	}
};

struct ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> ARTapToPlaceAvatar::hits
	List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * ___hits_8;

public:
	inline static int32_t get_offset_of_hits_8() { return static_cast<int32_t>(offsetof(ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3_StaticFields, ___hits_8)); }
	inline List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * get_hits_8() const { return ___hits_8; }
	inline List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D ** get_address_of_hits_8() { return &___hits_8; }
	inline void set_hits_8(List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * value)
	{
		___hits_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hits_8), (void*)value);
	}
};


// Activate
struct Activate_tE8FDB08082D994FB61E54E2B7D81FBD0A9AF8E88  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject Activate::thissdas
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___thissdas_4;

public:
	inline static int32_t get_offset_of_thissdas_4() { return static_cast<int32_t>(offsetof(Activate_tE8FDB08082D994FB61E54E2B7D81FBD0A9AF8E88, ___thissdas_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_thissdas_4() const { return ___thissdas_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_thissdas_4() { return &___thissdas_4; }
	inline void set_thissdas_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___thissdas_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___thissdas_4), (void*)value);
	}
};


// AddToCartScript
struct AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Text AddToCartScript::CartItemsCount
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___CartItemsCount_4;
	// System.Int32 AddToCartScript::CartItemsCounter
	int32_t ___CartItemsCounter_5;
	// System.Collections.Generic.List`1<AddToCartScript/ItemsData> AddToCartScript::MyItems
	List_1_t5A371EF4F5E6A6D6CD6E33ECF11BBAC748F84D8F * ___MyItems_6;

public:
	inline static int32_t get_offset_of_CartItemsCount_4() { return static_cast<int32_t>(offsetof(AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2, ___CartItemsCount_4)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_CartItemsCount_4() const { return ___CartItemsCount_4; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_CartItemsCount_4() { return &___CartItemsCount_4; }
	inline void set_CartItemsCount_4(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___CartItemsCount_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CartItemsCount_4), (void*)value);
	}

	inline static int32_t get_offset_of_CartItemsCounter_5() { return static_cast<int32_t>(offsetof(AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2, ___CartItemsCounter_5)); }
	inline int32_t get_CartItemsCounter_5() const { return ___CartItemsCounter_5; }
	inline int32_t* get_address_of_CartItemsCounter_5() { return &___CartItemsCounter_5; }
	inline void set_CartItemsCounter_5(int32_t value)
	{
		___CartItemsCounter_5 = value;
	}

	inline static int32_t get_offset_of_MyItems_6() { return static_cast<int32_t>(offsetof(AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2, ___MyItems_6)); }
	inline List_1_t5A371EF4F5E6A6D6CD6E33ECF11BBAC748F84D8F * get_MyItems_6() const { return ___MyItems_6; }
	inline List_1_t5A371EF4F5E6A6D6CD6E33ECF11BBAC748F84D8F ** get_address_of_MyItems_6() { return &___MyItems_6; }
	inline void set_MyItems_6(List_1_t5A371EF4F5E6A6D6CD6E33ECF11BBAC748F84D8F * value)
	{
		___MyItems_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MyItems_6), (void*)value);
	}
};


// Anim
struct Anim_tF3468CFBF078A3F63800C8B0BEA4A5119743F8D4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// AnimateModel
struct AnimateModel_t5FA7BFBC700198F4FD54C33909FB2F40BDF02877  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// ArCanvas
struct ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AndroidJavaObject ArCanvas::communicationBridge
	AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * ___communicationBridge_4;
	// UnityEngine.GameObject ArCanvas::CanvasHolder
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CanvasHolder_5;
	// UnityEngine.GameObject ArCanvas::CartPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CartPanel_6;
	// UnityEngine.GameObject ArCanvas::InfoButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___InfoButton_7;
	// UnityEngine.GameObject ArCanvas::CloseButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CloseButton_8;
	// UnityEngine.GameObject ArCanvas::ObjSelectionButton
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ObjSelectionButton_9;
	// System.Boolean ArCanvas::flag
	bool ___flag_10;
	// UnityEngine.GameObject ArCanvas::objSpaw
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___objSpaw_11;

public:
	inline static int32_t get_offset_of_communicationBridge_4() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___communicationBridge_4)); }
	inline AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * get_communicationBridge_4() const { return ___communicationBridge_4; }
	inline AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E ** get_address_of_communicationBridge_4() { return &___communicationBridge_4; }
	inline void set_communicationBridge_4(AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * value)
	{
		___communicationBridge_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___communicationBridge_4), (void*)value);
	}

	inline static int32_t get_offset_of_CanvasHolder_5() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___CanvasHolder_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CanvasHolder_5() const { return ___CanvasHolder_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CanvasHolder_5() { return &___CanvasHolder_5; }
	inline void set_CanvasHolder_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CanvasHolder_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CanvasHolder_5), (void*)value);
	}

	inline static int32_t get_offset_of_CartPanel_6() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___CartPanel_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CartPanel_6() const { return ___CartPanel_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CartPanel_6() { return &___CartPanel_6; }
	inline void set_CartPanel_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CartPanel_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CartPanel_6), (void*)value);
	}

	inline static int32_t get_offset_of_InfoButton_7() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___InfoButton_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_InfoButton_7() const { return ___InfoButton_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_InfoButton_7() { return &___InfoButton_7; }
	inline void set_InfoButton_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___InfoButton_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InfoButton_7), (void*)value);
	}

	inline static int32_t get_offset_of_CloseButton_8() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___CloseButton_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CloseButton_8() const { return ___CloseButton_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CloseButton_8() { return &___CloseButton_8; }
	inline void set_CloseButton_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CloseButton_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CloseButton_8), (void*)value);
	}

	inline static int32_t get_offset_of_ObjSelectionButton_9() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___ObjSelectionButton_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ObjSelectionButton_9() const { return ___ObjSelectionButton_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ObjSelectionButton_9() { return &___ObjSelectionButton_9; }
	inline void set_ObjSelectionButton_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ObjSelectionButton_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ObjSelectionButton_9), (void*)value);
	}

	inline static int32_t get_offset_of_flag_10() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___flag_10)); }
	inline bool get_flag_10() const { return ___flag_10; }
	inline bool* get_address_of_flag_10() { return &___flag_10; }
	inline void set_flag_10(bool value)
	{
		___flag_10 = value;
	}

	inline static int32_t get_offset_of_objSpaw_11() { return static_cast<int32_t>(offsetof(ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B, ___objSpaw_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_objSpaw_11() const { return ___objSpaw_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_objSpaw_11() { return &___objSpaw_11; }
	inline void set_objSpaw_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___objSpaw_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objSpaw_11), (void*)value);
	}
};


// CSharpscaling
struct CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CSharpscaling::initialFingersDistance
	float ___initialFingersDistance_4;
	// UnityEngine.Vector3 CSharpscaling::initialScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___initialScale_5;

public:
	inline static int32_t get_offset_of_initialFingersDistance_4() { return static_cast<int32_t>(offsetof(CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1, ___initialFingersDistance_4)); }
	inline float get_initialFingersDistance_4() const { return ___initialFingersDistance_4; }
	inline float* get_address_of_initialFingersDistance_4() { return &___initialFingersDistance_4; }
	inline void set_initialFingersDistance_4(float value)
	{
		___initialFingersDistance_4 = value;
	}

	inline static int32_t get_offset_of_initialScale_5() { return static_cast<int32_t>(offsetof(CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1, ___initialScale_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_initialScale_5() const { return ___initialScale_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_initialScale_5() { return &___initialScale_5; }
	inline void set_initialScale_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___initialScale_5 = value;
	}
};

struct CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1_StaticFields
{
public:
	// UnityEngine.Transform CSharpscaling::ScaleTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___ScaleTransform_6;

public:
	inline static int32_t get_offset_of_ScaleTransform_6() { return static_cast<int32_t>(offsetof(CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1_StaticFields, ___ScaleTransform_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_ScaleTransform_6() const { return ___ScaleTransform_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_ScaleTransform_6() { return &___ScaleTransform_6; }
	inline void set_ScaleTransform_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___ScaleTransform_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ScaleTransform_6), (void*)value);
	}
};


// ContentController
struct ContentController_tE2F771CAE14A2E52EF9E34E52B9776732FBA3DCB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// API ContentController::api
	API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * ___api_4;
	// UnityEngine.GameObject ContentController::ObjSpawner
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ObjSpawner_5;
	// UnityEngine.GameObject ContentController::TapSpawner
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TapSpawner_6;

public:
	inline static int32_t get_offset_of_api_4() { return static_cast<int32_t>(offsetof(ContentController_tE2F771CAE14A2E52EF9E34E52B9776732FBA3DCB, ___api_4)); }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * get_api_4() const { return ___api_4; }
	inline API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 ** get_address_of_api_4() { return &___api_4; }
	inline void set_api_4(API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2 * value)
	{
		___api_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___api_4), (void*)value);
	}

	inline static int32_t get_offset_of_ObjSpawner_5() { return static_cast<int32_t>(offsetof(ContentController_tE2F771CAE14A2E52EF9E34E52B9776732FBA3DCB, ___ObjSpawner_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ObjSpawner_5() const { return ___ObjSpawner_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ObjSpawner_5() { return &___ObjSpawner_5; }
	inline void set_ObjSpawner_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ObjSpawner_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ObjSpawner_5), (void*)value);
	}

	inline static int32_t get_offset_of_TapSpawner_6() { return static_cast<int32_t>(offsetof(ContentController_tE2F771CAE14A2E52EF9E34E52B9776732FBA3DCB, ___TapSpawner_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TapSpawner_6() const { return ___TapSpawner_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TapSpawner_6() { return &___TapSpawner_6; }
	inline void set_TapSpawner_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TapSpawner_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TapSpawner_6), (void*)value);
	}
};


// DragObject1
struct DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 DragObject1::dist
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___dist_4;
	// System.Single DragObject1::posX
	float ___posX_5;
	// System.Single DragObject1::posY
	float ___posY_6;

public:
	inline static int32_t get_offset_of_dist_4() { return static_cast<int32_t>(offsetof(DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5, ___dist_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_dist_4() const { return ___dist_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_dist_4() { return &___dist_4; }
	inline void set_dist_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___dist_4 = value;
	}

	inline static int32_t get_offset_of_posX_5() { return static_cast<int32_t>(offsetof(DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5, ___posX_5)); }
	inline float get_posX_5() const { return ___posX_5; }
	inline float* get_address_of_posX_5() { return &___posX_5; }
	inline void set_posX_5(float value)
	{
		___posX_5 = value;
	}

	inline static int32_t get_offset_of_posY_6() { return static_cast<int32_t>(offsetof(DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5, ___posY_6)); }
	inline float get_posY_6() const { return ___posY_6; }
	inline float* get_address_of_posY_6() { return &___posY_6; }
	inline void set_posY_6(float value)
	{
		___posY_6 = value;
	}
};


// LookAtCamera
struct LookAtCamera_t1A909DB8DF88C609D403662C931A1713806027B2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject LookAtCamera::Cam
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Cam_4;

public:
	inline static int32_t get_offset_of_Cam_4() { return static_cast<int32_t>(offsetof(LookAtCamera_t1A909DB8DF88C609D403662C931A1713806027B2, ___Cam_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Cam_4() const { return ___Cam_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Cam_4() { return &___Cam_4; }
	inline void set_Cam_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Cam_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Cam_4), (void*)value);
	}
};


// MyDrag
struct MyDrag_tBF6DCE4AEFE3669D9791EA6423CD9FCAEBD60F1F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean MyDrag::holding
	bool ___holding_4;

public:
	inline static int32_t get_offset_of_holding_4() { return static_cast<int32_t>(offsetof(MyDrag_tBF6DCE4AEFE3669D9791EA6423CD9FCAEBD60F1F, ___holding_4)); }
	inline bool get_holding_4() const { return ___holding_4; }
	inline bool* get_address_of_holding_4() { return &___holding_4; }
	inline void set_holding_4(bool value)
	{
		___holding_4 = value;
	}
};


// ObjectSpawner
struct ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Sprite ObjectSpawner::CloseIconPlaceHolder
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___CloseIconPlaceHolder_4;
	// UnityEngine.Sprite ObjectSpawner::CartIconPlaceHolder
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___CartIconPlaceHolder_5;
	// UnityEngine.GameObject ObjectSpawner::objectToSpawn
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___objectToSpawn_6;
	// PlacementIndicator ObjectSpawner::placementIndicator
	PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275 * ___placementIndicator_7;
	// UnityEngine.GameObject ObjectSpawner::ContentCont
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ContentCont_8;
	// System.String ObjectSpawner::RedeemID1
	String_t* ___RedeemID1_9;
	// UnityEngine.GameObject ObjectSpawner::DownloadingText
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___DownloadingText_10;
	// System.String ObjectSpawner::RedeemID2
	String_t* ___RedeemID2_11;
	// UnityEngine.GameObject ObjectSpawner::RedeemRefContainer
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___RedeemRefContainer_12;
	// System.Boolean ObjectSpawner::RedeemCaseCalled
	bool ___RedeemCaseCalled_13;
	// System.Boolean ObjectSpawner::AvatarCaseCalled
	bool ___AvatarCaseCalled_14;
	// System.String ObjectSpawner::_ReturnID
	String_t* ____ReturnID_15;
	// System.Boolean ObjectSpawner::DownloadedFlag
	bool ___DownloadedFlag_16;
	// System.Int32 ObjectSpawner::MaterialSize
	int32_t ___MaterialSize_17;
	// UnityEngine.Material[] ObjectSpawner::_Materials
	MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* ____Materials_18;
	// UnityEngine.Shader ObjectSpawner::_MyShadowShader
	Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * ____MyShadowShader_19;
	// System.String[] ObjectSpawner::OfflineModelID
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___OfflineModelID_20;
	// UnityEngine.GameObject[] ObjectSpawner::OfflineModel
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___OfflineModel_21;
	// System.Boolean ObjectSpawner::ShadowOnOffFlag
	bool ___ShadowOnOffFlag_22;
	// UnityEngine.GameObject ObjectSpawner::TapToSpawn
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TapToSpawn_23;
	// System.String ObjectSpawner::_characterReturn
	String_t* ____characterReturn_24;
	// System.String ObjectSpawner::_animationReturn
	String_t* ____animationReturn_25;
	// System.Int32 ObjectSpawner::_animationReturnInt
	int32_t ____animationReturnInt_26;
	// UnityEngine.GameObject ObjectSpawner::MyDirectionalLight
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___MyDirectionalLight_27;
	// UnityEngine.GameObject ObjectSpawner::_ShadowSwitch
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____ShadowSwitch_28;
	// UnityEngine.GameObject ObjectSpawner::MovementToggle
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___MovementToggle_29;
	// System.Boolean ObjectSpawner::vertMovement
	bool ___vertMovement_30;

public:
	inline static int32_t get_offset_of_CloseIconPlaceHolder_4() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___CloseIconPlaceHolder_4)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_CloseIconPlaceHolder_4() const { return ___CloseIconPlaceHolder_4; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_CloseIconPlaceHolder_4() { return &___CloseIconPlaceHolder_4; }
	inline void set_CloseIconPlaceHolder_4(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___CloseIconPlaceHolder_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CloseIconPlaceHolder_4), (void*)value);
	}

	inline static int32_t get_offset_of_CartIconPlaceHolder_5() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___CartIconPlaceHolder_5)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_CartIconPlaceHolder_5() const { return ___CartIconPlaceHolder_5; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_CartIconPlaceHolder_5() { return &___CartIconPlaceHolder_5; }
	inline void set_CartIconPlaceHolder_5(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___CartIconPlaceHolder_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CartIconPlaceHolder_5), (void*)value);
	}

	inline static int32_t get_offset_of_objectToSpawn_6() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___objectToSpawn_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_objectToSpawn_6() const { return ___objectToSpawn_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_objectToSpawn_6() { return &___objectToSpawn_6; }
	inline void set_objectToSpawn_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___objectToSpawn_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectToSpawn_6), (void*)value);
	}

	inline static int32_t get_offset_of_placementIndicator_7() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___placementIndicator_7)); }
	inline PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275 * get_placementIndicator_7() const { return ___placementIndicator_7; }
	inline PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275 ** get_address_of_placementIndicator_7() { return &___placementIndicator_7; }
	inline void set_placementIndicator_7(PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275 * value)
	{
		___placementIndicator_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placementIndicator_7), (void*)value);
	}

	inline static int32_t get_offset_of_ContentCont_8() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___ContentCont_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ContentCont_8() const { return ___ContentCont_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ContentCont_8() { return &___ContentCont_8; }
	inline void set_ContentCont_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ContentCont_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ContentCont_8), (void*)value);
	}

	inline static int32_t get_offset_of_RedeemID1_9() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___RedeemID1_9)); }
	inline String_t* get_RedeemID1_9() const { return ___RedeemID1_9; }
	inline String_t** get_address_of_RedeemID1_9() { return &___RedeemID1_9; }
	inline void set_RedeemID1_9(String_t* value)
	{
		___RedeemID1_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RedeemID1_9), (void*)value);
	}

	inline static int32_t get_offset_of_DownloadingText_10() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___DownloadingText_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_DownloadingText_10() const { return ___DownloadingText_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_DownloadingText_10() { return &___DownloadingText_10; }
	inline void set_DownloadingText_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___DownloadingText_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DownloadingText_10), (void*)value);
	}

	inline static int32_t get_offset_of_RedeemID2_11() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___RedeemID2_11)); }
	inline String_t* get_RedeemID2_11() const { return ___RedeemID2_11; }
	inline String_t** get_address_of_RedeemID2_11() { return &___RedeemID2_11; }
	inline void set_RedeemID2_11(String_t* value)
	{
		___RedeemID2_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RedeemID2_11), (void*)value);
	}

	inline static int32_t get_offset_of_RedeemRefContainer_12() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___RedeemRefContainer_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_RedeemRefContainer_12() const { return ___RedeemRefContainer_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_RedeemRefContainer_12() { return &___RedeemRefContainer_12; }
	inline void set_RedeemRefContainer_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___RedeemRefContainer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RedeemRefContainer_12), (void*)value);
	}

	inline static int32_t get_offset_of_RedeemCaseCalled_13() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___RedeemCaseCalled_13)); }
	inline bool get_RedeemCaseCalled_13() const { return ___RedeemCaseCalled_13; }
	inline bool* get_address_of_RedeemCaseCalled_13() { return &___RedeemCaseCalled_13; }
	inline void set_RedeemCaseCalled_13(bool value)
	{
		___RedeemCaseCalled_13 = value;
	}

	inline static int32_t get_offset_of_AvatarCaseCalled_14() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___AvatarCaseCalled_14)); }
	inline bool get_AvatarCaseCalled_14() const { return ___AvatarCaseCalled_14; }
	inline bool* get_address_of_AvatarCaseCalled_14() { return &___AvatarCaseCalled_14; }
	inline void set_AvatarCaseCalled_14(bool value)
	{
		___AvatarCaseCalled_14 = value;
	}

	inline static int32_t get_offset_of__ReturnID_15() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ____ReturnID_15)); }
	inline String_t* get__ReturnID_15() const { return ____ReturnID_15; }
	inline String_t** get_address_of__ReturnID_15() { return &____ReturnID_15; }
	inline void set__ReturnID_15(String_t* value)
	{
		____ReturnID_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ReturnID_15), (void*)value);
	}

	inline static int32_t get_offset_of_DownloadedFlag_16() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___DownloadedFlag_16)); }
	inline bool get_DownloadedFlag_16() const { return ___DownloadedFlag_16; }
	inline bool* get_address_of_DownloadedFlag_16() { return &___DownloadedFlag_16; }
	inline void set_DownloadedFlag_16(bool value)
	{
		___DownloadedFlag_16 = value;
	}

	inline static int32_t get_offset_of_MaterialSize_17() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___MaterialSize_17)); }
	inline int32_t get_MaterialSize_17() const { return ___MaterialSize_17; }
	inline int32_t* get_address_of_MaterialSize_17() { return &___MaterialSize_17; }
	inline void set_MaterialSize_17(int32_t value)
	{
		___MaterialSize_17 = value;
	}

	inline static int32_t get_offset_of__Materials_18() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ____Materials_18)); }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* get__Materials_18() const { return ____Materials_18; }
	inline MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492** get_address_of__Materials_18() { return &____Materials_18; }
	inline void set__Materials_18(MaterialU5BU5D_t3AE4936F3CA08FB9EE182A935E665EA9CDA5E492* value)
	{
		____Materials_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____Materials_18), (void*)value);
	}

	inline static int32_t get_offset_of__MyShadowShader_19() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ____MyShadowShader_19)); }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * get__MyShadowShader_19() const { return ____MyShadowShader_19; }
	inline Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 ** get_address_of__MyShadowShader_19() { return &____MyShadowShader_19; }
	inline void set__MyShadowShader_19(Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * value)
	{
		____MyShadowShader_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____MyShadowShader_19), (void*)value);
	}

	inline static int32_t get_offset_of_OfflineModelID_20() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___OfflineModelID_20)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_OfflineModelID_20() const { return ___OfflineModelID_20; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_OfflineModelID_20() { return &___OfflineModelID_20; }
	inline void set_OfflineModelID_20(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___OfflineModelID_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OfflineModelID_20), (void*)value);
	}

	inline static int32_t get_offset_of_OfflineModel_21() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___OfflineModel_21)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_OfflineModel_21() const { return ___OfflineModel_21; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_OfflineModel_21() { return &___OfflineModel_21; }
	inline void set_OfflineModel_21(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___OfflineModel_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OfflineModel_21), (void*)value);
	}

	inline static int32_t get_offset_of_ShadowOnOffFlag_22() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___ShadowOnOffFlag_22)); }
	inline bool get_ShadowOnOffFlag_22() const { return ___ShadowOnOffFlag_22; }
	inline bool* get_address_of_ShadowOnOffFlag_22() { return &___ShadowOnOffFlag_22; }
	inline void set_ShadowOnOffFlag_22(bool value)
	{
		___ShadowOnOffFlag_22 = value;
	}

	inline static int32_t get_offset_of_TapToSpawn_23() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___TapToSpawn_23)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TapToSpawn_23() const { return ___TapToSpawn_23; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TapToSpawn_23() { return &___TapToSpawn_23; }
	inline void set_TapToSpawn_23(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TapToSpawn_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TapToSpawn_23), (void*)value);
	}

	inline static int32_t get_offset_of__characterReturn_24() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ____characterReturn_24)); }
	inline String_t* get__characterReturn_24() const { return ____characterReturn_24; }
	inline String_t** get_address_of__characterReturn_24() { return &____characterReturn_24; }
	inline void set__characterReturn_24(String_t* value)
	{
		____characterReturn_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterReturn_24), (void*)value);
	}

	inline static int32_t get_offset_of__animationReturn_25() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ____animationReturn_25)); }
	inline String_t* get__animationReturn_25() const { return ____animationReturn_25; }
	inline String_t** get_address_of__animationReturn_25() { return &____animationReturn_25; }
	inline void set__animationReturn_25(String_t* value)
	{
		____animationReturn_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animationReturn_25), (void*)value);
	}

	inline static int32_t get_offset_of__animationReturnInt_26() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ____animationReturnInt_26)); }
	inline int32_t get__animationReturnInt_26() const { return ____animationReturnInt_26; }
	inline int32_t* get_address_of__animationReturnInt_26() { return &____animationReturnInt_26; }
	inline void set__animationReturnInt_26(int32_t value)
	{
		____animationReturnInt_26 = value;
	}

	inline static int32_t get_offset_of_MyDirectionalLight_27() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___MyDirectionalLight_27)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_MyDirectionalLight_27() const { return ___MyDirectionalLight_27; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_MyDirectionalLight_27() { return &___MyDirectionalLight_27; }
	inline void set_MyDirectionalLight_27(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___MyDirectionalLight_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MyDirectionalLight_27), (void*)value);
	}

	inline static int32_t get_offset_of__ShadowSwitch_28() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ____ShadowSwitch_28)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__ShadowSwitch_28() const { return ____ShadowSwitch_28; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__ShadowSwitch_28() { return &____ShadowSwitch_28; }
	inline void set__ShadowSwitch_28(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____ShadowSwitch_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ShadowSwitch_28), (void*)value);
	}

	inline static int32_t get_offset_of_MovementToggle_29() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___MovementToggle_29)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_MovementToggle_29() const { return ___MovementToggle_29; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_MovementToggle_29() { return &___MovementToggle_29; }
	inline void set_MovementToggle_29(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___MovementToggle_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MovementToggle_29), (void*)value);
	}

	inline static int32_t get_offset_of_vertMovement_30() { return static_cast<int32_t>(offsetof(ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388, ___vertMovement_30)); }
	inline bool get_vertMovement_30() const { return ___vertMovement_30; }
	inline bool* get_address_of_vertMovement_30() { return &___vertMovement_30; }
	inline void set_vertMovement_30(bool value)
	{
		___vertMovement_30 = value;
	}
};


// ParslAvatarController
struct ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.AndroidJavaObject ParslAvatarController::communicationBridge
	AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * ___communicationBridge_4;
	// UnityEngine.GameObject ParslAvatarController::FemaleCharacterPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___FemaleCharacterPanel_5;
	// UnityEngine.GameObject ParslAvatarController::MaleCharacterPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___MaleCharacterPanel_6;
	// UnityEngine.GameObject ParslAvatarController::ChildCharacterPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ChildCharacterPanel_7;
	// UnityEngine.GameObject ParslAvatarController::AnimationPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___AnimationPanel_8;
	// UnityEngine.GameObject ParslAvatarController::TapToLoad
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TapToLoad_9;
	// UnityEngine.GameObject[] ParslAvatarController::Characters
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___Characters_10;
	// UnityEngine.GameObject ParslAvatarController::ConfirmationPanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ConfirmationPanel_11;
	// System.Int32 ParslAvatarController::SelectedModel
	int32_t ___SelectedModel_12;
	// System.Int32 ParslAvatarController::CharacterNumber
	int32_t ___CharacterNumber_13;
	// System.Int32 ParslAvatarController::AnimationNumber
	int32_t ___AnimationNumber_14;

public:
	inline static int32_t get_offset_of_communicationBridge_4() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___communicationBridge_4)); }
	inline AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * get_communicationBridge_4() const { return ___communicationBridge_4; }
	inline AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E ** get_address_of_communicationBridge_4() { return &___communicationBridge_4; }
	inline void set_communicationBridge_4(AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * value)
	{
		___communicationBridge_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___communicationBridge_4), (void*)value);
	}

	inline static int32_t get_offset_of_FemaleCharacterPanel_5() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___FemaleCharacterPanel_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_FemaleCharacterPanel_5() const { return ___FemaleCharacterPanel_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_FemaleCharacterPanel_5() { return &___FemaleCharacterPanel_5; }
	inline void set_FemaleCharacterPanel_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___FemaleCharacterPanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FemaleCharacterPanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_MaleCharacterPanel_6() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___MaleCharacterPanel_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_MaleCharacterPanel_6() const { return ___MaleCharacterPanel_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_MaleCharacterPanel_6() { return &___MaleCharacterPanel_6; }
	inline void set_MaleCharacterPanel_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___MaleCharacterPanel_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MaleCharacterPanel_6), (void*)value);
	}

	inline static int32_t get_offset_of_ChildCharacterPanel_7() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___ChildCharacterPanel_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ChildCharacterPanel_7() const { return ___ChildCharacterPanel_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ChildCharacterPanel_7() { return &___ChildCharacterPanel_7; }
	inline void set_ChildCharacterPanel_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ChildCharacterPanel_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ChildCharacterPanel_7), (void*)value);
	}

	inline static int32_t get_offset_of_AnimationPanel_8() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___AnimationPanel_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_AnimationPanel_8() const { return ___AnimationPanel_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_AnimationPanel_8() { return &___AnimationPanel_8; }
	inline void set_AnimationPanel_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___AnimationPanel_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AnimationPanel_8), (void*)value);
	}

	inline static int32_t get_offset_of_TapToLoad_9() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___TapToLoad_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TapToLoad_9() const { return ___TapToLoad_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TapToLoad_9() { return &___TapToLoad_9; }
	inline void set_TapToLoad_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TapToLoad_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TapToLoad_9), (void*)value);
	}

	inline static int32_t get_offset_of_Characters_10() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___Characters_10)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_Characters_10() const { return ___Characters_10; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_Characters_10() { return &___Characters_10; }
	inline void set_Characters_10(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___Characters_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Characters_10), (void*)value);
	}

	inline static int32_t get_offset_of_ConfirmationPanel_11() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___ConfirmationPanel_11)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ConfirmationPanel_11() const { return ___ConfirmationPanel_11; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ConfirmationPanel_11() { return &___ConfirmationPanel_11; }
	inline void set_ConfirmationPanel_11(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ConfirmationPanel_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConfirmationPanel_11), (void*)value);
	}

	inline static int32_t get_offset_of_SelectedModel_12() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___SelectedModel_12)); }
	inline int32_t get_SelectedModel_12() const { return ___SelectedModel_12; }
	inline int32_t* get_address_of_SelectedModel_12() { return &___SelectedModel_12; }
	inline void set_SelectedModel_12(int32_t value)
	{
		___SelectedModel_12 = value;
	}

	inline static int32_t get_offset_of_CharacterNumber_13() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___CharacterNumber_13)); }
	inline int32_t get_CharacterNumber_13() const { return ___CharacterNumber_13; }
	inline int32_t* get_address_of_CharacterNumber_13() { return &___CharacterNumber_13; }
	inline void set_CharacterNumber_13(int32_t value)
	{
		___CharacterNumber_13 = value;
	}

	inline static int32_t get_offset_of_AnimationNumber_14() { return static_cast<int32_t>(offsetof(ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3, ___AnimationNumber_14)); }
	inline int32_t get_AnimationNumber_14() const { return ___AnimationNumber_14; }
	inline int32_t* get_address_of_AnimationNumber_14() { return &___AnimationNumber_14; }
	inline void set_AnimationNumber_14(int32_t value)
	{
		___AnimationNumber_14 = value;
	}
};


// PlaceContent
struct PlaceContent_t7015C9D0A2924D8469A9BFCCA4E0DC337FAF2299  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.XR.ARFoundation.ARRaycastManager PlaceContent::raycastManager
	ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * ___raycastManager_4;
	// UnityEngine.UI.GraphicRaycaster PlaceContent::raycaster
	GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * ___raycaster_5;

public:
	inline static int32_t get_offset_of_raycastManager_4() { return static_cast<int32_t>(offsetof(PlaceContent_t7015C9D0A2924D8469A9BFCCA4E0DC337FAF2299, ___raycastManager_4)); }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * get_raycastManager_4() const { return ___raycastManager_4; }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F ** get_address_of_raycastManager_4() { return &___raycastManager_4; }
	inline void set_raycastManager_4(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * value)
	{
		___raycastManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycastManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_raycaster_5() { return static_cast<int32_t>(offsetof(PlaceContent_t7015C9D0A2924D8469A9BFCCA4E0DC337FAF2299, ___raycaster_5)); }
	inline GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * get_raycaster_5() const { return ___raycaster_5; }
	inline GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 ** get_address_of_raycaster_5() { return &___raycaster_5; }
	inline void set_raycaster_5(GraphicRaycaster_tD6DFF30B8B7F1E0DA9522A4F2BB9DC18E19638E6 * value)
	{
		___raycaster_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___raycaster_5), (void*)value);
	}
};


// PlacementIndicator
struct PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.XR.ARFoundation.ARRaycastManager PlacementIndicator::rayManager
	ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * ___rayManager_4;
	// UnityEngine.GameObject PlacementIndicator::visual
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___visual_5;

public:
	inline static int32_t get_offset_of_rayManager_4() { return static_cast<int32_t>(offsetof(PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275, ___rayManager_4)); }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * get_rayManager_4() const { return ___rayManager_4; }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F ** get_address_of_rayManager_4() { return &___rayManager_4; }
	inline void set_rayManager_4(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * value)
	{
		___rayManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rayManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_visual_5() { return static_cast<int32_t>(offsetof(PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275, ___visual_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_visual_5() const { return ___visual_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_visual_5() { return &___visual_5; }
	inline void set_visual_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___visual_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___visual_5), (void*)value);
	}
};


// PlacementObject
struct PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean PlacementObject::IsSelected
	bool ___IsSelected_4;
	// System.Boolean PlacementObject::IsLocked
	bool ___IsLocked_5;
	// TMPro.TextMeshPro PlacementObject::OverlayText
	TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * ___OverlayText_6;
	// UnityEngine.Canvas PlacementObject::canvasComponent
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___canvasComponent_7;
	// System.String PlacementObject::OverlayDisplayText
	String_t* ___OverlayDisplayText_8;

public:
	inline static int32_t get_offset_of_IsSelected_4() { return static_cast<int32_t>(offsetof(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7, ___IsSelected_4)); }
	inline bool get_IsSelected_4() const { return ___IsSelected_4; }
	inline bool* get_address_of_IsSelected_4() { return &___IsSelected_4; }
	inline void set_IsSelected_4(bool value)
	{
		___IsSelected_4 = value;
	}

	inline static int32_t get_offset_of_IsLocked_5() { return static_cast<int32_t>(offsetof(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7, ___IsLocked_5)); }
	inline bool get_IsLocked_5() const { return ___IsLocked_5; }
	inline bool* get_address_of_IsLocked_5() { return &___IsLocked_5; }
	inline void set_IsLocked_5(bool value)
	{
		___IsLocked_5 = value;
	}

	inline static int32_t get_offset_of_OverlayText_6() { return static_cast<int32_t>(offsetof(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7, ___OverlayText_6)); }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * get_OverlayText_6() const { return ___OverlayText_6; }
	inline TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 ** get_address_of_OverlayText_6() { return &___OverlayText_6; }
	inline void set_OverlayText_6(TextMeshPro_t4C8C961C0939CD311CCC4F5F306C27C5301BD8E4 * value)
	{
		___OverlayText_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OverlayText_6), (void*)value);
	}

	inline static int32_t get_offset_of_canvasComponent_7() { return static_cast<int32_t>(offsetof(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7, ___canvasComponent_7)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_canvasComponent_7() const { return ___canvasComponent_7; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_canvasComponent_7() { return &___canvasComponent_7; }
	inline void set_canvasComponent_7(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___canvasComponent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___canvasComponent_7), (void*)value);
	}

	inline static int32_t get_offset_of_OverlayDisplayText_8() { return static_cast<int32_t>(offsetof(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7, ___OverlayDisplayText_8)); }
	inline String_t* get_OverlayDisplayText_8() const { return ___OverlayDisplayText_8; }
	inline String_t** get_address_of_OverlayDisplayText_8() { return &___OverlayDisplayText_8; }
	inline void set_OverlayDisplayText_8(String_t* value)
	{
		___OverlayDisplayText_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OverlayDisplayText_8), (void*)value);
	}
};


// PlacementWithMultipleDraggingDroppingController
struct PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject PlacementWithMultipleDraggingDroppingController::placedPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___placedPrefab_4;
	// UnityEngine.GameObject PlacementWithMultipleDraggingDroppingController::welcomePanel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___welcomePanel_5;
	// UnityEngine.UI.Button PlacementWithMultipleDraggingDroppingController::dismissButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___dismissButton_6;
	// UnityEngine.Camera PlacementWithMultipleDraggingDroppingController::arCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___arCamera_7;
	// PlacementObject[] PlacementWithMultipleDraggingDroppingController::placedObjects
	PlacementObjectU5BU5D_t4FA713DF94452422EB8FD49580571D344722A54E* ___placedObjects_8;
	// UnityEngine.Vector2 PlacementWithMultipleDraggingDroppingController::touchPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___touchPosition_9;
	// UnityEngine.XR.ARFoundation.ARRaycastManager PlacementWithMultipleDraggingDroppingController::arRaycastManager
	ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * ___arRaycastManager_10;
	// System.Boolean PlacementWithMultipleDraggingDroppingController::onTouchHold
	bool ___onTouchHold_11;
	// PlacementObject PlacementWithMultipleDraggingDroppingController::lastSelectedObject
	PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7 * ___lastSelectedObject_13;
	// UnityEngine.UI.Button PlacementWithMultipleDraggingDroppingController::redButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___redButton_14;
	// UnityEngine.UI.Button PlacementWithMultipleDraggingDroppingController::greenButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___greenButton_15;
	// UnityEngine.UI.Button PlacementWithMultipleDraggingDroppingController::blueButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___blueButton_16;

public:
	inline static int32_t get_offset_of_placedPrefab_4() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___placedPrefab_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_placedPrefab_4() const { return ___placedPrefab_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_placedPrefab_4() { return &___placedPrefab_4; }
	inline void set_placedPrefab_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___placedPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placedPrefab_4), (void*)value);
	}

	inline static int32_t get_offset_of_welcomePanel_5() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___welcomePanel_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_welcomePanel_5() const { return ___welcomePanel_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_welcomePanel_5() { return &___welcomePanel_5; }
	inline void set_welcomePanel_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___welcomePanel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___welcomePanel_5), (void*)value);
	}

	inline static int32_t get_offset_of_dismissButton_6() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___dismissButton_6)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_dismissButton_6() const { return ___dismissButton_6; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_dismissButton_6() { return &___dismissButton_6; }
	inline void set_dismissButton_6(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___dismissButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dismissButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_arCamera_7() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___arCamera_7)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_arCamera_7() const { return ___arCamera_7; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_arCamera_7() { return &___arCamera_7; }
	inline void set_arCamera_7(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___arCamera_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arCamera_7), (void*)value);
	}

	inline static int32_t get_offset_of_placedObjects_8() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___placedObjects_8)); }
	inline PlacementObjectU5BU5D_t4FA713DF94452422EB8FD49580571D344722A54E* get_placedObjects_8() const { return ___placedObjects_8; }
	inline PlacementObjectU5BU5D_t4FA713DF94452422EB8FD49580571D344722A54E** get_address_of_placedObjects_8() { return &___placedObjects_8; }
	inline void set_placedObjects_8(PlacementObjectU5BU5D_t4FA713DF94452422EB8FD49580571D344722A54E* value)
	{
		___placedObjects_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___placedObjects_8), (void*)value);
	}

	inline static int32_t get_offset_of_touchPosition_9() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___touchPosition_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_touchPosition_9() const { return ___touchPosition_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_touchPosition_9() { return &___touchPosition_9; }
	inline void set_touchPosition_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___touchPosition_9 = value;
	}

	inline static int32_t get_offset_of_arRaycastManager_10() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___arRaycastManager_10)); }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * get_arRaycastManager_10() const { return ___arRaycastManager_10; }
	inline ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F ** get_address_of_arRaycastManager_10() { return &___arRaycastManager_10; }
	inline void set_arRaycastManager_10(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F * value)
	{
		___arRaycastManager_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___arRaycastManager_10), (void*)value);
	}

	inline static int32_t get_offset_of_onTouchHold_11() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___onTouchHold_11)); }
	inline bool get_onTouchHold_11() const { return ___onTouchHold_11; }
	inline bool* get_address_of_onTouchHold_11() { return &___onTouchHold_11; }
	inline void set_onTouchHold_11(bool value)
	{
		___onTouchHold_11 = value;
	}

	inline static int32_t get_offset_of_lastSelectedObject_13() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___lastSelectedObject_13)); }
	inline PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7 * get_lastSelectedObject_13() const { return ___lastSelectedObject_13; }
	inline PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7 ** get_address_of_lastSelectedObject_13() { return &___lastSelectedObject_13; }
	inline void set_lastSelectedObject_13(PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7 * value)
	{
		___lastSelectedObject_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lastSelectedObject_13), (void*)value);
	}

	inline static int32_t get_offset_of_redButton_14() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___redButton_14)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_redButton_14() const { return ___redButton_14; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_redButton_14() { return &___redButton_14; }
	inline void set_redButton_14(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___redButton_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___redButton_14), (void*)value);
	}

	inline static int32_t get_offset_of_greenButton_15() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___greenButton_15)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_greenButton_15() const { return ___greenButton_15; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_greenButton_15() { return &___greenButton_15; }
	inline void set_greenButton_15(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___greenButton_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___greenButton_15), (void*)value);
	}

	inline static int32_t get_offset_of_blueButton_16() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9, ___blueButton_16)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_blueButton_16() const { return ___blueButton_16; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_blueButton_16() { return &___blueButton_16; }
	inline void set_blueButton_16(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___blueButton_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blueButton_16), (void*)value);
	}
};

struct PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit> PlacementWithMultipleDraggingDroppingController::hits
	List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * ___hits_12;

public:
	inline static int32_t get_offset_of_hits_12() { return static_cast<int32_t>(offsetof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_StaticFields, ___hits_12)); }
	inline List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * get_hits_12() const { return ___hits_12; }
	inline List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D ** get_address_of_hits_12() { return &___hits_12; }
	inline void set_hits_12(List_1_tDA68EC1B5CE9809C8709C1E58A7D0F4ACBB1252D * value)
	{
		___hits_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hits_12), (void*)value);
	}
};


// ReturnCartList
struct ReturnCartList_tED3BCB6B9F26337331DB4F94802112BC76F9FB02  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// ShowCanvasOnBuildingModels
struct ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject ShowCanvasOnBuildingModels::ARPanelForBuildingModels
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ARPanelForBuildingModels_4;
	// System.Boolean ShowCanvasOnBuildingModels::flag
	bool ___flag_5;

public:
	inline static int32_t get_offset_of_ARPanelForBuildingModels_4() { return static_cast<int32_t>(offsetof(ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA, ___ARPanelForBuildingModels_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ARPanelForBuildingModels_4() const { return ___ARPanelForBuildingModels_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ARPanelForBuildingModels_4() { return &___ARPanelForBuildingModels_4; }
	inline void set_ARPanelForBuildingModels_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ARPanelForBuildingModels_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ARPanelForBuildingModels_4), (void*)value);
	}

	inline static int32_t get_offset_of_flag_5() { return static_cast<int32_t>(offsetof(ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA, ___flag_5)); }
	inline bool get_flag_5() const { return ___flag_5; }
	inline bool* get_address_of_flag_5() { return &___flag_5; }
	inline void set_flag_5(bool value)
	{
		___flag_5 = value;
	}
};


// SwitchPointCloudVisualizationMode
struct SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Button SwitchPointCloudVisualizationMode::m_ToggleButton
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___m_ToggleButton_4;
	// UnityEngine.UI.Text SwitchPointCloudVisualizationMode::m_Log
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_Log_5;
	// UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode SwitchPointCloudVisualizationMode::m_Mode
	int32_t ___m_Mode_6;
	// System.Text.StringBuilder SwitchPointCloudVisualizationMode::m_StringBuilder
	StringBuilder_t * ___m_StringBuilder_7;

public:
	inline static int32_t get_offset_of_m_ToggleButton_4() { return static_cast<int32_t>(offsetof(SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE, ___m_ToggleButton_4)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_m_ToggleButton_4() const { return ___m_ToggleButton_4; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_m_ToggleButton_4() { return &___m_ToggleButton_4; }
	inline void set_m_ToggleButton_4(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___m_ToggleButton_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ToggleButton_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Log_5() { return static_cast<int32_t>(offsetof(SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE, ___m_Log_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_Log_5() const { return ___m_Log_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_Log_5() { return &___m_Log_5; }
	inline void set_m_Log_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_Log_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Log_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Mode_6() { return static_cast<int32_t>(offsetof(SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE, ___m_Mode_6)); }
	inline int32_t get_m_Mode_6() const { return ___m_Mode_6; }
	inline int32_t* get_address_of_m_Mode_6() { return &___m_Mode_6; }
	inline void set_m_Mode_6(int32_t value)
	{
		___m_Mode_6 = value;
	}

	inline static int32_t get_offset_of_m_StringBuilder_7() { return static_cast<int32_t>(offsetof(SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE, ___m_StringBuilder_7)); }
	inline StringBuilder_t * get_m_StringBuilder_7() const { return ___m_StringBuilder_7; }
	inline StringBuilder_t ** get_address_of_m_StringBuilder_7() { return &___m_StringBuilder_7; }
	inline void set_m_StringBuilder_7(StringBuilder_t * value)
	{
		___m_StringBuilder_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringBuilder_7), (void*)value);
	}
};


// ToggleAR
struct ToggleAR_t859D7D0F694C4107E234070CB221F9472D0BC2F7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.XR.ARFoundation.ARPlaneManager ToggleAR::planeManager
	ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4 * ___planeManager_4;
	// UnityEngine.XR.ARFoundation.ARPointCloudManager ToggleAR::pointCloudManager
	ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF * ___pointCloudManager_5;

public:
	inline static int32_t get_offset_of_planeManager_4() { return static_cast<int32_t>(offsetof(ToggleAR_t859D7D0F694C4107E234070CB221F9472D0BC2F7, ___planeManager_4)); }
	inline ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4 * get_planeManager_4() const { return ___planeManager_4; }
	inline ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4 ** get_address_of_planeManager_4() { return &___planeManager_4; }
	inline void set_planeManager_4(ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4 * value)
	{
		___planeManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___planeManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_pointCloudManager_5() { return static_cast<int32_t>(offsetof(ToggleAR_t859D7D0F694C4107E234070CB221F9472D0BC2F7, ___pointCloudManager_5)); }
	inline ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF * get_pointCloudManager_5() const { return ___pointCloudManager_5; }
	inline ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF ** get_address_of_pointCloudManager_5() { return &___pointCloudManager_5; }
	inline void set_pointCloudManager_5(ARPointCloudManager_tFB5917457B296992E01D1DB28761170B075ABADF * value)
	{
		___pointCloudManager_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pointCloudManager_5), (void*)value);
	}
};


// UnityEngine.XR.ARSubsystems.XRSessionSubsystem
struct XRSessionSubsystem_t8AD3C01568AA19BF038D23A6031FF9814CAF93CD  : public SubsystemWithProvider_3_t646DFCE31181130FB557E4AFA37198CF3170977F
{
public:
	// System.Nullable`1<UnityEngine.XR.ARSubsystems.Configuration> UnityEngine.XR.ARSubsystems.XRSessionSubsystem::<currentConfiguration>k__BackingField
	Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494  ___U3CcurrentConfigurationU3Ek__BackingField_4;
	// UnityEngine.XR.ARSubsystems.ConfigurationChooser UnityEngine.XR.ARSubsystems.XRSessionSubsystem::m_DefaultConfigurationChooser
	ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * ___m_DefaultConfigurationChooser_5;
	// UnityEngine.XR.ARSubsystems.ConfigurationChooser UnityEngine.XR.ARSubsystems.XRSessionSubsystem::m_ConfigurationChooser
	ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * ___m_ConfigurationChooser_6;

public:
	inline static int32_t get_offset_of_U3CcurrentConfigurationU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t8AD3C01568AA19BF038D23A6031FF9814CAF93CD, ___U3CcurrentConfigurationU3Ek__BackingField_4)); }
	inline Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494  get_U3CcurrentConfigurationU3Ek__BackingField_4() const { return ___U3CcurrentConfigurationU3Ek__BackingField_4; }
	inline Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494 * get_address_of_U3CcurrentConfigurationU3Ek__BackingField_4() { return &___U3CcurrentConfigurationU3Ek__BackingField_4; }
	inline void set_U3CcurrentConfigurationU3Ek__BackingField_4(Nullable_1_t0FF36C2ABCA6430FFCD4ED32922F18F36382E494  value)
	{
		___U3CcurrentConfigurationU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_m_DefaultConfigurationChooser_5() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t8AD3C01568AA19BF038D23A6031FF9814CAF93CD, ___m_DefaultConfigurationChooser_5)); }
	inline ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * get_m_DefaultConfigurationChooser_5() const { return ___m_DefaultConfigurationChooser_5; }
	inline ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 ** get_address_of_m_DefaultConfigurationChooser_5() { return &___m_DefaultConfigurationChooser_5; }
	inline void set_m_DefaultConfigurationChooser_5(ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * value)
	{
		___m_DefaultConfigurationChooser_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DefaultConfigurationChooser_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ConfigurationChooser_6() { return static_cast<int32_t>(offsetof(XRSessionSubsystem_t8AD3C01568AA19BF038D23A6031FF9814CAF93CD, ___m_ConfigurationChooser_6)); }
	inline ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * get_m_ConfigurationChooser_6() const { return ___m_ConfigurationChooser_6; }
	inline ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 ** get_address_of_m_ConfigurationChooser_6() { return &___m_ConfigurationChooser_6; }
	inline void set_m_ConfigurationChooser_6(ConfigurationChooser_t0CCF856A226297A702F306A2217CF17D652E72C4 * value)
	{
		___m_ConfigurationChooser_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ConfigurationChooser_6), (void*)value);
	}
};


// onClickForScaling
struct onClickForScaling_t4E6FED225317AC601426A8B8C57BDE93374439B8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// rotateController
struct rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single rotateController::_sensitivity
	float ____sensitivity_4;
	// UnityEngine.Vector3 rotateController::_mouseReference
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____mouseReference_5;
	// UnityEngine.Vector3 rotateController::_mouseOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____mouseOffset_6;
	// UnityEngine.Vector3 rotateController::_rotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____rotation_7;
	// System.Boolean rotateController::_isRotating
	bool ____isRotating_8;

public:
	inline static int32_t get_offset_of__sensitivity_4() { return static_cast<int32_t>(offsetof(rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24, ____sensitivity_4)); }
	inline float get__sensitivity_4() const { return ____sensitivity_4; }
	inline float* get_address_of__sensitivity_4() { return &____sensitivity_4; }
	inline void set__sensitivity_4(float value)
	{
		____sensitivity_4 = value;
	}

	inline static int32_t get_offset_of__mouseReference_5() { return static_cast<int32_t>(offsetof(rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24, ____mouseReference_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__mouseReference_5() const { return ____mouseReference_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__mouseReference_5() { return &____mouseReference_5; }
	inline void set__mouseReference_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____mouseReference_5 = value;
	}

	inline static int32_t get_offset_of__mouseOffset_6() { return static_cast<int32_t>(offsetof(rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24, ____mouseOffset_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__mouseOffset_6() const { return ____mouseOffset_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__mouseOffset_6() { return &____mouseOffset_6; }
	inline void set__mouseOffset_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____mouseOffset_6 = value;
	}

	inline static int32_t get_offset_of__rotation_7() { return static_cast<int32_t>(offsetof(rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24, ____rotation_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__rotation_7() const { return ____rotation_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__rotation_7() { return &____rotation_7; }
	inline void set__rotation_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____rotation_7 = value;
	}

	inline static int32_t get_offset_of__isRotating_8() { return static_cast<int32_t>(offsetof(rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24, ____isRotating_8)); }
	inline bool get__isRotating_8() const { return ____isRotating_8; }
	inline bool* get_address_of__isRotating_8() { return &____isRotating_8; }
	inline void set__isRotating_8(bool value)
	{
		____isRotating_8 = value;
	}
};


// UnityEngine.XR.ARKit.ARKitSessionSubsystem
struct ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216  : public XRSessionSubsystem_t8AD3C01568AA19BF038D23A6031FF9814CAF93CD
{
public:
	// UnityEngine.XR.ARKit.ARKitSessionDelegate UnityEngine.XR.ARKit.ARKitSessionSubsystem::<sessionDelegate>k__BackingField
	ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE * ___U3CsessionDelegateU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CsessionDelegateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216, ___U3CsessionDelegateU3Ek__BackingField_7)); }
	inline ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE * get_U3CsessionDelegateU3Ek__BackingField_7() const { return ___U3CsessionDelegateU3Ek__BackingField_7; }
	inline ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE ** get_address_of_U3CsessionDelegateU3Ek__BackingField_7() { return &___U3CsessionDelegateU3Ek__BackingField_7; }
	inline void set_U3CsessionDelegateU3Ek__BackingField_7(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE * value)
	{
		___U3CsessionDelegateU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsessionDelegateU3Ek__BackingField_7), (void*)value);
	}
};

struct ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216_StaticFields
{
public:
	// UnityEngine.XR.ARKit.ARKitSessionSubsystem/NativeApi/OnAsyncConversionCompleteDelegate UnityEngine.XR.ARKit.ARKitSessionSubsystem::s_OnAsyncWorldMapCompleted
	OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF * ___s_OnAsyncWorldMapCompleted_8;

public:
	inline static int32_t get_offset_of_s_OnAsyncWorldMapCompleted_8() { return static_cast<int32_t>(offsetof(ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216_StaticFields, ___s_OnAsyncWorldMapCompleted_8)); }
	inline OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF * get_s_OnAsyncWorldMapCompleted_8() const { return ___s_OnAsyncWorldMapCompleted_8; }
	inline OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF ** get_address_of_s_OnAsyncWorldMapCompleted_8() { return &___s_OnAsyncWorldMapCompleted_8; }
	inline void set_s_OnAsyncWorldMapCompleted_8(OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF * value)
	{
		___s_OnAsyncWorldMapCompleted_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_OnAsyncWorldMapCompleted_8), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4530;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4530 = { sizeof (UnityWebRequestMethod_tF538D9A75B76FFC81710E65697E38C1B12E4F7E5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4531;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4531 = { sizeof (UnityWebRequestError_t01C779C192877A58EBDB44371C42F9A5831EB9F6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4532;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4532 = { sizeof (Result_t3233C0F690EC3844C8E0C4649568659679AFBE75)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4533;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4533 = { sizeof (UnityWebRequest_tB75B39F6951CA0DBA2D5BEDF85FDCAAC6026A37E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4534;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4534 = { sizeof (CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E), sizeof(CertificateHandler_tDA66C86D1302CE4266DBB078361F7A363C7B005E_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4535;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4535 = { sizeof (DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB), sizeof(DownloadHandler_tEEAE0DD53DB497C8A491C4F7B7A14C3CA027B1DB_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4536;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4536 = { sizeof (DownloadHandlerBuffer_t74D11E891308B7FD5255C8D0D876AD0DBF512B6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4537;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4537 = { sizeof (UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA), sizeof(UploadHandler_t5F80A2A6874D4D330751BE3524009C21C9B74BDA_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4538;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4538 = { sizeof (U3CModuleU3E_t9BC900AC115CCA160074141915B355D8F694FB1F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4539;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4539 = { sizeof (VFXEventAttribute_tC4E90458100D52776F591CE62B19FF6051F423EF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4540;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4540 = { sizeof (VFXExpressionValues_tFB46D1CD053E9CD5BD04CBE4DB1B0ED24C9C0883), sizeof(VFXExpressionValues_tFB46D1CD053E9CD5BD04CBE4DB1B0ED24C9C0883_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4541;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4541 = { sizeof (VFXManager_tE525803FFE4F59D87D5B5E61AF8433037F226340), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4542;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4542 = { sizeof (VFXSpawnerCallbacks_t62128B7E3ADA64EBEA4705691DE0F045104801CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4543;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4543 = { sizeof (VFXSpawnerState_t5879CC401019E9C9D4F81128147AE52AAED167CD), sizeof(VFXSpawnerState_t5879CC401019E9C9D4F81128147AE52AAED167CD_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4544;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4544 = { sizeof (VisualEffectObject_tC7804AFDC2B4F2F0CE6833AC467ABC177A1617DB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545 = { sizeof (VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50), -1, sizeof(VisualEffectAsset_tEFF95BDCD904AF7D5DEA8CF020C62E23A978EC50_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546 = { sizeof (VFXOutputEventArgs_tE7E97EDFD67E4561E4412D2E4B1C999F95850BF5)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547 = { sizeof (VisualEffect_t7C6E2AAA4DB4F47960AF2029EA96D4B579B3A4CA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548 = { sizeof (U3CModuleU3E_t7757219A6D4DF3F0E2950E860119AEA621C68AF1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549 = { sizeof (VideoClipPlayable_tC49201F6C8E1AB1CC8F4E31EFC12C7E1C03BC2E1)+ sizeof (RuntimeObject), sizeof(VideoClipPlayable_tC49201F6C8E1AB1CC8F4E31EFC12C7E1C03BC2E1 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550 = { sizeof (VideoClip_tA8C2507553BEE394C46B7A876D6F56DD09F6C90F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551 = { sizeof (VideoRenderMode_tB2F8E98B2EBB3216E6322E55C246CE0587CC0A7B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552 = { sizeof (Video3DLayout_t128A1265A65BE3B41138D19C5A827986A2F22F45)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553 = { sizeof (VideoAspectRatio_tB3C11859B0FA98E77D62BE7E1BD59084E7919B5E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554 = { sizeof (VideoTimeSource_t881900D70589FDDD1C7471CB8C7FEA132B98038F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555 = { sizeof (VideoTimeReference_tDF02822B01320D3B0ADBE75452C8FA6B5FE96F1E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556 = { sizeof (VideoSource_t66E8298534E5BB7DFD28A7D8ADE397E328CD8896)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557 = { sizeof (VideoAudioOutputMode_tDD6B846B9A65F1C53DA4D4D8117CDB223BE3DE56)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558 = { sizeof (EventHandler_t99288A74FAB288C0033E28A5CD3DABE77B109BFD), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559 = { sizeof (ErrorEventHandler_tD47781EBB7CF0CC4C111496024BD59B1D1A6A1F2), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560 = { sizeof (FrameReadyEventHandler_t9529BD5A34E9C8BE7D8A39D46A6C4ABC673374EC), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561 = { sizeof (TimeEventHandler_t7CA131EB85E0FFCBE8660E030698BD83D3994DD8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562 = { sizeof (VideoPlayer_t47DCC396CBA28512CF97C6CC4F55878E8D62FE86), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563 = { sizeof (U3CModuleU3E_tEB1A1F441D3E4E8A35F3B9EE4147916810B1DD3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564 = { sizeof (EmbeddedAttribute_t05629E57A0E96598455481383D6D826715328F7C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565 = { sizeof (IsReadOnlyAttribute_t49E064784AB175290B77B6E6137930BE53B124B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566 = { sizeof (ARCoachingGoal_t1B0E9451A645B81235667827947BC3196ED1A6AC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567 = { sizeof (ARCoachingOverlayTransition_tFCB8AFB8BA3D335E98404B8BEE0433F8FCE84449)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568 = { sizeof (ARCollaborationData_t46F3E05335DC9EDF98A04CF88AE6394540B9D8DB)+ sizeof (RuntimeObject), sizeof(ARCollaborationData_t46F3E05335DC9EDF98A04CF88AE6394540B9D8DB ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569 = { sizeof (ARCollaborationDataBuilder_t292F9D3CA1D26159C77A9C69F8E0FD2CB298F293)+ sizeof (RuntimeObject), sizeof(ARCollaborationDataBuilder_t292F9D3CA1D26159C77A9C69F8E0FD2CB298F293 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570 = { sizeof (ARCollaborationDataPriority_t49B334CCC35BF3BB49565E91A2241B10AEABAC6B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { sizeof (ARKitProvider_tE10DC6803776ECE42E55F127E68C47E50319202E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { sizeof (ARKitAnchorSubsystem_t721F48C4B69507D7E38664A68AA42DD1DF75B3DF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (Api_tA818DF9363CED72D41D212D7B479C32A793F1D9C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (CameraConfigurationResult_t88B2CEBCA2C7C5310468B805A6C738A5ED5D04E9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252), -1, sizeof(ARKitProvider_tA94DC507BD799F0EB939B5D43E47850858DD9252_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (NativeApi_tA8D79572A35E338F74C186C460AA1780DD18222A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { sizeof (ARKitCameraSubsystem_tE327D2A1BE7A0BD90A9353C8EDBDE9A882315245), -1, sizeof(ARKitCameraSubsystem_tE327D2A1BE7A0BD90A9353C8EDBDE9A882315245_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { sizeof (ImageType_tA6813318BDBCD6CA15A1A64D300FD2A32911CEE4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (Native_tF04A9C18AA77B29062CE0428C310D081D0D35E77), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { sizeof (ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59), -1, sizeof(ARKitCpuImageApi_tD94B983FBF13116F7497F2089555084CCAC47D59_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { sizeof (ARKitProvider_tBED347A85C19012D5B5F124BC8C0FB4876FBEACC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { sizeof (ARKitEnvironmentProbeSubsystem_t628136AE15CA16280BA838B37E2B7CDF0473E9D2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { sizeof (ARKitErrorCode_t3236EDBBC228C5D3F07F79560500BDE1ACA03535)+ sizeof (RuntimeObject), sizeof(int64_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584 = { sizeof (NSErrorExtensions_t6DCF646D8E7E93886A929C22BC31AD2F85E5BDAB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585 = { sizeof (ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27), -1, sizeof(ARKitLoader_t6CD640FE046961B3884FBF87DC0C348C67DFCA27_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586 = { sizeof (ARKitLoaderConstants_t1A46CABB17835CC85326A66CF3921059BB927B13), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587 = { sizeof (ARKitLoaderSettings_t97CD3CA8B21183EC5C61B94BB7299FD217479B24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588 = { sizeof (NativeApi_tDB232E5FEE2CD4783CEB5C809F129E4A2A4E7EB5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589 = { sizeof (ARKitMeshSubsystemExtensions_tA52932FCB3EF1A3607716616D0217B10B222329D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590 = { sizeof (ARKitProvider_t3FB2A65F0CF8BCD91C4CC54F3841159E7F7DAA88), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591 = { sizeof (NativeApi_t62CE96BA0CF6D47C5FD80B91223590E60F948070), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592 = { sizeof (ARKitRaycastSubsystem_t8DF7A7F21C4610FBBEF6A240FD1790CD47BE9900), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593 = { sizeof (ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE), -1, sizeof(ARKitSessionDelegate_t476836DF28F3F6F2F59CF206FC59B72F052489FE_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594 = { sizeof (ARKitProvider_t9AF751FC81DD6B052A462DBB30F46C946CE9E7CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595 = { sizeof (Availability_tC71B9EC590C2DBAC608742646F563CEC07C89E79)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596 = { sizeof (OnAsyncConversionCompleteDelegate_tFE6205610918E7A87E7867F879A863FD2FE8ECDF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597 = { sizeof (NativeApi_t14734856858FD58426CAE2F6A09FA522E550C226), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598 = { sizeof (ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216), -1, sizeof(ARKitSessionSubsystem_tABA1BA2FE3CF0CECDDC519C4B545236FC8B9E216_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599 = { sizeof (ARMeshClassification_t848441DB74FC412CD7B765DD8BE3CE8EAF9EE0D8)+ sizeof (RuntimeObject), sizeof(uint8_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { sizeof (ARWorldAlignment_tF4BB107DD485C641CF563BBF31FEE5B5045FE81D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (ARWorldMap_t90151C78B15487234BE2E76D169DA191819704A2)+ sizeof (RuntimeObject), sizeof(ARWorldMap_t90151C78B15487234BE2E76D169DA191819704A2 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { sizeof (ARWorldMappingStatus_t64E63DB3361506D91570796A834A9F47DA563805)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (ARWorldMapRequest_tAF3FCBE9900DC7D9563E7D5D3B59B3EA9398E9A4)+ sizeof (RuntimeObject), sizeof(ARWorldMapRequest_tAF3FCBE9900DC7D9563E7D5D3B59B3EA9398E9A4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (ARWorldMapRequestStatus_t9A2ABBA3D64593F57CCFC959D7D294CE5E54E73C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (ARWorldMapRequestStatusExtensions_tBA8B558AE95AA57A9FB348869D8F63494FDB5C1A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (CoreLocationErrorCode_t1772BC652A434E0A4C551CD089CD7C10EF42125E)+ sizeof (RuntimeObject), sizeof(int64_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { sizeof (DefaultARKitSessionDelegate_t7C6D81321CB37E084745A9EEA026C154ED2DB639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { sizeof (ARKitProvider_tC6BBA25F076F6D64FA8E6336E6860D7DBD41A7A1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (TransformPositionsJob_t76836F51EABF9ECE4503F3ECAC1DA2C32D0DA468)+ sizeof (RuntimeObject), sizeof(TransformPositionsJob_t76836F51EABF9ECE4503F3ECAC1DA2C32D0DA468 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (ARKitXRDepthSubsystem_t50D82DC2115EED29E5F0EEBA8A0F4FA7876BD2D5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (EnvironmentProbeApi_t2B36D33D812E48CB4615B2FB0B41BD22D184BB19), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (ARKitProvider_t5CC80E0D6F9A6EFFA70F54E8CDBEDB36D0CC4ED0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { sizeof (NativeApi_tD8D7F6FC5A365EA4D3189425623AC3D23A7ED03B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (ARKitHumanBodySubsystem_t8BA2C528FDE109FBDDBD0ED2FF8BC1FD081E92B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (ReleaseDatabaseJob_t2712527A976BD231EF32DCEAAAF276FC834E8DA1)+ sizeof (RuntimeObject), sizeof(ReleaseDatabaseJob_t2712527A976BD231EF32DCEAAAF276FC834E8DA1 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { sizeof (ConvertRGBA32ToARGB32Job_t993F9DEAC7565A1537923428323E86A8B0506D78)+ sizeof (RuntimeObject), sizeof(ConvertRGBA32ToARGB32Job_t993F9DEAC7565A1537923428323E86A8B0506D78 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { sizeof (AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5)+ sizeof (RuntimeObject), sizeof(AddImageJob_t8EC95C9EA777B31823D128D52F383B45EF7F3AA5 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { sizeof (ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71), -1, sizeof(ARKitImageDatabase_t1D1B5D985355CF3D2597C8FD863EB46EF55B1F71_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (ARKitProvider_t6838013201F7369104C65DD65159D910B116E8AC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (ARKitImageTrackingSubsystem_t0F98C12C3E36B9F3819FC43494A883B600401237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { sizeof (ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1)+ sizeof (RuntimeObject), sizeof(ManagedReferenceImage_t867FD513C8CBF1F9824DBFB215783BE8C33F0FA1 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { sizeof (InputLayoutLoader_t792860969FB135A9DDEF032345D0AE0E6C4988A0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { sizeof (MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D)+ sizeof (RuntimeObject), sizeof(MemoryLayout_tF46C4348E803A3F6F3B2CF3A59BF3840D026C68D ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (NativeChanges_t2AB4CBD7DD7F23F421DFD0CE4D4CDB26402D6D76)+ sizeof (RuntimeObject), sizeof(NativeChanges_t2AB4CBD7DD7F23F421DFD0CE4D4CDB26402D6D76 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (NSData_t17EC9EAA93403854EE122C95559948C024C209D0)+ sizeof (RuntimeObject), sizeof(NSData_t17EC9EAA93403854EE122C95559948C024C209D0 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { sizeof (NSError_t2F84A3F44A97C98D4782E39A8052CCC879098DE9)+ sizeof (RuntimeObject), sizeof(NSError_t2F84A3F44A97C98D4782E39A8052CCC879098DE9 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (NSErrorDomain_tA7AC985B6B3ABA9CEA0E75DF868AD647A30F1B29)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { sizeof (NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F)+ sizeof (RuntimeObject), sizeof(NSMutableData_t65BB3F6AF69A4AD6CCAE4441131B9D1A20778C7F ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { sizeof (NSObject_tDE23F4BBB8E43766E6795164F9B80137E83E96C0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { sizeof (NSString_t73752BE746684D6BB8F70FE48A833CE2C9001350)+ sizeof (RuntimeObject), sizeof(NSString_t73752BE746684D6BB8F70FE48A833CE2C9001350 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { sizeof (ARKitReferenceObjectEntry_t43AE0F7071569F012C44B59BAF26D1398ADEDE47), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { sizeof (ARKitProvider_tDDBF5C474A3543A0F5FA7FB07A058B223F6DA8EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { sizeof (ARKitXRObjectTrackingSubsystem_tB611337EA1668BBCB3A7123F14117CB536707A6D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611), -1, sizeof(ARKitProvider_t8C73B4487170F7B34DF431897CADB4B9CA84C611_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (NativeApi_tFF41350E0941271CB10B461CA357ABF510ACB223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638 = { sizeof (ARKitOcclusionSubsystem_tE15296E017A597561D0C3B399ED52577B138F343), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639 = { sizeof (OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C)+ sizeof (RuntimeObject), sizeof(OSVersion_t2CC75C0F154131C42D64F50A2CBDF84858C2234C ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640 = { sizeof (ARKitProvider_t9908B32D1E5398BA2FD0D825E9343F8CA0277B4C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641 = { sizeof (ARKitParticipantSubsystem_tFBFB76108F5069693F4F40F8DAA4B6A907A709C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642 = { sizeof (FlipBoundaryWindingJob_t726CFFAB953CC7C841EE74365C27322E57A2A5DA)+ sizeof (RuntimeObject), sizeof(FlipBoundaryWindingJob_t726CFFAB953CC7C841EE74365C27322E57A2A5DA ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643 = { sizeof (TransformBoundaryPositionsJob_t538BF69F9F744EBC94AA0487075746AD113A64FE)+ sizeof (RuntimeObject), sizeof(TransformBoundaryPositionsJob_t538BF69F9F744EBC94AA0487075746AD113A64FE ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644 = { sizeof (ARKitProvider_tF9F620C89E671C028D10768C5300D3539661A2C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645 = { sizeof (NativeApi_tB65E4D304D37EA8B8ED6329919AEE7845A85C55E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646 = { sizeof (ARKitXRPlaneSubsystem_t03042D64756D81EC6B4657CD1323D7404EC7530B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647 = { sizeof (SerializedARCollaborationData_t21AF44A6BD46EF7C15CB5558B47AD839E14F7AC3)+ sizeof (RuntimeObject), sizeof(SerializedARCollaborationData_t21AF44A6BD46EF7C15CB5558B47AD839E14F7AC3 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648 = { sizeof (SetReferenceLibraryResult_t5F5FC87DC43CB92C6332217BE1C755F59E8E5303)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649 = { sizeof (__StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D32_t0BFEA9C6F9BC1DDA1D7FB61D35C94D361B765A5C ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650 = { sizeof (U3CPrivateImplementationDetailsU3E_t52768071AC670B242659CF974483AF846D6828BF), -1, sizeof(U3CPrivateImplementationDetailsU3E_t52768071AC670B242659CF974483AF846D6828BF_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651 = { sizeof (U3CModuleU3E_tE7281C0E269ACF224AB82F00FE8D46ED8C621032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652 = { sizeof (U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653 = { sizeof (SwitchPointCloudVisualizationMode_t892FEC4C8885FCDBD53C3C96DC94FCFC7CE581AE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654 = { sizeof (CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1), -1, sizeof(CSharpscaling_tD26A47DCED5B91E7D17D7897D17F9FDCCFEBF3D1_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655 = { sizeof (onClickForScaling_t4E6FED225317AC601426A8B8C57BDE93374439B8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656 = { sizeof (rotateController_tA50ABA66BC28E1D291F0FE27A4E4750682E76E24), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657 = { sizeof (NativeAPI_t315FBF47DE43B17EAD085102FBAA4F7286D06A69), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658 = { sizeof (U3CWaitForPanelOffU3Ed__4_tB20DAA370E45A1A52F3A5A363F8E9A48577CD9B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659 = { sizeof (ShowCanvasOnBuildingModels_t5BDAB4EF92769C0A50FCE6A9969E370DEC298EBA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660 = { sizeof (Activate_tE8FDB08082D994FB61E54E2B7D81FBD0A9AF8E88), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661 = { sizeof (ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27)+ sizeof (RuntimeObject), sizeof(ItemsData_tE877771433B3AA421871C1CA019E2B50EE074C27_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662 = { sizeof (AddToCartScript_t6761D63CFC0BBC1774B486534654F1375CC0D1C2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663 = { sizeof (U3CdelayU3Ed__3_t55162967D7666CCEF6EFD3B771CCBA07A9B57C79), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664 = { sizeof (Anim_tF3468CFBF078A3F63800C8B0BEA4A5119743F8D4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665 = { sizeof (AnimateModel_t5FA7BFBC700198F4FD54C33909FB2F40BDF02877), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666 = { sizeof (U3CWaitForNetworkU3Ed__6_t3B5634820D21C157BF8F0D593102836CCD6E68D8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667 = { sizeof (U3CWaitForValidAssetU3Ed__7_tAF06E6CE321EB07F4C29C7172E70189E3ACC5C0F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668 = { sizeof (U3CGetDisplayBundleRoutineU3Ed__8_tBC57EF6E87D992B83AA3EC37BDE8CDB51382AF07), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669 = { sizeof (U3CLoadObjectU3Ed__15_tAF48E0B4849DA246717AB13CB9AA4F9E21BF67F3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670 = { sizeof (API_t407FE93980B64D30D361B8CCB7E2F25479E0B7D2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671 = { sizeof (NativeAPI_t0C0CF7D8A37491A6EE905628D637573B751EFBA9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4672;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4672 = { sizeof (U3CWaitForPanelOffU3Ed__10_t5936D80D998085B9710012B5F137555C3678F6D1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4673;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4673 = { sizeof (U3CWaitForDestructionU3Ed__14_t8925DCB46E7386EEE24FEEFAC83D8284665426ED), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4674;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4674 = { sizeof (ArCanvas_tD15BFBFDD635718EECAF7ABFE598501D21D1F44B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4675;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4675 = { sizeof (NativeAPI_t6DB0B44081D67DC7D8297AAC58876922A63CDB03), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4676;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4676 = { sizeof (U3CWaitForBaseAnimationU3Ed__11_tAA5562B465D6AE311D5DB396A131037D39C43DEA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4677;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4677 = { sizeof (ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51), -1, sizeof(ARTapToPlace_tB6D429B2C4192A2DC6B7500A8A6BCE33BD0B4D51_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4678;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4678 = { sizeof (ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3), -1, sizeof(ARTapToPlaceAvatar_t09CD860D7DB7AFB6EF88442C15BBD03A154E07A3_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4679;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4679 = { sizeof (ContentController_tE2F771CAE14A2E52EF9E34E52B9776732FBA3DCB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4680;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4680 = { sizeof (DragObject1_t5F78FF530D002F7C4A0A36C82B45E748419C55E5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4681;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4681 = { sizeof (LookAtCamera_t1A909DB8DF88C609D403662C931A1713806027B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4682;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4682 = { sizeof (MyDrag_tBF6DCE4AEFE3669D9791EA6423CD9FCAEBD60F1F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4683;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4683 = { sizeof (NativeAPI_tA82FCF52A5EFF585B109939C30419CF8D4259A65), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4684;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4684 = { sizeof (U3CWaitForDownloadU3Ed__21_tAF5405578D5114FB3C5FA9D8C23CD3377C750F5F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4685;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4685 = { sizeof (U3CWaitForRedeemDownloadU3Ed__27_tFA3F86DFB2BF1A3F8F3367CDF9F1F8E0551DDB76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4686;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4686 = { sizeof (U3CWaitForCharacterDownloadU3Ed__32_tCCBFDA5F0426F9D9E2164C23BFCEA37652E658EE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4687;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4687 = { sizeof (ObjectSpawner_tF3A67BD2E71567BED1D0B7E4E8756B9C92D4E388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4688;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4688 = { sizeof (NativeAPI_t8130043C884E3E066EA5BD75C35CFD0662C8B501), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4689;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4689 = { sizeof (U3CWaitForBaseAnimationU3Ed__13_tF6E856DF202CE64B0AC1D5B0333CC123E532C1B9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4690;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4690 = { sizeof (ParslAvatarController_tC928F0091BCE305C438662BE49A477400DCCDBA3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4691;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4691 = { sizeof (PlaceContent_t7015C9D0A2924D8469A9BFCCA4E0DC337FAF2299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4692;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4692 = { sizeof (PlacementIndicator_t2D4693D5F2FE5168C7211C128287E4C4A4A73275), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4693;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4693 = { sizeof (PlacementObject_t793156EEEDFC9418B06E4AD4F3A2D79B361EECA7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4694;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4694 = { sizeof (PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9), -1, sizeof(PlacementWithMultipleDraggingDroppingController_t1E9A1D3ECE2ED9692A8090C8A48D24C8EBB01FC9_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4695;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4695 = { sizeof (ReturnCartList_tED3BCB6B9F26337331DB4F94802112BC76F9FB02), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4696;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4696 = { sizeof (ToggleAR_t859D7D0F694C4107E234070CB221F9472D0BC2F7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4697;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4697 = { sizeof (Mode_t3F065752D98A1D2AC6961F2183A645E20299CBE6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4698;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4698 = { sizeof (ARAllPointCloudPointsParticleVisualizer_tA7A20F0ABC39455ED4236FD21D811E29281DAC9B), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
